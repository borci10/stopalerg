<?php
	function toBoolean($val) {
		if ($val == 1) {
			return true;
		} else {
			return false;
		}
	}

	function is_set($var, $default = '') {
		if (isset($var)) {
			return $var;
		}
		return $default;
	}
	
	function is_checked($val1, $val2, $default_check = 0) {
		if (empty($val1) && $default_check == 1) {
			return 'checked';
		}
		if ($val1 == $val2) {
			return 'checked';
		}
		return '';
	}
	
	function is_selected($val1, $val2, $default_check = 0) {
		if (empty($val1) && $default_check == 1) {
			return 'selected';
		}
		if ($val1 == $val2) {
			return 'selected';
		}
		return '';
	}
	
	function cuteImage($photoUrl, $thumb = 0) {
		if ($photoUrl) {
			if ($thumb) {
				return (substr($photoUrl, 0, 4) == 'http' ? $photoUrl : base_url() . 'uploads/products/thumbs/' . $photoUrl);
			} else {
				return (substr($photoUrl, 0, 4) == 'http' ? $photoUrl : base_url() . 'uploads/products/' . $photoUrl);
			}
		} else {
			return  base_url() . 'images/No_image_available.jpg';
		}
	}
	
	function cutePrice($price, $currency = '€') {
		if ($currency) {
			return number_format($price ?? 0, 2, '.', ' ') . ' ' . $currency;
		} else {
			return number_format($price ?? 0, 2, '.', ' ');
		}
	}
	
	function priceCalc($price, $quantity = 1, $discount = 0, $vat = 0) {
		return round(($price ?? 0) * ((100 - $discount) / 100) * (1 + $vat), 2) * $quantity;
	}
	
	function vatCalc($price, $currency, $quantity = 1, $discount = 0, $vat = 0) {
		return number_format((($price ?? 0) * $quantity * ((100 - $discount) / 100) * $vat), 2, '.', '&nbsp;') . '&nbsp;' . $currency;
	}
	
	function vatCalcSimple($price, $vat) {
		return number_format(($price * ($vat / (100 + $vat))), 2, '.', ' ');
	}
	
	function vatCalcRaw($price, $quantity = 1, $discount = 0, $vat = 0) {
		return round(($price ?? 0) * ((100 - $discount) / 100) * ($vat / 100), 2) * $quantity;
	}
	
	function cuteDate($date) {
		if (empty($date)) {
			return '';
		}
		return date('d.m.Y', strtotime($date));
	}
	
	function cuteDateTime($date) {
		if (empty($date)) {
			return '';
		}
		return date('d.m.Y H:i', strtotime($date));
	}
	
	function mysqlDate($date) {
		if (empty($date)) {
			return '';
		}
		return date('Y-m-d', strtotime($date));
	}
	
	function ISO8601Date($date) {
		return date('c', strtotime($date));
	}
	
	function toVarSymbol($id){
		return str_pad($id, 8, "0", STR_PAD_LEFT);
	}
	
	function generateVarSymbol($invoiceId) {
		return "3" . date('m') . str_pad($invoiceId, 5, 0, STR_PAD_LEFT);
	}
	
	function GetSignature($key, $message){
		return strtoupper(hash_hmac('sha256', pack('A*', $message), pack('A*', $key)));
	}
    
	
	function generatePassword($passwordLength) {
		$characters = 'abcdefghijklmnopqrstuvwxyz0123456789';
		$string = '';
		$max = strlen($characters) - 1;
		for ($i = 0; $i < $passwordLength; $i++) {
			$string .= $characters[mt_rand(0, $max)];
		}
	 
		return $string;
	} 
	
	function removeqsvar($url, $varname) {
		return preg_replace('/([?&])'.$varname.'=[^&]+(&|$)/','',$url);
	}
	
	function timeDiffInDays ($date) {
		$dateNow  = date("Y-m-d H:i:s");
		$dayInSec = 86400;
		
		return (strtotime($dateNow)- strtotime($date)) / $dayInSec;
	}
	
	function getProductTag($date, $discount, $quantity, $reviews) {
		if ($quantity == 0) {
			return '<div class="vypredaneTag"><span>Vypredané</span></div>';
		}
		if ($discount > 0 && $quantity < 5) {
			return '<div class="vypredajTag"><span>Výpredaj</span></div>';
		}
		if ($discount > 0) {
			return '<div class="zlavaTag"><span>-' . $discount . '%</span></div>';
		}
		if ($quantity < 5) {
			return '<div class="poslednyKusTag"><span>Skladom < 5</span></div>';
		}
		if ($reviews > 95) {
			return '<div class="topTag"><span><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i></span></div>';
		}
		if (timeDiffInDays($date) <= 14) {
			return '<div class="novinkaTag"><span>Novinka</span></div>';
		}
	}
?>