<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Export extends CI_Controller {
	
	function __construct() {
		parent::__construct();
		require_once APPPATH.'third_party/PHPExcel.php';
        $this->excel = new PHPExcel(); 

		if ((!$this->session->has_userdata('logged_in')) && ($this->session->userdata('admin') != 1)) {
			redirect('/home');
		}
		
		$this->load->model('product_model');
		$this->load->model('admin_model');
		$this->load->model('profile_model');
	}

	public function index() {
		redirect('/home');
	}
	
	public function exportProducts() {
		$products	= $this->product_model->getAllProducts();
		$fileName 	= 'Produkty ' . date('d.m.Y');
		
		$this->excel->getActiveSheet()->SetCellValue('A1', 'Uid');
		$this->excel->getActiveSheet()->SetCellValue('B1', 'Názov produktu');
		$this->excel->getActiveSheet()->SetCellValue('C1', 'Cena (' . $this->config->item('currency_word') . ')');
		$this->excel->getActiveSheet()->SetCellValue('D1', 'Zľava');
		$this->excel->getActiveSheet()->SetCellValue('E1', 'Množstvo');
		$this->excel->getActiveSheet()->SetCellValue('F1', 'Dátum pridania');

		$rowNumber = 2;
		foreach($products as $product) {
			$this->excel->getActiveSheet()->SetCellValue('A' . $rowNumber,  $product['uid']);
			$this->excel->getActiveSheet()->SetCellValue('B' . $rowNumber,  $product['name']);
			$this->excel->getActiveSheet()->SetCellValue('C' . $rowNumber,  cutePrice($product['price']));
			$this->excel->getActiveSheet()->SetCellValue('D' . $rowNumber,  $product['discount']);
			$this->excel->getActiveSheet()->SetCellValue('E' . $rowNumber,  $product['quantity']);
			$this->excel->getActiveSheet()->SetCellValue('F' . $rowNumber,  cuteDate($product['creation_date']));
			
			$rowNumber++;
		}

		$this->excel->getActiveSheet()->setTitle($fileName);

		$objWriter = new PHPExcel_Writer_Excel2007($this->excel);

		header('Content-type: application/vnd.ms-excel');

		header('Content-Disposition: attachment; filename="' .$fileName . '.xlsx"');

		$objWriter->save('php://output');
	}
	
	public function exportOrdersXlsx($urlSegment = null) {
		$filter 	= $this->admin_model->getOrderSubpageFilter($urlSegment);
		$orders  	= $this->admin_model->getOrdersByFilterForDelivery($filter);
		$fileName 	= 'Objednávky ' . date('d.m.Y');

		$rowNumber = 1;
		foreach($orders as $order) {
			$this->excel->getActiveSheet()->SetCellValue('A' . $rowNumber,  $order['name'] . ' ' . $order['surname']);
			$this->excel->getActiveSheet()->SetCellValue('B' . $rowNumber,  $order['phone_number']);
			$this->excel->getActiveSheet()->SetCellValue('C' . $rowNumber,  $order['city']);
			$this->excel->getActiveSheet()->SetCellValue('D' . $rowNumber,  $order['street']);
			$this->excel->getActiveSheet()->SetCellValue('E' . $rowNumber,  $order['zip_code']);
			$this->excel->getActiveSheet()->SetCellValue('F' . $rowNumber,  $order['delivery']);
			$this->excel->getActiveSheet()->SetCellValue('F' . $rowNumber,  $order['payment']);
			$this->excel->getActiveSheet()->SetCellValue('F' . $rowNumber,  cutePrice($order['totalPrice'], $this->config->item('currency_word')));			
			$rowNumber++;
			
			$orderProducts = $this->profile_model->getOrderDetails($order['id']);
			
			foreach($orderProducts as $orderProduct) {
				$this->excel->getActiveSheet()->SetCellValue('B' . $rowNumber,  $orderProduct['name']);
				$this->excel->getActiveSheet()->SetCellValue('C' . $rowNumber,  $orderProduct['quantity'] . 'x');
				$rowNumber++;
			}
			$rowNumber++;
		}

		$this->excel->getActiveSheet()->setTitle($fileName);

		$objWriter = new PHPExcel_Writer_Excel2007($this->excel);

		header('Content-type: application/vnd.ms-excel');

		header('Content-Disposition: attachment; filename="' .$fileName . '.xlsx"');

		$objWriter->save('php://output');
	}
}
