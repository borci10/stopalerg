<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cart extends CI_Controller {
	
	function __construct() {
		parent::__construct();
		
		if (!isset($_SESSION['shoppingCart'])) {
			$_SESSION['shoppingCart'] = [];
			$_SESSION['cart_price'] = 0;
		}

		$this->load->model('product_model');
		$this->load->model('admin_model');
		$this->load->model('cart_model');
		$this->load->model('email_model');
		$this->load->model('profile_model');
		$this->load->model('ikros_model');
		$this->load->model('Admin/order_model');
		$this->load->model('Admin/settings_model');

//		 $this->output->enable_profiler(true);
	}
	
	public function index() {
		$this->data['page_title'] 		= 'Nákupný košík' . $this->config->item('title_delimiter') . $this->config->item('eshop_name');
		$this->data['pageHeaderTitle'] 	= 'Nákupný košík';
		$this->data['subpages']   		= $this->admin_model->getAllSubpageNames();

		if (count($_SESSION['shoppingCart']) > 0) {
			$this->data['shoppingCart'] = $_SESSION['shoppingCart'];
		}
		
		$this->load->view('header', $this->data);
		$this->load->view('cart/cart_header', $this->data);
		$this->load->view('cart/cart_main', $this->data);
		$this->load->view('footer', $this->data);
	}
	
	public function personalInfo() {
		if ($this->cart_model->checkShoppingCartStepDone()) {
			if ($this->input->post('formConfirm')){ 
				$this->editPersonalInfoSession();
			}
			
			$this->data['page_title'] 		= 'Dodacie informácie' . $this->config->item('title_delimiter') . $this->config->item('eshop_name');
			$this->data['pageHeaderTitle'] 	= 'Dodacie informácie';
			$this->data['subpages']   		= $this->admin_model->getAllSubpageNames();
			$this->data['shoppingCart'] 	= $_SESSION['shoppingCart'];
			
			$this->load->view('header', $this->data);		
			$this->load->view('cart/cart_header', $this->data);		
			$this->load->view('cart/cart_personal_info', $this->data);
			$this->load->view('footer', $this->data);
		} else {
			redirect('/cart');
		}
	}
	
	public function paymentInfo() {
		if ($this->cart_model->checkPersonalInfoStepDone() && $this->cart_model->checkShoppingCartStepDone()) {
			if (count($_SESSION['shoppingCart']) > 0) {
				$productIds = array_keys($_SESSION['shoppingCart']);
				$this->data['products'] 		= $this->product_model->getMultipleItemsById($productIds);
				$this->data['supplierPrice'] 	= $this->cart_model->getSupplierPricesFromArrId(array_unique(array_column($this->data['products'], 'supplier_id')));
			}
			
			if ($this->input->post('formConfirm')){ 
				$this->editPaymentInfoSession($this->data['supplierPrice']);
			}
		
			$this->data['page_title'] 		= 'Platobné informácie' . $this->config->item('title_delimiter') . $this->config->item('eshop_name');
			$this->data['pageHeaderTitle'] 	= 'Platobné informácie';
			$this->data['subpages']   		= $this->admin_model->getAllSubpageNames();
			$this->data['delivery']   		= $this->cart_model->getAllDeliveryOptions();
			$this->data['payment']   		= $this->cart_model->getAllPaymentOptions();
			$this->data['shoppingCart'] 	= $_SESSION['shoppingCart'];
			
			$this->load->view('header', $this->data);		
			$this->load->view('cart/cart_header', $this->data);			
			$this->load->view('cart/cart_payment_info', $this->data);
			$this->load->view('footer', $this->data);
		} else {
			redirect('/cart/personal-info');
		}
	}
	
	public function orderSummary() {
		if ($this->cart_model->checkPaymentInfoStepDone() && $this->cart_model->checkPersonalInfoStepDone() && $this->cart_model->checkShoppingCartStepDone()) {
			if ($this->input->post('formConfirm')){ 
				$this->createOrder();
			}
			$productIds = array_keys($_SESSION['shoppingCart']);
			$this->data['products'] 		= $this->product_model->getMultipleItemsById($productIds);
			$this->data['supplierPrice'] 	= $this->cart_model->getSupplierPricesFromArrId(array_unique(array_column($this->data['products'], 'supplier_id')));
			$this->data['page_title'] 		= 'Sumár objednávky' . $this->config->item('title_delimiter') . $this->config->item('eshop_name');
			$this->data['pageHeaderTitle'] 	= 'Sumár objednávky';
			$this->data['subpages']   		= $this->admin_model->getAllSubpageNames();
			$this->data['shoppingCart'] 	= $_SESSION['shoppingCart'];
			
			$this->load->view('header', $this->data);		
			$this->load->view('cart/cart_header', $this->data);			
			$this->load->view('cart/cart_summary', $this->data);
			$this->load->view('footer', $this->data);
		} else {
			redirect('/cart/payment-info');
		}
	}
	
	public function addItemToCart() {
		$productId 			= $this->input->post('productId');
		$productChildId 	= $this->input->post('productChildId');
		$productQuantity 	= $this->input->post('quantity');
		
		if ($productInfo = $this->product_model->getShortProductById($productId)) {
			if ($productChildInfo = $this->product_model->getProductVariableShort($productChildId)) {
				$productInfo->quantity 	= $productChildInfo->quantity;
				$productInfo->deleted 	= $productChildInfo->deleted;
				$productInfo->price 	= $productChildInfo->price;
				$productInfo->discount 	= $productChildInfo->discount;
				$productChildId 		= $productChildInfo->id;
			} else {
				$productChildId = 0;
			}
		
			if (($productQuantity > 0) && ($productQuantity <= $productInfo->quantity) && ($productInfo->deleted == 0)) {
				if ($this->session->userdata('logged_in')) {
					if (!$this->cart_model->insertProductIntoCart($productInfo->id, $productChildId, $productQuantity)) { // if DB cart failed, end and refresh
						$this->session->set_flashdata('error', 'Vyskytla sa chyba. Skúste to prosím znovu.');
						redirect($_SERVER['HTTP_REFERER']);
						exit();
					}
				}
				
				$_SESSION['cart_price'] += priceCalc($productInfo->price, $productQuantity, $productInfo->discount, $productInfo->tax / 100);
				
				if (!isset($_SESSION['shoppingCart'][$productInfo->id][$productChildId])) {
					$_SESSION['shoppingCart'][$productInfo->id][$productChildId] = [
						'itemCount' => $productQuantity,
					];
				} else {
					$_SESSION['shoppingCart'][$productId][$productChildId]['itemCount'] += $productQuantity;
				}

				if ($productInfo->file_name) {
					$productInfo->file_name = cuteImage($productInfo->file_name, 1);
				}
				
				echo json_encode([cutePrice($_SESSION['cart_price']), cutePrice(priceCalc($productInfo->price, $_SESSION['shoppingCart'][$productInfo->id][$productChildId]['itemCount'] ?? 0, $productInfo->discount, $productInfo->tax / 100)), $productInfo]);
			} else {
				echo json_encode(['err']);
			}
		} else {
			echo json_encode(['err']);
		}
	}
	
	public function removeItemFromCart() {
		$productId 			= $this->input->post('productId');
		$productChildId 	= $this->input->post('productChildId');
		$productQuantity 	= $this->input->post('quantity');
		
		if ($productInfo = $this->product_model->getProductPrice($productId)) {
			if ($productChildInfo = $this->product_model->getProductVariableShort($productChildId)) {
				$productInfo->quantity 	= $productChildInfo->quantity;
				$productInfo->deleted 	= $productChildInfo->deleted;
				$productInfo->price 	= $productChildInfo->price;
				$productInfo->discount 	= $productChildInfo->discount;
				$productChildId 		= $productChildInfo->id;
			} else {
				$productChildId = 0;
			}
			
			if ($productQuantity > 0 || $productQuantity == -1) {
				if ($this->session->userdata('logged_in')) {
					if (!$this->cart_model->deleteProductFromCart($productInfo->id, $productChildId, $productQuantity)) { // if DB cart failed, end and refresh
						$this->session->set_flashdata('error', 'Vyskytla sa chyba. Skúste to prosím znovu.');
						redirect($_SERVER['HTTP_REFERER']);
					}
				}
				
				if ($productQuantity == -1) {
					/*
						remove all 
					*/
					$_SESSION['cart_price'] -= priceCalc($productInfo->price, $_SESSION['shoppingCart'][$productInfo->id][$productChildId]['itemCount'], $productInfo->discount, $productInfo->tax / 100);
						
					unset($_SESSION['shoppingCart'][$productInfo->id][$productChildId]);
				} else {
					if (($_SESSION['shoppingCart'][$productInfo->id][$productChildId]['itemCount'] - $productQuantity) <= 0) {
						/*
							if no items left in the cart, remove 
						*/
						$_SESSION['cart_price'] -= priceCalc($productInfo->price, $_SESSION['shoppingCart'][$productInfo->id][$productChildId]['itemCount'], $productInfo->discount, $productInfo->tax / 100);
						
						unset($_SESSION['shoppingCart'][$productInfo->id]);
					} else {
						$_SESSION['shoppingCart'][$productInfo->id][$productChildId]['itemCount'] -= $productQuantity;
						$_SESSION['cart_price'] -= priceCalc($productInfo->price, $productQuantity, $productInfo->discount, $productInfo->tax / 100);
					}
				}
				
				if (empty($_SESSION['shoppingCart'])) {
					$_SESSION['cart_price'] = 0;
				}
			
				echo json_encode([cutePrice($_SESSION['cart_price'], $this->config->item('currency')), cutePrice(priceCalc($productInfo->price, $_SESSION['shoppingCart'][$productId][$productChildId]['itemCount'] ?? 0, $productInfo->discount, $productInfo->tax / 100))]);
			}
		} else {
			echo json_encode(['err']);
		}
	}
	
	private function createOrder() {
		if ($_SESSION['orderId'] = $this->cart_model->insertOrder()) {
			if ($this->config->item('product_out_of_stock') !== false) {
				$productIds = array_keys($_SESSION['shoppingCart']);
				$products	= $this->product_model->getMultipleOutOfStockItemsById($productIds, $this->config->item('product_out_of_stock'));
				
				foreach($products as $product) {
					$this->sendEmailOutOfStock($product);
				}
			}

			if($this->input->post('cartNewsletter')) {
				$this->load->model('newsletter_model');
				$email = [
					'email' => $_SESSION['newsletter']
				];
				$this->newsletter_model->insertEmail($email);
			}
			
			$this->sendEmailOrderCreated($this->session->userdata('email'), 'Objednávka prijatá', 'Customer', $this->session->userdata('orderId'));
			$this->sendEmailOrderCreated($this->session->userdata('email'), 'Nová objednávka', 'Owner', $this->session->userdata('orderId'));
			
			if ($this->session->userdata('payment') == 'TrustPay') {	
				$this->cart_model->TrustPayPayment();
			}

			$this->session->set_flashdata('payment', $this->session->userdata('payment'));
			$this->session->set_flashdata('orderId', $this->session->userdata('orderId'));

			$this->unsetOrderSession();

			$this->session->set_flashdata('success', 'Objednávka bola vytvorená!');


			if ($this->session->has_userdata('logged_in')) {
				redirect('/profile/orders');
			} else {
				redirect('/objednavka-vytvorena');
			}
		} else {
			$this->session->set_flashdata('error', 'Vyskytla sa chyba. Skúste to prosím znovu.');
		}

	}
	
	private function sendEmailOrderCreated($email, $subject, $target, $orderId) {
		$attachment = NULL;
		$this->data['orderSummary'] = $_SESSION;
		$this->data['order_items']  = $this->profile_model->getOrderDetails($this->session->userdata('orderId'));
		$this->data['shopSettings']	= $this->settings_model->getAllSettingsParsed();
		$html = $this->load->view('email/email_order_created', $this->data, true);
		if($this->data['orderSummary']['payment'] == 'Dobierka'){
			$attachment = $this->getInvoiceAttachment($orderId);
			$this->ikros_model->sendInvoiceExport($orderId);
		}

		if($target == 'Owner') {
			$this->email_model->sendEmailToOwner($email, $subject, $html);
		}
		else {
			$this->email_model->sendEmail($email, $subject, $html, $orderId, $attachment);
		}
	}

	private function getInvoiceAttachment($orderId) {
		$this->data['order_info']		= $this->order_model->getOrderDetails($orderId);
		$this->data['order_items']		= $this->order_model->getOrderItems($orderId);
		$this->data['productTax']		= array_column($this->data['order_items'], 'tax');
		$this->data['shopSettings']		= $this->settings_model->getAllSettingsParsed();
		$this->data['documentColor']	= '#1DA680';
		$this->data['documentHeader']	= 'Faktúra ' . generateVarSymbol($this->data['order_info']->id);

		$html = $this->load->view('admin/orders/_invoice/invoice_template', $this->data, true);

		ini_set('memory_limit','128M'); // boost the memory limit if it's low ;)

		$this->load->library('pdf');

		$pdf = $this->pdf->load();
		$pdf->SetTitle('Faktúra - ' . generateVarSymbol($this->data['order_info']->id));
		$pdf->SetHTMLHeader($this->load->view('admin/orders/_invoice/_header', $this->data, true));
		$pdf->SetHTMLFooter($this->load->view('admin/orders/_invoice/_footer', $this->data, true));
		$pdf->AddPage('', // L - landscape, P - portrait
			'', '', '', '',
			10, // margin_left
			10, // margin right
			30, // margin top
			20, // margin bottom
			10, // margin header
			5); // margin footer
		$pdf->WriteHTML($html);

		return $pdf->Output('', 'S');
	}
	
	private function sendEmailOutOfStock($product) {
		$this->data['product'] = $product;	
		$html = $this->load->view('email/email_product_out_of_stock', $this->data, true);

		$this->email_model->sendEmail($this->config->item('eshop_email'), 'Produk takmer vypredaný', $html);
	}
	
	private function editPaymentInfoSession($supplierPrice) {
		if (($delivery = $this->cart_model->getDelivery($this->input->post('delivery'))) && ($payment = $this->cart_model->getPayment($this->input->post('payment')))) {
			if ($payment->uid == 'ttp') {
				if (!$this->config->item('trust_pay')) {
					$this->session->set_flashdata('error', 'Vyskytla sa chyba. Skúste to prosím znovu.');
					redirect('cart/payment-info');
				}
			}
			$_SESSION['delivery'] 		= $delivery->title;
			$_SESSION['delivery_price'] = $delivery->price + $supplierPrice;
			$_SESSION['payment']  		= $payment->title;
			$_SESSION['payment_price']  = $payment->price;
			
			redirect('cart/order-summary');
		} else {
			$this->session->set_flashdata('error', 'Vyskytla sa chyba. Skúste to prosím znovu.');
			redirect('cart/payment-info');
		}
	}
	
	private function editPersonalInfoSession() {
		$this->form_validation->set_rules('cartName', 	  'Meno', 			 'trim|required');
		$this->form_validation->set_rules('cartSurname',  'Priezvisko', 	 'trim|required');
		$this->form_validation->set_rules('cartEmail', 	  'Emailová adresa', 'trim|required|valid_email');
		$this->form_validation->set_rules('cartStreet',   'Ulica', 			 'trim|required');
		$this->form_validation->set_rules('cartCity', 	  'Mesto', 			 'trim|required');
		$this->form_validation->set_rules('cartZipCode',  'PSČ', 			 'trim|required');
		$this->form_validation->set_rules('cartPhone', 	  'Telefónne číslo', 'trim|required');
		
		if ($this->input->post('cartDifferentDeliveyAdress')) {
			if($this->input->post('cartDeliveryName')) {
				$this->form_validation->set_rules('cartDeliveryName', 	  'Meno', 			 'trim|required');
			}
			if($this->input->post('cartDeliverySurname')) {
				$this->form_validation->set_rules('cartDeliverySurname',  'Priezvisko', 	 'trim|required');
			}
			$this->form_validation->set_rules('cartDeliveryStreet', 	'Ulica', 	'trim|required');
			$this->form_validation->set_rules('cartDeliveryCity', 	  	'Mesto', 	'trim|required');
			$this->form_validation->set_rules('cartDeliveryZipCode', 	'PSČ', 		'trim|required');
		}
		
		if($this->form_validation->run()){
			$_SESSION['name'] 			= strip_tags($this->input->post('cartName'));
			$_SESSION['surname'] 		= strip_tags($this->input->post('cartSurname'));
			$_SESSION['email'] 			= strip_tags($this->input->post('cartEmail'));
			$_SESSION['newsletter'] 	= strip_tags($this->input->post('cartEmail'));
			$_SESSION['city'] 			= strip_tags($this->input->post('cartCity'));
			$_SESSION['street'] 		= strip_tags($this->input->post('cartStreet'));
			$_SESSION['zip_code'] 		= strip_tags($this->input->post('cartZipCode'));
			$_SESSION['phone_number'] 	= strip_tags($this->input->post('cartPhone'));
			$_SESSION['diff_delivery'] 	= false;
			
			
			if ($this->input->post('cartDifferentDeliveyAdress')) {
				if($this->input->post('cartDeliveryName')) {
					$_SESSION['delivery_name'] 		= strip_tags($this->input->post('cartDeliveryName'));
				}
				if($this->input->post('cartDeliverySurname')) {
					$_SESSION['delivery_surname'] 	= strip_tags($this->input->post('cartDeliverySurname'));
				}
				$_SESSION['delivery_street']	= strip_tags($this->input->post('cartDeliveryStreet'));
				$_SESSION['delivery_city']    	= strip_tags($this->input->post('cartDeliveryCity'));
				$_SESSION['delivery_zip_code']	= strip_tags($this->input->post('cartDeliveryZipCode'));
				$_SESSION['diff_delivery'] 		= true;
			}
	
			redirect('/cart/payment-info');
		}
	}
	
	private function unsetOrderSession() {
		$this->session->unset_userdata('products');
		$this->session->unset_userdata('payment');
		$this->session->unset_userdata('delivery');
		$this->session->unset_userdata('delivery_price');
		$this->session->unset_userdata('shoppingCart');
		$this->session->unset_userdata('cart_price');
	}
	
	//payments
	public function successPayment($orderId, $token) {
		if (md5($this->config->item('salt') . $orderId) == $token) {
			$this->session->set_flashdata('success', 'Platba prebehla úspešne. Objednávka čaká na vybavenie.');
			$this->cart_model->updateOrderStatus($orderId, 20);
			$this->unsetOrderSession();

			if ($this->session->has_userdata('logged_in')) {
				redirect('/profile/orders');
			} else {
				redirect('/products');
			}
		} else {
			$this->session->set_flashdata('error', 'Vyskytla sa chyba. Neplatný token.');
			$this->unsetOrderSession();
			
			redirect('');
		}
	}
	
	public function cancelPayment($orderId, $token) {
		if (md5($this->config->item('salt') . $orderId) == $token) {
			$this->session->set_flashdata('warning', 'Objednávka bola zrušená');
			$this->cart_model->updateOrderStatus($orderId, 40);
			$this->cart_model->returnProductsToStock();

			$this->unsetOrderSession();

			redirect('/cart');
		} else {
			$this->session->set_flashdata('error', 'Vyskytla sa chyba. Neplatný token.');
			$this->unsetOrderSession();
			
			redirect('');
		}
	}
	
	public function errorPayment($orderId, $token) {
		if (md5($this->config->item('salt') . $orderId) == $token) {
			$this->session->set_flashdata('error', 'Vyskytla sa chyba. Platba nebola dokončená. Skúste to prosím znovu.');
			$this->cart_model->updateOrderStatus($orderId, 40);
			$this->cart_model->returnProductsToStock();
			$this->unsetOrderSession();
			
			redirect('/cart/order-summary');
		} else {
			$this->session->set_flashdata('error', 'Vyskytla sa chyba. Neplatný token.');
			$this->unsetOrderSession();
			
			redirect('');
		}
	}
}
