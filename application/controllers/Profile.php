<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Profile extends CI_Controller {
	
	function __construct() {
		parent::__construct();
		if (!$this->session->has_userdata('logged_in')) {
			redirect('/login');
		}
		
		$this->load->model('profile_model');
		$this->load->model('admin_model');
		$this->load->model('product_model');
		$this->load->model('Admin/order_model');
	}

	public function index() {
		redirect('profile/orders');
		
		$this->data['page_title'] 		= 'Profil' . $this->config->item('title_delimiter') . $this->config->item('eshop_name');
		$this->data['pageHeaderTitle']	= 'Prehľad';
		$this->data['subpages']   		= $this->admin_model->getAllSubpageNames();
	
		$this->load->view('header', $this->data);		
		$this->load->view('profile/profile_header');
		$this->load->view('profile/profile');
		$this->load->view('footer', $this->data);
	}
	
	public function orders() {
		$this->data['page_title'] 		= 'Moje nákupy' . $this->config->item('title_delimiter') . $this->config->item('eshop_name');
		$this->data['pageHeaderTitle']	= 'Moje nákupy';
		$this->data['subpages']  		= $this->admin_model->getAllSubpageNames();
		$this->data['orders']    		= $this->profile_model->getUserOrders($this->session->userdata('id'));
		
		$this->load->view('header', $this->data);
		$this->load->view('profile/profile_header');
		$this->load->view('profile/profile_orders', $this->data);
		$this->load->view('footer', $this->data);
	}
	
	public function settings() {
		if($this->input->post('changePasswordConfirm')){
			$this->changePassword();
		}
		
		if($this->input->post('changeUserDataConfirm')){
			$this->changeUserData();
		}
		
		$this->data['page_title'] 		= 'Nastavenia profilu' . $this->config->item('title_delimiter') . $this->config->item('eshop_name');
		$this->data['pageHeaderTitle']	= 'Nastavenia profilu';
		$this->data['subpages']   		= $this->admin_model->getAllSubpageNames();
		
		$this->load->view('header', $this->data);
		$this->load->view('profile/profile_header');
		$this->load->view('profile/profile_settings');
		$this->load->view('footer', $this->data);
	}
	
	public function orderDetail($orderId) {
		$this->data['page_title'] 		= 'Detail objednávky #' . generateVarSymbol($orderId) . $this->config->item('title_delimiter') . $this->config->item('eshop_name');
		$this->data['pageHeaderTitle']	= 'Detail objednávky #' . generateVarSymbol($orderId);
		$this->data['subpages']  		= $this->admin_model->getAllSubpageNames();
		$this->data['order_items']		= $this->profile_model->getOrderDetails($orderId);
		$this->data['order_info']		= $this->order_model->getOrderDetails($orderId);
		
		$this->load->view('header', $this->data);
		$this->load->view('profile/profile_header');
		$this->load->view('profile/profile_order_detail', $this->data);
		$this->load->view('footer', $this->data);
	}
	
	private function changePassword() {
		$this->form_validation->set_rules('changePasswordOld', 	 'Staré heslo', 'trim|required');
		$this->form_validation->set_rules('changePasswordNew1',  'Nové heslo', 	'trim|required');
		$this->form_validation->set_rules('changePasswordNew2',  'Nové heslo', 	'trim|required|matches[changePasswordNew1]');
		
		if($this->form_validation->run()){
			if (password_verify($this->input->post('changePasswordOld'), $this->profile_model->getOldPass())) {
				if ($this->profile_model->updatePass(password_hash($this->input->post('changePasswordNew1'), PASSWORD_DEFAULT))) {
					$this->session->set_flashdata('success', 'Zmena hesla prebehla úspešne');
					
					redirect('/profile/settings');
				} else {
					$this->session->set_flashdata('error', 'Zmena hesla sa nepodarila. Skúste to prosím znovu.');
					
					redirect('/profile/settings');
				}
			} else {
				$this->session->set_flashdata('error', 'Vaše staré heslo nie je správne!');
					
				redirect('/profile/settings');
			}
		}
	}
	
	private function changeUserData() {
		$this->form_validation->set_rules('changeName', 	 		'Meno', 			'trim|required');
        $this->form_validation->set_rules('changeStreet', 	 		'Ulica', 			'trim|required');
        $this->form_validation->set_rules('changeCity', 	 		'Mesto', 			'trim|required');
        $this->form_validation->set_rules('changeZipCode',   		'PSČ', 			 	'trim|required');
        $this->form_validation->set_rules('changePhone', 	 		'Telefónne číslo',  'trim|required');
        $this->form_validation->set_rules('changeBirthDate', 	 	'Dátum narodenia',  'trim|required');
		$this->form_validation->set_rules('changeDeliveryStreet', 	'Ulica', 			'trim');
		$this->form_validation->set_rules('changeDeliveryCity', 	'Mesto', 			'trim');
		$this->form_validation->set_rules('changeDeliveryZipCode', 	'PSČ', 				'trim');
		
		if (strip_tags($this->input->post('changeEmail')) == $this->session->userdata('email')) {
			$this->form_validation->set_rules('changeEmail', 	 'Emailová adresa',  'trim|required|valid_email');
		} else {
			$this->form_validation->set_rules('changeEmail', 	 'Emailová adresa',  'trim|required|valid_email|is_unique[users.email]');
		}
		
		if($this->form_validation->run()){
			$userData = array(
				'name' 				=> strip_tags($this->input->post('changeName')),
				'email' 			=> strip_tags($this->input->post('changeEmail')),
				'city' 				=> strip_tags($this->input->post('changeCity')),
				'street'	 		=> strip_tags($this->input->post('changeStreet')),
				'zip_code' 			=> strip_tags($this->input->post('changeZipCode')),
				'delivery_city' 	=> strip_tags($this->input->post('changeDeliveryCity')),
				'delivery_name' 	=> strip_tags($this->input->post('changeDeliveryName')),
				'delivery_surname' 	=> strip_tags($this->input->post('changeDeliverySurName')),
				'delivery_street'	=> strip_tags($this->input->post('changeDeliveryStreet')),
				'delivery_zip_code' => strip_tags($this->input->post('changeDeliveryZipCode')),
				'phone_number' 		=> strip_tags($this->input->post('changePhone')),
				'birth_date' 		=> strip_tags($this->input->post('changeBirthDate')),
			);
				
			if ($this->profile_model->updateUserData($userData)) {
				$this->session->set_flashdata('success', 'Údaje boli zmenené!');
				
				$this->session->set_userdata('name', 				$userData['name']);
				$this->session->set_userdata('email',	 			$userData['email']);
				$this->session->set_userdata('city',	 			$userData['city']);
				$this->session->set_userdata('street',	 			$userData['street']);
				$this->session->set_userdata('zip_code',			$userData['zip_code']);
				$this->session->set_userdata('delivery_city',	 	$userData['delivery_city']);
				$this->session->set_userdata('delivery_name',	 	$userData['delivery_name']);
				$this->session->set_userdata('delivery_surname',	$userData['delivery_surname']);
				$this->session->set_userdata('delivery_street',	 	$userData['delivery_street']);
				$this->session->set_userdata('delivery_zip_code',	$userData['delivery_zip_code']);
				$this->session->set_userdata('phone_number',		$userData['phone_number']);				
			} else {
				$this->session->set_flashdata('error', 'Údaje sa nepodarilo zmeniť, skúste to prosím znovu.');
			}
			redirect('/profile/settings');
		}
		
	}
	
	
}
