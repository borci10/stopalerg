<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Delivery extends MY_Controller {
	
	function __construct() {
		parent::__construct();

		$this->load->model('cart_model');

		$this->data['breadcrumbsTitle']	= 'Možnosti dopravy';
		$this->data['searchEnabled']	= true;
	}
	
	public function index() {
		if($this->input->post('formConfirm')){
			$this->addDeliveryOption();
		}

		$this->data['page_title'] 		= 'Možnosti dopravy' . $this->config->item('title_delimiter') . $this->config->item('eshop_name');
		$this->data['pageHeaderTitle']	= 'Možnosti dopravy';
		$this->data['delivery']  		= $this->cart_model->getAllDeliveryOptions();
	
		$this->load->view('admin/header', $this->data);
		$this->load->view('admin/delivery/delivery_list', $this->data);

	}
	
	public function edit($deliveryId) {
		if ($this->data['delivery'] = $this->cart_model->getDelivery($deliveryId)) {
			if($this->input->post('formConfirm')){
				$this->editDeliveryOption($deliveryId);
			}

			$this->data['page_title'] 		= 'Upraviť možnosti dopravy' . $this->config->item('title_delimiter') . $this->config->item('eshop_name');
			$this->data['pageHeaderTitle']	= 'Upraviť možnosti dopravy';	
			$this->data['subpages']   		= $this->admin_model->getAllSubpageNames();
		
			$this->load->view('admin/header', $this->data);
			$this->load->view('admin/delivery/delivery_form', $this->data);

		} else {
			redirect('admin/delivery');
		}
	}
	
	public function remove($deliveryId) {
		if ($this->admin_model->changeDeliveryDeletedStatus($deliveryId, 1)) {
			$this->session->set_flashdata('success', 'Spôsob doravy bol odstránený');
		} else {
			$this->session->set_flashdata('error', 'Vyskytla sa chyba. Skúste to prosím znovu.');
		}

		redirect($_SERVER['HTTP_REFERER']);
	}

	private function addDeliveryOption() {
		$this->form_validation->set_rules('deliveryTitle', 'Spôsob dopravy', 'trim|required');
		$this->form_validation->set_rules('deliveryPrice', 'Cena dopravy',   'trim|required|numeric|greater_than_equal_to[0]');
		
		if ($this->form_validation->run()) {
			$deliveryOption = array(
				'title' => strip_tags($this->input->post('deliveryTitle')),
				'price' => strip_tags($this->input->post('deliveryPrice'))
			); 
			
			if ($this->admin_model->insertDeliveryOption($deliveryOption)) {
				$this->session->set_flashdata('success', 'Spôsob dopravy bol pridaný!');
				
				redirect('/admin/delivery');
			} else {
				$this->session->set_flashdata('error', 'Spôsob dopravy nebol pridaný. Skúste to prosím znovu.');
				
				redirect('/admin/delivery');
			}
		} else {
			$this->session->set_flashdata('error', validation_errors('<span>', '</span>'));
		}
	}
	
	private function editDeliveryOption($deliveryId) {
		$this->form_validation->set_rules('deliveryTitle', 'Spôsob dopravy', 'trim|required');
		$this->form_validation->set_rules('deliveryPrice', 'Cena dopravy',   'trim|required|numeric|greater_than_equal_to[0]');
		
		if ($this->form_validation->run()) {
			$deliveryOption = array(
				'title' => strip_tags($this->input->post('deliveryTitle')),
				'price' => strip_tags($this->input->post('deliveryPrice'))
			); 
			
			if ($this->admin_model->updateDeliveryOption($deliveryId, $deliveryOption)) {
				$this->session->set_flashdata('success', 'Spôsob dopravy bol upravený!');
				
				redirect('/admin/delivery');
			} else {
				$this->session->set_flashdata('error', 'Spôsob dopravy nebol upravený. Skúste to prosím znovu.');
				
				redirect('/admin/delivery');
			}
		} else {
			$this->session->set_flashdata('error', validation_errors('<span>', '</span>'));
		}
	}
}
