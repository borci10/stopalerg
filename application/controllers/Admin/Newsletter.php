<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Newsletter extends MY_Controller {
	
	function __construct() {
		parent::__construct();
		
		$this->load->model('newsletter_model');

		$this->data['breadcrumbsTitle']	= 'Newsletter';
	}
	
	public function index() {
		$this->data['page_title'] 	= 'Správa newslettra' . $this->config->item('title_delimiter') . $this->config->item('eshop_name');
		$this->data['emails']		= $this->newsletter_model->getAllNewsletters();
		
		$this->load->view('admin/header', $this->data);
		$this->load->view('admin/newsletter/newsletter_list', $this->data);
	}
	
	public function add() {
		if($this->input->post('formConfirm')){
			$this->addNewsletter();
		} 
		
		$this->data['page_title'] 	= 'Pridať newsletter' . $this->config->item('title_delimiter') . $this->config->item('eshop_name');
		$this->data['button']		= 'Pridať newsletter';
		$this->data['pageHeader']	= 'Pridať newsletter';

		$this->load->view('admin/header', $this->data);
		$this->load->view('admin/newsletter/newsletter_form', $this->data);
	}
	
	public function edit($newsletterId) {
		if($this->input->post('formConfirm')){
			$this->editNewsletter($newsletterId);
		} 
		$this->data['page_title'] 	= 'Upraviť newsletter' . $this->config->item('title_delimiter') . $this->config->item('eshop_name');
		$this->data['newsletter']	= $this->newsletter_model->getNewsletter($newsletterId);
		$this->data['button']		= 'Upraviť newsletter';
		$this->data['pageHeader']	= 'Upraviť newsletter';

		$this->load->view('admin/header', $this->data);
		$this->load->view('admin/newsletter/newsletter_form', $this->data);
	}
	
	public function delete($newsletterId) {
		if ($this->newsletter_model->deleteNewsletter($newsletterId)) {
			$this->session->set_flashdata('success', 'Newsletter bol odstránený!');
		} else {
			$this->session->set_flashdata('error', 'Nastala chyba. Skúste to prosím znovu.');
		}
		redirect('/admin/newsletter');
	}
	
	public function export() {
		$emails = array_column($this->newsletter_model->getAllNewsletters(), 'email');

		if (!file_exists('csv')) {
			mkdir('csv');
		}
		
		$fp = fopen('csv/emails.csv', 'w');

		fputcsv($fp, ['Email Address']);
		foreach ($emails as $email) {
			fputcsv($fp, [$email]);
		}

		fclose($fp);
		
		header("Content-type: text/csv");
		header("Content-disposition: attachment; filename = emails.csv");
		readfile("csv/emails.csv");
	}
	
	private function addNewsletter() {
		$this->form_validation->set_rules('newsletterEmail', 'Email', 'trim|required|max_length[255]|valid_email');
        $this->form_validation->set_rules('newsletterDate',  'Dátum', 'trim|required');
		
		if($this->form_validation->run()) {
			$newsletterData = array(
				'email' => strip_tags($this->input->post('newsletterEmail')),
				'date' 	=> mysqlDate(strip_tags($this->input->post('newsletterDate'))),
			);
			if ($this->newsletter_model->insertNewsletter($newsletterData)) {
				$this->session->set_flashdata('success', 'Newsletter bol pridaný!');
				
				redirect('/admin/newsletter');
			} else {
				$this->session->set_flashdata('error', 'Newsletter nebol pridaný. Skúste to prosím znovu.');
				
				redirect('/admin/newsletter/add');
			}
		} else {
			$this->session->set_flashdata('error', validation_errors('<span>', '</span>'));
		}
	}
	
	private function editNewsletter($newsletterId) {
		$this->form_validation->set_rules('newsletterEmail', 'Email', 'trim|required|max_length[255]|valid_email');
        $this->form_validation->set_rules('newsletterDate',  'Dátum', 'trim|required');
		
		if($this->form_validation->run()){
			$newsletterData = array(
				'email' => strip_tags($this->input->post('newsletterEmail')),
				'date' 	=> mysqlDate(strip_tags($this->input->post('newsletterDate'))),
			);

			if ($this->newsletter_model->updateNewsletter($newsletterId, $newsletterData)) {
				$this->session->set_flashdata('success', 'Newsletter bol upravený!');
				
				redirect('/admin/newsletter');
			} else {
				$this->session->set_flashdata('error', 'Newsletter nebol upravený. Skúste to prosím znovu.');
				
				redirect('/admin/newsletter/edit/' . $newsletterId);
			}
		} else {
			$this->session->set_flashdata('error', validation_errors('<span>', '</span>'));
		}
	}

}