<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Customers extends MY_Controller {
	
	function __construct() {
		parent::__construct();

		$this->load->model('Admin/order_model');
		$this->load->model('Admin/customers_model');

		$this->data['breadcrumbsTitle']	= 'Zákazníci';
		$this->data['searchEnabled']	= true;
	}
	
	public function index() {
		$filter = $this->parseFilter();
		
		$this->data['pageHeaderTitle']	= 'Zákazníci';
		$this->data['page_title'] 		= 'Zákazníci' . $this->config->item('title_delimiter') . $this->config->item('eshop_name');
		$this->data['customers']  		= $this->customers_model->getFilteredCustomers((($this->input->get('page') ?? 1) - 1) * $this->config->item('sql_result_limit'), $filter);
		$this->data['subpages']   		= $this->admin_model->getAllSubpageNames();
		$this->data['totalCount'] 		= $this->customers_model->getTotalCountOfCustomers($filter);
	
		$this->load->view('admin/header', $this->data);
		$this->load->view('admin/customers/customer_list_header', $this->data);
		$this->load->view('admin/customers/customer_list', $this->data);

	}
	
	public function detail($customerId) {
		if ($this->data['customer_info']	= $this->customers_model->getCustomerDetails($customerId)) {
			$this->data['page_title'] 		= 'Detail zákazníka' . $this->config->item('title_delimiter') . $this->config->item('eshop_name');
			$this->data['subpages']   		= $this->admin_model->getAllSubpageNames();
			$this->data['orders']   		= $this->customers_model->getCustomerOrders($customerId);
		
			$this->load->view('admin/header', $this->data);
			$this->load->view('admin/customers/customer_detail', $this->data);

		} else {
			redirect('admin/customers');
		}
	}
	
	private function parseFilter() {
		$filter = [];
		
		// $filter['users.admin'] = 0;
		
		if ($this->input->get('keyWordFilter')) {
			$filter['(
				users.name LIKE "%' . $this->input->get('keyWordFilter') . '%" OR 
				users.surname LIKE "%' . $this->input->get('keyWordFilter') . '%" OR 
				users.email LIKE "%' . $this->input->get('keyWordFilter') . '%" OR 
				users.phone_number LIKE "%' . $this->input->get('keyWordFilter') . '%"
			)'] = null;
		}
		
		if ($this->input->get('dateFromFilter')) {
			$filter['users.registration_time >='] = mysqlDate($this->input->get('dateFromFilter'));
		}
		
		if ($this->input->get('dateToFilter')) {
			$filter['users.registration_time <='] = mysqlDate($this->input->get('dateToFilter'));
		}
		
		return $filter;
	}
	
}