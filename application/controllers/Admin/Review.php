<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Review extends MY_Controller {
	
	function __construct() {
		parent::__construct();

		$this->load->model('Admin/review_model');

		$this->data['breadcrumbsTitle']	= 'Review';
	}
	
	public function index() {
		$this->data['page_title'] = 'Administrácia' . $this->config->item('title_delimiter') . $this->config->item('eshop_name');
		$this->data['reviews']   = $this->review_model->getReviewList();

		$this->load->view('admin/header', $this->data);
		$this->load->view('admin/review/review_list', $this->data);
	}
	
	public function add() {
		if($this->input->post('formConfirm')){
			$this->addReview();
		} 
		
		$this->data['page_title'] 		= 'Pridať recenziu' . $this->config->item('title_delimiter') . $this->config->item('eshop_name');
		$this->data['button']			= 'Pridať recenziu';
		$this->data['breadcrumbsTitle']	= 'Pridať recenziu';

		$this->load->view('admin/header', $this->data);
		$this->load->view('admin/review/review_form', $this->data);
	}
	
	public function edit($reviewId) {
		if ($this->data['review'] = $this->review_model->getReview($reviewId)) {
			if($this->input->post('formConfirm')){
				$this->editReview($reviewId);
			}

			$this->data['page_title'] 		= 'Upraviť recenziu' . $this->config->item('title_delimiter') . $this->config->item('eshop_name');
			$this->data['button']			= 'Upraviť recenziu';
			$this->data['breadcrumbsTitle']	= 'Upraviť recenziu';

			$this->load->view('admin/header', $this->data);
			$this->load->view('admin/review/review_form', $this->data);
		} else {
			redirect('/admin/reviews');
		}
	}
	
	public function delete($reviewId) {
		if ($this->review_model->deleteReview($reviewId)) {
			$this->session->set_flashdata('success', 'Recenzia bola odstránená!');
		} else {
			$this->session->set_flashdata('error', 'Nastala chyba. Skúste to prosím znovu.');
		}
		redirect($_SERVER['HTTP_REFERER']);
	}
	
	public function deleteComment($commentId) {
		if ($this->review_model->deleteComment($commentId)) {
			$this->session->set_flashdata('success', 'Komentár bol odstránený!');
		} else {
			$this->session->set_flashdata('error', 'Nastala chyba. Skúste to prosím znovu.');
		}
		redirect($_SERVER['HTTP_REFERER']);
	}

	public function publish($reviewId) {
		if ($this->review_model->changePublication($reviewId, 1)) {
			$this->session->set_flashdata('success', 'Recenzia bola publikovaná.');
		} else {
			$this->session->set_flashdata('error', 'Vyskytla sa chyba. Skúste to prosím znovu.');
		}
		redirect($_SERVER['HTTP_REFERER']);
	}
	
	public function unpublish($reviewId) {
		if ($this->review_model->changePublication($reviewId, 0)) {
			$this->session->set_flashdata('success', 'Článok bol odstránený z publikácie.');
		} else {
			$this->session->set_flashdata('error', 'Vyskytla sa chyba. Skúste to prosím znovu.');
		}
		redirect($_SERVER['HTTP_REFERER']);
	}
	
	public function commentsOn($reviewId) {
		if ($this->review_model->changeCommenting($reviewId, 1)) {
			$this->session->set_flashdata('success', 'Komentovanie bolo zapnuté.');
		} else {
			$this->session->set_flashdata('error', 'Vyskytla sa chyba. Skúste to prosím znovu.');
		}
		redirect($_SERVER['HTTP_REFERER']);
	}
	
	public function commentsOff($reviewId) {
		if ($this->review_model->changeCommenting($reviewId, 0)) {
			$this->session->set_flashdata('success', 'Komentovanie bolo vypnuté.');
		} else {
			$this->session->set_flashdata('error', 'Vyskytla sa chyba. Skúste to prosím znovu.');
		}
		redirect($_SERVER['HTTP_REFERER']);
	}
	
	public function uploadPhoto() {
		if ($_SERVER['REQUEST_METHOD'] === 'POST') {
			if (!file_exists ('uploads')) {
				mkdir('uploads', 0777, true);
			}
			if (!file_exists ('uploads/review')) {
				mkdir('uploads/review', 0777, true);
			}
			if (!file_exists ('uploads/review/400')) {
				mkdir('uploads/review/400', 0777, true);
			}
			if (!file_exists ('uploads/review/1000')) {
				mkdir('uploads/review/1000', 0777, true);
			}
			
			$config['upload_path'] 		= 'uploads/review/';
			$config['allowed_types'] 	= 'gif|jpg|png';
			$config['max_filename'] 	= '255';
			$config['encrypt_name'] 	= TRUE;
			$config['max_size'] 		= '10024'; //10 MB
	 
			if (isset($_FILES['file']['name'])) {
				if (0 < $_FILES['file']['error']) {
					echo json_encode(['err']);
				} else {           
					$this->load->library('upload', $config);
					if ($this->upload->do_upload('file')) {
						$upload_data = $this->upload->data();
						
						$this->load->library('image_lib');

						$config_resize_1['image_library'] 	= 'gd2';
						$config_resize_1['source_image'] 	= 'uploads/review/' . $upload_data['file_name'];
						$config_resize_1['create_thumb'] 	= false;
						$config_resize_1['maintain_ratio'] 	= true;
						$config_resize_1['quality']		 	= '80%';
						$config_resize_1['width']		 	= 400;
						$config_resize_1['new_image']		= 'uploads/review/400/' . $upload_data['file_name'];

						$this->image_lib->initialize($config_resize_1);
						$this->image_lib->resize();
						
						$config_resize_2['image_library'] 	= 'gd2';
						$config_resize_2['source_image'] 	= 'uploads/review/' . $upload_data['file_name'];
						$config_resize_2['create_thumb'] 	= false;
						$config_resize_2['maintain_ratio'] 	= true;
						$config_resize_2['quality']		 	= '80%';
						$config_resize_2['width']		 	= 1000;
						$config_resize_2['new_image']		= 'uploads/review/1000/' . $upload_data['file_name'];

						$this->image_lib->initialize($config_resize_2);
						$this->image_lib->resize();

						$this->data['image'] = $upload_data['file_name'];
						
						echo json_encode(['ok', $upload_data['file_name'], $this->load->view('admin/review/_partials/image_upload_item', $this->data, true)]);
					} else {
						echo json_encode(['err']);
					}
				}
			} else {
				echo json_encode(['err']);
			}
		} else {
			redirect('');
		}
    }
	
	public function removePhoto() {
		if ($fileName = $this->input->post('file_name')) {
			if ($this->review_model->deletePhoto($fileName)) {
				echo $fileName;
			}
		}
	}
	
	private function addReview() {
		$this->form_validation->set_rules('header', 	'Názov recenzie', 	'trim|required|max_length[255]');
		$this->form_validation->set_rules('rating', 	'Hodnotenie', 		'trim|required');
        $this->form_validation->set_rules('content',  	'Text recenzie',  	'trim');
		
		if($this->form_validation->run()) {
			$reviewData = array(
				'header' 	=> strip_tags($this->input->post('header')),
				'rating' 	=> strip_tags($this->input->post('rating')),
				'link' 		=> strtolower(utf8_encode(convert_accented_characters(url_title(strip_tags($this->input->post('header'))), "dash", true))),
				'content' 	=> $this->input->post('content'),
				'image'		=> $this->input->post('image')
			);
			
			if ($this->review_model->insertReview($reviewData)) {
				$this->session->set_flashdata('success', 'Recenzia bola pridaná!');
				
				redirect('/admin/reviews');
			} else {
				$this->session->set_flashdata('error', 'Recenzia nebola pridaná. Skúste to prosím znovu.');
				
				redirect('/admin/reviews/add');
			}
		} else {
			$this->session->set_flashdata('error', validation_errors('<span>', '</span>'));
		}
	}
	
	private function editReview($reviewId) {
		$this->form_validation->set_rules('header', 	'Názov recenzie', 'trim|required|max_length[255]');
		$this->form_validation->set_rules('rating', 	'Hodnotenie',     'trim|required');
        $this->form_validation->set_rules('content',  	'Text recenzie',  'trim');
		
		if($this->form_validation->run()){
			$reviewData = array(
				'header' 	=> strip_tags($this->input->post('header')),
				'rating' 	=> strip_tags($this->input->post('rating')),
				'link' 		=> strtolower(utf8_encode(convert_accented_characters(url_title(strip_tags($this->input->post('header'))), "dash", true))),
				'content' 	=> $this->input->post('content'),
				'image'		=> $this->input->post('image')
			);

			if ($this->review_model->updateReview($reviewId, $reviewData)) {
				$this->session->set_flashdata('success', 'Recenzia bola upraveníá!');
				
				redirect('/admin/reviews');
			} else {
				$this->session->set_flashdata('error', 'Recenzia nebola upravený. Skúste to prosím znovu.');
				
				redirect('/admin/reviews/edit/' . $reviewId);
			}
		} else {
			$this->session->set_flashdata('error', validation_errors('<span>', '</span>'));
		}
	}
}
