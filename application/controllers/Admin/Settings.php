<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Settings extends MY_Controller {
	
	function __construct() {
		parent::__construct();

		$this->load->model('Admin/settings_model');

		$this->data['breadcrumbsTitle']	= 'Nastavenia';
		$this->data['searchEnabled']	= true;
	}

	public function index() {
		if ($this->input->post('changeCompanyInfoConfirm')) {
			$this->saveCompanyInfo();
		}
		
		if ($this->input->post('changeBankInfoConfirm')) {
			$this->saveBankInfo();
		}

		$this->data['pageHeaderTitle']		= 'Nastavenia';
		$this->data['page_title'] 			= 'Nastavenia' . $this->config->item('title_delimiter') . $this->config->item('eshop_name');
		$this->data['company_name']			= $this->settings_model->getSingleValue('company_name');
		$this->data['company_email']		= $this->settings_model->getSingleValue('company_email');
		$this->data['company_ico']			= $this->settings_model->getSingleValue('company_ico');
		$this->data['company_dic']			= $this->settings_model->getSingleValue('company_dic');
		$this->data['company_icdph']		= $this->settings_model->getSingleValue('company_icdph');
		$this->data['company_street']		= $this->settings_model->getSingleValue('company_street');
		$this->data['company_city']			= $this->settings_model->getSingleValue('company_city');
		$this->data['company_zip_code']		= $this->settings_model->getSingleValue('company_zip_code');
		$this->data['company_phone']		= $this->settings_model->getSingleValue('company_phone');
		$this->data['vat_payer']			= $this->settings_model->getSingleValue('vat_payer');
		$this->data['bank_name']			= $this->settings_model->getSingleValue('bank_name');
		$this->data['bank_account_number']	= $this->settings_model->getSingleValue('bank_account_number');
		$this->data['bank_code']			= $this->settings_model->getSingleValue('bank_code');
		$this->data['bank_iban']			= $this->settings_model->getSingleValue('bank_iban');
		$this->data['bank_swift']			= $this->settings_model->getSingleValue('bank_swift');

		$this->load->view('admin/header', $this->data);
		$this->load->view('admin/settings/settings_form', $this->data);

	}
	
	private function saveCompanyInfo() {
		$this->form_validation->set_rules('company_name',   	'Názov spoločnosti','trim|max_length[255]');
        $this->form_validation->set_rules('company_email',		'Email',  			'trim|max_length[255]|valid_email');
        $this->form_validation->set_rules('company_ico',   		'IČO', 				'trim|max_length[255]');
		$this->form_validation->set_rules('company_dic',   		'DIČ', 				'trim|max_length[255]');
        $this->form_validation->set_rules('company_icdph', 	  	'IČ DPH', 	  		'trim|max_length[255]');
        $this->form_validation->set_rules('company_street', 	'Ulica', 	  		'trim|max_length[255]');
        $this->form_validation->set_rules('company_city', 	  	'Mesto', 	  		'trim|max_length[255]');
        $this->form_validation->set_rules('company_zip_code', 	'PSČ', 	  			'trim|max_length[255]');
        $this->form_validation->set_rules('company_phone', 	  	'Telefón', 	  		'trim|max_length[255]');
			
		if ($this->form_validation->run()) {		
			$companyData = array(
				'company_name'  	=> strip_tags($this->input->post('company_name')),
				'company_email'		=> strip_tags($this->input->post('company_email')),
				'company_ico'   	=> strip_tags($this->input->post('company_ico')),
				'company_dic'  		=> strip_tags($this->input->post('company_dic')),
				'company_icdph' 	=> strip_tags($this->input->post('company_icdph')),
				'company_street' 	=> strip_tags($this->input->post('company_street')),
				'company_city' 		=> strip_tags($this->input->post('company_city')),
				'company_zip_code'	=> strip_tags($this->input->post('company_zip_code')),
				'company_phone'		=> strip_tags($this->input->post('company_phone')),
				'vat_payer'			=> strip_tags($this->input->post('vat_payer')),
			);
	
			if ($this->settings_model->setMultipleValues($companyData)) {
				$this->session->set_flashdata('success', 'Informácie o spoločnosti boli zmenené!');
			} else {
				$this->session->set_flashdata('error', 'Informácie o spoločnosti neboli zmenené. Skúste to prosím znovu.');	
			}
			redirect('/admin/settings');
		} else {
			$this->session->set_flashdata('error', validation_errors('<span>', '</span>'));
		}
	}
	
	private function saveBankInfo() {
		$this->form_validation->set_rules('bank_name',   		'Názov banky', 	'trim|max_length[255]');
        $this->form_validation->set_rules('bank_account_number','Číslo účtu',  	'trim|max_length[255]');
        $this->form_validation->set_rules('bank_code',   		'Kód banky', 	'trim|max_length[255]');
		$this->form_validation->set_rules('bank_iban',   		'IBAN', 		'trim|max_length[255]');
        $this->form_validation->set_rules('bank_swift', 	  	'SWIFT', 	  	'trim|max_length[255]');
			
		if ($this->form_validation->run()) {		
			$bankData = array(
				'bank_name' 			=> strip_tags($this->input->post('bank_name')),
				'bank_account_number' 	=> strip_tags($this->input->post('bank_account_number')),
				'bank_code' 			=> strip_tags($this->input->post('bank_code')),
				'bank_iban' 			=> strip_tags($this->input->post('bank_iban')),
				'bank_swift' 			=> strip_tags($this->input->post('bank_swift')),
			);
	
			if ($this->settings_model->setMultipleValues($bankData)) {
				$this->session->set_flashdata('success', 'Informácie o banke boli zmenené!');
			} else {
				$this->session->set_flashdata('error', 'Informácie o banke neboli zmenené. Skúste to prosím znovu.');	
			}
			redirect('/admin/settings');
		} else {
			$this->session->set_flashdata('error', validation_errors('<span>', '</span>'));
		}
	}
}
