<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Categories extends MY_Controller {
	
	function __construct() {
		parent::__construct();

		$this->load->model('Admin/admin_category_model');

		$this->data['breadcrumbsTitle']	= 'Kategórie';
		$this->data['searchEnabled']	= true;
	}

	public function index() {		
		$this->data['pageHeaderTitle']	= 'Kategórie';
		$this->data['page_title'] 		= 'Kategórie' . $this->config->item('title_delimiter') . $this->config->item('eshop_name');
		$this->data['categories'] 		= $this->admin_model->getCategoryTree(1);
	
		$this->load->view('admin/header', $this->data);
		$this->load->view('admin/categories/category_tree', $this->data);
	}

	public function add() {
		if($this->input->post('formConfirm')){
			$this->addCategory();
		}

		$this->data['page_title'] 		= 'Pridať kategóriu' . $this->config->item('title_delimiter') . $this->config->item('eshop_name');
		$this->data['button']			= 'Pridať kategóriu';
		$this->data['breadcrumbsTitle']	= 'Pridať kategóriu';

		$this->load->view('admin/header', $this->data);
		$this->load->view('admin/categories/category_form', $this->data);
	}
	
	public function edit($categoryId) {
		if ($this->data['category'] = $this->admin_category_model->getCategory($categoryId)) {
			if($this->input->post('formConfirm')){
				$this->editCategory($categoryId);
			}

			$this->data['page_title'] 		= 'Upraviť kategóriu' . $this->config->item('title_delimiter') . $this->config->item('eshop_name');
			$this->data['button']			= 'Upraviť kategóriu';
			$this->data['breadcrumbsTitle']	= 'Upraviť kategóriu';

			$this->load->view('admin/header', $this->data);
			$this->load->view('admin/categories/category_form', $this->data);
		} else {
			redirect('/admin/category');
		}
	}
	
	public function remove($categoryId) {
		if ($this->admin_category_model->deleteCategory($categoryId)) {
			$this->session->set_flashdata('success', 'Kategória bola odstránená.');
		} else {
			$this->session->set_flashdata('error', 'Vyskytla sa chyba. Skúste to prosím znovu.');		
		}

		redirect($_SERVER['HTTP_REFERER']);
	}
	
	public function show($categoryId) {
		if ($this->admin_category_model->updateCategoryVisibility($categoryId, 1)) {
			$this->session->set_flashdata('success', 'Kategória a jej podkategórie sú viditeľné.');
		} else {
			$this->session->set_flashdata('error', 'Vyskytla sa chyba. Skúste to prosím znovu.');		
		}
		redirect($_SERVER['HTTP_REFERER']);
	}
	
	public function hide($categoryId) {
		if ($this->admin_category_model->updateCategoryVisibility($categoryId, 0)) {
			$this->session->set_flashdata('success', 'Kategória a jej podkategórie sú skryté.');
		} else {
			$this->session->set_flashdata('error', 'Vyskytla sa chyba. Skúste to prosím znovu.');		
		}
		redirect($_SERVER['HTTP_REFERER']);
	}
	
	public function uploadPhoto() {
		if ($_SERVER['REQUEST_METHOD'] === 'POST') {
			if (!file_exists ('uploads')) {
				mkdir('uploads', 0777, true);
			}
			if (!file_exists ('uploads/categories')) {
				mkdir('uploads/categories', 0777, true);
			}
			if (!file_exists ('uploads/categories/400')) {
				mkdir('uploads/categories/400', 0777, true);
			}
			if (!file_exists ('uploads/categories/1000')) {
				mkdir('uploads/categories/1000', 0777, true);
			}
			
			$config['upload_path'] 		= 'uploads/categories/';
			$config['allowed_types'] 	= 'gif|jpg|png';
			$config['max_filename'] 	= '255';
			$config['encrypt_name'] 	= TRUE;
			$config['max_size'] 		= '10024'; //10 MB
	 
			if (isset($_FILES['file']['name'])) {
				if (0 < $_FILES['file']['error']) {
					echo json_encode(['err']);
				} else {           
					$this->load->library('upload', $config);
					if ($this->upload->do_upload('file')) {
						$upload_data = $this->upload->data();
						
						$this->load->library('image_lib');

						$config_resize_1['image_library'] 	= 'gd2';
						$config_resize_1['source_image'] 	= 'uploads/categories/' . $upload_data['file_name'];
						$config_resize_1['create_thumb'] 	= false;
						$config_resize_1['maintain_ratio'] 	= true;
						$config_resize_1['quality']		 	= '80%';
						$config_resize_1['width']		 	= 400;
						$config_resize_1['new_image']		= 'uploads/categories/400/' . $upload_data['file_name'];

						$this->image_lib->initialize($config_resize_1);
						$this->image_lib->resize();
						
						$config_resize_2['image_library'] 	= 'gd2';
						$config_resize_2['source_image'] 	= 'uploads/categories/' . $upload_data['file_name'];
						$config_resize_2['create_thumb'] 	= false;
						$config_resize_2['maintain_ratio'] 	= true;
						$config_resize_2['quality']		 	= '80%';
						$config_resize_2['width']		 	= 1000;
						$config_resize_2['new_image']		= 'uploads/categories/1000/' . $upload_data['file_name'];

						$this->image_lib->initialize($config_resize_2);
						$this->image_lib->resize();

						$this->data['photo'] = $upload_data['file_name'];
						
						echo json_encode(['ok', $upload_data['file_name'], $this->load->view('admin/categories/_partials/image_upload_item', $this->data, true)]);
					} else {
						echo json_encode(['err']);
					}
				}
			} else {
				echo json_encode(['err']);
			}
		} else {
			redirect('');
		}
    }
	
	public function removePhoto() {
		if ($fileName = $this->input->post('file_name')) {
			if ($this->admin_category_model->deletePhoto($fileName)) {
				echo $fileName;
			}
		}
	}
	
	public function changeCategoryStructure() {
		$categoryTree = json_decode($this->input->post('categorytree'));
		
		if ($this->admin_category_model->saveCategoryTree($categoryTree)) {
			$this->session->set_flashdata('success', 'Kategórie boli uložené.');
		} else {
			$this->session->set_flashdata('error', 'Vyskytla sa chyba. Skúste to prosím znovu.');
		}
	}
	
	private function addCategory() {
		$this->form_validation->set_rules('title', 		'Názov kategórie', 'trim|required|max_length[255]');
		
		if($this->form_validation->run()){
			$categoryData = [
				'title' 	=> strip_tags($this->input->post('title')),
				'link' 		=> strtolower(utf8_encode(convert_accented_characters(url_title(strip_tags($this->input->post('title'))), "dash", true))),
				'photo'		=> $this->input->post('photo')
			];

			if ($this->admin_category_model->insertCategory($categoryData)) {
				$this->session->set_flashdata('success', 'Kategória bola pridaná!');
				
				redirect('/admin/category');
			} else {
				$this->session->set_flashdata('error', 'Kategória nebola pridaná. Skúste to prosím znovu.');
				
				redirect('/admin/category/add');
			}
		} else {
			$this->session->set_flashdata('error', validation_errors('<span>', '</span>'));
		}
	}
	
	private function editCategory($categoryId) {
		$this->form_validation->set_rules('title', 		'Názov kategórie', 'trim|required|max_length[255]');
		
		if($this->form_validation->run()){
			$categoryData = [
				'title' 	=> strip_tags($this->input->post('title')),
				'link' 		=> strtolower(utf8_encode(convert_accented_characters(url_title(strip_tags($this->input->post('title'))), "dash", true))),
				'photo'		=> $this->input->post('photo')
			];

			if ($this->admin_category_model->updateCategory($categoryId, $categoryData)) {
				$this->session->set_flashdata('success', 'Kategória bola upravená!');
				
				redirect('/admin/category');
			} else {
				$this->session->set_flashdata('error', 'Kategória nebola upravená. Skúste to prosím znovu.');
				
				redirect('/admin/category/edit/' . $categoryId);
			}
		} else {
			$this->session->set_flashdata('error', validation_errors('<span>', '</span>'));
		}
	}
}
