<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Neural_network extends MY_Controller {
	
	function __construct() {
		parent::__construct();

		$this->load->model('admin/neural_network_model');
	}
	
	public function index() {
		$items = $this->neural_network_model->getUniqueOrderItems();
		
		$this->addItemOrdersId($items);
		$this->addAffinityItems($items);
		$this->countAffinityItemValues($items);
		$this->calculateAffinityScore($items);

		echo "<pre>";
		print_r($items);
		echo "</pre>";
		
		$this->neural_network_model->updateRecomendedProductsScores($items);
	}
	
	private function addItemOrdersId(&$items) {
		foreach ($items as &$item) {
			$item['orderIds'] = array_column($this->neural_network_model->getItemOrderIds($item['itemId']), 'id_order');
		}
	}
	
	private function addAffinityItems(&$items) {
		foreach ($items as &$item) {
			$item['affinityProducts'] = [];
			foreach ($item['orderIds'] as $orderId) {
				$item['affinityProducts'] = array_merge($item['affinityProducts'], array_column($this->neural_network_model->getAffinityItems($orderId, $item['itemId']), 'affinityItemId'));
			}
		}
	}
	
	private function countAffinityItemValues(&$items) {
		foreach ($items as &$item) {
			$item['affinityValues'] = array_count_values($item['affinityProducts']);
		}
	}
	
	
	private function calculateAffinityScore(&$items) {
		foreach ($items as &$item) {
			$item['affinityScore'] 	= [];
			$affinityScoreSum 		= array_sum($item['affinityValues']);
			foreach ($item['affinityValues'] as $itemIdKey => $scoreValue) {
				$item['affinityScore'][$itemIdKey] =  $scoreValue / $affinityScoreSum;
			}
		}
	}
	
}