<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Content extends MY_Controller {
	
	function __construct() {
		parent::__construct();

		$this->data['breadcrumbsTitle']	= 'Správa podstránok';
		$this->data['searchEnabled']	= true;
	}
	
	public function index() {
		$this->data['page_title'] 			= 'Správa obsahu' . $this->config->item('title_delimiter') . $this->config->item('eshop_name');
		$this->data['pageHeaderTitle']		= 'Správa obsahu';
		$this->data['createButtonAdress']	= 'admin/content/add';
		$this->data['createButtonCaption']	= 'Pridať podstránku';
		$this->data['subpages'] 			= $this->admin_model->getAllSubpageNames();
	
		$this->load->view('admin/header', $this->data);
		$this->load->view('admin/content/content_list', $this->data);

	}
	
	public function add() {
		if($this->input->post('formConfirm')){
			$this->addNewSubpage();
		} 

		$this->data['button_title'] 	= 'Pridať podstránku';
		$this->data['pageHeaderTitle']	= 'Pridať podstránku';
		$this->data['subpages'] 		= $this->admin_model->getAllSubpageNames();
	
		$this->load->view('admin/header', $this->data);
		$this->load->view('admin/content/content_form');

	}
	
	public function edit($subpageId) {
		if ($this->data['subpageContent'] 	= $this->admin_model->getSubpageContentById($subpageId)) {
			if($this->input->post('formConfirm')){
				$this->editSubpageContent($subpageId);
			} 

			$this->data['button_title'] 	= 'Upraviť podstránku';
			$this->data['pageHeaderTitle']	= 'Upraviť podstránku';
			$this->data['subpages'] 		= $this->admin_model->getAllSubpageNames();
			
		
			$this->load->view('admin/header', $this->data);
			$this->load->view('admin/content/content_form', $this->data);

		} else {
			redirect('/admin/content');
		}
	}
	
	public function remove($subpageId) {
		if ($this->admin_model->deleteSubpage($subpageId)) {
			$this->session->set_flashdata('success', 'Podstránka bola odstránená!');
		} else {
			$this->session->set_flashdata('error', 'Podstránka nebola odstránená. Skúste to prosím znovu.');	
		}
		
		redirect('/admin/content');
	}
	
	private function addNewSubpage() {
		$this->form_validation->set_rules('subpageTitle', 'Názov podstránky', 'trim|required|max_length[50]');
        $this->form_validation->set_rules('subpageText',  'Text podstránky',  'trim');
		
		if ($this->form_validation->run()) {
			$subpageData = array(
				'title' 	=> strip_tags($this->input->post('subpageTitle')),
				'content' 	=> $this->input->post('subpageText'),
			);
			
			if ($this->admin_model->insertSubpageData($subpageData)) {
				$this->session->set_flashdata('success', 'Podstránka bola pridaná!');
				
				redirect('/admin/content');
			} else {
				$this->session->set_flashdata('error', 'Podstránka nebola pridaná. Skúste to prosím znovu.');
				
				redirect('/admin/content/add');
			}
		} else {
			$this->session->set_flashdata('error', validation_errors('<span>', '</span>'));
		}
	}
	
	private function editSubpageContent($subpageId) {
		$this->form_validation->set_rules('subpageTitle', 'Názov podstránky', 'trim|required|max_length[50]');
        $this->form_validation->set_rules('subpageText',  'Text podstránky',  'trim');
		
		if ($this->form_validation->run()) {
			$subpageData = array(
				'title' 	=> strip_tags($this->input->post('subpageTitle')),
				'content' 	=> $this->input->post('subpageText'),
			);
			
			if ($this->admin_model->updateSubpageData($subpageId, $subpageData)) {
				$this->session->set_flashdata('success', 'Podstránka bola upravená!');
				
				redirect('/admin/content');
			} else {
				$this->session->set_flashdata('error', 'Podstránka nebola upravená. Skúste to prosím znovu.');
				
				redirect('/admin/content/edit/' . $subpageId);
			}
		} else {
			$this->session->set_flashdata('error', validation_errors('<span>', '</span>'));
		}
	}
}
