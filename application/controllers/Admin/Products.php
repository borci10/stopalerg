<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Products extends MY_Controller {
	
	function __construct() {
		parent::__construct();

		$this->data['page_title'] = 'Produkty' . $this->config->item('title_delimiter') . $this->config->item('eshop_name');

		$this->load->model('Admin/product_model2', 'admin_product_model');
		
		$this->data['searchEnabled']	= true;
		$this->data['breadcrumbsTitle']	= 'Produkty';
	}

	public function index() {
		$filter = $this->parseFilter();
		
		$this->data['products']   			= $this->admin_product_model->getAllProductsShort((($this->input->get('page') ?? 1) - 1) * $this->config->item('sql_result_limit'), $filter);
		$this->data['subpages']   			= $this->admin_model->getAllSubpageNames();
		$this->data['totalCount'] 			= $this->product_model->getTotalCountOfProducts($filter);
	
		$this->load->view('admin/header', $this->data);
		$this->load->view('admin/products/product_list_header', $this->data);
		$this->load->view('admin/products/product_list', $this->data);

	}

	public function add() {		
		if($this->input->post('formConfirm')){
			$this->addNewProduct();
		} 

		$this->data['buttone_title']		= 'Pridať produkt';
		$this->data['pageHeaderTitle']		= 'Pridať produkt';
		$this->data['breadcrumbsTitle']		= 'Pridať produkt';
		$this->data['categories']           = $this->admin_model->getCategoryTree();
		$this->data['subpages'] 			= $this->admin_model->getAllSubpageNames();
		$this->data['suppliers']  			= $this->admin_model->getAllSuppliers();
		$this->data['attribute_suggestions']= json_encode(array_column($this->admin_model->getAllProductsAttributes(), 'title'));
	
		$this->load->view('admin/header', $this->data);
		$this->load->view('admin/products/product_form', $this->data);

	}
	
	public function edit($productId) {
		if($this->input->post('formConfirm')){
			$this->editProductInfo($productId);
		}

		if ($this->data['productInfo'] 		= $this->product_model->getProductById($productId)) {
			$this->data['buttone_title']		= 'Uložiť zmeny';
			$this->data['pageHeaderTitle']		= 'Upraviť produkt';
			$this->data['breadcrumbsTitle']		= 'Upraviť produkt';
			$this->data['categories']   		= $this->admin_model->getCategoryTree();
			$this->data['productPhotos'] 		= $this->product_model->getProductPhotosById($productId);
			$this->data['subpages'] 			= $this->admin_model->getAllSubpageNames();
			$this->data['suppliers']  			= $this->admin_model->getAllSuppliers();
			$this->data['product_categories'] 	= array_column($this->product_model->getProductCategories($productId), 'id_category');
			$this->data['search_suggestions'] 	= json_encode($this->admin_model->getAllProductsForSimilarSeacrh());
			$this->data['attribute_suggestions']= json_encode(array_column($this->admin_model->getAllProductsAttributes(), 'title'));
			
			if ($this->config->item('product_similar')) {
				$this->data['productSimilar'] 		= $this->product_model->getProductSimilar($productId);
			}
			if ($this->config->item('product_attributes')) {
				$this->data['productAttributes']    = $this->admin_model->getProductAttributes($productId);
			}
			if ($this->config->item('product_variable')) {
				$this->data['productVariables']     = $this->product_model->getProductVariables($productId);
			}
		
			$this->load->view('admin/header', $this->data);
			$this->load->view('admin/products/product_form' , $this->data);

		} else {
			redirect('admin/products');
		}
	}

	public function remove($productId) {
		if ($this->admin_product_model->deleteProduct($productId)) {
			$this->session->set_flashdata('success', 'Produkt bol odstránený.');
		} else {
			$this->session->set_flashdata('error', 'Vyskytla sa chyba. Skúste to prosím znovu.');	
		}
	
		redirect($_SERVER['HTTP_REFERER']);
	}
	
	public function hide($productId) {
		if ($this->admin_product_model->updateProductVisibility($productId, 0)) {
			$this->session->set_flashdata('success', 'Produkt teraz nie je viditeľný.');
		} else {
			$this->session->set_flashdata('error', 'Vyskytla sa chyba. Skúste to prosím znovu.');	
		}
		redirect($_SERVER['HTTP_REFERER']);
	}
	
	public function show($productId) {
		if ($this->admin_product_model->updateProductVisibility($productId, 1)) {
			$this->session->set_flashdata('success', 'Produkt je teraz viditeľný.');
		} else {
			$this->session->set_flashdata('error', 'Vyskytla sa chyba. Skúste to prosím znovu.');	
		}
		redirect($_SERVER['HTTP_REFERER']);
	}
	
	public function massAction() {
		if ($_SERVER['REQUEST_METHOD'] === 'POST') {
			$productIdsArr  = $this->input->post('productIdsArr');
			$massAction		= $this->input->post('massAction');
			
			switch($massAction) {
				case 'show':
					$this->showMultiple($productIdsArr);
					break;
				case 'hide':
					$this->hideMultiple($productIdsArr);
					break;
				case 'delete':
					$this->removeMultiple($productIdsArr);
					break;
			}
		} else {
			redirect('');
		}
	}
	
	private function removeMultiple($productIdsArr) {		
		if ($this->admin_product_model->deleteMultipleProducts($productIdsArr)) {
			$this->session->set_flashdata('success', 'Produkty boli odstránené.');
		} else {
			$this->session->set_flashdata('error', 'Vyskytla sa chyba. Skúste to prosím znovu.');	
		}
	}
	
	private function hideMultiple($productIdsArr) {
		if ($this->admin_product_model->updateMultipleProductVisibility($productIdsArr, 0)) {
			$this->session->set_flashdata('success', 'Produkty nie sú teraz viditeľné.');
		} else {
			$this->session->set_flashdata('error', 'Vyskytla sa chyba. Skúste to prosím znovu.');	
		}
	}
	
	private function showMultiple($productIdsArr) {
		if ($this->admin_product_model->updateMultipleProductVisibility($productIdsArr, 1)) {
			$this->session->set_flashdata('success', 'Produkty sú teraz viditeľné.');
		} else {
			$this->session->set_flashdata('error', 'Vyskytla sa chyba. Skúste to prosím znovu.');	
		}
	}
	
	public function addVariableProduct($productId = null) {
		if ($_SERVER['REQUEST_METHOD'] === 'POST') {
			if (($this->admin_product_model->productExist($productId)) || ($productId == null)) {
				$this->form_validation->set_rules('variableProductName',   			'Názov', 		'trim|required');
				$this->form_validation->set_rules('variableProductShortDescription','Krátky popis', 'trim');
				$this->form_validation->set_rules('variableProductPrice',   		'Cena', 		'trim|numeric');
				$this->form_validation->set_rules('variableProductDiscount',   		'Zľava', 		'trim|numeric|greater_than_equal_to[0]|less_than_equal_to[100]');
				$this->form_validation->set_rules('variableProductQuantity', 	  	'Počet kusov', 	'trim|is_natural');
					
				if ($this->form_validation->run()) {
					$productLink 			= strtolower(utf8_encode(convert_accented_characters(url_title(strip_tags($this->input->post('variableProductName'))), "dash", true)));
					$sameProductNameCounter = $this->admin_product_model->getSameVariableProductNameCount($productLink);
					if ($sameProductNameCounter > 0) {
						$productLink .= '-' . ($sameProductNameCounter + 1);
					}
					
					$variableProductData = array(
						'id_product' 		=> $productId,
						'name' 				=> strip_tags($this->input->post('variableProductName')),
						'link' 				=> $productLink,
						'short_description'	=> strip_tags($this->input->post('variableProductShortDescription')),
						'price' 			=> strip_tags($this->input->post('variableProductPrice')),
						'discount' 			=> strip_tags($this->input->post('variableProductDiscount')),
						'quantity'	 		=> strip_tags($this->input->post('variableProductQuantity')),
						'show_product'	 	=> 0,
					);
					
					$varibleProductPhotos = $this->input->post('variableProductPhotos');
							
					if ($variableProductData['id'] = $this->admin_product_model->insertVariableProductData($variableProductData, $varibleProductPhotos)) {
						$this->data['variableProduct'] = $variableProductData;

						echo json_encode(['ok', $this->load->view('admin/products/partials/_product_variable_item', $this->data, true)]);
					} else {
						echo json_encode(['err']);
					}
				} else {
					echo json_encode(['err']);
				}
			} else {
				echo json_encode(['err']);
			}
		} else {
			redirect('');
		}
	}
	
	public function updateVariableProduct() {
		if ($_SERVER['REQUEST_METHOD'] === 'POST') {
			$this->form_validation->set_rules('variableProductName',   			'Názov', 		'trim|required');
			$this->form_validation->set_rules('variableProductShortDescription','Krátky popis', 'trim');
			$this->form_validation->set_rules('variableProductPrice',   		'Cena', 		'trim|numeric');
			$this->form_validation->set_rules('variableProductDiscount',   		'Zľava', 		'trim|numeric|greater_than_equal_to[0]|less_than_equal_to[100]');
			$this->form_validation->set_rules('variableProductQuantity', 	  	'Počet kusov', 	'trim|is_natural');
				
			if ($this->form_validation->run()) {
				$variableProductId 	 = $this->input->post('variableProductId');
				$variableProductData = array(
					'name' 				=> strip_tags($this->input->post('variableProductName')),
					'link' 				=> strtolower(utf8_encode(convert_accented_characters(url_title(strip_tags($this->input->post('variableProductName'))), "dash", true))),
					'short_description'	=> strip_tags($this->input->post('variableProductShortDescription')),
					'price' 			=> strip_tags($this->input->post('variableProductPrice')),
					'discount' 			=> strip_tags($this->input->post('variableProductDiscount')),
					'quantity'	 		=> strip_tags($this->input->post('variableProductQuantity')),
				);
				
				$varibleProductPhotos = $this->input->post('variableProductPhotos');
						
				if ($this->admin_product_model->updateVariableProductData($variableProductId, $variableProductData, $varibleProductPhotos)) {
					$variableProductData['price'] 		= cutePrice($variableProductData['price']);
					$variableProductData['discount']   .= '&nbsp;%';

					echo json_encode(['ok', $variableProductId, $variableProductData]);
				} else {
					echo json_encode(['err']);
				}
			} else {
				echo json_encode(['err']);
			}
		} else {
			redirect('');
		}
	}
	
	public function removeVariableProduct() {
		if ($_SERVER['REQUEST_METHOD'] === 'POST') {
			$variableProductId = $this->input->post('variableProductId');
			if ($this->admin_product_model->variableProductExist($variableProductId)) {
				if ($this->admin_product_model->deleteVariableProduct($variableProductId)) {
					echo json_encode(['ok', $variableProductId]);
				} else {
					echo json_encode(['err']);
				}
			} else {
				echo json_encode(['err']);
			}
		} else {
			redirect('');
		}
	}
	
	public function editVariableProduct() {
		if ($_SERVER['REQUEST_METHOD'] === 'POST') {
			$variableProductId = $this->input->post('variableProductId');
			if ($variableProductData = $this->admin_product_model->getVariableProduct($variableProductId)) {
				$this->data['variableProductPhotos'] = $this->admin_product_model->getVariableProductPhotos($variableProductId);
				echo json_encode(['ok', $variableProductData, $this->load->view('admin/products/partials/_product_variable_photos', $this->data, true)]);
			} else {
				echo json_encode(['err']);
			}
		} else {
			redirect('');
		}
	}
	
	public function toggleVariableProduct() {
		if ($_SERVER['REQUEST_METHOD'] === 'POST') {
			$variableProductId = $this->input->post('variableProductId');
			if ($this->admin_product_model->variableProductExist($variableProductId)) {
				if ($this->admin_product_model->toggleVariableProduct($variableProductId)) {
					echo json_encode(['ok', $variableProductId]);
				} else {
					echo json_encode(['err']);
				}
			} else {
				echo json_encode(['err']);
			}
		} else {
			redirect('');
		}
	}
	
	public function uploadPhoto() {
		if ($_SERVER['REQUEST_METHOD'] === 'POST') {
			if (!file_exists ('uploads')) {
				mkdir('uploads', 0777, true);
			}
			if (!file_exists ('uploads/products')) {
				mkdir('uploads/products', 0777, true);
			}
			if (!file_exists ('uploads/products/thumbs')) {
				mkdir('uploads/products/thumbs', 0777, true);
			}
	 
			if (isset($_FILES['file']['name'])) {
				if ($_FILES['file']['error'] == 0) {
					$config['upload_path'] 		= 'uploads/products';
					$config['allowed_types'] 	= 'gif|jpg|png';
					$config['max_filename'] 	= '255';
					$config['encrypt_name'] 	= TRUE;
					$config['max_size'] 		= '10024'; //10 MB
					
					$this->load->library('upload', $config);
			
					if ($this->upload->do_upload('file')) {
						$upload_data = $this->upload->data();
						$this->load->library('image_lib');

						$config_watermark['source_image'] 		= 'uploads/products/' . $upload_data['file_name'];
						$config_watermark['wm_text'] 			= 'OnlimaShop';
						$config_watermark['wm_type'] 			= 'text';
						$config_watermark['wm_font_path'] 		= base_url() . 'system/fonts/texb.ttf';
						$config_watermark['wm_font_size'] 		= '40';
						$config_watermark['wm_font_color'] 		= 'ffffff';
						$config_watermark['wm_shadow_color'] 	= '000000';
						$config_watermark['wm_shadow_distance'] = 1;
					
						$this->image_lib->initialize($config_watermark);
						$this->image_lib->watermark();
						
						$config_resize['image_library'] 	= 'gd2';
						$config_resize['source_image'] 		= 'uploads/products/' . $upload_data['file_name'];
						$config_resize['create_thumb'] 		= false;
						$config_resize['maintain_ratio'] 	= true;
						$config_resize['quality']		 	= '80%';
						$config_resize['width']		 		= 300;		
						$config_resize['new_image']		 	= 'uploads/products/thumbs/' . $upload_data['file_name'];

						$this->image_lib->initialize($config_resize);
						$this->image_lib->resize();

						if ($insertedPhotoId = $this->addProductBlindPhoto($upload_data['file_name'])) {
							$this->data['productPhoto'] = [
								'file_name' => $upload_data['file_name'],
								'id' 		=> $insertedPhotoId,
								'title' 	=> '',
								'alt' 		=> '',
							];
							echo json_encode(['ok', $this->load->view('admin/products/partials/_product_upload_photo_item', $this->data, true)]);
						} else {
							echo json_encode(['err']);
						}
					} else {
						echo json_encode(['err']);
					}
				} else {
					echo json_encode(['err']);
				}
			} else {
				echo json_encode(['err']);
			}
		} else {
			redirect('');
		}
    }
	
	private function addProductBlindPhoto($fileName) {
		return $this->admin_model->insertProductBlindPhoto($fileName);
	}
	
	public function removePhoto() {
		if ($imgId = $this->input->post('imgId')) {
			if ($this->admin_product_model->deletePhoto($imgId)) {
				echo $imgId;
			}
		}
	}
	
	public function uploadVariablePhoto() {
		if ($_SERVER['REQUEST_METHOD'] === 'POST') {
			if (!file_exists ('uploads')) {
				mkdir('uploads', 0777, true);
			}
			if (!file_exists ('uploads/product_variables')) {
				mkdir('uploads/product_variables', 0777, true);
			}
			if (!file_exists ('uploads/product_variables/thumbs')) {
				mkdir('uploads/product_variables/thumbs', 0777, true);
			}
			
			$config['upload_path'] 		= 'uploads/product_variables';
			$config['allowed_types'] 	= 'gif|jpg|png';
			$config['max_filename'] 	= '255';
			$config['encrypt_name'] 	= TRUE;
			$config['max_size'] 		= '10024'; //10 MB
	 
			if (isset($_FILES['file']['name'])) {
				if (0 < $_FILES['file']['error']) {
					echo json_encode(['err']);
				} else {           
					$this->load->library('upload', $config);
					if ($this->upload->do_upload('file')) {
						$upload_data = $this->upload->data();
						$this->load->library('image_lib');

						$config_watermark['source_image'] 		= 'uploads/product_variables/' . $upload_data['file_name'];
						$config_watermark['wm_text'] 			= 'OnlimaShop';
						$config_watermark['wm_type'] 			= 'text';
						$config_watermark['wm_font_path'] 		= './system/fonts/texb.ttf';
						$config_watermark['wm_font_size'] 		= '40';
						$config_watermark['wm_font_color'] 		= 'ffffff';
						$config_watermark['wm_shadow_color'] 	= '000000';
						$config_watermark['wm_shadow_distance'] = 1;
						$config['wm_vrt_alignment'] 			= 'bottom';
						$config['wm_hor_alignment'] 			= 'center';
						$config['wm_padding'] 					= '20';
					
						$this->image_lib->initialize($config_watermark);
						$this->image_lib->watermark();
						
						$config_resize['image_library'] 	= 'gd2';
						$config_resize['source_image'] 		= 'uploads/product_variables/' . $upload_data['file_name'];
						$config_resize['create_thumb'] 		= false;
						$config_resize['maintain_ratio'] 	= true;
						$config_resize['quality']		 	= '80%';
						$config_resize['width']		 		= 300;		
						$config_resize['new_image']		 	= 'uploads/product_variables/thumbs/' . $upload_data['file_name'];

						$this->image_lib->initialize($config_resize);
						$this->image_lib->resize();

						if ($insertedPhotoId = $this->addVariableProductBlindPhoto($upload_data['file_name'])) {
							$this->data['variableProductPhoto'] = [
								'file_name' => $upload_data['file_name'],
								'id' 		=> $insertedPhotoId,
								'title' 	=> '',
								'alt' 		=> '',
							];
							echo json_encode(['ok', $this->load->view('admin/products/partials/_product_variable_upload_photo_item', $this->data, true)]);
						} else {
							echo json_encode(['err']);
						}
					} else {
						echo json_encode(['err']);
					}
				}
			} else {
				echo json_encode(['err']);
			}
		} else {
			redirect('');
		}
    }
	
	private function addVariableProductBlindPhoto($fileName) {
		return $this->admin_product_model->insertVariableProductBlindPhoto($fileName);
	}
	
	public function removeVariablePhoto() {
		if ($id = $this->input->post('id')) {
			if ($this->admin_product_model->deleteVariablePhoto($id)) {
				echo $id;
			}
		}
	}
	
	public function addProductAttributeHTML($attributeRowNumber) {
		$this->data['rowNumber'] = $attributeRowNumber;
		
		echo $this->load->view('admin/products/_product_attribute_item', $this->data, true);
	}
	
	public function getSearchSuggestionsSimilarProducts() {
		$search_expression = $this->input->post('search_expression');
		
		echo json_encode($this->admin_model->getProductsForSimilarSeacrh($search_expression));
	}

	private function addNewProduct() {
		$this->form_validation->set_rules('productName',   			'Názov produktu', 		'trim|required');
        $this->form_validation->set_rules('productShortDescription','Krátky popis',   		'trim');
        $this->form_validation->set_rules('productPrice',   		'Cena', 		  		'trim|numeric');
		$this->form_validation->set_rules('productDiscount',   		'Zľava', 		  		'trim|numeric|greater_than_equal_to[0]|less_than_equal_to[100]');
		$this->form_validation->set_rules('productTax',   			'DPH', 		  	  		'trim|numeric|greater_than_equal_to[0]|less_than_equal_to[100]');
        $this->form_validation->set_rules('productQuantity', 	  	'Počet kusov', 	  		'trim|is_natural');
		$this->form_validation->set_rules('productHeight',   		'Výška', 		  		'trim|numeric|greater_than_equal_to[0]');
		$this->form_validation->set_rules('productWidth',   		'Šírka', 		  		'trim|numeric|greater_than_equal_to[0]');
		$this->form_validation->set_rules('productDepth',   		'Hĺbka', 		  		'trim|numeric|greater_than_equal_to[0]');
		$this->form_validation->set_rules('productWeight',   		'Hmotnosť', 	  		'trim|numeric|greater_than_equal_to[0]');
		$this->form_validation->set_rules('recomendationRatio', 	'Miera odporúčanosti', 	'trim|numeric|greater_than_equal_to[0]|less_than_equal_to[100]');
			
		if ($this->form_validation->run()) {		
			$productData = array(
				'name' 				=> strip_tags($this->input->post('productName')),
				'supplier_id' 		=> strip_tags($this->input->post('supplier')),
				'short_description'	=> strip_tags($this->input->post('productShortDescription')),
				'long_description' 	=> $this->input->post('productLongDescription'),
				'main_photo_id' 	=> ($this->input->post('productMainPhoto') ? strip_tags($this->input->post('productMainPhoto')) : (isset($productPhotos[0]) ? $productPhotos[0] : null)),
				'price' 			=> strip_tags($this->input->post('productPrice')),
				'discount' 			=> strip_tags($this->input->post('productDiscount')),
				'tax' 				=> strip_tags($this->input->post('productTax')),
				'quantity'	 		=> strip_tags($this->input->post('productQuantity')),
				'height'	 		=> strip_tags($this->input->post('productHeight')),
				'width'	 			=> strip_tags($this->input->post('productWidth')),
				'depth'	 			=> strip_tags($this->input->post('productDepth')),
				'weight'	 		=> strip_tags($this->input->post('productWeight')),
				'origin' 			=> strip_tags($this->input->post('origin')),
				'variable_product' 	=> strip_tags($this->input->post('productVariable')),
				'recomended'	 	=> strip_tags($this->input->post('recomendationRatio')),
				'link' 				=> strtolower(utf8_encode(convert_accented_characters(url_title(strip_tags($this->input->post('productName'))), "dash", true))),
				'tag' 				=> $this->session->userdata('id'),
			);
			
			$productCategory 	= $this->input->post('productCategory');	
			$productPhotos	 	= $this->input->post('productPhoto');
			$productAttributes 	= $this->input->post('productAttributes');
			$productSimilar		= $this->input->post('productSimilar');
			$productVariables	= $this->input->post('productVariables');
					
			if ($this->admin_product_model->insertProductData($productData, $productCategory, $productPhotos, $productAttributes, $productSimilar, $productVariables)) {
				$this->session->set_flashdata('success', 'Produkt bol pridaný!');
				
				redirect('/admin/products');
			} else {
				$this->session->set_flashdata('error', 'Produkt nebol pridaný. Skúste to prosím znovu.');
				
				redirect('/admin/products/add');
			}
		} else {
			$this->session->set_flashdata('error', validation_errors('<span>', '</span>'));
		}
	}
	
	private function editProductInfo($productId) {
		$this->form_validation->set_rules('productName',   			'Názov produktu', 		'trim|required');
        $this->form_validation->set_rules('productShortDescription','Krátky popis',   		'trim');
        $this->form_validation->set_rules('productPrice',   		'Cena', 		  		'trim|numeric');
        $this->form_validation->set_rules('productDiscount',   		'Zľava', 		  		'trim|numeric|greater_than_equal_to[0]|less_than_equal_to[100]');
		$this->form_validation->set_rules('productTax',   			'DPH', 		  	  		'trim|numeric|greater_than_equal_to[0]|less_than_equal_to[100]');
        $this->form_validation->set_rules('productQuantity', 	  	'Počet kusov', 	  		'trim|is_natural');
		$this->form_validation->set_rules('productHeight',   		'Výška', 		  		'trim|numeric|greater_than_equal_to[0]');
		$this->form_validation->set_rules('productWidth',   		'Šírka', 		  		'trim|numeric|greater_than_equal_to[0]');
		$this->form_validation->set_rules('productDepth',   		'Hĺbka', 		  		'trim|numeric|greater_than_equal_to[0]');
		$this->form_validation->set_rules('productWeight',   		'Hmotnosť', 	  		'trim|numeric|greater_than_equal_to[0]');
        $this->form_validation->set_rules('recomendationRatio', 	'Miera odporúčanosti', 	'trim|numeric|greater_than_equal_to[0]|less_than_equal_to[100]');
			
		if ($this->form_validation->run()) {
			$productData = array(
				'name' 				=> strip_tags($this->input->post('productName')),
				'supplier_id' 		=> strip_tags($this->input->post('supplier')),
				'short_description'	=> strip_tags($this->input->post('productShortDescription')),
				'long_description' 	=> $this->input->post('productLongDescription'),
				'main_photo_id' 	=> strip_tags($this->input->post('productMainPhoto')),
				'price' 			=> strip_tags($this->input->post('productPrice')),
				'discount' 			=> strip_tags($this->input->post('productDiscount')),
				'tax' 				=> strip_tags($this->input->post('productTax')),
				'quantity'	 		=> strip_tags($this->input->post('productQuantity')),
				'height'	 		=> strip_tags($this->input->post('productHeight')),
				'width'	 			=> strip_tags($this->input->post('productWidth')),
				'depth'	 			=> strip_tags($this->input->post('productDepth')),
				'weight'	 		=> strip_tags($this->input->post('productWeight')),
				'origin' 			=> strip_tags($this->input->post('origin')),
				'variable_product' 	=> strip_tags($this->input->post('productVariable')),
				'recomended'	 	=> strip_tags($this->input->post('recomendationRatio')),
				'link' 				=> strtolower(utf8_encode(convert_accented_characters(url_title(strip_tags($this->input->post('productName'))), "dash", true))),
				'tag' 				=> $this->session->userdata('id'),
			);
			
			$productCategory 	= $this->input->post('productCategory');
			$productPhotos	 	= $this->input->post('productPhoto');
			$productAttributes 	= $this->input->post('productAttributes');
			$productSimilar		= $this->input->post('productSimilar');

			if ($this->admin_product_model->updateProductData($productId, $productData, $productCategory, $productPhotos, $productAttributes, $productSimilar)) {
				$this->session->set_flashdata('success', 'Produkt bol upravený!');
				
				redirect('/admin/products');
			} else {
				$this->session->set_flashdata('error', 'Produkt nebol upravený. Skúste to prosím znovu.');
				
				redirect('/admin/products/edit/' . $productId);
			}
		} else {
			$this->session->set_flashdata('error', validation_errors('<span>', '</span>'));
		}
	}

	private function parseFilter() {
		$filterArr 						= [];
		$filterArr['product.deleted'] 	= 0;
		
		if ($this->input->get('keyWordFilter')) {
			$filterArr['product.name LIKE'] 	= '%' . $this->input->get('keyWordFilter') . '%';
		}
		
		if ($this->input->get('stockFilter')) {
			switch ($this->input->get('stockFilter')) {
				case 'all':	
					break;
				case 'instock':
					$filterArr['product.quantity >'] 	= 0;
					break;
				case 'outofstock':
					$filterArr['product.quantity'] 		= 0;
					break;
				case 'last3':
					$filterArr['product.quantity <='] 	= 3;
					break;
			}
		}
		
		if ($this->input->get('discountFilter')) {
			switch ($this->input->get('discountFilter')) {
				case 'all':	
					break;
				case 'discount':
					$filterArr['product.discount >'] 	= 0;
					break;
				case 'notdiscount':
					$filterArr['product.discount'] 		= 0;
					break;
			}
		}
		
		return $filterArr;
	}
}
