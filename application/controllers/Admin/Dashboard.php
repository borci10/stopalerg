<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends MY_Controller {
	
	function __construct() {
		parent::__construct();

		$this->load->model('Admin/dashboard_model');

		$this->data['breadcrumbsTitle']	= 'Dashboard';
		$this->data['searchEnabled']	= true;
	}

	public function index() {
		$this->data['pageHeaderTitle']		= 'Prehľad';
		$this->data['page_title'] 			= 'Prehľad' . $this->config->item('title_delimiter') . $this->config->item('eshop_name');
		$this->data['subpages']   			= $this->admin_model->getAllSubpageNames();
		$this->data['orderCount']			= $this->dashboard_model->getNumberOfOrders();
		$this->data['orderTotalPrice']		= $this->dashboard_model->getValueOfOrders();
		$this->data['customersCount']		= $this->dashboard_model->getNumberOfRegisteredCustomers();
		$this->data['activeCartsCount']		= $this->dashboard_model->getNumberOfActiveCarts();
		$this->data['orderCountChart']		= $this->prepareChartData($this->dashboard_model->getOrderCountChart());
		$this->data['orderSumCountChart']	= $this->prepareChartData($this->dashboard_model->getOrderSumCountChart());

	
		$this->load->view('admin/header', $this->data);
		$this->load->view('admin/dashboard/dashboard');
	}
	
	private function prepareChartData($orderArr) {
		$output = [
			1 => 0,
			2 => 0,
			3 => 0,
			4 => 0,
			5 => 0,
			6 => 0,
			7 => 0,
			8 => 0,
			9 => 0,
			10=> 0,
			11=> 0,
			12=> 0
		];
		
		foreach($orderArr as $orderItem) {
			$output[$orderItem['month']] = $orderItem['count'];
		}
		
		return $output;
	}
}
