<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Photogallery extends MY_Controller {
	
	function __construct() {
		parent::__construct();

		$this->load->model('photogallery_model');

		$this->data['breadcrumbsTitle']	= 'Galéria';
	}

	public function index() {
		$this->data['page_title'] 	= 'Správa fotogalérie' . $this->config->item('title_delimiter') . $this->config->item('eshop_name');
		$this->data['albums']		= $this->photogallery_model->getAllAlbums();

		$this->load->view('admin/header', $this->data);
		$this->load->view('admin/photogallery/album_list', $this->data);
	}
	
	public function create() {
		if ($this->input->post('formConfirm')) {
			$this->createAlbum();
		}
		
		$this->data['page_title'] 	= 'Nový album' . $this->config->item('title_delimiter') . $this->config->item('eshop_name');
		$this->data['page_header']	= 'Nový album';
		$this->data['albums']		= $this->photogallery_model->getAllAlbums();
		$this->data['button_title']	= 'Vytvoriť album';

		$this->load->view('admin/header', $this->data);
		$this->load->view('admin/photogallery/album_form', $this->data);
	}
	
	public function edit($albumID) {			
		if ($this->input->post('formConfirm')) {
			$this->editAlbum($albumID);
		}
		
		$this->data['page_title'] 	= 'Upraviť album' . $this->config->item('title_delimiter') . $this->config->item('eshop_name');
		$this->data['page_header']	= 'Upraviť album';
		$this->data['album_info']	= $this->photogallery_model->getAlbumById($albumID);
		$this->data['albumPhotos']	= $this->photogallery_model->getPhotosOfAlbum($albumID);
		$this->data['button_title']	= 'Uložiť zmeny';

		$this->load->view('admin/header', $this->data);
		$this->load->view('admin/photogallery/album_form', $this->data);
	}
	
	public function delete($albumID) {
		if ($this->photogallery_model->deleteAlbum($albumID) && $this->deleteAlbumPhotos($albumID)) {
			$this->session->set_flashdata('success', 'Album bol odstránený');
	
			redirect('admin/photogallery');
		} else {
			$this->session->set_flashdata('error', 'Vyskytla sa chyba. Skúste to prosím znovu.');
				
			redirect('admin/photogallery');
		}
	}
	
	public function uploadPhoto() {
		if ($_SERVER['REQUEST_METHOD'] === 'POST') {
			if (!file_exists ('uploads')) {
				mkdir('uploads', 0777, true);
			}
			if (!file_exists ('uploads/photogallery')) {
				mkdir('uploads/photogallery', 0777, true);
			}
			if (!file_exists ('uploads/photogallery/thumbs')) {
				mkdir('uploads/photogallery/thumbs', 0777, true);
			}
			
			$config['upload_path'] 		= 'uploads/photogallery';
			$config['allowed_types'] 	= 'gif|jpg|png';
			$config['max_filename'] 	= '255';
			$config['encrypt_name'] 	= TRUE;
			$config['max_size'] 		= '10024'; //10 MB

			if (isset($_FILES['file']['name'])) {
				if (0 < $_FILES['file']['error']) {
					echo json_encode(['err']);
				} else {
					$this->load->library('upload', $config);
					if ($this->upload->do_upload('file')) {
						$upload_data = $this->upload->data();
						
						$config_resize['image_library'] 	= 'gd2';
						$config_resize['source_image'] 		= 'uploads/photogallery/' . $upload_data['file_name'];
						$config_resize['create_thumb'] 		= false;
						$config_resize['maintain_ratio'] 	= true;
						$config_resize['quality']		 	= '80%';
						$config_resize['width']		 		= 400;		
						$config_resize['new_image']		 	= 'uploads/photogallery/thumbs/' . $upload_data['file_name'];

						$this->load->library('image_lib', $config_resize);

						@$this->image_lib->resize();
						
						if ($photoId = $this->photogallery_model->insertImage($upload_data['file_name'])) {
							$this->data['photo'] = [
								'id'	=> $photoId,
								'path'	=> $upload_data['file_name'],
								'title'	=> '',
								'alt'	=> '',
							];
							
							echo json_encode(['ok', $this->load->view('admin/photogallery/_photo_item', $this->data, true)]);
						} else {
							echo json_encode(['err']);
						}
					} else {
						echo json_encode(['err']);
					}
				}
			} else {
				echo json_encode(['err']);
			}
		} else {
			redirect('');
		}
    }
	
	public function removePhoto() {
		if ($photoId = $this->input->post('imgId')) {
			if ($this->photogallery_model->deletePhoto($photoId)) {
				echo $photoId;
			}
		}
	}
	
	private function deleteAlbumPhotos($albumId) {
		$photos	= $this->photogallery_model->getPhotosOfAlbum($albumId);
		
		foreach($photos as $photo) {
			$this->photogallery_model->deletePhoto($photo['path']);
		}
		
		return true;
	}
	
	private function createAlbum() {	
		$this->form_validation->set_rules('albumName', 'Názov albumu', 'trim|required');
		
		if($this->form_validation->run()) {
			$albumInfo = array(
				'name' 			=> strip_tags($this->input->post('albumName')),
				'description'	=> strip_tags($this->input->post('albumDescription')),
				'date'			=> mysqlDate(strip_tags($this->input->post('albumDate')))
			);
			
			$albumPhotos = $this->input->post('albumPhotoNames');
			$albumTitles = $this->input->post('albumPhotoTitle');
			$albumAlt 	 = $this->input->post('albumPhotoAlt');
			
			if ($this->photogallery_model->insertNewAlbum($albumInfo, $albumPhotos, $albumTitles, $albumAlt)) {
				$this->session->set_flashdata('success', 'Album bol vytvorený');
		
				redirect('admin/photogallery/');
			} else {
				$this->session->set_flashdata('error', 'Vyskytla sa chyba. Skúste to prosím znovu.');
					
				redirect('admin/photogallery/create');
			}
		}
	}
	
	private function editAlbum($albumId) {	
		$this->form_validation->set_rules('albumName', 'Názov albumu', 'trim|required');
		
		if($this->form_validation->run()) {
			$albumInfo = array(
				'name' 			=> strip_tags($this->input->post('albumName')),
				'description'	=> strip_tags($this->input->post('albumDescription')),
				'date'			=> mysqlDate(strip_tags($this->input->post('albumDate')))
			);
			
			$albumPhotos = $this->input->post('albumPhotoNames');
			$albumTitles = $this->input->post('albumPhotoTitle');
			$albumAlt 	 = $this->input->post('albumPhotoAlt');
			
			if ($this->photogallery_model->updateAlbum($albumId, $albumInfo, $albumPhotos, $albumTitles, $albumAlt)) {
				$this->session->set_flashdata('success', 'Album bol upravený');
		
				redirect('admin/photogallery/');
			} else {
				$this->session->set_flashdata('error', 'Vyskytla sa chyba. Skúste to prosím znovu.');
					
				redirect('admin/photogallery/edit/' . $albumId);
			}
		}
	}
}