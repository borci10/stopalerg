<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Orders extends MY_Controller {
	
	function __construct() {
		parent::__construct();

		$this->load->model('profile_model');
		$this->load->model('Admin/settings_model');
		$this->load->model('Admin/order_model');
		$this->load->model('email_model');
		$this->load->model('ikros_model');

		$this->data['breadcrumbsTitle']	= 'Objednávky';
		$this->data['searchEnabled']	= true;
	}
	
	public function index($urlSegment = null) {
		$filter = $this->parseFilter();
		
		$this->data['breadcrumbsTitle']		= 'Objednávky';
		$this->data['page_title'] 			= 'Objednávky' . $this->config->item('title_delimiter') . $this->config->item('eshop_name');
		$this->data['orders']  	  			= $this->order_model->getOrdersByFilter((($this->input->get('page') ?? 1) - 1) * $this->config->item('sql_result_limit'), $filter);
		$this->data['subpages']   			= $this->admin_model->getAllSubpageNames();
		$this->data['totalCount'] 			= $this->order_model->getTotalCountOfOrders($filter);
		$this->data['exportButtonAdress']	= 'export/exportOrdersXlsx/' . $urlSegment;
		$this->data['exportButtonCaption']	= 'Exportovať objednávky';
	
		$this->load->view('admin/header', $this->data);
		$this->load->view('admin/orders/order_list_header', $this->data);
		$this->load->view('admin/orders/order_list', $this->data);

	}

	public function orderDetail($orderId) {
		if ($this->data['order_info']		= $this->order_model->getOrderDetails($orderId)) {
			if ($this->input->post('trackingNumberConfirm')) {
				$this->updateTrackingNumber($orderId);
			}
			$this->data['page_title'] 		= 'Detail objednávky' . $this->config->item('title_delimiter') . $this->config->item('eshop_name');
			$this->data['subpages']   		= $this->admin_model->getAllSubpageNames();
			$this->data['order_items']		= $this->profile_model->getOrderDetails($orderId);
			$this->data['breadcrumbsTitle']	= 'Detail objednávky #' . generateVarSymbol($orderId);
			
			$this->load->view('admin/header', $this->data);
			$this->load->view('admin/orders/order_detail', $this->data);

		} else {
			redirect('admin/orders/created');
		}
	}
	
	public function invoice($orderId) {
		if ($this->data['order_info']		= $this->order_model->getOrderDetails($orderId)) {
			$this->data['order_items']		= $this->order_model->getOrderItems($orderId);
			$this->data['productTax']		= array_column($this->data['order_items'], 'tax');
			$this->data['shopSettings']		= $this->settings_model->getAllSettingsParsed();
			$this->data['documentColor']	= '#90B716';
			$this->data['documentHeader']	= 'Faktúra ' . generateVarSymbol($this->data['order_info']->id);
	
// echo "<pre>";
// print_r($this->data['order_info']);
// echo "</pre>";
// echo "<pre>";
// print_r($this->data['order_items']);
// echo "</pre>";
// echo "<pre>";
// print_r($this->data['shopSettings']);
// echo "</pre>";

			$html = $this->load->view('admin/orders/_invoice/invoice_template', $this->data, true);

			ini_set('memory_limit','128M'); // boost the memory limit if it's low ;)

			$this->load->library('pdf');

			$pdf = $this->pdf->load();
			$pdf->SetTitle('Faktúra - ' . generateVarSymbol($this->data['order_info']->id));
			$pdf->SetHTMLHeader($this->load->view('admin/orders/_invoice/_header', $this->data, true));
			$pdf->SetHTMLFooter($this->load->view('admin/orders/_invoice/_footer', $this->data, true));
			$pdf->AddPage('', // L - landscape, P - portrait 
				'', '', '', '',
				10, // margin_left
				10, // margin right
				30, // margin top
				20, // margin bottom
				10, // margin header
				5); // margin footer
			$pdf->WriteHTML($html);
			$pdf->debug = true;
			$pdf->Output('Faktúra - ' . generateVarSymbol($this->data['order_info']->id) . '.pdf', 'I');
			
			return true;
		} else {
			redirect('admin/orders/created');
		}
	}
	
	public function changeOrderStatus($orderId, $status) {
		if($this->order_model->updateOrderStatus($orderId, $status)) {
			if($order = $this->order_model->getOrderDetails($orderId)) {
				$attachment = null;
				$header = 'Dobrý deň pán/pani ' . $order->surname . ', <br><br>Status vašej objednávky bol zmenený.<br><br>';
				$footer = '<br><br>-----------------------<br><br>
							<strong>RM PHARM S.R.O.</strong><br>
							Vlárska 66<br>
							Bratislava 831 01<br>
							IČO: 47807512<br>
							DIČ: 2024100749<br>
							IČ DPH: SK2024100749<br>
							email: shop@stopalerg.sk';
				switch ($status) {
					case 10:
						$subject = 'Objednávka čakajúca na platbu';
						if($order->delivery == 'Osobne na odbernom mieste') {
							$html = $header . 'Prosíme Vás o úhradu faktúry v dátume splatnosti, t.j. 5 dní od vystavenia faktúry. Vždy uveďte <strong>VARIABILNÝ SYMBOL PODĽA ČÍSLA FAKTÚRY</strong>. O možnosti vyzdvihnutia tovaru na odbernom mieste Vás budeme informovať hneď po prijatí Vašej platby na účet.' . $footer;
						} else {
							$html = $header . 'Prosíme Vás o úhradu faktúry v dátume splatnosti, t.j. 5 dní od vystavenia faktúry. Vždy uveďte <strong>VARIABILNÝ SYMBOL PODĽA ČÍSLA FAKTÚRY</strong>. O expedovaní tovaru Vás budeme informovať hneď po prijatí Vašej platby na účet.' . $footer;
						}
						if ($order->payment == 'Prevodom na účet (max. splatnosť 5 dní)') {
							$attachment = $this->getInvoiceAttachment($orderId);
							$this->ikros_model->sendInvoiceExport($orderId);
						}
						break;
					case 20:
						$subject = 'Platba za vašu objednávku bola prijatá';
						if($order->delivery == 'Osobne na odbernom mieste') {
							$html = $header . 'Vaša platba bola potvrdená. Váš tovar z objednávky <strong>' . generateVarSymbol($order->id) . '</strong> si môžete vyzdvihnúť na adrese: <strong>Lekáreň Santal, Dobrovičova 10, 811 09 Bratislava.</strong>' . $footer;
						} else {
							$html = $header . 'Vaša platba bola potvrdená a objednávka <strong>' . generateVarSymbol($order->id) . '</strong> sa pripravuje na vyexpedovanie. O expedovaní tovaru Vás budeme informovať emailom.' . $footer;
						}
						break;
					case 25:
						$subject = 'Vyexpedovaná objednávka';
						$html = $header;
						$html .= 'Vaša objednávka <strong>' . generateVarSymbol($order->id) . '</strong> bola vyexpedovaná a v najbližších dňoch Vám bude doručená.';
						if($order->tracking_number) {
							$html .= '<br> Podacie číslo Vašej zásielky je: <strong>' . $order->tracking_number . '</strong>';
						}
						$html .= $footer;
						break;
					case 30:
						$subject = 'Vybavená objednávka';
						$html = $header . 'Vaša objednávka <strong>' . generateVarSymbol($order->id) . '</strong> bola vybavená. Ďakujeme za dôveru a prajeme Vám príjemné dni bez alergie.' . $footer;
						if ($order->payment == 'Dobierka') {
							$attachment = $this->getInvoiceAttachment($orderId);
							$this->ikros_model->sendInvoiceExport($orderId);
						}
						break;
					case 40:
						$subject = 'Zrušená objednávka';
						$html = $header . 'Vaša objednávka <strong>' . generateVarSymbol($order->id) . '</strong> bola zrušená.' . $footer;
						break;
					case 50:
						$subject = 'Refundovaná objednávka';
						$html = $header . 'Vaša objednávka <strong>' . generateVarSymbol($order->id) . '</strong> bola refundovaná.' . $footer;
						break;
				}
				$this->email_model->sendEmail($order->email, $subject, $html, $orderId, $attachment);
			}
		}
		return $this->order_model->updateOrderStatus($orderId, $status);
	}
	
	private function updateTrackingNumber($orderId) {
		$this->form_validation->set_rules('tracking_number', 'Číslo zásielky', 'trim|required|max_length[255]');
		
		if($this->form_validation->run()) {
			if ($this->order_model->updateTrackingNumber($orderId, $this->input->post('tracking_number'))) {
				$this->session->set_flashdata('success', 'Číslo zásielky bolo uložené.');
			} else {
				$this->session->set_flashdata('error', 'Nastala chyba. Skúste to prosím znovu.');
			}
		} else {
			$this->session->set_flashdata('error', validation_errors('<span>', '</span>'));
		}
		redirect($_SERVER['HTTP_REFERER']);
	}
	
	private function parseFilter() {
		$filterArr 						= [];
		
		if ($this->input->get('keyWordFilter')) {
			$filterArr['(
				orders.name LIKE "%' . $this->input->get('keyWordFilter') . '%" OR 
				orders.surname LIKE "%' . $this->input->get('keyWordFilter') . '%" OR 
				orders.email LIKE "%' . $this->input->get('keyWordFilter') . '%" OR 
				orders.phone_number LIKE "%' . $this->input->get('keyWordFilter') . '%" OR 
				orders.city LIKE "%' . $this->input->get('keyWordFilter') . '%" OR 
				orders.street LIKE "%' . $this->input->get('keyWordFilter') . '%" OR 
				orders.zip_code LIKE "%' . $this->input->get('keyWordFilter') . '%"
			)'] = null;
		}
		
		if ($this->input->get('dateFromFilter')) {
			$filterArr['orders.order_date >='] = mysqlDate($this->input->get('dateFromFilter'));
		}
		
		if ($this->input->get('dateToFilter')) {
			$filterArr['orders.order_date <='] = mysqlDate($this->input->get('dateToFilter'));
		}
		
		if ($this->input->get('statusFilter') != '') {
			if ($this->input->get('statusFilter') != 'all') {
				$filterArr['orders.status'] = $this->input->get('statusFilter');
			}
		}
		
		return $filterArr;
	}
	
	private function getInvoiceAttachment($orderId) {
		$this->data['order_info']		= $this->order_model->getOrderDetails($orderId);
		$this->data['order_items']		= $this->order_model->getOrderItems($orderId);
		$this->data['productTax']		= array_column($this->data['order_items'], 'tax');
		$this->data['shopSettings']		= $this->settings_model->getAllSettingsParsed();
		$this->data['documentColor']	= '#1DA680';
		$this->data['documentHeader']	= 'Faktúra ' . generateVarSymbol($this->data['order_info']->id);

		$html = $this->load->view('admin/orders/_invoice/invoice_template', $this->data, true);

		ini_set('memory_limit','128M'); // boost the memory limit if it's low ;)

		$this->load->library('pdf');

		$pdf = $this->pdf->load();
		$pdf->SetTitle('Faktúra - ' . generateVarSymbol($this->data['order_info']->id));
		$pdf->SetHTMLHeader($this->load->view('admin/orders/_invoice/_header', $this->data, true));
		$pdf->SetHTMLFooter($this->load->view('admin/orders/_invoice/_footer', $this->data, true));
		$pdf->AddPage('', // L - landscape, P - portrait 
			'', '', '', '',
			10, // margin_left
			10, // margin right
			30, // margin top
			20, // margin bottom
			10, // margin header
			5); // margin footer
		$pdf->WriteHTML($html);

		return $pdf->Output('', 'S');
	}
}