<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Payment extends MY_Controller {
	
	function __construct() {
		parent::__construct();

		$this->load->model('Admin/payment_model');

		$this->data['breadcrumbsTitle']	= 'Možnosti platby';
		$this->data['searchEnabled']	= true;
	}
	
	public function index() {
		$this->data['page_title'] 		= 'Možnosti platby' . $this->config->item('title_delimiter') . $this->config->item('eshop_name');
		$this->data['pageHeaderTitle']	= 'Možnosti platby';
		$this->data['payments']  		= $this->payment_model->getPaymentList();
	
		$this->load->view('admin/header', $this->data);
		$this->load->view('admin/payment/payment_list', $this->data);

	}
	
	public function edit($paymentId) {
		if ($this->data['payment'] = $this->payment_model->getPayment($paymentId)) {
			if (($this->data['payment']->uid == 'ttp')) {
				if (!$this->config->item('trust_pay')) {
					$this->session->set_flashdata('error', 'Prístup zamietnutý.');
					redirect('admin/payment');
				}
			}
			if($this->input->post('formConfirm')){
				$this->editPayment($paymentId);
			}

			$this->data['page_title'] 		= 'Upraviť možnosti platby' . $this->config->item('title_delimiter') . $this->config->item('eshop_name');
			$this->data['pageHeaderTitle']	= 'Upraviť možnosti platby';	
		
			$this->load->view('admin/header', $this->data);
			$this->load->view('admin/payment/payment_cod_form', $this->data);

		} else {
			redirect('admin/payment');
		}
	}

	private function editPayment($paymentId) {
		$this->form_validation->set_rules('price', 'Cena', 'trim|required|numeric|greater_than_equal_to[0]');
		
		if ($this->form_validation->run()) {
			$paymentOption = array(
				'price' => strip_tags($this->input->post('price'))
			); 
			
			if ($this->payment_model->updatePayment($paymentId, $paymentOption)) {
				$this->session->set_flashdata('success', 'Možnosť platby bola upravená!');
				
				redirect('/admin/payment');
			} else {
				$this->session->set_flashdata('error', 'Možnosť platby nebola upravená. Skúste to prosím znovu.');
				
				redirect('/admin/payment');
			}
		} else {
			$this->session->set_flashdata('error', validation_errors('<span>', '</span>'));
		}
	}
}
