<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Import extends MY_Controller {
	
	function __construct() {
		parent::__construct();

		$this->load->model('profile_model');
		// $this->load->library('csvreader');

		$this->data['breadcrumbsTitle']	= 'Import';
	}
	
	public function index() {
		$this->data['page_title'] 			= 'Import produktov (BETA)' . $this->config->item('title_delimiter') . $this->config->item('eshop_name');
		$this->data['pageHeaderTitle']		= 'Import produktov (BETA)';
		
		$this->data['suppliers']  			= $this->admin_model->getAllSuppliers();
		$this->data['categories']   		= $this->admin_model->getAllCategories();
		
		$this->load->view('admin/header', $this->data);
		$this->load->view('admin/import/import_menu');

	}
	// public function uploadCSV() {
		// if (!file_exists('uploads/csv')) {
			// mkdir('uploads/csv', 0777, true);
		// }
		// $config['upload_path'] = 'uploads/csv/';
        // $config['allowed_types'] = 'csv';
        // $config['max_filename'] = '255';
        // $config['encrypt_name'] = TRUE;
        // $config['max_size'] = '100024'; //100 MB

		// $realName = $_FILES['file']['name'];
		
        // if (isset($_FILES['file']['name'])) {
            // if (0 < $_FILES['file']['error']) {
                // echo 'Error during file upload' . $_FILES['file']['error'];
            // } else {
				// $this->load->library('upload', $config);
				// if ($this->upload->do_upload('file')) {
					// $upload_data = $this->upload->data();
					// return $upload_data;
				// }
            // }
        // }
	// }
	
	public function parseData() {
		$file	= $this->uploadCSV();
		
		$result = $this->csvreader->parse_file(base_url() . 'uploads/csv/' . $file['file_name']);

		echo "<pre>";
		print_r($result);
		echo "</pre>";
	}
	
	public function importXMLMontessorihracky() {
		$item_counter 	=	 0;
		$tag			= 'monte';

		if ($this->input->post('xmlUrl')) {
			if ($xmlFile = simplexml_load_file($this->input->post('xmlUrl'))) {
				if (isset($xmlFile->SHOPITEM)) {
					foreach($xmlFile->SHOPITEM as $shopItem) {
						$minAge				= 0;
						$maxAge				= 99;
						$itemAttributes 	= [];
						$itemPhotos 		= [];
						$itemCategories 	= [];
						
						$pomItemPhotos = (array)$shopItem->IMGURL;
						foreach($pomItemPhotos as $photoItem) {
							$itemPhotos[] 	= [
								'file_name'	=>  $photoItem,
								'title'		=> '',
								'alt'		=> '',
							];
						}

						for($i = 0; $i < count($shopItem->PARAM); $i++) {
							$paramName 	= (string)$shopItem->PARAM[$i]->PARAM_NAME;
							$paramVal	= (string)$shopItem->PARAM[$i]->VAL;
							
							if (in_array($paramName, ['Věk', 'Vek', 'Štítky'])) {
								if (in_array($paramName, ['Věk', 'Vek'])) {
									if (strpos($paramVal, '-') !== false) {
										list($pomMinAge, $maxAge) = explode('-', $paramVal);
										if ($minAge == 0) {
											$minAge = $pomMinAge;
										}
									}
									if (strpos($paramVal, '+') !== false) {
										$maxAge = 99;
										$pomMinAge = str_replace("+", "", $paramVal);
										if ($minAge == 0) {
											$minAge = $pomMinAge;
										}
									}
								}
								continue;
							}
							
							$itemAttributes[] = [
								'title' => $paramName,
								'value' => $paramVal,
							];
						}
						
						$itemData 			= [
							'uid' 				=> (string)$shopItem->ITEM_ID,
							'ean' 				=> (string)$shopItem->EAN,
							'supplier_id' 		=> $this->input->post('supplier'),
							'name'				=> (string)$shopItem->PRODUCTNAME,
							'price'				=> (string)$shopItem->PRICE_VAT / $this->input->post('currencyRate'),
							'link'				=> strtolower(utf8_encode(convert_accented_characters(url_title(strip_tags($shopItem->PRODUCTNAME)), "dash", true))),
							'short_description'	=> character_limiter((string)$shopItem->DESCRIPTION, 200, '...'),
							'long_description'	=> (string)$shopItem->DESCRIPTION,
							'age_from'			=> $minAge,
							'age_to'			=> $maxAge,
							'tag'				=> $tag,
							'quantity'	 		=> 32767,
						];
			
						if ($this->admin_model->updateOrInsertProductData($itemData, $itemCategories, $itemPhotos, [], [], $this->input->post('category'), $tag)) {
							$item_counter++;
						}
					}
					$this->session->set_flashdata('success', $item_counter . ' produktov bolo importovaných');
				} else {
					$this->session->set_flashdata('error', 'Neplatný formát');
				}
			} else {
				$this->session->set_flashdata('error', 'Neplatný formát');
			}
		}
		redirect('admin/import');
	}
	
	public function importXMLDebra() {
		$item_counter 	= 0;
		$tag			= 'debra';
		
		if ($this->input->post('xmlUrl')) {
			if ($xmlFile = simplexml_load_file($this->input->post('xmlUrl'))) {
				if (isset($xmlFile->product)) {
					foreach($xmlFile->product as $shopItem) {
						$minAge				= 0;
						$maxAge				= 99;
						$itemAttributes 	= [];
						$itemPhotos 		= [];
						
						$itemCategories 	= array_map('trim', explode('/', (string)$shopItem->kategorie));

						if (!empty((string)$shopItem->picture1)) {
							$itemPhotos[] 	= ['file_name'	=>  (string)$shopItem->picture1,'title'	=> '', 'alt' => ''];
						}
						if (!empty((string)$shopItem->picture2)) {
							$itemPhotos[] 	= ['file_name'	=>  (string)$shopItem->picture2,'title'	=> '', 'alt' => ''];
						}
						if (!empty((string)$shopItem->picture3)) {
							$itemPhotos[] 	= ['file_name'	=>  (string)$shopItem->picture3,'title'	=> '', 'alt' => ''];
						}
						if (!empty((string)$shopItem->picture4)) {
							$itemPhotos[] 	= ['file_name'	=>  (string)$shopItem->picture4,'title'	=> '', 'alt' => ''];
						}
						if (!empty((string)$shopItem->picture5)) {
							$itemPhotos[] 	= ['file_name'	=>  (string)$shopItem->picture5,'title'	=> '', 'alt' => ''];
						}
						if (!empty((string)$shopItem->picture6)) {
							$itemPhotos[] 	= ['file_name'	=>  (string)$shopItem->picture6,'title'	=> '', 'alt' => ''];
						}

						$itemData 			= [
							'uid' 				=> (string)$shopItem->product_id,
							'ean' 				=> null,
							'supplier_id' 		=> $this->input->post('supplier'),
							'name'				=> (string)$shopItem->text1,
							'price'				=> (string)$shopItem->price_czk / $this->input->post('currencyRate'),
							'link'				=> strtolower(utf8_encode(convert_accented_characters(url_title(strip_tags($shopItem->text1)), "dash", true))),
							'short_description'	=> character_limiter((string)$shopItem->detail, 200, '...'),
							'long_description'	=> (string)$shopItem->detail,
							'age_from'			=> $minAge,
							'age_to'			=> $maxAge,
							'tag'				=> $tag,
							'quantity'	 		=> 32767,
						];
						if ($this->admin_model->updateOrInsertProductData($itemData, $itemCategories, $itemPhotos, [], [], $this->input->post('category'), $tag)) {
							$item_counter++;
						}
					}
					$this->session->set_flashdata('success', $item_counter . ' produktov bolo importovaných');
				} else {
					$this->session->set_flashdata('error', 'Neplatný formát');
				}
			} else {
				$this->session->set_flashdata('error', 'Neplatný formát');
			}
		}
		redirect('admin/import');
	}
	
	
}