<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Suppliers extends MY_Controller {
	
	function __construct() {
		parent::__construct();

		$this->load->model('cart_model');

		$this->data['breadcrumbsTitle']	= 'Dodávatelia';
		$this->data['searchEnabled']	= true;
	}
	
	public function index() {
		if($this->input->post('formConfirm')){
			$this->addSupplier();
		}

		$this->data['page_title'] 		= 'Dodávatelia' . $this->config->item('title_delimiter') . $this->config->item('eshop_name');
		$this->data['pageHeaderTitle']	= 'Dodávatelia';
		$this->data['suppliers']  		= $this->admin_model->getAllSuppliers();
	
		$this->load->view('admin/header', $this->data);
		$this->load->view('admin/suppliers/supplier_list', $this->data);

	}
	
	public function edit($supplierId) {
		if ($this->data['supplier'] = $this->cart_model->getSupplierDetail($supplierId)) {
			if($this->input->post('formConfirm')){
				$this->editSupplier($supplierId);
			}

			$this->data['page_title'] 		= 'Upraviť dodávateľa' . $this->config->item('title_delimiter') . $this->config->item('eshop_name');
			$this->data['pageHeaderTitle']	= 'Upraviť dodávateľa';	
			$this->data['subpages']   		= $this->admin_model->getAllSubpageNames();
		
			$this->load->view('admin/header', $this->data);
			$this->load->view('admin/suppliers/supplier_form', $this->data);

		} else {
			redirect('admin/suppliers');
		}
	}
	
	public function remove($supplierId) {
		if ($this->admin_model->changeSupplierDeletedStatus($supplierId, 1)) {
			$this->session->set_flashdata('success', 'Dodávateľ bol odstránený');
		} else {
			$this->session->set_flashdata('error', 'Vyskytla sa chyba. Skúste to prosím znovu.');
		}

		redirect($_SERVER['HTTP_REFERER']);
	}
	
	private function addSupplier() {
		$this->form_validation->set_rules('supplierTitle', 'Názov dodávateľa', 	'trim|required');
		$this->form_validation->set_rules('supplierPrice', 'Cena dopravy',   	'trim|required|numeric|greater_than_equal_to[0]');
		
		if ($this->form_validation->run()) {
			$supplier = array(
				'title' => strip_tags($this->input->post('supplierTitle')),
				'price' => strip_tags($this->input->post('supplierPrice')),
			); 
			
			if ($this->admin_model->insertSupplier($supplier)) {
				$this->session->set_flashdata('success', 'Dodávateľ bol pridaný!');
				
				redirect('/admin/suppliers');
			} else {
				$this->session->set_flashdata('error', 'Dodávateľ nebol pridaný. Skúste to prosím znovu.');
				
				redirect('/admin/suppliers');
			}
		} else {
			$this->session->set_flashdata('error', validation_errors('<span>', '</span>'));
		}
	}
	
	private function editSupplier($supplierId) {
		$this->form_validation->set_rules('supplierTitle', 'Názov dodávateľa', 'trim|required');
		$this->form_validation->set_rules('supplierPrice', 'Cena dopravy',   'trim|required|numeric|greater_than_equal_to[0]');
		
		if ($this->form_validation->run()) {
			$supplier = array(
				'title' => strip_tags($this->input->post('supplierTitle')),
				'price' => strip_tags($this->input->post('supplierPrice')),
			); 
			
			if ($this->admin_model->updateSupplier($supplierId, $supplier)) {
				$this->session->set_flashdata('success', 'Dodávateľ bol upravený!');
				
				redirect('/admin/suppliers');
			} else {
				$this->session->set_flashdata('error', 'Dodávateľ nebol upravený. Skúste to prosím znovu.');
				
				redirect('/admin/suppliers');
			}
		} else {
			$this->session->set_flashdata('error', validation_errors('<span>', '</span>'));
		}
	}
}
