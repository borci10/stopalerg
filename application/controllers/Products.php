<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Products extends CI_Controller {
	
	function __construct() {
		parent::__construct();
		
		$this->load->model('product_model');
		$this->load->model('admin_model');
	
		$this->data['productCol'] 		= 4;

		if ($this->input->get('debug')) {
			$this->output->enable_profiler(true);
		}
	}

	public function index() {
		$this->data['filter'] 				= $this->parseFilter();
		$this->data['page_title'] 			= 'Produkty' . $this->config->item('title_delimiter') . $this->config->item('eshop_name');
		$this->data['pageHeaderTitle'] 		= 'Všetky produkty';
		$this->data['products']   			= $this->product_model->getFilteredProducts($this->data['filter'] ?? [], (($this->input->get('page') ?? 1) - 1) * $this->config->item('sql_result_limit'));
		$this->data['categories'] 			= $this->admin_model->getCategoryTree();
		$this->data['subpages']   			= $this->admin_model->getAllSubpageNames();
		$this->data['totalCount'] 			= $this->product_model->getTotalCountOfProducts($this->data['filter']);
		$this->data['colorFilter']			= array_column($this->product_model->getAllAtrributeValue('Farba'), 'value');
		$this->data['materialFilter']		= array_column($this->product_model->getAllAtrributeValue('Materiál'), 'value');
		$this->data['searchEnabled']		= true;
		$this->data['filterEnabled']		= true;

		$this->load->view('header', $this->data);		
		$this->load->view('product/list', $this->data);
		$this->load->view('footer', $this->data);
	}
	
	public function detail($productLink) {
		if ($product = $this->product_model->getProductByLink($productLink)) {
			$this->data['page_title']    		= $product->name . $this->config->item('title_delimiter') . $this->config->item('eshop_name');
			$this->data['pageHeaderTitle'] 		= $product->name;
			$this->data['pageDescription'] 		= $product->short_description;
			$this->data['productInfo']  		= $product;
			$this->data['categories']    		= $this->admin_model->getCategoryTree();
			$this->data['productPhotos'] 		= $this->product_model->getProductPhotosById($product->id);
			$this->data['subpages']      		= $this->admin_model->getAllSubpageNames();
			$this->data['searchEnabled']		= true;
			$this->data['openGraph']			= [
				'og:title'					=> $product->name,
				'og:description'			=> $product->short_description,
				'og:image'					=> cuteImage($this->data['productPhotos'][0]['file_name'] ?? ''),
				'og:url'					=> base_url() . 'products/' . $product->link,
				'og:type'					=> 'product.item',
				'product:price:amount'		=> priceCalc($product->price, 1, $product->discount, $product->tax / 100),
				'product:price:currency'	=> $this->config->item('currency'),
				'product:availability'		=> $product->quantity == 0 ? 'Vypredané' : 'Skladom',
			];

			if ($this->config->item('product_attributes')) {
				$this->data['productAttributes']    = $this->admin_model->getProductAttributes($product->id);
			}
			
			if ($this->config->item('product_reviews')) {
				$this->data['aldready_reviewed'] = $this->product_model->checkIfUserAlreadyDidReview($product->id, $this->session->userdata('id'));
				$this->data['product_reviews']   = $this->product_model->getProductReviews($product->id);
				$this->data['product_rating']    = $product->rating;
			}
			
			if ($this->config->item('product_variable')) {
				$this->data['productVariables']  = $this->product_model->getProductVariables($product->id);
			}
			
			$this->load->view('header', $this->data);	
			$this->load->view('product/detail', $this->data);
		
			if ($this->config->item('product_similar')) {
				$this->data['productCol']		= 3;
				$this->data['similarProducts']  = $this->product_model->getProductSimilarAllInfo($product->id);

				if (count($this->data['similarProducts'] > 4)) {
					$this->data['similarProducts'] = array_merge($this->data['similarProducts'], $this->product_model->getProductRecommanded($product->id, 4 - count($this->data['similarProducts'])));
				}
				
				$this->load->view('product/similar_products', $this->data);
			}
			
			$this->load->view('footer', $this->data);
		} else {
			redirect($_SERVER['HTTP_REFERER']);
		}		
	}
	
	public function variableDetail($productLink, $variableProductLink) {
		if ($product = $this->product_model->getShortProductByLink($productLink)) {
			if ($variableProduct = $this->product_model->getProductVariableByLink($product->id, $variableProductLink)) {		
				$this->data['parentProduct']		= clone $product;
				
				$product->child_id 			= $variableProduct->id;
				$product->price 			= $variableProduct->price;
				$product->discount 			= $variableProduct->discount;
				$product->name 				= $variableProduct->name;
				$product->quantity 			= $variableProduct->quantity;
				$product->short_description = $variableProduct->short_description;
				
				$this->data['page_title']    		= $product->name . $this->config->item('title_delimiter') . $this->config->item('eshop_name');
				$this->data['pageHeaderTitle'] 		= $product->name;
				$this->data['pageDescription'] 		= $product->short_description;
				$this->data['categories']    		= $this->admin_model->getCategoryTree();
				$this->data['variableProductPhotos']= $this->product_model->getProductVariablePhotosById($variableProduct->id);
				$this->data['subpages']      		= $this->admin_model->getAllSubpageNames();
				$this->data['searchEnabled']		= true;
				$this->data['productInfo']  		= $product;
				$this->data['openGraph']			= [
					'og:title'					=> $product->name,
					'og:description'			=> $product->short_description,
					'og:image'					=> base_url() . 'uploads/product_variables/thumbs/' . @$this->data['variableProductPhotos'][0]['file_name'] ?? '',
					'og:url'					=> base_url() . 'products/' . $product->link . '/' . $variableProduct->link,
					'og:type'					=> 'product.item',
					'product:price:amount'		=> priceCalc($product->price, 1, $product->discount, $product->tax / 100),
					'product:price:currency'	=> $this->config->item('currency'),
					'product:availability'		=> $product->quantity == 0 ? 'Vypredané' : 'Skladom',
				];
				
				if ($this->config->item('product_attributes')) {
					$this->data['productAttributes']    = $this->admin_model->getProductAttributes($product->id);
				}
				
				if ($this->config->item('product_reviews')) {
					$this->data['aldready_reviewed'] = $this->product_model->checkIfUserAlreadyDidReview($product->id, $this->session->userdata('id'));
					$this->data['product_reviews']   = $this->product_model->getProductReviews($product->id);
					$this->data['product_rating']    = $product->rating;
				}
				
				if ($this->config->item('product_variable')) {
					$this->data['productVariables']  = $this->product_model->getProductVariables($product->id);
				}
				
				$this->load->view('header', $this->data);	
				$this->load->view('product/detail', $this->data);
			
				if ($this->config->item('product_similar')) {
					$this->data['productCol']		= 3;
					$this->data['similarProducts']  = $this->product_model->getProductSimilarAllInfo($product->id);

					if (count($this->data['similarProducts'] > 4)) {
						$this->data['similarProducts'] = array_merge($this->data['similarProducts'], $this->product_model->getProductRecommanded($product->id, 4 - count($this->data['similarProducts'])));
					}
					
					$this->load->view('product/similar_products', $this->data);
				}

				$this->load->view('footer', $this->data);
			} else {
				redirect($_SERVER['HTTP_REFERER']);
			}	
		} else {
			redirect($_SERVER['HTTP_REFERER']);
		}		
	}
	
	public function news() {	
		$this->data['page_title'] 			= 'Produkty' . $this->config->item('title_delimiter') . $this->config->item('eshop_name');
		$this->data['pageHeaderTitle'] 		= 'Novinky';
		$this->data['products']   			= $this->product_model->getFilteredProducts(array_merge($this->data['filter'] ?? [], ['creation_date >' => date('Y-m-d', strtotime(date('Y-m-d') . '-14 day'))]), (($this->input->get('page') ?? 1) - 1) * $this->config->item('sql_result_limit'));
		$this->data['categories'] 			= $this->admin_model->getCategoryTree();
		$this->data['subpages']   			= $this->admin_model->getAllSubpageNames();
		$this->data['totalCount'] 			= $this->product_model->getTotalCountOfProducts($this->data['filter'])->totalCount;
		$this->data['colorFilter']			= array_column($this->product_model->getAllAtrributeValue('Farba'), 'value');
		$this->data['materialFilter']		= array_column($this->product_model->getAllAtrributeValue('Materiál'), 'value');
		$this->data['searchEnabled']		= true;
		$this->data['filterEnabled']		= true;

		$this->load->view('header', $this->data);		
		$this->load->view('product/category_menu', $this->data);	
		$this->load->view('product/list', $this->data);
		$this->load->view('footer', $this->data);
	}
	
	public function indexPage() {
		$page 			= $this->input->post('page');
		$categoryLink 	= $this->input->post('category');

		if (is_numeric($page)) {
			if ($page >= 0) {
				if ($categoryLink == '-1') {
					$this->data['products']	= $this->product_model->getFilteredProducts($this->data['filter'] ?? [], $page * $this->config->item('sql_result_limit'));
				} else {
					$category = $this->product_model->getCategoryByLink($categoryLink);
					$this->data['products'] = $this->product_model->getFilteredProductByCaregoryId($category->category_id, $this->data['filter'] ?? [], $page * $this->config->item('sql_result_limit'));
				}

				if (!empty($this->data['products'])) {
					echo $this->load->view('product/items', $this->data, true);
				} else {
					echo false;
				}
			} else {
				echo false;
			}
		} else {
			echo false;
		}
	}
	
	public function favourite() {
		$this->data['page_title'] 			= 'Obľúbené produkty' . $this->config->item('title_delimiter') . $this->config->item('eshop_name');
		$this->data['pageHeaderTitle'] 		= 'Obľúbené produkty';
		$this->data['products']   			= $this->product_model->getProductsFromIdList($this->session->userdata('favourite_products'));
		$this->data['categories'] 			= $this->admin_model->getCategoryTree();
		$this->data['subpages']   			= $this->admin_model->getAllSubpageNames();		
		$this->data['searchEnabled']		= true;
		$this->data['listCol']				= 12;

		$this->load->view('header', $this->data);		
		$this->load->view('product/list', $this->data);
		$this->load->view('footer', $this->data);
	}
	
	public function compare() {	
		$this->data['page_title'] 			= 'Porovnanie produktov' . $this->config->item('title_delimiter') . $this->config->item('eshop_name');
		$this->data['pageHeaderTitle'] 		= 'Porovnanie produktov';
		$this->data['products']   			= $this->product_model->getProductsFromIdList($this->session->userdata('compared_products'));
		$this->data['categories'] 			= $this->admin_model->getCategoryTree();
		$this->data['subpages']   			= $this->admin_model->getAllSubpageNames();		
		$this->data['searchEnabled']		= true;

		$this->load->view('header', $this->data);		
		$this->load->view('product/category_menu', $this->data);	
		$this->load->view('product/list', $this->data);
		$this->load->view('footer', $this->data);
	}
	
	public function addProductIntoFavourite() {
		if ($_SERVER['REQUEST_METHOD'] === 'POST') {
			$productId = $this->input->post('productId');
			
			if ($productInfo 		= $this->product_model->getShortProductById($productId)) {
				$favouriteProducts 	= $this->session->userdata('favourite_products') ?? [];
				
				if ($productInfo) {
					if (!in_array($productInfo->id, $favouriteProducts)) {
						array_push($favouriteProducts, $productInfo->id);

						if ($this->session->userdata('logged_in')) {
							$this->product_model->insertProductIntoFavourite($productInfo->id);			
						}
					}
					
					$this->session->set_userdata('favourite_products', $favouriteProducts);
					
					$productInfo->file_name = cuteImage($productInfo->file_name, true);
					
					echo json_encode(['ok', $productInfo, count($favouriteProducts)]);
				} else {
					echo json_encode(['err']);
				}
			} else {
				echo json_encode(['err']);
			}
		} else {
			redirect('');
		}
	}
	
	public function removeProductFromFavourite() {
		if ($_SERVER['REQUEST_METHOD'] === 'POST') {
			$productId 			= $this->input->post('productId');
			$favouriteProducts 	= $this->session->userdata('favourite_products') ?? [];
			
			if (in_array($productId, $favouriteProducts)) {
				if (($key = array_search($productId, $favouriteProducts)) !== false) {
					unset($favouriteProducts[$key]);
				}
			}
			
			$this->session->set_userdata('favourite_products', $favouriteProducts);
			
			if ($this->session->userdata('logged_in')) {
				$this->product_model->deleteProductFromFavourite($productId);		
			}
			echo json_encode(['ok', count($favouriteProducts)]);
		} else {
			redirect('');
		}
	}
	
	public function addProductIntoCompare() {
		if ($_SERVER['REQUEST_METHOD'] === 'POST') {
			$productId = $this->input->post('productId');
			
			if ($productInfo 		= $this->product_model->getShortProductById($productId)) {
				$comparedProducts 	= $this->session->userdata('compared_products') ?? [];

				if (!in_array($productInfo->id, $comparedProducts)) {
					array_push($comparedProducts, $productInfo->id);

					if ($this->session->userdata('logged_in')) {
						$this->product_model->insertProductIntoCompare($productInfo->id);			
					}
				}

				$this->session->set_userdata('compared_products', $comparedProducts);
				
				$productInfo->file_name = cuteImage($productInfo->file_name, true);

				echo json_encode(['ok', $productInfo, count($comparedProducts)]);
			} else {
				echo json_encode(['err']);
			}
		} else {
			redirect('');
		}
	}
	
	public function removeProductFromCompare() {
		if ($_SERVER['REQUEST_METHOD'] === 'POST') {
			$productId 			= $this->input->post('productId');
			$comparedProducts 	= $this->session->userdata('compared_products') ?? [];
			
			if (in_array($productId, $comparedProducts)) {
				if (($key = array_search($productId, $comparedProducts)) !== false) {
					unset($comparedProducts[$key]);
				}
			}
			
			$this->session->set_userdata('compared_products', $comparedProducts);
			
			if ($this->session->userdata('logged_in')) {
				$this->product_model->deleteProductFromCompare($productId);		
			}
			
			echo json_encode(['ok', count($comparedProducts)]);
		} else {
			redirect('');
		}
	}
	
	public function categoryProducts($categoryLink) {
		$category = $this->product_model->getCategoryByLink($categoryLink);
		
		if ($category) {
			$this->data['filter'] 				= $this->parseFilter();
			$this->data['page_title'] 			= $category->title . $this->config->item('title_delimiter') . $this->config->item('eshop_name');
			$this->data['pageHeaderTitle'] 		= $category->title;
			$this->data['actualCategoryId']		= $category->category_id;
			$this->data['totalCount'] 			= $this->product_model->getTotalCountOfProductsCategory($category->category_id, $this->data['filter']);
			$this->data['products']   			= $this->product_model->getFilteredProductByCaregoryId($category->category_id, $this->data['filter']);
			$this->data['categories'] 			= $this->admin_model->getCategoryTree();
			$this->data['subpages']   			= $this->admin_model->getAllSubpageNames();
			$this->data['colorFilter']			= array_column($this->product_model->getAllAtrributeValue('Farba'), 'value');
			$this->data['materialFilter']		= array_column($this->product_model->getAllAtrributeValue('Materiál'), 'value');
			$this->data['searchEnabled']		= true;
			$this->data['filterEnabled']		= true;	
			$this->data['breadCrumbs']			= $this->makeBreadcrumbs($category->category_id);
			$this->data['actualCategoryIds']	= array_map(function($e) {return is_object($e) ? $e->category_id : $e['category_id'];}, $this->data['breadCrumbs']);
			
			$this->load->view('header', $this->data);		
			$this->load->view('product/category_menu', $this->data);	
			$this->load->view('product/list', $this->data);		
			$this->load->view('footer', $this->data);
		} else {
			redirect('products');
		}
	}
	
	public function searchProducts() {
		$searchExpression = urldecode($this->input->get('q'));
		
		$this->data['page_title'] 			= 'Výsledky vyhľadávania pre: ' . $searchExpression . $this->config->item('title_delimiter') . $this->config->item('eshop_name');
		$this->data['pageHeaderTitle'] 		= 'Výsledky vyhľadávania pre: ' . $searchExpression;
		$this->data['products']   			= $this->product_model->getSearchProducts($searchExpression, $this->data['filter']);
		$this->data['categories'] 			= $this->admin_model->getCategoryTree();
		$this->data['subpages']   			= $this->admin_model->getAllSubpageNames();
		$this->data['search_expression'] 	= $searchExpression;
		$this->data['colorFilter']			= array_column($this->product_model->getAllAtrributeValue('Farba'), 'value');
		$this->data['materialFilter']		= array_column($this->product_model->getAllAtrributeValue('Materiál'), 'value');
		$this->data['searchEnabled']		= true;
		$this->data['filterEnabled']		= true;
		
		$this->load->view('header', $this->data);		
		$this->load->view('product/category_menu', $this->data);
		$this->load->view('product/list', $this->data);		
		$this->load->view('footer', $this->data);
	}
	
	public function content($subPageId) {
		$this->data['subpages'] 	= $this->admin_model->getAllSubpageNames();
		$this->data['content']  	= $this->admin_model->getSubpageContentById($subPageId);
		$this->data['page_title'] 	= $this->data['content']->header . $this->config->item('title_delimiter') . $this->config->item('eshop_name');

		$this->load->view('header', $this->data);				
		$this->load->view('content', $this->data);
		$this->load->view('footer', $this->data);
	}
	
	public function addReview($productId) {
		if ($this->config->item('product_reviews')) {
			if (!$this->session->has_userdata('logged_in')) {
				redirect('/home');
			}
			
			if ($this->input->post('productReviewConfirm')) { 
				if ($this->product_model->productExists($productId)) {
					if (!$this->product_model->checkIfUserAlreadyDidReview($productId, $this->session->userdata('id'))) {
						$this->form_validation->set_rules('productRating', 	 	'Hodnotenie produktu', 	 'trim|required|greater_than_equal_to[0]|less_than_equal_to[100]');
						$this->form_validation->set_rules('productReviewText',  'Komentá k hodnoteniu',  'trim');
						
						if ($this->form_validation->run()) {
							$productReview = array(
								'product_id' 	=> $productId,
								'user_id' 		=> $this->session->userdata('id'),
								'rating' 		=> strip_tags($this->input->post('productRating')),
								'comment'		=> strip_tags($this->input->post('productReviewText'))
							);
							$productRating = $this->calculateNewRating($productId, $productReview['rating']);

							if ($this->product_model->insertProductReview($productId, $productReview, $productRating)) {
								$this->session->set_flashdata('success', 'Recenzia bola pridaná');
							} else {
								$this->session->set_flashdata('error', 'Vyskytla sa chyba. Skúste to prosím znovu.');
							}
						} else {
							$this->session->set_flashdata('error', validation_errors('<span>', '</span>'));
						}
					} else {
						$this->session->set_flashdata('error', 'Tento produkt ste už hodnotili.');
					}
				} else {
					$this->session->set_flashdata('error', 'Produkt neexistuje.');
				}
			}
			redirect($_SERVER['HTTP_REFERER']);
		}
	}
	
	public function getSearchSuggestions() {
		$search_expression = $this->input->post('search_expression');
		
		// echo json_encode(array_map('strtolower', array_map('trim', $this->product_model->getSuggestedProductNames($search_expression))), JSON_UNESCAPED_UNICODE);
		echo json_encode(array_map('trim',$this->product_model->getSuggestedProductNames($search_expression)), JSON_UNESCAPED_UNICODE);
	}
	
	private function calculateNewRating($productId, $newRating) {
		$ratings = $this->product_model->getProductRatings($productId);
		
		return round((array_sum(array_column($ratings, 'rating')) + $newRating) / (count($ratings) + 1));
	}
	
	private function parseFilter() {
		$filterArr 					= [];
		$filterArr['deleted'] 		= 0;
		
		if ($this->session->userdata('admin') != 1) {
			$filterArr['show_product'] 	= 1;
		}
		
		if ($this->input->get('material')) {
			$filterArr['product_attributes.value'] = $this->input->get('material');
		}
		
		if ($this->input->get('color')) {
			$filterArr['product_attributes.value'] = $this->input->get('color');
		}
		
		if ($this->input->get('origin')) {
			$filterArr['origin'] = $this->input->get('origin');
		}
		
		if ($this->input->get('handmade')) {
			$filterArr['handmade'] = 1;
		}
		
		// if (($this->input->get('ageFrom') != null) && ($this->input->get('ageTo') != null)) {
			// $filterArr['((age_from BETWEEN "' . $this->input->get('ageFrom') . '" AND "' . $this->input->get('ageTo') . '") OR (age_to BETWEEN "' . $this->input->get('ageFrom') . '" AND "' . $this->input->get('ageTo') . '")  OR ((age_from > "' . $this->input->get('ageFrom') . '") AND (age_to < "' . $this->input->get('ageTo') . '")))'] = null;
		// }
		
		if ($this->input->get('stock') == 1) {
			$filterArr['quantity >'] = 0;
		}
		
		if ($this->input->get('discount') == 1) {
			$filterArr['discount >'] = 0;
		}
		
		if (is_numeric($this->input->get('priceFrom'))) {
			$filterArr['price >='] = (int)$this->input->get('priceFrom');
		}
		
		if (is_numeric($this->input->get('priceTo'))) {
			$filterArr['price <='] = (int)$this->input->get('priceTo');
		}
		
		return $filterArr;
	}
	
	private function makeBreadcrumbs($categoryId) {
		$breadCrumbs = $this->product_model->getBreadCrumbs($categoryId);
		
		return $breadCrumbs;
	}

	
}
