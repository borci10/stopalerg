<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Auth extends CI_Controller {
	
	function __construct() {
		parent::__construct();
		
		$this->load->model('cart_model');
		$this->load->model('auth_model');
		$this->load->model('admin_model');
		$this->load->model('product_model');
	}

	public function index() {
		if ($this->session->has_userdata('logged_in')) {
			if($this->session->userdata('admin') == 1) {
				redirect('/admin');
			}
			else {
				redirect('/profile');
			}
		} else {
			if($this->input->post('loginConfirm')){
				$this->doLogin();
			}
			
			$this->data['page_title'] = 'Prihlásenie' . $this->config->item('title_delimiter') . $this->config->item('eshop_name');

			$this->load->view('admin/header', $this->data);
			$this->load->view('inc/alerts', $this->data);
			$this->load->view('admin/auth/login_form');
		}
	}
	
	public function registration() {
		if($this->input->post('regConfirm')){
			$this->doRegister();
		}
		
		$this->data['page_title'] = 'Registrácia' . $this->config->item('title_delimiter') . $this->config->item('eshop_name');

		$this->load->view('admin/header', $this->data);
		$this->load->view('inc/alerts', $this->data);
		$this->load->view('admin/auth/registration_form');
	}
	
	public function logout() {
		if ($this->session->userdata('admin') == 1) {
			$this->admin_model->cleanGarbageImageUploads();
		}
		
		$user_data = $this->session->all_userdata();

		foreach ($user_data as $key => $value) {
			if ($key != 'session_id' && $key != 'ip_address' && $key != 'user_agent' && $key != 'last_activity') {
				$this->session->unset_userdata($key);
			}
		}
		
		redirect('');
	}
	
	public function lostPassword() {
		if($this->input->post('formConfirm')){
			$this->newUserPass($this->input->post('email'));
		}

		$this->data['page_title'] = 'Zabudnuté heslo';

		$this->load->view('admin/header', $this->data);
		$this->load->view('admin/auth/lost_pass_form');
	}
	
	private function newUserPass($userEmail) {	
		if ($userId = $this->auth_model->getUserIdByEmail($userEmail)) {
			$newPassword = generatePassword(5);

			if ($this->auth_model->updateUserPass($userId->id,password_hash($newPassword, PASSWORD_DEFAULT))) {
				$this->newPassEmail($newPassword, $userEmail);
				$this->session->set_flashdata('success', 'Nové heslo bolo odoslané na vašu emailovú adresu!');	
			} else {
				$this->session->set_flashdata('error', 'Heslo sa nepodarilo zmeniť.');
			}
			redirect('/auth');
		} else {
			$this->session->set_flashdata('error', 'Email ktorý ste zadali, neexistuje!');
		}
	}
	
	private function doRegister() {
		$this->form_validation->set_rules('regName', 	  'Meno', 			 'trim|required');
		$this->form_validation->set_rules('regEmail', 	  'Emailová adresa', 'trim|required|valid_email|is_unique[users.email]');
		$this->form_validation->set_rules('regStreet', 	  'Ulica', 			 'trim|required');
		$this->form_validation->set_rules('regCity', 	  'Mesto', 			 'trim|required');
		$this->form_validation->set_rules('regZipCode',   'PSČ', 			 'trim|required');
		$this->form_validation->set_rules('regPhone', 	  'Telefónne číslo', 'trim|required');
		$this->form_validation->set_rules('regPassword',  'Heslo', 		 	 'required');
		$this->form_validation->set_rules('regPassword2', 'Potvrdenie hesla','required|matches[regPassword]');
		$this->form_validation->set_rules('op', 	  	  'Obchodné podmienky', 'trim|required');
		$this->form_validation->set_rules('oou', 	  	  'Ochrana osobných údajov', 'trim|required');

		if($this->form_validation->run()){
			$userData = array(
				'name' 			=> strip_tags($this->input->post('regName')),
				'email' 		=> strip_tags($this->input->post('regEmail')),
				'city' 			=> strip_tags($this->input->post('regCity')),
				'street'	 	=> strip_tags($this->input->post('regStreet')),
				'zip_code' 		=> strip_tags($this->input->post('regZipCode')),
				'phone_number' 	=> strip_tags($this->input->post('regPhone')),
				'password' 		=> password_hash($this->input->post('regPassword'), PASSWORD_DEFAULT)
			);
			
			if ($this->auth_model->insertRegistrationData($userData)) {
				$this->session->set_flashdata('success', 'Registrácia prebehla úspešne');
				redirect('/login');
			} else {
				$this->session->set_flashdata('error', 'Registrácia sa nepodarila. Skúste to prosím znovu.');
				redirect('/auth/registration');
			}
		} else {
			$this->session->set_flashdata('error', validation_errors('<span>', '</span>'));
			redirect('/auth/registration');
		}
	}

	private function doLogin() {
		$this->form_validation->set_rules('loginEmail', 	'Emailová adresa',  'trim|required|valid_email');
		$this->form_validation->set_rules('loginPassword',  'Heslo', 		 	'trim|required');

		if($this->form_validation->run()) {
			$loginData = array(
				'email' 		=> strip_tags($this->input->post('loginEmail')),
			);

			$userData = $this->auth_model->getLoginUserData($loginData['email']);

			if ($userData) {
				if (password_verify($this->input->post('loginPassword'), $userData->password)) {
					$this->session->set_userdata('id', 					$userData->id);
					$this->session->set_userdata('name', 				$userData->name);
					$this->session->set_userdata('birth_date',	 		$userData->birth_date);
					$this->session->set_userdata('email',	 			$userData->email);
					$this->session->set_userdata('city',	 			$userData->city);
					$this->session->set_userdata('street',	 			$userData->street);
					$this->session->set_userdata('zip_code',			$userData->zip_code);
					$this->session->set_userdata('delivery_city',		$userData->delivery_city);
					$this->session->set_userdata('delivery_street',		$userData->delivery_street);
					$this->session->set_userdata('delivery_zip_code',	$userData->delivery_zip_code);
					$this->session->set_userdata('phone_number',		$userData->phone_number);
					$this->session->set_userdata('admin', 				$userData->admin);
					$this->session->set_userdata('uam_can_delete', 		$userData->uam_can_delete);
					$this->session->set_userdata('favourite_products', 	$this->product_model->getFavouriteProducts());
					$this->session->set_userdata('compared_products', 	$this->product_model->getCompareProducts());
					$this->session->set_userdata('shoppingCart', 		$this->cart_model->getUserShoppingCart());
					$this->session->set_userdata('cart_price', 			$this->cart_model->getCartTotalPrice());
					$this->session->set_userdata('logged_in', 	1);

					$this->auth_model->setLastLogin($userData->id);

					if ($this->session->userdata('admin') == 1) {
						redirect('/admin');
					}
					else {
						redirect('/profile');
					}
				} else {
					$this->session->set_flashdata('error', 'Nesprávne heslo!');
					redirect('/login');
				}
			} else {
				$this->session->set_flashdata('error', 'Nesprávny email!');
				redirect('/login');
			}
		} else {
			$this->session->set_flashdata('error', validation_errors('<span>', '</span>'));
			redirect('/login');
		}
	}

	private function newPassEmail($newPass, $userEmail) {
		$this->email->from($this->config->item('eshop_email'), $this->config->item('eshop_name'));
		$this->email->to($userEmail);

		$this->email->subject('Nové heslo');
		$this->email->message('Vaše nové heslo je: ' . $newPass . '. Ihneď po prihlásení si ho môžete zmeniť.');

		$this->email->send();
	}
}
