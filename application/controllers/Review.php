<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Review extends CI_Controller {
	
	function __construct() {
		parent::__construct();
		
		$this->load->model('review_model');
		$this->load->model('product_model');

		$this->data['products']			= $this->product_model->getFilteredproducts(['deleted' => 0, 'show_product' => 1], 0, 1);

	}
	
	public function review() {
		$this->data['page_title'] 		= 'Recenzie' . $this->config->item('title_delimiter') . $this->config->item('eshop_name');
		$this->data['pageHeaderTitle'] 	= 'Recenzie';
		$this->data['pageDescription'] = 'Prírodné antihistamikum, na boj proti alergii';
		$this->data['reviews']			= $this->review_model->getReviews();

		$this->load->view('header', $this->data);
		$this->load->view('main/reviews', $this->data);
		$this->load->view('footer');
	}

	public function review_detail($reviewLink) {
		$this->data['review']				= $this->review_model->getReviewByLink($reviewLink);
		if ($this->data['review']) {
			if ($this->input->post('addComment')) {
				$this->addComment($this->data['review']->id);
			}

			$this->data['page_title'] 		 = $this->data['review']->header . $this->config->item('title_delimiter') . $this->config->item('eshop_name');
			$this->data['pageDescription']  = strip_tags(character_limiter($this->data['review']->content, 295, '...'));

			if ($this->data['review']->comments) {
				$this->data['review_comments'] = $this->review_model->getReviewComments($this->data['review']->id);
			}

			$this->load->view('header', $this->data);
			$this->load->view('review/review_detail', $this->data);
			$this->load->view('footer');
		} else {
			redirect('recenzie');
		}
	}
	
	private function addComment($reviewId) {
		$this->form_validation->set_rules('commentName',    'Meno',  	'trim|required');
		$this->form_validation->set_rules('commentText',   	'Komentár', 'trim|required');

		if($this->form_validation->run()) {  
			$commentData = [
				'review_id' => $reviewId,
				'user_name' => strip_tags($this->input->post('commentName')),
				'content' 	=> strip_tags($this->input->post('commentText'))
			];

			if ($this->review_model->insertComment($commentData)) {
				$this->session->set_flashdata('success', 'Komentár bol pridaný.');
			} else {
				$this->session->set_flashdata('error', 'Vyskytla sa chyba. Skúste to prosím znovu.');
			}
		} else {
			$this->session->set_flashdata('error', validation_errors('<span>', '</span>'));
		}
		
		redirect($_SERVER['HTTP_REFERER']);
	}
	
}
