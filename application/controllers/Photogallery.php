<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Photogallery extends MY_Controller {
	
	function __construct() {
		parent::__construct();

		$this->load->model('photogallery_model');
	}

	public function index() {
		$this->data['page_title'] 		= 'Fotogaléria' . $this->config->item('title_delimiter') . $this->config->item('eshop_name');
		$this->data['page_header']		= 'Fotogaléria';
		$this->data['pageDescription'] = '';
		$this->data['albums']			= $this->photogallery_model->getAllAlbums();
		
		for($i = 0; $i < count($this->data['albums']); $i++) {
			$this->data['albums'][$i]['photos'] = $this->photogallery_model->getPhotosOfAlbum($this->data['albums'][$i]['id']);
		}
		
		$this->load->view('header', $this->data);
		$this->load->view('photogallery/gallery', $this->data);
		$this->load->view('footer');
	}
}