<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Main extends CI_Controller {
	
	function __construct() {
		parent::__construct();
		
		$this->load->model('product_model');
		$this->load->model('admin_model');
		$this->load->model('review_model');
		$this->load->model('auth_model');
		$this->data['products']			= $this->product_model->getFilteredproducts(['deleted' => 0, 'show_product' => 1], 0, 1);

	}

	public function index() {
		$this->data['page_title'] 		= $this->config->item('eshop_name')  . $this->config->item('title_delimiter') . ' Prírodné antihistamikum, na boj proti alergii';
		$this->data['pageDescription'] = 'Stopalerg je prírodné antihistaminikum. Stopalerg je vo forme žuvacej tablety ktoré uľavujú od sennej nádchy, astmy či atopického ekzému.';
		$this->data['reviews']			= $this->review_model->getReviews(6);

		$this->load->view('header', $this->data);
		$this->load->view('main/main', $this->data);
		$this->load->view('footer');
	}

	public function co_je_alergia() {
		$this->data['page_title'] 		= 'Čo je alergia' . $this->config->item('title_delimiter') . $this->config->item('eshop_name');
		$this->data['pageDescription'] = 'Dnes trpí alergiou každý piaty obyvateľ v priemyselne rozvinutých krajnách. Vychádza to z údajov Svetovej zdravotníckej organizácie.';

		$this->load->view('header', $this->data);
		$this->load->view('main/co-je-alergia', $this->data);
		$this->load->view('footer');
	}

	public function ucinne_latky() {
		$this->data['page_title'] 		= 'Účinné látky' . $this->config->item('title_delimiter') . $this->config->item('eshop_name');
		$this->data['pageDescription'] = 'Ak trpíte alergiou a hľadáte prírodný produkt, ktorý môžete kombinovať s liekmi na predpis, StopALERG je vhodnou alternatívou pre vás.';

		$this->load->view('header', $this->data);
		$this->load->view('main/ucinne-latky', $this->data);
		$this->load->view('footer');
	}

	public function kontakt() {
		$this->data['page_title'] 		= 'Kontakt' . $this->config->item('title_delimiter') . $this->config->item('eshop_name');
		$this->data['pageHeaderTitle'] 	= 'Kontakt';
		$this->data['pageDescription'] 	= 'V prípade akýchkoľvek otázok, nás neváhajte kontaktovať.';

		$this->load->view('header', $this->data);
		$this->load->view('main/kontakt', $this->data);
		$this->load->view('footer');
	}

	public function objednavka_vytvorena() {
		$this->load->model('Admin/settings_model');
		$this->load->model('Admin/order_model');

		$this->data['page_title'] 		= 'Objednávka vytvorená' . $this->config->item('title_delimiter') . $this->config->item('eshop_name');
		$this->data['pageHeaderTitle'] 	= 'Objednávka vytvorená';
		$this->data['shopSettings']		= $this->settings_model->getAllSettingsParsed();
		$this->data['order_info']		= $this->order_model->getOrderDetails($this->session->flashdata('orderId'));
		$this->data['payment']			= $this->session->flashdata('payment');

		$this->load->view('header', $this->data);
		$this->load->view('main/objednavka-vytvorena', $this->data);
		$this->load->view('footer');
	}

	public function doprava() {
		$this->data['page_title'] 		= 'Doprava' . $this->config->item('title_delimiter') . $this->config->item('eshop_name');
		$this->data['pageHeaderTitle'] 	= 'Doprava';


		$this->load->view('header', $this->data);
		$this->load->view('main/doprava', $this->data);
		$this->load->view('footer');
	}
	
	public function obchodne_podmienky() {
		$this->data['page_title'] 		= 'Obchodné podmienky' . $this->config->item('title_delimiter') . $this->config->item('eshop_name');
		$this->data['pageHeaderTitle'] 	= 'Obchodné podmienky';

		$this->load->view('header', $this->data);
		$this->load->view('main/obchodne_podmienky', $this->data);
		$this->load->view('footer');
	}


	public function pravidla_cookies() {
		$this->data['page_title'] 		= 'Pravidlá cookies' . $this->config->item('title_delimiter') . $this->config->item('eshop_name');
		$this->data['pageHeaderTitle'] 	= 'Pravidlá cookies';

		$this->load->view('header', $this->data);
		$this->load->view('main/pravidla-cookies', $this->data);
		$this->load->view('footer');
	}
	
	public function cookie_accept() {
		if ($_SERVER['REQUEST_METHOD'] === 'POST') {
			setcookie('accepted', 'yes', time() + (86400 * 30), "/"); // 86400 = 1 day

			echo 'ok';
		} else {
			redirect('');
		}
	}
	
	public function ochrana_osobnych_udajov() {
		$this->data['page_title'] 		= 'Ochrana osobných údajov' . $this->config->item('title_delimiter') . $this->config->item('eshop_name');
		$this->data['pageHeaderTitle'] 	= 'Ochrana osobných údajov';

		$this->load->view('header', $this->data);
		$this->load->view('main/ochrana-osobnych-udajov', $this->data);
		$this->load->view('footer');
	}
	
	public function vyziadanie_osobnych_udajov() {
		if($this->input->post('formConfirm')){
			$this->sendPersonalInfoEmail($this->input->post('email'));
		}

		$this->data['page_title'] 		= 'Vyžiadanie osobných údajov' . $this->config->item('title_delimiter') . $this->config->item('eshop_name');
		$this->data['searchEnabled']	= true;
		$this->data['pageHeaderTitle'] 	= 'Vyžiadanie osobných údajov';

		$this->load->view('header', $this->data);
		$this->load->view('main/vyziadanie_osobnych_udajov', $this->data);
		$this->load->view('footer');
	}

	public function email_send() {
		$this->form_validation->set_rules('meno',     	'Name',  	'trim|required');
		$this->form_validation->set_rules('e-mail',   	'Email',  	'trim|required|valid_email');
		$this->form_validation->set_rules('phone',  	'Telefón', 	'trim|required');
		$this->form_validation->set_rules('sprava',  	'Message', 	'trim|required');
		$this->form_validation->set_rules('orderOou',	'Ochrana osobných údajov',  'trim|required');

		if($this->form_validation->run()) {
			$this->email->from($this->config->item('eshop_email'), $this->input->post('meno'));
			$this->email->reply_to($this->input->post('e-mail'), $this->input->post('meno'));
			$this->email->to($this->config->item('eshop_email'));

			$this->email->subject('Kontaktný formulár | '. $this->config->item('eshop_name'));
			$this->email->message('Meno odosielateľa: '.$this->input->post('meno') . '<br />E-mail odosielateľa: '.$this->input->post('e-mail') . '<br />Telefón odosielateľa: ' . $this->input->post('phone') .'<br /><br />'. $this->input->post('sprava'));

			if ($this->email->send()) {
				$this->session->set_flashdata('success', 'E-mail bol odoslaný! Ďakujeme, Vašej požiadavke sa budeme čo najrýchlejšie venovať.');
			} else {
				$this->session->set_flashdata('error', 'Vyskytla sa chyba. Skúste to prosím znovu.');
			}
		} else {
			$this->session->set_flashdata('error', validation_errors('<span>', '</span>'));
		}

		redirect($_SERVER['HTTP_REFERER']);
	}

	private function sendPersonalInfoEmail($userEmail) {
		if ($this->data['userData'] = $this->auth_model->getUserDataByEmail($userEmail)) {		
			$this->email->from($this->config->item('eshop_email'), $this->config->item('eshop_name'));
			$this->email->to($userEmail);

			$this->email->subject('Osobné údaje');
			$this->email->message($this->load->view('email/email_personal_info', $this->data, true));

			if ($this->email->send()) {
				$this->session->set_flashdata('success', 'Osobné údaje Vám boli odoslané na zadaný email.');
			} else {
				$this->session->set_flashdata('error', 'Nastala chyba. Skúste to prosím znovu.');
			}	
		} else {
			$this->session->set_flashdata('error', 'Zadaný email sa nenachádza v našej dazabáze.');
		}
		redirect('vyziadanie-osobnych-udajov');
	}
}
