<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class MY_Controller extends CI_Controller {
	
	function __construct() {
		parent::__construct();
		if ($this->session->userdata('admin') != 1) {
			redirect('');
		}
		
		$this->load->model('product_model');
		$this->load->model('admin_model');
	
		if (isset($_GET['debug'])) {
            $this->output->enable_profiler(true);
        }
	}
}