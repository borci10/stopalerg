<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
$route['default_controller'] 							= 'Main';
$route['login'] 										= 'Auth';
$route['logout'] 										= 'Auth/logout';
$route['co-je-alergia']	 								= 'Main/co_je_alergia';
$route['ucinne-latky']	 								= 'Main/ucinne_latky';
$route['recenzie']	 									= 'Review/review';
$route['recenzie/(:any)'] 								= 'Review/review_detail/$1';
$route['kontakt'] 										= 'Main/kontakt';
$route['doprava'] 										= 'Main/doprava';
$route['cookie_accept'] 								= 'Main/cookie_accept';
$route['obchodne-podmienky'] 							= 'Main/obchodne_podmienky';
$route['pravidla-cookies'] 								= 'Main/pravidla_cookies';
$route['ochrana-osobnych-udajov'] 						= 'Main/ochrana_osobnych_udajov';
$route['vyziadanie-osobnych-udajov'] 					= 'Main/vyziadanie_osobnych_udajov';
$route['objednavka-vytvorena'] 							= 'Main/objednavka_vytvorena';
$route['registration'] 									= 'Auth/registration';
$route['lost-password'] 								= 'Auth/lostPassword';
$route['novinky'] 										= 'Products/news';
$route['category/indexPage'] 							= 'Products/indexPage';
$route['category/(:any)'] 								= 'Products/categoryProducts/$1';
$route['products/favourite']							= 'Products/favourite';
$route['products/compare']								= 'Products/compare';
$route['products/getSearchSuggestions']					= 'Products/getSearchSuggestions';
$route['products/indexPage']	 						= 'Products/indexPage';
$route['products/search']	 							= 'Products/searchProducts';
$route['products/addReview/(:any)'] 					= 'Products/addReview/$1';
$route['products/addProductIntoFavourite'] 				= 'Products/addProductIntoFavourite';
$route['products/removeProductFromFavourite'] 			= 'Products/removeProductFromFavourite';
$route['products/addProductIntoCompare'] 				= 'Products/addProductIntoCompare';
$route['products/removeProductFromCompare'] 			= 'Products/removeProductFromCompare';
$route['products/(:any)'] 								= 'Products/detail/$1';
$route['products/(:any)/(:any)'] 						= 'Products/variableDetail/$1/$2';
$route['content/add'] 									= 'Products/content/add';
$route['content/(:num)'] 								= 'Products/content/$1';
$route['cart/addItemToCart'] 							= 'Cart/addItemToCart';
$route['cart/removeItemFromCart'] 						= 'Cart/removeItemFromCart';
$route['cart/personal-info'] 							= 'Cart/personalInfo';
$route['cart/payment-info'] 							= 'Cart/paymentInfo';
$route['cart/order-summary'] 							= 'Cart/orderSummary';
$route['admin'] 										= 'Admin/Dashboard';
$route['admin/category'] 								= 'Admin/Categories/index';
$route['admin/category/add'] 							= 'Admin/Categories/add';
$route['admin/category/edit/(:num)'] 					= 'Admin/Categories/edit/$1';
$route['admin/category/remove/(:num)'] 					= 'Admin/Categories/remove/$1';
$route['admin/category/show/(:num)'] 					= 'Admin/Categories/show/$1';
$route['admin/category/hide/(:num)'] 					= 'Admin/Categories/hide/$1';
$route['admin/category/changeCategoryName'] 			= 'Admin/Categories/changeCategoryName';
$route['admin/category/changeCategoryStructure'] 		= 'Admin/Categories/changeCategoryStructure';
$route['admin/reviews/add'] 							= 'Admin/Review/add';
$route['admin/reviews/edit/(:num)'] 					= 'Admin/Review/edit/$1';
$route['admin/reviews/delete/(:num)'] 					= 'Admin/Review/delete/$1';
$route['admin/reviews/publish/(:num)'] 					= 'Admin/Review/publish/$1';
$route['admin/reviews/unpublish/(:num)'] 				= 'Admin/Review/unpublish/$1';
$route['admin/reviews/deleteComment/(:num)'] 			= 'Admin/Review/deleteComment/$1';
$route['admin/reviews'] 								= 'Admin/Review/index';
$route['admin/photogallery'] 							= 'Admin/photogallery';
$route['admin/photogallery/create'] 					= 'Admin/photogallery/create';
$route['admin/photogallery/edit/(:num)'] 				= 'Admin/photogallery/edit/$1';
$route['admin/photogallery/delete/(:num)'] 				= 'Admin/photogallery/delete/$1';
$route['admin/settings'] 								= 'Admin/Settings/index';
$route['admin/content'] 								= 'Admin/Content/index';
$route['admin/import'] 									= 'Admin/Import/index';
$route['admin/payment'] 								= 'Admin/Payment/index';
$route['admin/payment/edit/(:num)'] 					= 'Admin/Payment/edit/$1';
$route['admin/delivery'] 								= 'Admin/Delivery/index';
$route['admin/delivery/edit/(:num)'] 					= 'Admin/Delivery/edit/$1';
$route['admin/suppliers'] 								= 'Admin/Suppliers/index';
$route['admin/suppliers/add'] 							= 'Admin/Suppliers/add';
$route['admin/suppliers/edit/(:num)'] 					= 'Admin/Suppliers/edit/$1';
$route['admin/customers'] 								= 'Admin/Customers/index';
$route['admin/customers/detail/(:num)'] 				= 'Admin/Customers/detail/$1';
$route['admin/products'] 								= 'Admin/Products/index';
$route['admin/products/add'] 							= 'Admin/Products/add';
$route['admin/products/remove/(:num)'] 					= 'Admin/Products/remove/$1';
$route['admin/products/hide/(:num)'] 					= 'Admin/Products/hide/$1';
$route['admin/products/show/(:num)'] 					= 'Admin/Products/show/$1';
$route['admin/products/edit/(:num)'] 					= 'Admin/Products/edit/$1';
$route['admin/products/uploadPhoto']					= 'Admin/Products/uploadPhoto';
$route['admin/products/removePhoto'] 					= 'Admin/Products/removePhoto';
$route['admin/products/massAction'] 					= 'Admin/Products/massAction';
$route['admin/orders'] 									= 'Admin/Orders/index';
$route['admin/orders/invoice/(:num)'] 					= 'Admin/Orders/invoice/$1';
$route['admin/orders/detail/(:num)'] 					= 'Admin/Orders/orderDetail/$1';
$route['admin/orders/changeOrderStatus/(:num)/(:num)'] 	= 'Admin/Orders/changeOrderStatus/$1/$2';
$route['admin/newsletter'] 								= 'Admin/Newsletter';
$route['addEmailIntoNewsletter'] 						= 'Newsletter';
$route['profile'] 										= 'Profile';
$route['profile/settings'] 								= 'Profile/settings';
$route['profile/orders'] 								= 'Profile/orders';
$route['profile/orders/detail/(:num)'] 					= 'Profile/orderDetail/$1';
$route['home'] 											= 'main';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;
