<?php
	$config = array(
		'owner'                => [
			'company'			=> 'StopALERG',
			'street'			=> 'Vlárska 66',
			'city'				=> 'Bratislava',
			'zip_code'			=> '831 01',
			'ico' 				=> '47807512',
			'commercial_reg' 	=> '99211/B',
			'url' 				=> 'https://www.stopalerg.sk/',
			'email' 			=> 'office@onlima.sk',
		],
		'eshop_name'           => 'StopALERG',
		'eshop_email'          => 'office@onlima.sk', //change to shop@stopalerg.sk
		'currency'             => '&euro;',
		'currency_unicode'     => '\u20AC',
		'currency_word'        => 'euro',
		'title_delimiter'      => ' | ',
		'tax'                  => 0.2,
		'product_out_of_stock' => 5,        //set false for disable sending email
		'sql_result_limit'     => 24,
		'trust_pay_AID'        => '2107023767',
		'trust_pay_KEY'        => '6pJcU7hvNAxGR4OEQKS4pmPgthkmrnsP',
		'Review'               => true,
		'photogallery'         => false,
		'breadcrumbs'          => true,
		'product_variable'     => false,
		'product_recomended'   => false,
		'product_similar'      => false,
		'product_attributes'   => false,
		'product_reviews'      => false,
		'product_favourite'    => false,
		'product_compare'      => false,
		'product_tags'         => false,
		'trust_pay'            => false,
		'salt'                 => 'njdcvB77ABegnyXsrzwBDu6x3VaghzGy5nEHDvgZ2Dj4Nwkm6S9CXjaLVLZXPBPMBhCg9cYJDxhNQVZUTxhR6ue7BhHgyLUeJF63Q4Jt4dW5HKZ4qFnzHGnZ7JDMRnLX9eV8R45g9y6WCSXxF6ebTpMTzpD3Y43UgrCNLHhJwNpYuHw4KeztWq7UzuqZ5X2JtQQ4E3jxDS9TtEaC9v3wejYBybUyQd6mu4SSfDqDxtfUwztZVJMexvbEX32HgE7Z',
	);
?>