<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Auth_model extends CI_Model{
	
	public function insertRegistrationData($data) {
		return $this->db->insert('users', $data);
	}
	
	public function getLoginUserData($email) {
		return $this->db->get_where('users', ['email' => $email])->row() ?? false;
	}
	
	public function getUserIdByEmail($userEmail) {
		$this->db->select('id');
		
		return $this->db->get_where('users', ['email' => $userEmail])->row();
	}
	
	public function getUserDataByEmail($userEmail) {
		return $this->db->get_where('users', ['email' => $userEmail])->row();
	}
	
	public function updateUserPass($userId, $password) {
		$this->db->set('password', $password);
		$this->db->where('id',  $userId);
		
		return $this->db->update('users');
	}

    public function email_exist($email){
        $this->db->select('id');
        $this->db->from('users');
		$this->db->where('email',$email);
		
		$query = $this->db->get();
		
		return ($query->num_rows() > 0) ? true : false;
    }
	
	public function setLastLogin($userId) {
		$now = date('Y-m-d H:i:s');
		
		return $this->db->update('users', ['last_login' => $now], ['id' => $userId]);
	}
}