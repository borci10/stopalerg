<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Review_model extends CI_Model{
	
	public function getReviewList() {
		$this->db->order_by('date', 'DESC');
		$this->db->select('id, header, date, published, comments, image, name');

		return $this->db->get('review')->result_array();
	}
	
	public function getReviews($limit = 99999) {
		$this->db->select('review.*,  COUNT(review_comments.id) AS comment_count');
		$this->db->join('review_comments', 'review.id = review_comments.review_id', 'left');
		$this->db->group_by('review.id');
		$this->db->order_by('review.date', 'DESC');

		return $this->db->get_where('review', ['review.published' => 1], $limit)->result_array();
	}

	public function getReviewComments($reviewId) {
		return $this->db->get_where('review_comments', ['review_id' => $reviewId])->result_array();
	}
	
	public function getReview($reviewID = null) {
		if ($reviewID !== null) {
			return $this->db->get_where('review', ['id' => $reviewID])->row();
		} else {
			return $this->db->get('review', 1)->row();
		}
	}
	
	public function getReviewByLink($reviewLink) {
		return $this->db->get_where('review', ['link' => $reviewLink, 'published' => 1])->row() ?? false;
	}
	
	public function insertComment($commentData) {
		return $this->db->insert('review_comments', $commentData);
	}
}