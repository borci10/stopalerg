<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Profile_model extends CI_Model{
	
	public function updateUserData($userData) {
		$this->db->where('id', $this->session->userdata('id'));
		
		return $this->db->update('users', $userData);
	}
	
	public function updatePass($newPass) {
		$sql = "
			UPDATE users 
			SET password = ?
			WHERE id = ?
		";
	
		return $this->db->query($sql, [$newPass, $this->session->userdata('id')]);
	}
	
	public function getOldPass() {
		$this->db->select('password');
		
		return $this->db->get_where('users', ['id' => $this->session->userdata('id')])->row()->password ?? false;
	}
	
	public function getUserOrders($userId) {
		$this->db->select('orders.id, orders.order_date, orders.delivery_price, orders.status, orders.order_price, orders.payment_price');
		$this->db->group_by('orders.id');
		$this->db->order_by('orders.order_date', 'DESC');
		
		return $this->db->get_where('orders', ['orders.user_id' => $userId])->result_array();
	}
	
	public function getOrderDetails($orderId){
		$this->db->select('orders.*, product.id, IF(order_product.id_product_child = 0, product.name, product_variables.name) AS name, product.link, product_variables.link AS product_variable_link, order_product.product_price, order_product.quantity, order_product.tax, order_product.discount, order_product.id_product_child');
		$this->db->join('product', 'order_product.id_product = product.id');
		$this->db->join('product_variables', 'order_product.id_product_child = product_variables.id', 'left');
		$this->db->join('orders', 'order_product.id_order = orders.id');
		
		return $this->db->get_where('order_product', ['order_product.id_order' => $orderId])->result_array();
	}
    
}