<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Admin_model extends CI_Model{
	
	public function getAllCategories() {	
		return $this->db->get('categories')->result_array();
	}
	
	public function getSameProductNameCount($productLink) {
		$this->db->select('COUNT(id) AS productCount');
		
		return $this->db->get_where('product', ['link' => $productLink])->row()->productCount ?? 0;
	}
	
	public function getAllTopTreeCategories() {	
		return $this->db->get_where('categories', ['parent_id' => null])->result_array();
	}
	
	public function getCategoryTree($admin = 0) {
		if ($admin) {
			$tree = $this->db->get_where('categories', ['parent_id' => null])->result_array();
			
			function getCategoryChildrens($parent) {
				$ci =& get_instance();
				$childrens = $ci->db->get_where('categories', ['parent_id' => $parent['category_id']])->result_array();
				
				for($i = 0; $i < count($childrens); $i++) {
					$childrens[$i]['childrens'] = getCategoryChildrens($childrens[$i]);
				}
			
				return $childrens;
			}
			
			for($i = 0; $i < count($tree); $i++) {
				$tree[$i]['childrens'] = getCategoryChildrens($tree[$i]);
			}
		
			return $tree;
		} else {
			$tree = $this->db->get_where('categories', ['parent_id' => null, 'show_category' => 1])->result_array();
			
			function getCategoryChildrens($parent) {
				$ci =& get_instance();
				$childrens = $ci->db->get_where('categories', ['parent_id' => $parent['category_id'], 'show_category' => 1])->result_array();
				
				for($i = 0; $i < count($childrens); $i++) {
					$childrens[$i]['childrens'] = getCategoryChildrens($childrens[$i]);
				}
			
				return $childrens;
			}
			
			for($i = 0; $i < count($tree); $i++) {
				$tree[$i]['childrens'] = getCategoryChildrens($tree[$i]);
			}
		
			return $tree;
		}
	}
	
	public function changeSupplierDeletedStatus($supplierId, $deleteStatus) {
		return $this->db->update('suppliers', ['deleted' => $deleteStatus], ['id' => $supplierId]);
	}
	
	public function changeDeliveryDeletedStatus($deliveryId, $deleteStatus) {
		return $this->db->update('delivery', ['deleted' => $deleteStatus], ['id' => $deliveryId]);
	}
	
	public function getProductsForSimilarSeacrh($expr) {
		$this->db->select('id, name, price');
		$this->db->like('name', $expr);
		
		return $this->db->get_where('product',  ['deleted' => 0], 10)->result_array();
	}
	
	public function getAllProductsForSimilarSeacrh() {
		$this->db->select('id, name, price');
		
		return $this->db->get_where('product',  ['deleted' => 0])->result_array();
	}
	
	public function getAllProductsAttributes() {
		$this->db->select('DISTINCT(title)');
		
		return $this->db->get('product_attributes')->result_array();
	}
	
	public function getAllOrderStatuses() {
		$this->db->select('status');
		
		return $this->db->get('orders')->result_array();
	}
	
	public function getAllSuppliers() {
		return $this->db->get_where('suppliers', ['deleted' => 0])->result_array();
	}
	
	public function getAllOrders() {
		$this->db->select('orders.id, orders.email, orders.phone_number, orders.order_date, orders.delivery_price, orders.status, SUM(order_product.product_price * order_product.quantity) AS totalPrice');
		$this->db->join('order_product', 'orders.id = order_product.id_order');
		$this->db->group_by('orders.id'); 
		
		return $this->db->get('orders')->result_array();
	}
	
	public function getOrdersByFilterForDelivery($filter) {
		$this->db->select('orders.*, SUM(order_product.product_price * order_product.quantity) AS totalPrice');
		$this->db->join('order_product', 'orders.id = order_product.id_order');
		$this->db->group_by('orders.id'); 
		
		return $this->db->get_where('orders', $filter)->result_array();
	}
	
	public function getProductAttributes($productId) {		
		return $this->db->get_where('product_attributes', ['id_product' => $productId])->result_array();
	}
	
	public function getSubpageContentById($subpageId) {
		$this->db->select('*');
		
		return $this->db->get_where('content', ['id' => $subpageId], 1)->row();
	}

	public function updateOrInsertProductData($productData, $productCategories = [], $productPhotos = [], $productAttributes = [], $productSimilar = [], $rootCategoryId, $tag) {
		$this->db->trans_start();
		
		if ($productExists = $this->db->get_where('product', ['uid' => $productData['uid']])->row()) {
			$this->db->update('product', $productData, ['id' => $productExists->id]);
			$this->db->delete('product_photo', ['id_product' => $productExists->id]);
			$this->db->delete('product_category', ['id_product' => $productExists->id]);

			$insertedProductId = $productExists->id;
		} else {
			$this->db->insert('product', $productData);
			$insertedProductId = $this->db->insert_id();
		}
		
		if (!$productInCategory = $this->db->get_where('product_category', ['id_category' => $rootCategoryId, 'id_product' => $insertedProductId])->row()) {
			$this->db->insert('product_category', ['id_category' => $rootCategoryId, 'id_product' => $insertedProductId]);
		}
			
		if (!empty($productCategories)) {
			foreach ($productCategories as $category) {
				if ($categoryExist = $this->db->get_where('categories', ['title_cz' => $category])->row()) {
					$this->db->insert('product_category', ['id_category' => $categoryExist->category_id, 'id_product' => $insertedProductId]);
				} else {
					$this->db->select('MAX(category_id) as maxId');
					$maxCatId = $this->db->get('categories')->row()->maxId ?? 0;

					$this->db->insert('categories', [
						'category_id' 	=> $maxCatId + 1, 
						'title' 		=> $category, 
						'title_cz' 		=> $category,
						'link'			=> strtolower(utf8_encode(convert_accented_characters(url_title(strip_tags($category)), "dash", true))),
						'parent_id'		=> $rootCategoryId,
						'show_category'	=> 0,
						'tag'			=> $tag
					]);
					
					$rootChilds = $this->db->get_where('categories', ['category_id' => $rootCategoryId])->row()->childrens ?? '';
					$this->db->update('categories', ['childrens' => (($rootChilds == '') ? ($maxCatId + 1) : $rootChilds . ',' . ($maxCatId + 1))], ['category_id' => $rootCategoryId]);
					
					$rootCategoryId = $maxCatId + 1;
				}
			}
		}
		
		if (!empty($productPhotos)) {
			for ($i = 0; $i < count($productPhotos); $i++) {
				$this->db->insert('product_photo', ['id_product' => $insertedProductId, 'photo_order' => $i, 'file_name' => $productPhotos[$i]['file_name'], 'title' => $productPhotos[$i]['title'], 'alt' => $productPhotos[$i]['alt']]);
			}
		}
		
		if (!empty($productAttributes)) {
			for ($i = 0; $i < count($productAttributes['title']); $i++) {
				if ((!empty($productAttributes['title'][$i])) && (!empty($productAttributes['value'][$i]))) {
					$this->db->insert('product_attributes', ['id_product' => $insertedProductId, 'title' => $productAttributes['title'][$i], 'value' => $productAttributes['value'][$i]]);
				}
			}
		}
		
		if (!empty($productSimilar)) {
			foreach ($productSimilar as $productSimilarId) {
				$this->db->insert('product_similar', ['id_similar_product' => $productSimilarId, 'id_product' => $insertedProductId]);
			}	
		}
		
		return $this->db->trans_complete();
	}
	
	public function insertDeliveryOption($deliveryOption) {
		return $this->db->insert('delivery', $deliveryOption);
	}
	
	public function updateDeliveryOption($deliveryId, $deliveryOption) {
		return $this->db->update('delivery', $deliveryOption, ['id' => $deliveryId]);
	}
	
	public function insertSupplier($supplierData) {
		return $this->db->insert('suppliers', $supplierData);
	}
	
	public function updateSupplier($supplierId, $supplierData) {
		return $this->db->update('suppliers', $supplierData, ['id' => $supplierId]);
	}
	
	public function insertProductBlindPhoto($fileName) {
		$this->db->insert('product_photo', ['file_name' => $fileName, 'user_id' => $this->session->userdata('id')]);
		
		return $this->db->insert_id();
	}
	
	public function insertSubpageData($subpageData) {
		return $this->db->insert('content', $subpageData);
	}
	
	public function updateSubpageData($subpageId, $subpageData) {
		$this->db->where('id', $subpageId);
		
		return $this->db->update('content', $subpageData);
	}
	
	public function deleteSubpage($subpageId) {
		return $this->db->delete('content', ['id' => $subpageId]);
	}
	
	public function getAllSubpageNames() {
		$this->db->select('id, header');
		
		return $this->db->get('content')->result_array();
	}
	
	public function getLastPhotoOrder($productId) {
		$this->db->select('photo_order');
		$this->db->order_by("photo_order", "desc");
		
		return $this->db->get_where('product_photo', ['id_product' => $productId], 1)->row();
	}
	
	public function deletePhoto($fileName) {
		if (file_exists('uploads/products/' . $fileName)) {
			unlink('uploads/products/' . $fileName);
		}
		if (file_exists('uploads/products/thumbs/' . $fileName)) {
			unlink('uploads/products/thumbs/' . $fileName);
		}
		return $this->db->delete('product_photo', ['file_name' => $fileName]);
	}
	
	public function cleanGarbageImageUploads() {
		$garbageImages = $this->getGarbagePhotos();
		
		foreach($garbageImages as $garbageImage) {
			$this->deletePhoto($garbageImage['file_name']);
		} 
	}
	
	public function getGarbagePhotos() {
		$this->db->select('file_name');
		
		return $this->db->get_where('product_photo', ['user_id' => $this->session->userdata('id'), 'id_product' => null])->result_array();
	}
    
	public function getOrderSubpageFilter($urlSegment) {
		switch($urlSegment) {
			case 'all':
				$filter = [];
				break;
			case 'created':
				$filter = ['status' => 1];
				break;
			case 'accepted':
				$filter = ['status' => 2];
				break;
			case 'sent':
				$filter = ['status' => 3];
				break;
			case 'resolved':
				$filter = ['status' => 4];
				break;
			case 'declined':
				$filter = ['status' => 5];
				break;
			default:
				$filter = [];
		}
		
		return $filter;
	}
}