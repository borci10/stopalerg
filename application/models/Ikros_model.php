<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Ikros_model extends CI_Model {
	private $apiKey 	= "e0e60bf9-ca5b-4ce5-9dc8-5de99e14bb64";
	private $invoiceUrl	= "https://eshops.inteo.sk/api/v1/invoices/";
	private $orderUrl	= "https://eshops.inteo.sk/api/v1/incomingorders/";
	
	public function sendInvoiceExport($orderId) {
		if ($this->apiKey) {
			if ($invoiceExport = $this->getInvoiceExport($orderId)) {
				$ch 	= curl_init();

				curl_setopt($ch, CURLOPT_URL, $this->invoiceUrl);
				curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
				curl_setopt($ch, CURLOPT_HEADER, FALSE);
				curl_setopt($ch, CURLOPT_POST, TRUE);
				curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode([$invoiceExport]));
				curl_setopt($ch, CURLOPT_HTTPHEADER, array(
					"Content-Type: application/json",
					"Authorization: Bearer " . $this->apiKey
				));

				$response = curl_exec($ch);
				curl_close($ch);

//				 echo "<pre>";
//				 print_r($response);
//				 echo "</pre>";
			} else {
				return false;
			}
		}
	}
	
	public function getInvoiceExport($orderId) {
		if ($order = $this->getOrder($orderId)) {
			return $this->parseInvoiceData($order);
		} else {
			return null;
		}
	}
	
	private function getOrder($orderId) {
		return $this->db->get_where('orders', ['id' => $orderId])->row();
	}
	
	private function getVariableProduct($variableProductId) {
		$this->db->select('product_variables.name, product_variables.short_description');
		
		return $this->db->get_where('product_variables', ['product_variables.id' => $variableProductId])->row();
	}
	
	private function getOrderItems($orderId) {
		$this->db->select('order_product.quantity, order_product.tax, order_product.discount, order_product.product_price, order_product.id_product_child, product.name, product.short_description, product.uid');
		$this->db->join('product', 'order_product.id_product = product.id', 'left');
		
		return $this->db->get_where('order_product', ['order_product.id_order' => $orderId])->result_array();
	}

	private function parseInvoiceData($order) {
		$invoiceData = [
			"documentNumber"					=> generateVarSymbol($order->id),
			"numberingSequence"					=> "OF",
			"totalPrice"						=> cutePrice($order->order_price + $order->delivery_price + $order->payment_price, null) - vatCalcSimple($order->order_price + $order->delivery_price + $order->payment_price, 20),
			"totalPriceWithVat"					=> cutePrice($order->order_price + $order->delivery_price + $order->payment_price, null),
			"createDate"						=> ISO8601Date($order->order_date),
			"dueDate"							=> ISO8601Date($order->order_date . "+7 days"),
			"completionDate"					=> ISO8601Date($order->order_date),
			"clientName"						=> $order->name . ' ' . $order->surname,
			"clientContactName"					=> $order->name,
			"clientContactSurname"				=> $order->surname,
			"clientStreet"						=> $order->street,
			"clientPostCode"					=> $order->zip_code,
			"clientTown"						=> $order->city,
			"clientCountry"						=> "",
			"clientPhone"						=> $order->phone_number,
			"clientEmail"						=> $order->email,
			"clientRegistrationId"				=> "",
			"clientTaxId"						=> "",
			"clientVatId"						=> "",
			"clientInternalId"					=> null,
			"variableSymbol"					=> generateVarSymbol($order->id),
			"openingText"						=> "",
			"closingText"						=> "",
			"senderName"						=> $this->settings_model->getSingleValue('company_name'),
			"senderRegistrationId"				=> $this->settings_model->getSingleValue('company_ico'),
			"senderRegistrationCourt"			=> $this->settings_model->getSingleValue('commercial_reg'),
			"senderVatId"						=> $this->settings_model->getSingleValue('company_dic'),
			"senderTaxId"						=> $this->settings_model->getSingleValue('company_icdph'),
			"senderStreet"						=> $this->settings_model->getSingleValue('company_icdph'),
			"senderPostCode"					=> $this->settings_model->getSingleValue('company_zip_code'),
			"senderTown"						=> $this->settings_model->getSingleValue('company_city'),
			"senderRegion"						=> null,
			"senderCountry"						=> "",
			"senderBankAccount"					=> $this->settings_model->getSingleValue('bank_account_number'),
			"senderBankIban"					=> $this->settings_model->getSingleValue('bank_iban'),
			"senderBankSwift"					=> $this->settings_model->getSingleValue('bank_swift'),
			"paymentType"						=> $order->payment,
			"deliveryType"						=> $order->delivery,
			"senderContactName"					=> $this->settings_model->getSingleValue('company_name'),
			"senderPhone"						=> $this->settings_model->getSingleValue('company_phone'),
			"senderEmail"						=> $this->settings_model->getSingleValue('company_email'),
			"senderWeb"							=> "",
			"clientPostalName"					=> $order->name . ' ' . $order->surname,
			"clientPostalContactName"			=> $order->name,
			"clientPostalContactSurname"		=> $order->surname,
			"clientPostalPhone"					=> $order->phone_number,
			"clientPostalStreet"				=> $order->street,
			"clientPostalPostCode"				=> $order->zip_code,
			"clientPostalTown"					=> $order->city,
			"clientPostalCountry"				=> "",
			"clientHasDifferentPostalAddress"	=> toBoolean($order->diff_delivery),
			"currency"							=> "EUR",
			"exchangeRate"						=> 1,
			"senderIsVatPayer"					=> toBoolean($this->settings_model->getSingleValue('vat_payer')),
			"discountPercent"					=> null,
			"discountValue"						=> null,
			"discountValueWithVat"				=> null,
			"priceDecimalPlaces"				=> null,
			"deposit"							=> 0,
			"depositText"						=> null,
			"depositDate"						=> null,
			"orderNumber"						=> generateVarSymbol($order->id),
			"clientNote"						=> "",
			"isVatAccordingPayment"				=> true,
			"items"								=> $this->parseItems($order)
		];
		
		return $invoiceData;
	}
	
	private function parseItems($order) {
		$items 		= $this->parseOrderItems($order->id);
		$items[] 	= $this->parsePaymentItem($order);
		$items[] 	= $this->parseDeliveryItem($order);
		
		return $items;
	} 
	
	private function parseOrderItems($orderId) {
		$items = [];
		
		if ($orderItems = $this->ikros_model->getOrderItems($orderId)) {
			foreach($orderItems as $orderItem) {
				if ($orderItem['id_product_child'] != 0) {
					if ($variableOrderItem = $this->getVariableProduct($orderItem['id_product_child'])) {
						$orderItem['name']				= $variableOrderItem->name;
						$orderItem['short_description']	= $variableOrderItem->short_description;
					}
				}
				$items[] = [
					"name"					=> $orderItem['name'],
					"description"			=> $orderItem['short_description'],
					"count"					=> $orderItem['quantity'],
					"measureType"			=> "ks",
					"totalPrice"			=> priceCalc($orderItem['product_price'], $orderItem['quantity'], 0),
					"totalPriceWithVat"		=> priceCalc($orderItem['product_price'], $orderItem['quantity'], 0, $orderItem['tax'] / 100),
					"unitPrice"				=> $orderItem['product_price'],
					"unitPriceWithVat"		=> priceCalc($orderItem['product_price'], 1, 0, $orderItem['tax'] / 100),
					"vat"					=> $orderItem['tax'],
					"hasDiscount"			=> ($orderItem['discount'] > 0 ? true : false),
					"discountName"			=> 'Zľava',
					"discountPercent"		=> $orderItem['discount'],
					"discountValue"			=> priceCalc($orderItem['product_price'], 1) - priceCalc($orderItem['product_price'], 1, $orderItem['discount']),
					"discountValueWithVat"	=> priceCalc($orderItem['product_price'], 1, 0, $orderItem['tax'] / 100) - priceCalc($orderItem['product_price'], 1, $orderItem['discount'], $orderItem['tax'] / 100),
					"productCode"			=> $orderItem['uid'],
					"typeId"				=> 0,
					"warehouseCode"			=> null,
					"foreignName"			=> "",
					"customText"			=> "",
					"ean"					=> "",
					"jkpov"					=> "",
					"plu"					=> 0,
					"numberingSequenceCode"	=> "",
					"specialAttribute"		=> null
				];
			}
		}
		
		return $items;
	}
	
	private function parsePaymentItem($order) {
		$paymentItem = [
			"name"					=> $order->payment,
			"description"			=> null,
			"count"					=> 1,
			"measureType"			=> "ks",
			"totalPrice"			=> $order->payment_price - vatCalcSimple($order->payment_price, 20),
			"totalPriceWithVat"		=> $order->payment_price,
			"unitPrice"				=> $order->payment_price - vatCalcSimple($order->payment_price, 20),
			"unitPriceWithVat"		=> $order->payment_price,
			"vat"					=> 20,
			"hasDiscount"			=> false,
			"discountName"			=> null,
			"discountPercent"		=> null,
			"discountValue"			=> null,
			"discountValueWithVat"	=> null,
			"productCode"			=> "",
			"typeId"				=> 0,
			"warehouseCode"			=> null,
			"foreignName"			=> "",
			"customText"			=> "",
			"ean"					=> "",
			"jkpov"					=> "",
			"plu"					=> 0,
			"numberingSequenceCode"	=> "",
			"specialAttribute"		=> null
		];

		return $paymentItem;
	}
	
	private function parseDeliveryItem($order) {
		$deliveryItem = [
			"name"					=> $order->delivery,
			"description"			=> null,
			"count"					=> 1,
			"measureType"			=> "ks",
			"totalPrice"			=> $order->delivery_price - vatCalcSimple($order->delivery_price, 20),
			"totalPriceWithVat"		=> $order->delivery_price,
			"unitPrice"				=> $order->delivery_price - vatCalcSimple($order->delivery_price, 20),
			"unitPriceWithVat"		=> $order->delivery_price,
			"vat"					=> 20,
			"hasDiscount"			=> false,
			"discountName"			=> null,
			"discountPercent"		=> null,
			"discountValue"			=> null,
			"discountValueWithVat"	=> null,
			"productCode"			=> "",
			"typeId"				=> 0,
			"warehouseCode"			=> null,
			"foreignName"			=> "",
			"customText"			=> "",
			"ean"					=> "",
			"jkpov"					=> "",
			"plu"					=> 0,
			"numberingSequenceCode"	=> "",
			"specialAttribute"		=> null
		];
		
		return $deliveryItem;
	}
}
	