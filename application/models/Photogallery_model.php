<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Photogallery_model extends CI_Model {
	
	public function getAllAlbums() {
		$this->db->order_by('date', 'DESC');
		
		return $this->db->get('photogallery_albums')->result_array();
	}
	
	public function getAlbumById($albumId) {	
		return $this->db->get_where('photogallery_albums', ['photogallery_albums.id' => $albumId])->row();
	}
	
	public function getPhotosOfAlbum($albumId) {
		$this->db->order_by('photogallery_photos.photo_order', 'ASC');
		return $this->db->get_where('photogallery_photos', ['album_id' => $albumId])->result_array();
	}
	
	public function insertNewAlbum($albumInfo, $albumPhotos, $albumTitles, $albumAlt) {
		$this->db->trans_start();
		
		$this->db->insert('photogallery_albums', $albumInfo);
		$albumId = $this->db->insert_id();
			
		if (!empty($albumPhotos)) {
			$photoOrder = 1;
			for ($i = 0; $i < count($albumPhotos); $i++) {
				$this->db->where('path', $albumPhotos[$i]);
				$this->db->update('photogallery_photos', ['album_id' => $albumId, 'title' => $albumTitles[$i], 'alt' => $albumAlt[$i], 'photo_order' => $photoOrder]);
				$photoOrder++;
			}
		}
		
		return $this->db->trans_complete(); 
	}
	
	public function updateAlbum($albumId, $albumInfo, $albumPhotos, $albumTitles, $albumAlt) {
		$this->db->trans_start();
		$this->db->update('photogallery_albums', $albumInfo, ['id' => $albumId]);
			
		if (!empty($albumPhotos)) {
			$photoOrder = 1;
			for ($i = 0; $i < count($albumPhotos); $i++) {
				$this->db->where('path', $albumPhotos[$i]);
				$this->db->update('photogallery_photos', ['album_id' => $albumId, 'title' => $albumTitles[$i], 'alt' => $albumAlt[$i], 'photo_order' => $photoOrder]);
				$photoOrder++;
			}
		}
		
		return $this->db->trans_complete(); 
	}
	
	public function deleteAlbum($albumId) {
		return $this->db->delete('photogallery_albums', ['id' => $albumId]);
	}
	
	public function insertImage($fileName) {
		$this->db->insert('photogallery_photos', ['path' => $fileName]);
		
		return $this->db->insert_id();
	}
	
	public function getPhotoFileName($imageId) {
		$this->db->select('path');
		
		return $this->db->get_where('photogallery_photos', ['id' => $imageId])->row('path');
	}
	
	public function deletePhoto($imageId) {
		if ($fileName = $this->getPhotoFileName($imageId)) {	
			if (file_exists('uploads/photogallery/' . $fileName)) {
				unlink('uploads/photogallery/' . $fileName);
			}
			if (file_exists('uploads/photogallery/thumbs/' . $fileName)) {
				unlink('uploads/photogallery/thumbs/' . $fileName);
			}
			
			return $this->db->delete('photogallery_photos', ['id' => $imageId]);
		} else {
			 return false;
		}
	}
}
