<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Review_model extends CI_Model{
	
	public function getReviewList() {
		$this->db->order_by('date', 'DESC');
		$this->db->select('id, header, date, published, comments, image, name, rating');
		
		return $this->db->get('review')->result_array();
	}
	
	public function getReviews() {
		$this->db->order_by('date', 'DESC');
		return $this->db->get_where('review', ['published' => 1])->result_array();
	}
	
	public function deleteComment($commentId) {
		return $this->db->delete('review_comments', ['id' => $commentId]);
	}
	
	public function getReviewComments($reviewId) {
		return $this->db->get_where('review_comments', ['review_id' => $reviewId])->result_array();
	}
	
	public function insertReview($reviewData) {
		return $this->db->insert('review', $reviewData);
	}
	
	public function insertComment($commentData) {
		return $this->db->insert('review_comments', $commentData);
	}
	
	public function getReview($reviewId = null) {
		if ($reviewId !== null) {
			return $this->db->get_where('review', ['id' => $reviewId])->row();
		} else {
			return $this->db->get('review', 1)->row();
		}
	}
	
	public function getReviewByLink($reviewLink) {
		return $this->db->get_where('review', ['link' => $reviewLink, 'published' => 1])->row() ?? false;
	}
	
	public function changePublication($reviewId, $publication) {
		return $this->db->update('review', ['published' => $publication], ['id' => $reviewId]);
	}
	
	public function changeCommenting($reviewId, $publication) {
		return $this->db->update('review', ['comments' => $publication], ['id' => $reviewId]);
	}
	
	public function updateReview($reviewId, $reviewData) {
		return $this->db->update('review', $reviewData, ['id' => $reviewId]);
	}
	
	public function deleteReview($reviewId) {
		$this->db->trans_start();
		
		$this->db->select('image');
		$fileName = $this->db->get_where('review', ['id' => $reviewId])->row()->image ?? null;
		
		if (file_exists('uploads/review/' . $fileName)) {
			unlink('uploads/review/' . $fileName);
		}
		if (file_exists('uploads/review/400/' . $fileName)) {
			unlink('uploads/review/400/' . $fileName);
		}
		if (file_exists('uploads/review/1000/' . $fileName)) {
			unlink('uploads/review/1000/' . $fileName);
		}
		
		$this->db->delete('review', ['id' => $reviewId]);
		
		return $this->db->trans_complete();
	}

	public function deletePhoto($fileName) {
		if (file_exists('uploads/review/' . $fileName)) {
			unlink('uploads/review/' . $fileName);
		}
		if (file_exists('uploads/review/400/' . $fileName)) {
			unlink('uploads/review/400/' . $fileName);
		}
		if (file_exists('uploads/review/1000/' . $fileName)) {
			unlink('uploads/review/1000/' . $fileName);
		}
	
		return $this->db->update('review', ['image' => null], ['image' => $fileName]);
	}
	
}