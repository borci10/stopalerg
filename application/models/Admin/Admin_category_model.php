<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Admin_category_model extends CI_Model{

	public function getCategory($categoryId) {
		return $this->db->get_where('categories', ['id' => $categoryId])->row();
	}
	
	public function updateCategory($categoryId, $categoryData) {
		return $this->db->update('categories', $categoryData, ['id' => $categoryId]);
	}
	
	public function insertCategory($categoryData) {	
		$this->db->trans_start();
		$this->db->select_max('category_id');
		$categoryId = $this->db->get('categories')->row()->category_id + 1;
		
		$this->db->insert('categories', array_merge($categoryData, ['category_id' => $categoryId]));
		
		return $this->db->trans_complete();
	}
	
	public function saveCategoryTree($categoryTree) {		
		$this->db->trans_start();
		$this->db->empty_table('categories');
	
		function insertChildNode($categoryId, $categoryTitle, $categoryLink, $categoryPhoto, $parentId, $childs, $show_category) {			
			$ci =& get_instance();
			$ci->db->insert('categories', [
				'category_id' 	=> $categoryId, 
				'title' 		=> $categoryTitle, 
				'link' 			=> $categoryLink . '-' . $categoryId,
				'photo' 		=> $categoryPhoto,
				'parent_id' 	=> $parentId, 
				'childrens' 	=> implode(',', array_map(function($e) {return $e->id;}, $childs)),
				'show_category'	=> $show_category,
			]);
			
			foreach($childs as $child) {
				insertChildNode($child->id, $child->title, $child->link, $child->photo, $child->parentId, $child->child, $child->show_category);
			}
		}
		
		foreach ($categoryTree as $category) {
			insertChildNode($category->id, $category->title, $category->link, $category->photo, $category->parentId, $category->child, $category->show_category);
		}
			
		return $this->db->trans_complete();
	}
	
	public function updateCategoryVisibility($categoryId, $visibility) {
		$this->db->trans_start();
		
		function updateVisibilityRecursively($categoryId, $visibility) {
			$ci =& get_instance();
			$ci->db->update('categories', ['show_category' => $visibility], ['category_id' => $categoryId]);
			
			if ($childrens = $ci->db->get_where('categories', ['category_id' => $categoryId])->row()->childrens) {
				$childrensArr = explode(',', $childrens);
				
				foreach ($childrensArr ?? [] as $childrenId) {			
					updateVisibilityRecursively($childrenId, $visibility);
				}
			}
		}
		
		if ($root = $this->db->get_where('categories', ['id' => $categoryId])->row()) {
			updateVisibilityRecursively($root->category_id, $visibility);
		}
	
		return $this->db->trans_complete();
	}
	
	public function deleteCategory($categoryId) {
		$this->db->trans_start();
		
		$this->db->select('photo');
		$fileName = $this->db->get_where('categories', ['id' => $categoryId])->row('photo');
		
		if (file_exists('uploads/categories/' . $fileName)) {
			unlink('uploads/categories/' . $fileName);
		}
		if (file_exists('uploads/categories/400/' . $fileName)) {
			unlink('uploads/categories/400/' . $fileName);
		}
		if (file_exists('uploads/categories/1000/' . $fileName)) {
			unlink('uploads/categories/1000/' . $fileName);
		}
		
		$this->db->where('id', $categoryId);
		$this->db->delete('categories');
		$this->db->where('id_category', $categoryId);
		$this->db->delete('product_category');
		
		return $this->db->trans_complete(); 
	}
	
	public function deletePhoto($fileName) {
		if (file_exists('uploads/categories/' . $fileName)) {
			unlink('uploads/categories/' . $fileName);
		}
		if (file_exists('uploads/categories/400/' . $fileName)) {
			unlink('uploads/categories/400/' . $fileName);
		}
		if (file_exists('uploads/categories/1000/' . $fileName)) {
			unlink('uploads/categories/1000/' . $fileName);
		}
	
		return $this->db->update('categories', ['photo' => null], ['photo' => $fileName]);
	}
}
