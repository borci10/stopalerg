<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Neural_network_model extends CI_Model {

	public function getUniqueOrderItems() {
		$this->db->select('DISTINCT(id_product) as itemId');
		
		return $this->db->get('order_product')->result_array();
	}
	
	public function getAffinityItems($orderId, $itemId) {
		$this->db->select('id_product as affinityItemId');
		
		return $this->db->get_where('order_product', ['id_order' => $orderId, 'id_product !=' => $itemId])->result_array();
	}
	
	public function getItemOrderIds($itemId) {
		$this->db->select('id_order');
		
		return $this->db->get_where('order_product', ['id_product' => $itemId])->result_array();
	}
	
	public function updateRecomendedProductsScores($items) {
		$this->db->trans_start();
		$this->db->truncate('product_recommended');
		
		foreach($items as $item) {
			foreach($item['affinityScore'] as $key => $value) {
				$affinityData = [
					'product_1' => $item['itemId'],
					'product_2' => $key,
					'score' 	=> $value,
				];
				$this->db->insert('product_recommended', $affinityData);
			}
		}
		
		return $this->db->trans_complete();
	}
}