<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Order_model extends CI_Model {
	public static $statuses = [
		0 => [
			'name' 	=> 'Prijatá',
			'class' => 'own-badge'
		],
		10 => [
			'name' 	=> 'Čakajúca',
			'class' => 'own-badge red',
		],
		20 => [
			'name' 	=> 'Zaplatená',
			'class' => 'own-badge blue',
		],
		25 => [
			'name' 	=> 'Vyexpedovaná',
			'class' => 'own-badge green',
		],
		30 => [
			'name' 	=> 'Vybavená',
			'class' => 'own-badge gold',
		],
		40 => [
			'name' 	=> 'Zrušená',
			'class' => 'own-badge orange',
		],
		50 => [
			'name' 	=> 'Refundovaná',
			'class' => 'own-badge purple',
		],
	];
	
	public function updateTrackingNumber($orderId, $tracking_number) {
		return $this->db->update('orders', ['tracking_number' => $tracking_number], ['id' => $orderId]);
	}
	
	public function getOrdersByFilter($offset = 0, $filter = []) {
		$this->db->select('orders.id, orders.email, orders.phone_number, orders.order_date, orders.delivery_price, orders.status, orders.order_price, orders.payment_price');
		$this->db->order_by('orders.order_date', 'DESC');
		
		return $this->db->get_where('orders', $filter, $this->config->item('sql_result_limit'), $offset)->result_array();
	}

	
	public function getTotalCountOfOrders($filter = []) {
		$this->db->select('COUNT(orders.id) as totalCount');
		
		return $this->db->get_where('orders', $filter)->row('totalCount');
	}
	
	public function getOrderItems($orderId) {
		$this->db->select('IF(order_product.id_product_child = 0, product.name, product_variables.name) AS name, order_product.id_product, order_product.quantity, order_product.discount, order_product.tax, order_product.product_price');
		$this->db->join('product', 'order_product.id_product = product.id', 'left');
		$this->db->join('product_variables', 'order_product.id_product_child = product_variables.id', 'left');
		
		return $this->db->get_where('order_product', ['order_product.id_order' => $orderId])->result_array();
	}
	
	public function getOrderDetails($orderId){
		$this->db->select('*');
		
		return $this->db->get_where('orders', ['id' => $orderId])->row();
	}
	
	public function updateOrderStatus($orderId, $status) {
		$this->db->where('id', $orderId);
		
		return $this->db->update('orders', ['status' => $status]);
	}
}