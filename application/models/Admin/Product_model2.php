<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Product_model2 extends CI_Model{
	
	public function productExist($productId) {
		return $this->db->get_where('product', ['id' => $productId])->num_rows() > 0;
	}
	
	public function variableProductExist($variableProductId) {
		return $this->db->get_where('product_variables', ['id' => $variableProductId])->num_rows() > 0;
	}
	
	public function deleteVariableProduct($variableProductId) {
		return $this->db->update('product_variables', ['deleted' => 1], ['id' => $variableProductId]);
	}
	
	public function toggleVariableProduct($variableProductId) {
		$this->db->set('show_product', '1 XOR show_product', false);
		$this->db->where('id', $variableProductId);
		
		return $this->db->update('product_variables');
	}
	
	public function getAllProductsShort($offset = 0, $filter = []) {
		$this->db->select('product.id, product.uid, product.name, product.price, product.discount, product.tax, product.quantity, product.creation_date, product.link, product.show_product, IF(product.main_photo_id IS NULL, pp1.file_name, pp2.file_name) AS file_name');
		$this->db->join('product_photo pp1', '(product.id = pp1.id_product) AND (pp1.photo_order = 0) AND (product.main_photo_id IS NULL)', 'left');
		$this->db->join('product_photo pp2', '(product.main_photo_id = pp2.id) AND (product.main_photo_id IS NOT NULL)', 'left');
		$this->db->order_by('product.id', 'asc');
		$this->db->group_by('product.id');
		
		return $this->db->get_where('product', $filter, $this->config->item('sql_result_limit'), $offset)->result_array();
	}
	
	public function getVariableProduct($variableProductId) {
		return $this->db->get_where('product_variables', ['id' => $variableProductId])->row();
	}
	
	public function getSameVariableProductNameCount($variableProductLink) {
		$this->db->select('COUNT(id) AS productCount');
		
		return $this->db->get_where('product_variables', ['link' => $variableProductLink])->row()->productCount ?? 0;
	}
	
	public function getVariableProductPhotos($variableId) {
		$this->db->order_by('photo_order', 'asc');
		
		return $this->db->get_where('product_variables_photos', ['variable_id' => $variableId])->result_array();
	}
	
	public function insertVariableProductData($variableProductData, $varibleProductPhotos) {
		$this->db->trans_start();
		$this->db->insert('product_variables', $variableProductData);
		$variableProductId = $this->db->insert_id();
		
		if (!empty($varibleProductPhotos)) {
			for ($i = 0; $i < count($varibleProductPhotos); $i++) {
				$this->db->where('id', $varibleProductPhotos[$i]);
				$this->db->update('product_variables_photos', ['variable_id' => $variableProductId, 'photo_order' => $i, 'title' => '', 'alt' => '']);
			}
		}	
		
		return $this->db->trans_complete() ? $variableProductId : false;
	}
	
	public function updateVariableProductData($variableProductId, $variableProductData, $varibleProductPhotos) {
		$this->db->trans_start();
		$this->db->update('product_variables', $variableProductData, ['id' => $variableProductId]);
		
		if (!empty($varibleProductPhotos)) {
			for ($i = 0; $i < count($varibleProductPhotos); $i++) {
				$this->db->where('id', $varibleProductPhotos[$i]);
				$this->db->update('product_variables_photos', ['variable_id' => $variableProductId, 'photo_order' => $i, 'title' => '', 'alt' => '']);
			}
		}
		
		return $this->db->trans_complete();
	}
	
	public function deleteProduct($productId) {
		$this->db->where('id', $productId);
		
		return $this->db->update('product', ['deleted' => '1']);
	}
	
	public function updateProductVisibility($productId, $visibility) {
		return $this->db->update('product', ['show_product' => $visibility], ['id' => $productId]);
	}
	
	public function deleteMultipleProducts($productIdsArr) {
		$this->db->trans_start();
		
		foreach($productIdsArr as $productId) {
			$this->db->update('product', ['deleted' => 1] , ['id' => $productId]);
		}
		
		return $this->db->trans_complete();
	}

	public function updateMultipleProductVisibility($productIdsArr, $visibility) {
		$this->db->trans_start();
		
		foreach($productIdsArr as $productId) {
			$this->db->update('product', ['show_product' => $visibility] , ['id' => $productId]);
		}
		
		return $this->db->trans_complete();
	}

	public function insertProductData($productData, $productCategoryIds, $productPhotos, $productAttributes, $productSimilar, $productVariables) {
		$this->db->trans_start();
		$this->db->insert('product', $productData);
		$insertedProductId = $this->db->insert_id();
		
		if (!empty($productCategoryIds)) {
			foreach ($productCategoryIds as $categoryId) {
				$this->db->insert('product_category', ['id_category' => $categoryId, 'id_product' => $insertedProductId]);
			}
		}
			
		if (!empty($productPhotos)) {
			for ($i = 0; $i < count($productPhotos['id']); $i++) {
				$this->db->where('id', $productPhotos['id'][$i]);
				$this->db->update('product_photo', ['id_product' => $insertedProductId, 'photo_order' => $i, 'title' => $productPhotos['title'][$i], 'alt' => $productPhotos['alt'][$i]]);
			}
		}
		
		if (!empty($productAttributes)) {
			for ($i = 0; $i < count($productAttributes['title']); $i++) {
				if ((!empty($productAttributes['title'][$i])) && (!empty($productAttributes['value'][$i]))) {
					$this->db->insert('product_attributes', ['id_product' => $insertedProductId, 'title' => $productAttributes['title'][$i], 'value' => $productAttributes['value'][$i]]);
				}
			}
		}
		
		if (!empty($productSimilar)) {
			foreach ($productSimilar as $productSimilarId) {
				$this->db->insert('product_similar', ['id_similar_product' => $productSimilarId, 'id_product' => $insertedProductId]);
			}	
		}
		
		if (!empty($productVariables)) {
			foreach ($productVariables as $productVariableId) {
				$this->db->update('product_variables', ['id_product' => $insertedProductId], ['id' => $productVariableId]);
			}	
		}
		
		return $this->db->trans_complete();
	}
	
	public function updateProductData($productId, $productData, $productCategoryIds, $productPhotos, $productAttributes, $productSimilar) {
		// $photoOrder = ($this->getLastPhotoOrder($productId)->photo_order ?? 0) + 1;
		
		$this->db->trans_start();
		$this->db->update('product', $productData, ['id' => $productId]);
		$this->db->delete('product_category', ['id_product' => $productId]);
		$this->db->delete('product_attributes', ['id_product' => $productId]);
		$this->db->delete('product_similar', ['id_product' => $productId]);
		
		if (!empty($productCategoryIds)) {
			foreach ($productCategoryIds as $categoryId) {
				$this->db->insert('product_category', ['id_category' => $categoryId, 'id_product' => $productId]);
			}	
		}

		if (!empty($productPhotos)) {
			for ($i = 0; $i < count($productPhotos['id']); $i++) {
				$this->db->where('id', $productPhotos['id'][$i]);
				$this->db->update('product_photo', ['id_product' => $productId, 'photo_order' => $i, 'title' => $productPhotos['title'][$i], 'alt' => $productPhotos['alt'][$i]]);
			}
		}

		if (!empty($productAttributes)) {
			for ($i = 0; $i < count($productAttributes['title']); $i++) {
				if ((!empty($productAttributes['title'][$i])) && (!empty($productAttributes['value'][$i]))) {
					$this->db->insert('product_attributes', ['id_product' => $productId, 'title' => $productAttributes['title'][$i], 'value' => $productAttributes['value'][$i]]);
				}
			}
		}
		
		if (!empty($productSimilar)) {
			foreach ($productSimilar as $productSimilarId) {
				$this->db->insert('product_similar', ['id_similar_product' => $productSimilarId, 'id_product' => $productId]);
			}	
		}
		
		return $this->db->trans_complete();
	}
	
	public function insertVariableProductBlindPhoto($fileName) {
		$this->db->insert('product_variables_photos', ['file_name' => $fileName, 'user_id' => $this->session->userdata('id')]);
		
		return $this->db->insert_id();
	}
	
	public function deleteVariablePhoto($fileId) {
		if ($fileName = $this->db->get_where('product_variables_photos', ['id' => $fileId])->row('file_name')) {
			if (file_exists('uploads/product_variables/' . $fileName)) {
				unlink('uploads/product_variables/' . $fileName);
			}
			if (file_exists('uploads/product_variables/thumbs/' . $fileName)) {
				unlink('uploads/product_variables/thumbs/' . $fileName);
			}
			return $this->db->delete('product_variables_photos', ['file_name' => $fileName]);
		} else {
			return false;
		}
	}
	
	public function deletePhoto($imgId) {
		if ($fileName = $this->db->get_where('product_photo', ['id' => $imgId])->row('file_name')) {
			if (file_exists('uploads/products/' . $fileName)) {
				unlink('uploads/products/' . $fileName);
			}
			if (file_exists('uploads/products/thumbs/' . $fileName)) {
				unlink('uploads/products/thumbs/' . $fileName);
			}
			return $this->db->delete('product_photo', ['id' => $imgId]);
		} else {
			return false;
		}
	}
}