<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Dashboard_model extends CI_Model {
	
	public function getNumberOfOrders() {
		$this->db->select('COUNT(*) as orderCount');
		
		return $this->db->get('orders')->row()->orderCount ?? 0;
	}
	
	public function getNumberOfRegisteredCustomers() {
		$this->db->select('COUNT(*) as customerCount');
		
		return $this->db->get('users')->row()->customerCount ?? 0;
	}
	
	public function getNumberOfActiveCarts() {
		$this->db->select('COUNT(DISTINCT(carts.id_user)) as activeCartsCount');
		
		return $this->db->get('carts')->row()->activeCartsCount ?? 0;
	}
	
	public function getValueOfOrders() {
		$this->db->select('SUM(orders.delivery_price + orders.order_price) AS orderTotalPrice');
		
		return $this->db->get('orders')->row()->orderTotalPrice ?? 0;
	}
	
	public function getOrderCountChart() {
		$this->db->select('COUNT(*) as count, MONTH(order_date) AS month');
		$this->db->group_by('MONTH(order_date)');
		$this->db->order_by('order_date');
		
		return $this->db->get_where('orders', ['YEAR(order_date)' => date('Y')])->result_array();
	}
	
	public function getOrderSumCountChart() {
		$this->db->select('SUM(orders.delivery_price + orders.order_price) AS count, MONTH(order_date) AS month');
		$this->db->group_by('MONTH(order_date)');
		$this->db->order_by('order_date');
		
		return $this->db->get_where('orders', ['YEAR(order_date)' => date('Y')])->result_array();
	}
}