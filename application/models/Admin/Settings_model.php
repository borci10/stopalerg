<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Settings_model extends CI_Model{
	
	public function getSingleValue($key) {
		return $this->db->get_where('settings', ['key' => $key])->row('value');
	}
	
	public function getAllSettingsParsed() {
		return $this->parseSettings($this->db->get('settings')->result_array());
	}
	
	public function setMultipleValues($data) {
		$this->db->trans_start();
		
		foreach($data as $key => $value) {
			$this->db->update('settings', ['value' => $value], ['key' => $key]);
		}
		
		return $this->db->trans_complete();
	}
	
	private function parseSettings($settingsArr) {
		$output = [];
		
		foreach($settingsArr as $setting) {
			$output[$setting['key']] = $setting['value'];
		}
		
		return $output;
	}
}