<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Customers_model extends CI_Model{
	
	public function getAllCustomers() {
		return $this->db->get('users')->result_array();
	}
	
	public function getFilteredCustomers($offset = 0, $filter = []) {
		$this->db->select('users.*, SUM(orders.order_price) as orders_price');
		$this->db->join('orders', 'users.id = orders.user_id', 'left');
		$this->db->group_by('users.id');
		
		return $this->db->get_where('users', $filter, $this->config->item('sql_result_limit'), $offset)->result_array();
	}
	
	public function getCustomerDetails($customerId) {
		return $this->db->get_where('users', ['id' => $customerId])->row();
	}
	
	public function getCustomerOrders($customerId) {
		$this->db->select('orders.id, orders.email, orders.phone_number, orders.order_date, orders.delivery, orders.payment, orders.delivery_price, orders.status, orders.order_price');
		$this->db->order_by('orders.order_date', 'DESC');
		
		return $this->db->get_where('orders', ['user_id'])->result_array();
	}
	
	public function getTotalCountOfCustomers($filter = []) {
		$this->db->select('COUNT(users.id) as totalCount');
		
		return $this->db->get_where('users', $filter)->row('totalCount');
	}
	
}