<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Payment_model extends CI_Model {
	
	public function getPaymentList() {
		return $this->db->get('payment')->result_array();
	}
	
	public function getPayment($paymentId) {
		return $this->db->get_where('payment', ['id' => $paymentId])->row();
	}
	
	public function getPaymentUid($paymentId) {
		return $this->db->get_where('payment', ['id' => $paymentId])->row('uid');
	}
	
	public function updatePayment($paymentId, $paymentData) {
		return $this->db->update('payment', $paymentData, ['id' => $paymentId]);
	}
	
}