<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Email_model extends CI_Model{
	
	public function sendEmail($email, $subject, $message, $orderId = 0, $attachment = null) {	
		$this->email->from($this->config->item('eshop_email'), $this->config->item('eshop_name'));
		$this->email->to($email);
		$this->email->subject($subject);
		$this->email->message($message);

		if ($attachment) {
			$this->email->attach($attachment, 'attachment', generateVarSymbol($orderId), 'application/pdf');	
		}
		
		return $this->email->send();
	}

	public function sendEmailToOwner($email, $subject, $message) {
		$this->email->from($this->config->item('eshop_email'), $this->config->item('eshop_name'));
		$this->email->to($this->config->item('eshop_email'));
		$this->email->reply_to($email);
		$this->email->subject($subject);
		$this->email->message($message);

		return $this->email->send();
	}
}