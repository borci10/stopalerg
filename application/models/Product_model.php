<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Product_model extends CI_Model{
	
	public function getFilteredproducts($filter = [], $offset = 0, $limit = null) {
		if ($limit == null) {
			$limit = $this->config->item('sql_result_limit');
		}
		
		$this->db->select('product.id, product.uid, product.name, product.link, product.short_description, product.long_description, product.price, product.discount, product.tax, product.quantity, product.rating, product.creation_date, product.show_product, product.adults, IF(product.main_photo_id IS NULL, pp1.file_name, pp2.file_name) AS file_name');
		// $this->db->join('product_attributes', 'product.id = product_attributes.id_product', 'left');
		$this->db->join('product_photo pp1', '(product.id = pp1.id_product) AND (pp1.photo_order = 0) AND (product.main_photo_id IS NULL)', 'left');
		$this->db->join('product_photo pp2', '(product.main_photo_id = pp2.id) AND (product.main_photo_id IS NOT NULL)', 'left');
		$this->db->group_by('product.id');

		$this->parseOrderBy();

		return $this->db->get_where('product', $filter, $limit, $offset)->result_array();
	}
	
	public function getSearchproducts($expression, $filter) {
		$this->db->select('product.id, product.uid, product.name, product.link, product.short_description, product.price, product.discount, product.tax, product.quantity, product.rating, product.creation_date, product.show_product, product.adults, IF(product.main_photo_id IS NULL, pp1.file_name, pp2.file_name) AS file_name');
		// $this->db->join('product_attributes', 'product.id = product_attributes.id_product', 'left');
		$this->db->join('product_photo pp1', '(product.id = pp1.id_product) AND (pp1.photo_order = 0) AND (product.main_photo_id IS NULL)', 'left');
		$this->db->join('product_photo pp2', '(product.main_photo_id = pp2.id) AND (product.main_photo_id IS NOT NULL)', 'left');
		$this->db->like('name', $expression);
		$this->db->or_where('uid', $expression);
		$this->db->group_by('product.id');
		
		$this->parseOrderBy();
		
		return $this->db->get_where('product', $filter, $this->config->item('sql_result_limit'))->result_array();
	}
	
	public function getBreadCrumbs($categoryId) {
		$categoryTrace = [];
		$this->db->select('category_id, title, link, parent_id');
		$actualCategory = $this->db->get_where('categories', ['category_id' => $categoryId])->row();
		$categoryTrace[] = $actualCategory;
		
		while($actualCategory->parent_id != null) {
			$this->db->select('category_id, title, link, parent_id');
			$actualCategory = $this->db->get_where('categories', ['category_id' => $actualCategory->parent_id])->row();
			$categoryTrace[] = $actualCategory;
		}
		
		return array_reverse($categoryTrace);
	}
	
	public function getSuggestedProductNames($expr) {
		$this->db->select('DISTINCT(name) AS name');
		$this->db->like('name', $expr);
		
		if ($this->session->userdata('admin') == 1) {
			return array_column($this->db->get_where('product',  ['deleted' => 0], 10)->result_array(), 'name');
		} else {
			return array_column($this->db->get_where('product',  ['deleted' => 0, 'show_product' => 1], 10)->result_array(), 'name');
		}
	}
	
	public function getAllAtrributeValue($attr) {
		$this->db->select('DISTINCT(value) AS value');
		
		return $this->db->get_where('product_attributes', ['title' => $attr])->result_array();
	}
	
	public function getTotalCountOfproducts($filter = []) {
		$this->db->select('COUNT(product.id) as totalCount');
		
		return $this->db->get_where('product', $filter)->row('totalCount');
	}
	
	public function getProductReviews($productId) {
		$this->db->select('product_reviews.*, users.name');
		$this->db->join('users', 'product_reviews.user_id = users.id');
		
		return $this->db->get_where('product_reviews', ['product_id' => $productId])->result_array();
	}
	
	public function checkIfUserAlreadyDidReview($productId, $userId) {
		$this->db->select('*');
		
		return $this->db->get_where('product_reviews', ['product_id' => $productId, 'user_id' => $userId])->num_rows() != 0;	
	}
	
	public function insertProductReview($productId, $productReview, $productRating) {
		$this->db->trans_start();
		$this->db->insert('product_reviews', $productReview);
		$this->db->update('product', ['rating' => $productRating], ['id' => $productId]);
		
		return $this->db->trans_complete();
	}
	
	public function getProductRatings($productId) {
		$this->db->select('rating');
		
		return $this->db->get_where('product_reviews', ['product_id' => $productId])->result_array();
	}
	
	public function getProductSimilar($productId) {
		$this->db->select('product.id, product.name, product.price');
		$this->db->join('product', 'product_similar.id_similar_product = product.id');
		
		return $this->db->get_where('product_similar', ['product_similar.id_product' => $productId])->result_array();
	}
	
	public function getProductShort($productId) {
		$this->db->select('product.id, product.name, product.price, product.discount, product.tax, product.quantity, IF(product.main_photo_id IS NULL, pp1.file_name, pp2.file_name) AS file_name');
		$this->db->join('product_photo pp1', '(product.id = pp1.id_product) AND (pp1.photo_order = 0) AND (product.main_photo_id IS NULL)', 'left');
		$this->db->join('product_photo pp2', '(product.main_photo_id = pp2.id) AND (product.main_photo_id IS NOT NULL)', 'left');


		return $this->db->get_where('product', ['product.id' => $productId, 'product.show_product' => 1, 'product.deleted' => 0])->row();
	}
	
	public function getProductVariableShort($variableProductId) {
		$this->db->select('product_variables.*');
		
		return $this->db->get_where('product_variables', ['product_variables.id' => $variableProductId, 'product_variables.show_product' => 1, 'product_variables.deleted' => 0])->row();
	}
	
	public function getProductVariables($productId) {
		$this->db->select('product_variables.*, product_variables_photos.file_name');
		$this->db->join('product_variables_photos', 'product_variables.id = product_variables_photos.variable_id AND product_variables_photos.photo_order = 0', 'left');
		
		return $this->db->get_where('product_variables', ['product_variables.id_product' => $productId, 'product_variables.show_product' => 1, 'product_variables.deleted' => 0])->result_array();
	}
	
	public function getProductVariableByLink($productId, $variableProductLink) {
		$this->db->select('product_variables.*, product_variables_photos.file_name');
		$this->db->join('product_variables_photos', 'product_variables.id = product_variables_photos.variable_id AND product_variables_photos.photo_order = 0', 'left');
		
		return $this->db->get_where('product_variables', ['product_variables.id_product' => $productId, 'product_variables.show_product' => 1, 'product_variables.deleted' => 0, 'product_variables.link' => $variableProductLink])->row();
	}

	public function getProductSimilarAllInfo($productId) {
		$this->db->select('product.id, product.uid, product.name, product.link, product.short_description, product.price, product.discount, product.tax, product.quantity, product.rating, product.creation_date, product.show_product, product.adults, IF(product.main_photo_id IS NULL, pp1.file_name, pp2.file_name) AS file_name');
		$this->db->join('product', 'product_similar.id_similar_product = product.id', 'left');
		$this->db->join('product_photo pp1', '(product.id = pp1.id_product) AND (pp1.photo_order = 0) AND (product.main_photo_id IS NULL)', 'left');
		$this->db->join('product_photo pp2', '(product.main_photo_id = pp2.id) AND (product.main_photo_id IS NOT NULL)', 'left');
		$this->db->group_by("product.id");
		
		return $this->db->get_where('product_similar', ['product_similar.id_product' => $productId], 4)->result_array();
	}
	
	public function getProductRecommanded($productId, $limit = 4) {
		$this->db->select('product.id, product.uid, product.name, product.link, product.short_description, product.price, product.discount, product.tax, product.quantity, product.rating, product.creation_date, product.show_product, product.adults, IF(product.main_photo_id IS NULL, pp1.file_name, pp2.file_name) AS file_name');
		$this->db->join('product', 'product_recommended.product_2 = product.id', 'left');
		$this->db->join('product_photo pp1', '(product.id = pp1.id_product) AND (pp1.photo_order = 0) AND (product.main_photo_id IS NULL)', 'left');
		$this->db->join('product_photo pp2', '(product.main_photo_id = pp2.id) AND (product.main_photo_id IS NOT NULL)', 'left');
		$this->db->group_by("product.id");
		$this->db->order_by("product_recommended.score", "desc");
		
		return $this->db->get_where('product_recommended', ['product_recommended.product_1' => $productId], $limit)->result_array();
	}
	
	public function getProductPrice($productId) {
		$this->db->select('product.id, product.price, product.discount, product.tax');
		
		return $this->db->get_where('product', ['id' => $productId])->row();
	}
	
	public function getProductQuantity($productId) {
		$this->db->select('product.quantity');
		
		return $this->db->get_where('product', ['id' => $productId])->row()->quantity;
	}
	
	public function getProductDeleted($productId) {
		$this->db->select('deleted');
		
		return $this->db->get_where('product', ['id' => $productId])->row()->deleted;
	}
	
	public function getMultipleItemsById($productIds) {
		$this->db->select('product.*');
		$this->db->where_in('product.id', $productIds);

		return $this->db->get('product')->result_array();
	}
	
	public function getMultipleOutOfStockItemsById($productIds, $stockLimit) {
		$this->db->select('product.id, product.uid, product.name,  product.link, product.price, product.discount, product.tax, product.quantity');
		$this->db->where_in('product.id', $productIds);
		$this->db->where('product.quantity <=', $stockLimit);

		return $this->db->get('product')->result_array();
	}
	
	public function getTotalCountOfProductsCategory($idCategory, $filter) {
		$this->db->select('COUNT(DISTINCT(product.id)) as totalCount');
		$this->db->join('product', 'product_category.id_product = product.id');
		
		return $this->db->get_where('product_category', array_merge($filter, ['product_category.id_category' => $idCategory]), $this->config->item('sql_result_limit'))->row('totalCount');
	}
	
	public function getFilteredProductByCaregoryId($idCategory, $filter, $offset = 0, $limit = null) {
		if ($limit == null) {
			$limit = $this->config->item('sql_result_limit');
		}

		$this->db->select('product.id, product.uid, product.name,  product.link, product.short_description, product.price, product.discount, product.tax, product.quantity, product.rating, product.creation_date, product.show_product, product.adults, IF(product.main_photo_id IS NULL, pp1.file_name, pp2.file_name) AS file_name');
		$this->db->join('product', 'product_category.id_product = product.id');
		$this->db->join('product_photo pp1', '(product.id = pp1.id_product) AND (pp1.photo_order = 0) AND (product.main_photo_id IS NULL)', 'left');
		$this->db->join('product_photo pp2', '(product.main_photo_id = pp2.id) AND (product.main_photo_id IS NOT NULL)', 'left');
		$this->db->where('product_category.id_category', $idCategory);
		$this->db->group_by('product.id');
		
		$this->parseOrderBy();
		
		return $this->db->get_where('product_category', $filter, $limit, $offset)->result_array();
	}
	
	public function getProductsFromIdList($productIdList) {
		if (!empty($productIdList)) {
			$this->db->select('product.id, product.uid, product.name,  product.link, product.short_description, product.price, product.discount, product.tax, product.quantity, product.rating, product.creation_date, product.show_product, product.adults, IF(product.main_photo_id IS NULL, pp1.file_name, pp2.file_name) AS file_name');
			$this->db->join('product_photo pp1', '(product.id = pp1.id_product) AND (pp1.photo_order = 0) AND (product.main_photo_id IS NULL)', 'left');
			$this->db->join('product_photo pp2', '(product.main_photo_id = pp2.id) AND (product.main_photo_id IS NOT NULL)', 'left');
			$this->db->group_by("product.id");
			$this->db->where_in('product.id', $productIdList);

			return $this->db->get('product')->result_array();
		} else {
			return [];
		}
	}
	
	public function getFavouriteProducts() {
		$this->db->select('DISTINCT(product_favourite.id_product)');
		
		return array_column($this->db->get_where('product_favourite', ['product_favourite.id_user' => $this->session->userdata('id')])->result_array(), 'id_product');
	}
	
	public function insertProductIntoFavourite($productId) {
		return $this->db->insert('product_favourite', ['id_user' => $this->session->userdata('id'), 'id_product' => $productId]);
	}
	
	public function getFavouriteProductsNumber() {
		return count($this->session->userdata('favourite_products') ?? []) ?? 0;
	}
	
	public function deleteProductFromFavourite($productId) {
		return $this->db->delete('product_favourite', ['id_product' => $productId]);	
	}
	
	public function getCompareProducts() {
		$this->db->select('DISTINCT(product_compare.id_product)');
		
		return array_column($this->db->get_where('product_compare', ['product_compare.id_user' => $this->session->userdata('id')])->result_array(), 'id_product');
	}
	
	public function insertProductIntoCompare($productId) {
		return $this->db->insert('product_compare', ['id_user' => $this->session->userdata('id'), 'id_product' => $productId]);
	}
	
	public function getComparedProductsNumber() {
		return count($this->session->userdata('compared_products') ?? []) ?? 0;
	}
	
	public function deleteProductFromCompare($productId) {
		return $this->db->delete('product_compare', ['id_product' => $productId]);	
	}
	
	public function getAllProductNames() {
		$this->db->select('DISTINCT(name)');
		
		return array_column($this->db->get_where('product',  ['deleted' => 0])->result_array(), 'name');
	}
	
	public function getAllProducts() {
		return $this->db->get_where('product',  ['deleted' => 0])->result_array();
	}
    
	public function getProductById($productId) {
		$this->db->select('product.*');
		
		return $this->db->get_where('product', ['id' => $productId])->row();
	}
	
	public function productExists($productId) {
        return $this->db->get_where('product', ['id' => $productId])->num_rows() > 0;
    }

	
	public function getProductByLink($productLink) {
		$this->db->select('product.*');
		
		return $this->db->get_where('product', ['link' => $productLink])->row();
	}
	
	public function getShortProductById($productId) {
		$this->db->select('product.id, product.deleted, product.name, product.short_description, product.link, product.quantity, product.discount, product.tax, product.price, IF(product.main_photo_id IS NULL, pp1.file_name, pp2.file_name) AS file_name');
		$this->db->join('product_photo pp1', '(product.id = pp1.id_product) AND (pp1.photo_order = 0) AND (product.main_photo_id IS NULL)', 'left');
		$this->db->join('product_photo pp2', '(product.main_photo_id = pp2.id) AND (product.main_photo_id IS NOT NULL)', 'left');
		
		return $this->db->get_where('product', ['product.id' => $productId])->row();
	}
	
	public function getShortProductByLink($productLink) {
		$this->db->select('product.*, IF(product.main_photo_id IS NULL, pp1.file_name, pp2.file_name) AS file_name');
		$this->db->join('product_photo pp1', '(product.id = pp1.id_product) AND (pp1.photo_order = 0) AND (product.main_photo_id IS NULL)', 'left');
		$this->db->join('product_photo pp2', '(product.main_photo_id = pp2.id) AND (product.main_photo_id IS NOT NULL)', 'left');
		
		return $this->db->get_where('product', ['product.link' => $productLink])->row();
	}
	
	public function getProductCategories($productId) {
		$this->db->select('id_category');
		
		return $this->db->get_where('product_category', ['id_product' => $productId])->result_array();
	}
	
	public function getCategoryNameById($categoryId) {
		$this->db->select('title');
		
		return $this->db->get_where('categories', ['category_id' => $categoryId])->row();
	}
	
	public function getCategoryByLink($categoryLink) {
		return $this->db->get_where('categories', ['link' => $categoryLink])->row();
	}
	
	public function getProductPhotosById($productId) {
		$this->db->select('id, title, alt, file_name, photo_order');
		$this->db->order_by("photo_order", "asc");
		
		return $this->db->get_where('product_photo', ['id_product' => $productId])->result_array();
	}
	
	public function getProductVariablePhotosById($variableProductId) {
		$this->db->select('id, title, alt, file_name, photo_order');
		$this->db->order_by("photo_order", "asc");
		
		return $this->db->get_where('product_variables_photos', ['variable_id' => $variableProductId])->result_array();
	}
	
	private function parseOrderBy() {
		$orderBy = null;
		
		if ($this->input->get('orderBy')) {
			$orderBy = $this->input->get('orderBy');
		}
		if ($this->input->post('orderBy')) {
			$orderBy = $this->input->post('orderBy');
		}
		
		switch ($orderBy) {
			case 'priceDesc':
				$this->db->order_by('price', 'desc');
				break;
			case 'priceAsc':
				$this->db->order_by('price', 'asc');
				break;
			case 'recommendedDesc':
				$this->db->order_by('recomended', 'desc');
				break;
			case 'soldDesc':
				$this->db->order_by('sold', 'desc');
				break;
			case 'ratingDesc':
				// $this->db->order_by('product_reviews.rating', 'desc');
				// $this->db->group_by('product_reviews.rating');
				break;
			default:
				// $this->db->order_by('product_reviews.rating', 'desc');
				// $this->db->group_by('product_reviews.rating');
		}
	}
}