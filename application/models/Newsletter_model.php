<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Newsletter_model extends CI_Model{
	
	public function insertEmail($email) {
		return $this->db->insert('newsletter_emails', $email);
	}
	
	public function getNewsletter($newsletterId) {
		return $this->db->get_where('newsletter_emails', ['id' => $newsletterId])->row();
	}
	
	public function getAllNewsletters() {
		return $this->db->get('newsletter_emails')->result_array();
	}
	
	public function insertNewsletter($newsletterData) {
		return $this->db->insert('newsletter_emails', $newsletterData);
	}

	public function updateNewsletter($newsletterId, $newsletterData) {
		return $this->db->update('newsletter_emails', $newsletterData, ['id' => $newsletterId]);
	}
	
	public function deleteNewsletter($newsletterId) {
		return $this->db->delete('newsletter_emails', ['id' => $newsletterId]);	
	}
}