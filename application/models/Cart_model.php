<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Cart_model extends CI_Model{
	
	public function insertOrder() {
		$this->db->trans_start();
		
		$this->db->insert('orders', [
			'name' 			 => $this->session->userdata('name'),
			'user_id' 		 => $this->session->userdata('id'),
			'surname'		 => $this->session->userdata('surname'),
			'email' 		 => $this->session->userdata('email'),
			'phone_number' 	 => $this->session->userdata('phone_number'),
			'city' 			 => $this->session->userdata('city'),
			'street'	 	 => $this->session->userdata('street'),
			'zip_code' 		 => $this->session->userdata('zip_code'),
			'diff_delivery'  => $this->session->userdata('diff_delivery'),
			'delivery_name'  => $this->session->userdata('delivery_name'),
			'delivery_surname'  => $this->session->userdata('delivery_surname'),
			'delivery_street'=> $this->session->userdata('delivery_street'),
			'delivery_city'  => $this->session->userdata('delivery_city'),
			'delivery_zip_code' => $this->session->userdata('delivery_zip_code'),
			'delivery' 		 => $this->session->userdata('delivery'),
			'delivery_price' => $this->session->userdata('delivery_price'),
			'payment' 		 => $this->session->userdata('payment'),
			'payment_price'  => $this->session->userdata('payment_price'),
			'order_price' 	 => $this->session->userdata('cart_price'),
			'status' 		 => 0
		]);

		$insertedId = $this->db->insert_id();		
		
		foreach($this->session->userdata('shoppingCart') as $productId => $productVariation) {
			$parentProductInfo = $this->product_model->getProductShort($productId);
			foreach($productVariation as $productVariableId => $itemCount) {
				$itemCountInt = (int)$itemCount['itemCount'];
				if ($productVariableId == 0) {
					$product = [
						'id' 		=> $parentProductInfo->id,
						'child_id' 	=> 0,
						'name' 		=> $parentProductInfo->name,
						'price' 	=> $parentProductInfo->price,
						'discount' 	=> $parentProductInfo->discount,
						'tax' 		=> $parentProductInfo->tax,
						'quantity' 	=> $parentProductInfo->quantity,
					];
					
					$this->db->where('id', $productId);
					$this->db->set('quantity', 'quantity - ' . $itemCountInt, false);
					$this->db->set('sold', 'sold + ' . $itemCountInt, false);
					$this->db->update('product');
				} else {
					$variableProductInfo = $this->product_model->getProductVariableShort($productVariableId);
					$product = [
						'id' 		=> $parentProductInfo->id,
						'child_id' 	=> $variableProductInfo->id,
						'name' 		=> $variableProductInfo->name,
						'price' 	=> $variableProductInfo->price,
						'discount' 	=> $variableProductInfo->discount,
						'tax' 		=> $parentProductInfo->tax,
						'quantity' 	=> $variableProductInfo->quantity,
					];
					
					$this->db->where('id', $product['id']);
					$this->db->set('quantity', 'quantity - ' . $itemCountInt, false);
					$this->db->set('sold', 'sold + ' . $itemCountInt, false);
					$this->db->update('product_variables');
				}
				$this->db->insert('order_product', ['id_order' => $insertedId, 'id_product' => $product['id'], 'id_product_child' => $product['child_id'], 'quantity' => $itemCountInt, 'tax' => $product['tax'], 'discount' => $product['discount'], 'product_price' => $product['price']]);	
			}
		}
		
		if ($this->session->userdata('logged_in')) {
			$this->db->delete('carts', ['id_user' => $this->session->userdata('id')]);
		}
		
		return $this->db->trans_complete() ? $insertedId : false;
	}
	
	public function updateOrderStatus($orderId, $status) {
		$this->db->where('id', $orderId);
		
		return $this->db->update('orders', ['status' => $status]);
	}
	
	public function getSupplierPricesFromArrId($suppliersIds) {
		$this->db->select('SUM(suppliers.price) as price');
		$this->db->where_in('suppliers.id', $suppliersIds);

		return $this->db->get('suppliers')->row()->price ?? 0;
	}
		
	public function getSupplierDetail($supplierId) {
		return $this->db->get_where('suppliers', ['id' => $supplierId])->row();
	}
	
	public function getDelivery($deliveryId) {
		return $this->db->get_where('delivery', ['id' => $deliveryId])->row();
	}
	
	public function getPayment($paymentId) {
		return $this->db->get_where('payment', ['id' => $paymentId])->row();
	}
	
	public function getAllDeliveryOptions() {
		return $this->db->get_where('delivery', ['deleted' => 0])->result_array();
	}
	
	public function getAllPaymentOptions() {
		return $this->db->get_where('payment', ['deleted' => 0])->result_array();
	}

	public function checkShoppingCartStepDone() {
		return !empty($this->session->userdata('shoppingCart'));
	}
	
	public function getUserShoppingCart() {
		$output = [];
		$cart 	= $this->db->get_where('carts', ['id_user' => $this->session->userdata('id')])->result_array();
		
		foreach ($cart as $item) {
			$output[$item['id_product']][$item['id_product_child']] = ['itemCount' => $item['quantity']];
		}
		
		return $output;
	}
	
	public function getCartTotalPrice() {
		$sum = 0;
		
		$this->db->select('product.id, carts.id_product_child, IF(carts.id_product_child = 0, product.discount, product_variables.discount) AS discount, IF(carts.id_product_child = 0, product.price, product_variables.price) AS price, product.tax, carts.quantity');
		$this->db->join('product', 'carts.id_product = product.id', 'left');
		$this->db->join('product_variables', 'carts.id_product_child = product_variables.id', 'left');
		
		$cart = $this->db->get_where('carts', ['id_user' => $this->session->userdata('id')])->result_array();
		
		foreach ($cart as $item) {
			$sum += priceCalc($item['price'], $item['quantity'], $item['discount'], $item['tax'] / 100);
		}
		
		return $sum;
	}
	
	public function insertProductIntoCart($productId, $productChildId, $quantity) {
		$this->db->trans_start();
		
		$this->db->select('carts.id, carts.quantity');
		$productStack = $this->db->get_where('carts', ['id_user' => $this->session->userdata('id'), 'id_product' => $productId, 'id_product_child' => $productChildId])->row();
		
		if (empty($productStack)) {
			$this->db->insert('carts', ['id_user' => $this->session->userdata('id'), 'id_product' => $productId, 'id_product_child' => $productChildId, 'quantity' => $quantity]);
		} else {
			$this->db->update('carts', ['quantity' => $quantity + $productStack->quantity], ['id_user' => $this->session->userdata('id'), 'id_product' => $productId, 'id_product_child' => $productChildId]);
		}
		
		return $this->db->trans_complete();
	}
	
	public function deleteProductFromCart($productId, $productChildId, $quantity) {
		$this->db->trans_start();
		
		$this->db->select('carts.id, carts.quantity');
		$productStack = $this->db->get_where('carts', ['id_user' => $this->session->userdata('id'), 'id_product' => $productId, 'id_product_child' => $productChildId])->row();
		
		if ($quantity == -1) {
			$this->db->delete('carts', ['id_user' => $this->session->userdata('id'), 'id_product' => $productId, 'id_product_child' => $productChildId]);
		} else {
			if (($productStack->quantity - $quantity) > 0) {
				$this->db->update('carts', ['quantity' => $productStack->quantity - $quantity], ['id_user' => $this->session->userdata('id'), 'id_product' => $productId, 'id_product_child' => $productChildId]);
			}
		}
		
		return $this->db->trans_complete();
	}
	
	public function checkPersonalInfoStepDone() {
		if ($this->session->has_userdata('name') &&
			$this->session->has_userdata('surname') &&
			$this->session->has_userdata('email') &&
			$this->session->has_userdata('city') &&
			$this->session->has_userdata('street') &&
			$this->session->has_userdata('zip_code') &&
			$this->session->has_userdata('phone_number')
		) {
			return true;
		} else {
			return false;
		}
	}
	
	public function checkPaymentInfoStepDone() {
		if ($this->session->has_userdata('delivery') &&
			$this->session->has_userdata('delivery_price') &&
			$this->session->has_userdata('payment')
		) {
			return true;
		} else {
			return false;
		}
	}
	
	public function returnProductsToStock() {
		$this->db->trans_start();
		$productList = $this->session->userdata('shoppingCart');
		
		if (!empty($productList)) {
			foreach($productList as $productId => $productVariable) {
				foreach($productVariable as $productVariableId => $itemCount) {
					if ($productVariableId == 0) {
						$this->db->where('id', $productId);
						$this->db->set('quantity', 'quantity + ' . (int)$itemCount, false);
						$this->db->set('sold', 'sold - ' . (int)$itemCount, false);
						$this->db->update('product');
					} else {
						$this->db->where('id', $productVariableId);
						$this->db->set('quantity', 'quantity + ' . (int)$itemCount, false);
						$this->db->set('sold', 'sold - ' . (int)$itemCount, false);
						$this->db->update('product_variables');
					}
				}
			}
		}

		return $this->db->trans_complete();
	}
	
	//payments

	public function TrustPayPayment() {
		$REF 		= $this->session->userdata('orderId');
		$token 		= md5($this->config->item('salt') . $REF);
		$baseUrl 	= "https://playground.trustpay.eu/mapi5/Card/Pay";
		$AID 		= $this->config->item('trust_pay_AID');
		$AMT 		= $this->session->userdata('cart_price') + $this->session->userdata('delivery_price') + $this->session->userdata('payment_price');
		$CUR 		= "EUR";
		$RURL		= urlencode(base_url() . 'cart/successPayment/' . $REF . '/' . $token);
		$CURL		= urlencode(base_url() . 'cart/cancelPayment/' . $REF . '/' . $token);
		$EURL		= urlencode(base_url() . 'cart/errorPayment/' . $REF . '/' . $token);
		$secretKey	= $this->config->item('trust_pay_KEY');
		$sigData 	= sprintf("%d%s%s%s", $AID, number_format($AMT, 2, '.', ''), $CUR, $REF);
		$SIG 		= GetSignature($secretKey, $sigData);

		$url = sprintf(
			"%s?AID=%d&AMT=%s&CUR=%s&REF=%s&SIG=%s&RURL=%s&CURL=%s&EURL=%s", 
			$baseUrl, $AID, number_format($AMT, 2, '.', ''), $CUR, urlencode($REF), $SIG, $RURL, $CURL, $EURL);
		header("Location: $url");
		exit();
	}
}