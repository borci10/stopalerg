<?php
	if ($this->session->userdata('admin') == 1) {
		?>
			<div class="modal fade" id="textsModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
				<div class="modal-dialog modal-dialog-centered" role="document">
					<div class="modal-content">
						<form id="textsModalForm" method="post">
							<div class="modal-header">
								<h5 class="modal-title" id="exampleModalLongTitle">Editácia textu</h5>
								<button type="button" class="close" data-dismiss="modal" aria-label="Close">
									<span aria-hidden="true">&times;</span>
								</button>
							</div>
							<div class="modal-body">
								<input type="hidden" id="textsFormHiddenTextId">
								<input type="hidden" id="textsFormHiddenLang">
								<textarea id="textsFormTextarea"  class="form-control" name="text_val" rows="6"></textarea>
							</div>
							<div class="modal-footer">
								<button type="button" class="btn btn-secondary" data-dismiss="modal">Zatvoriť</button>
								<button class="btn btn-primary">Uložiť</button>
							</div>
						</form>
					</div>
				</div>
			</div>
			<script>
				$(document).on('click', '.js-editText', function (e) {
					var textId 	= $(this).data('text_id');
					var lang 	= $(this).data('lang');

					$.ajax({
						url: "<?=base_url();?>Admin/Texts/getTextValue",
						type: 'post',
						data: {text_id: textId, lang: lang},
						success: function (data) {
							data = JSON.parse(data);
							if (data[0] == 'ok') {
								$('#textsModal').modal('show');
								$('#textsFormTextarea').val(data[1]);
								$('#textsFormHiddenTextId').val(textId);
								$('#textsFormHiddenLang').val(lang);
							} else {
								alert('Nastala chyba. Skúste to prosím znovu');
							}
						}
					});
				});
				
				$(document).on('submit', '#textsModalForm', function(e) {
					e.preventDefault();
					var textVal = $('#textsFormTextarea').val();
					var textId 	= $('#textsFormHiddenTextId').val();
					var lang 	= $('#textsFormHiddenLang').val();

					$.ajax({
						url: "<?=base_url();?>Admin/Texts/saveText",
						type: 'post',
						data: {
							text_id: 	textId,
							text_val: 	textVal,
							lang: 		lang
						},
						success: function (data) {
							data = JSON.parse(data);
							if (data[0] == 'ok') {
								console.log(data[1]);
								$('#textsModal').modal('hide');
								$('#text_' + textId).html(data[1]);
							} else {
								alert('Nastala chyba. Skúste to prosím znovu');
							}
						}
					});
				});
			</script>
		<?php
	}
?>