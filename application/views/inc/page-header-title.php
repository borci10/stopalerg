<section class="page-header">
	<div class="container">
		<div class="row">
			<div class="col-12 text-center">
				<h1 class="page_header_title"><?=$pageHeaderTitle?></h1>
			</div>
		</div>
	</div>
</section>