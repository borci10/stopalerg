<?php
if ((@$_COOKIE['accepted'] != 'yes') && (@$_COOKIE['accepted'] != 'no')) {
	?>
	<div id="cookies">
		<p>Táto stránka používa súbory cookies, ktoré nám umožňujú identifikovať zlepšenia našich služieb. <a href="<?= base_url() ?>pravidla-cookies" class="highlighted" rel="nofollow" title="Viac informácií">Viac informácií</a></p>
		<button class="btn btn-cookies d-inline js-cookiesAccept" rel="nofollow">Rozumiem</button>
	</div>
	<?php
}
?>