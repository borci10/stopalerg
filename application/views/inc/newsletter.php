<form id="newsletter" method="post" action="<?=base_url()?>newsletter" role="form">
	<div class="d-flex">
		<input type="email" class="form-control" id="email" maxlength="255" name="email" placeholder="Váš email" aria-label="Email" aria-labelledby="email" required>
		<input type="submit" class="own-btn large" value="Odoslať" name="formConfirm" id="formConfirm">
	</div>
</form>
