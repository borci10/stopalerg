<!--Cart-->
<script>
	$(document).on('click', '.js-addItemToCart', function() {
		var row 			= $(this).data('row');
		var productId 		= $(this).data('product_id');
		var productChildId 	= $(this).data('product_child_id') || null;
		var quantity  		= 1;
		var actualQuantity 	= parseInt($('#itemQuantity' + row).html());
		var maxQuantity 	= parseInt($('#itemAvailableQuantity' + row).html());

		if (actualQuantity < maxQuantity) {
			$.ajax({
				type: 'POST',
				url: '<?=base_url()?>cart/addItemToCart',
				data: {
					productId 		: productId,
					productChildId 	: productChildId,
					quantity  		: quantity,
				},
				success: function(data){
					console.log('aaa')
					var data = JSON.parse(data);
					$('.js-shoppingCartPrice').html(data[0]);
					$('#totalPrice').html(data[0]);
					$('#itemQuantity' + row).html(function(i, val) { return +val+1});
					$('#itemTotalPrice' + row).html(data[1]);
				},
				error: function (xhr, ajaxOptions, thrownError) {
					console.log(xhr.status);
					console.log(thrownError);
				}
			});
		}
	});

	$(document).on('click', '.js-removeItemFromCart', function() {
		var row 			= $(this).data('row');
		var productId 		= $(this).data('product_id');
		var productChildId 	= $(this).data('product_child_id') || null;
		var quantity  		= 1;

		if ($('#itemQuantity' + row).html() - 1 > 0) {
			$.ajax({
				type: 'POST',
				url: '<?=base_url()?>cart/removeItemFromCart',
				data: {
					productId 		: productId,
					productChildId 	: productChildId,
					quantity  		: quantity,
				},
				success: function(data){
					var data = JSON.parse(data);
					$('.js-shoppingCartPrice').html(data[0]);
					$('#totalPrice').html(data[0]);
					$('#itemQuantity' + row).html(function(i, val) { return +val-1});
					$('#itemTotalPrice' + row).html(data[1]);
				}
			});
		}
	});

	$(document).on('click', '.removeItemAllFromCart', function() {
		var productId 		= $(this).data('product_id');
		var productChildId 	= $(this).data('product_child_id') || null;
		var productName 	= $(this).data('product_name');
		var quantity  		= -1;

		if (confirm("Naozaj chcete produkt " + productName + " odstrániť z košíka?")) {
			$.ajax({
				type: 'POST',
				url: '<?=base_url()?>cart/removeItemFromCart',
				data: {
					productId 		: productId,
					productChildId 	: productChildId,
					quantity  		: quantity,
				},
				success: function(data){
					//console.log(data);
					location.reload();
				}
			});
		}
	});
</script>
<!--Cart Personal Info-->
<script>
	$(document).on('change', '#cartDifferentDeliveyAdress', function() {
		$('.deliveryInfoForm').slideToggle();
		if ($('#cartDifferentDeliveyAdress').is(':checked')) {
			$("#cartDeliveryStreet").prop('required'	,true);
			$("#cartDeliveryCity").prop('required'		,true);
			$("#cartDeliveryZipCode").prop('required'	,true);
		} else {
			$("#cartDeliveryStreet").prop('required'	,false);
			$("#cartDeliveryCity").prop('required'		,false);
			$("#cartDeliveryZipCode").prop('required'	,false);
		}
	});
</script>
<script>
	if($('.payment-wrap').length !== 0) {
		$(".payment-option input[type=radio]").click(function () {
			if ($(this).attr("id") === "delivery_7") {
				$('input[type=radio][id=payment_1]').prop("checked", false).attr('disabled',true);
				$('input[type=radio][id=payment_3]').prop("checked", true);
			}
			else {
				$('input[type=radio][id=payment_1]').attr('disabled',false);
			}
		});
		if($('input[type=radio][id=delivery_7]').is(':checked')) {
			$('input[type=radio][id=payment_1]').attr('disabled',true);
		}
	}
</script>
<!--HomepageAddCount-->
<script>
	//reservation add pesron into price list
	$(document).on('click', '.js-addCountItem', function() {
		var inputElem	= $(".js-countItem");
		var actualNum	= parseInt(inputElem.val());

		if (!actualNum) {
			actualNum = 0;
		}

		inputElem.val(actualNum + 1);
		inputElem.trigger('input');
	});

	$(document).on('click', '.js-subCountItem', function() {
		var inputElem	= $(".js-countItem");
		var actualNum	= parseInt(inputElem.val());

		if (!actualNum) {
			actualNum = 0;
			inputElem.val(actualNum);
		}

		if (actualNum - 1 >= 1) {
			inputElem.val(actualNum - 1);
			inputElem.trigger('input');
		}
	});
</script>
<!--Cookies-->
<script>
	$(function () {
		/*cookies acept*/
		$(document).on('click', '.js-cookiesAccept', function (event) {
			$.ajax({
				url: '<?=base_url()?>cookie_accept',
				dataType: 'text',
				cache: false,
				data: {},
				type: 'post',
				success: function (data) {
					$('#cookies').addClass('active');
				}
			});
			event.preventDefault();
		});
		//cookies reset
		$(document).on('click', '.js-cookiesReset', function (event) {
			$.ajax({
				url: '<?=base_url()?>cookie_reset',
				dataType: 'text',
				cache: false,
				data: {},
				type: 'post',
				success: function (data) {
					location.reload();
				}
			});
			event.preventDefault();
		});
	});
</script>
<script>
	// add item into shopping cart
	$(document).on('click', '#addItemToShoppingCart', function() {
		var productId 		= $(this).data('product_id');
		var productChildId 	= $(this).data('product_child_id') || null;
		var quantity  		= $('#productQuantity' + productId).val();

		$.ajax({
			type: 'POST',
			url: '<?=base_url()?>cart/addItemToCart',
			data: {
				productId 		: productId,
				productChildId 	: productChildId,
				quantity  		: quantity,
			},
			success: function(data){
				var data = JSON.parse(data);
				$('.js-shoppingCartPrice').html(data[0]);
				if (data[2].quantity > 0) {
					$(".modalCartImage").attr("src", (data[2].file_name ? data[2].file_name : "<?=base_url()?>images/No_image_available.jpg"));
					$('.modalCartName').html(data[2].name);
					$('.modalShortDescription').html(data[2].short_description);
					$('.js-modalCartPrice').html(priceCalc(data[2].price, 1, data[2].discount, data[2].tax / 100) + '<?=$this->config->item('currency_unicode')?>');
					$('.modalCartQuantity').html(quantity + 'x');
					$('#modalAddItemToCart').modal('show');
				}
			}
		});
	});

	function priceCalc(price, quantity, discount, vat) {
		quantity = quantity || 1;
		discount = discount || 0;
		vat = vat || 0;
		return (price * ((100 - discount) / 100) * (1 + vat) * quantity).toFixed(2);
	}
</script>