
	<!--Bootstrap-->
	<link rel="stylesheet" href="<?=base_url()?>assets/css/bootstrap-reboot.min.css"/>
	<link rel="stylesheet" href="<?=base_url()?>assets/css/bootstrap-grid.min.css"/>
	<link rel="stylesheet" href="<?=base_url()?>assets/css/bootstrap-utilities.min.css"/>
	<link rel="stylesheet" href="<?=base_url()?>assets/css/bootstrap-carousel.min.css"/>
	<link rel="stylesheet" href="<?=base_url()?>assets/css/bootstrap-custom.min.css"/>
	<link rel="stylesheet" href="<?=base_url()?>assets/css/jquery-ui.min.css"/>
	<!--Carousel-->
	<link rel="stylesheet" href="<?=base_url()?>assets/css/owlcarousel/owl.carousel.min.css"/>
	<!--Lightbox-->
	<link rel="stylesheet" href="<?=base_url()?>assets/css/lightbox/lightbox.min.css">
	<!--Own CSS-->
	<?php
		if (($this->session->userdata('admin') == 1) && ($this->uri->segment(1) != 'admin')) {
			?>
				<link rel="stylesheet" href="<?=base_url()?>assets/css/fontawesome/all.min.css"/>
				<link rel="stylesheet" href="<?=base_url()?>assets/css/bootstrap-admin.min.css"/>
				<link rel="stylesheet" href="<?=base_url()?>assets/css/perfect-scrollbar/perfect-scrollbar.css" />
				<link rel="stylesheet" href="<?=base_url()?>assets/css/admin-bar.min.css?v=<?=filemtime('assets/css/admin-bar.min.css');?>"/>
			<?php
		}
	?>
	<link rel="stylesheet" href="<?=base_url()?>assets/css/main.min.css?v=<?=filemtime('assets/css/main.min.css');?>"/>
	<?php
		if (($this->session->userdata('admin') == 1) && ($this->uri->segment(1) == 'admin')) {
			?>
				<link rel="stylesheet" href="<?=base_url()?>assets/css/perfect-scrollbar/perfect-scrollbar.css" />
				<link rel="stylesheet" href="<?=base_url()?>assets/css/admin.min.css?v=<?=filemtime('assets/css/admin.min.css');?>"/>
			<?php
		}
	?>
	<!--Fontawesome-->
	<link rel="stylesheet" href="<?=base_url()?>assets/css/fontawesome/all.min.css"/>
	<link rel="stylesheet" href="<?=base_url()?>assets/css/alert-box.min.css?v=<?=filemtime('assets/css/alert-box.min.css');?>"/>
	<?php
	if ((@$_COOKIE['accepted'] != 'yes') && (@$_COOKIE['accepted'] != 'no')) {
		?>
			<link rel="stylesheet" href="<?=base_url()?>assets/css/cookies-bar.min.css?v=<?=filemtime('assets/css/cookies-bar.min.css');?>"/>
		<?php
	}
	?>
