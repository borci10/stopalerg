<a href="<?=base_url()?>
<?php
if($this->session->userdata('logged_in') && $this->session->userdata('admin') != 1) {
	echo "profile";
}
elseif ($this->session->userdata('admin') == 1)
	echo "admin";
else {
	echo "login";
}
?>
">
	<div class="icon-holder icon-sm">
		<?php include 'images/icons/user.svg';?>
	</div>
</a>
<a href="<?=base_url()?>cart" class="ml-3">
	<div class="icon-holder icon-sm">
		<?php include 'images/icons/cart.svg';?>
		<?php
			if(isset($_SESSION['shoppingCart'][1][0]['itemCount'])) {
				?>
					<span class="cart-count">
						<?php
							echo $_SESSION['shoppingCart'][1][0]['itemCount'];
						?>
					</span>
				<?php
			}
		?>
	</div>
</a>