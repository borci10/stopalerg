<div id="slider" class="half-slider <?=isset($this->data['img']) ? 'overlay' : '';?>">
	<div class="container">
		<div class="slider-content-wrap d-flex flex-row">
			<div class="slider-content d-flex flex-column justify-content-center align-items-center text-center">
				<?php
					if(isset($this->data['img'])) {
						?>
							<img src="<?=base_url()?>/uploads/residences/<?= $this->data['img'] ?>" class="slider-img" alt="<?=$this->data['title']?>">
						<?php
					}
					else {
						?>
							<img src="<?=base_url()?>images/slider-subpage.jpg" class="slider-img" alt="<?=$this->data['title']?>">
						<?php
					}
				?>
				<h1><?=$this->data['title']?></h1>
				<?php
					if((isset($this->data['subheading']))) {
						?>
							<p class="subheading"><?=$this->data['subheading']?></p>
						<?php
					}
				?>
				<?php
					if($this->data['filter'] == true) {
						$this->load->view('inc/filter');
					}
				?>
			</div>
		</div>
	</div>
	<div id="loader">
		<div class="spinner-wrap">
			<svg viewBox="25 25 50 50">
				<circle cx="50" cy="50" r="20"></circle>
			</svg>
		</div>
	</div>
</div>