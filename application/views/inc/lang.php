<?php
if ($_SESSION['lang'] == 'en') {
	?>
	<div class="langWrap menu-dropdown-toggle">
		<span>
			EN
		</span>
		<ul class="menu-dropdown">
			<li>
				<a href="<?=base_url()?>lang/sk" data-lang="sk" class="js-changeLang">
					SK
				</a>
			</li>
		</ul>
	</div>
	<?php
}
if ($_SESSION['lang'] == 'sk') {
	?>
	<div class="langWrap menu-dropdown-toggle">
		<span>
			SK
		</span>
		<ul class="menu-dropdown">
			<li>
				<a href="<?=base_url()?>lang/en" data-lang="en" class="js-changeLang">
					EN
				</a>
			</li>
		</ul>
	</div>
	<?php
}
?>