<a href="<?=base_url()?>">
	<picture>
		<source srcset="<?= base_url() ?>images/stopalerg-logo.webp" type="image/webp">
		<source srcset="<?= base_url() ?>images/stopalerg-logo.png" type="image/jpeg">
		<img src="<?= base_url() ?>images/stopalerg-logo.png" alt="Stopalerg" class="img-fluid">
	</picture>
</a>