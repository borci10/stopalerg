<?php
if (!empty($this->session->flashdata('success'))) {
	?>
	<div class="alert-box-wrap">
		<div class="alert-box green">
			<div class="card-head">
				<div class="close-alert">
					<i class="fas fa-times"></i>
				</div>
				<i class="fas fa-bell"></i>
				<span class="title">
							Úspech!
						</span>
				<p><?php echo $this->session->flashdata('success'); ?></p>
			</div>
		</div>
	</div>
	<?php
}
if (!empty($this->session->flashdata('error'))) {
	?>
	<div class="alert-box-wrap">
		<div class="alert-box red">
			<div class="card-head">
				<div class="close-alert">
					<i class="fas fa-times"></i>
				</div>
				<i class="fas fa-bell"></i>
				<span class="title">
							Chyba!
						</span>
				<p><?php echo $this->session->flashdata('error'); ?></p>
			</div>
		</div>
	</div>
	<?php
}
if (!empty($this->session->flashdata('info'))) {
	?>
	<div class="alert-box-wrap">
		<div class="alert-box blue">
			<div class="card-head">
				<div class="close-alert">
					<i class="fas fa-times"></i>
				</div>
				<i class="fas fa-bell"></i>
				<span class="title">
							Informácia!
						</span>
				<p><?php echo $this->session->flashdata('info'); ?></p>
			</div>
		</div>
	</div>
	<?php
}
if (!empty($this->session->flashdata('warning'))) {
	?>
	<div class="alert-box-wrap">
		<div class="alert-box orange">
			<div class="card-head">
				<div class="close-alert">
					<i class="fas fa-times"></i>
				</div>
				<i class="fas fa-bell"></i>
				<span class="title">
							Varovanie!
						</span>
				<p><?php echo $this->session->flashdata('warning'); ?></p>
			</div>
		</div>
	</div>
	<?php
}
?>