<!--Jquery-->
<script src="<?=base_url()?>assets/js/jquery/jquery-3.4.1.min.js"></script>
<script src="<?=base_url()?>assets/js/jquery/jquery-ui.min.js"></script>
<!--Carousel-->
<script src="<?=base_url()?>assets/js/owlcarousel/owl.carousel.min.js"></script>
<!--Bootstrap-->
<script src="<?=base_url()?>assets/js/popper/popper.min.js"></script>
<script src="<?=base_url()?>assets/js/bootstrap/bootstrap.min.js"></script>
<script src="<?=base_url()?>assets/js/perfect-scrollbar/perfect-scrollbar.min.js"></script>
<!--FancyBox-->
<script src="<?=base_url()?>assets/js/lightbox/lightbox.min.js"></script>
<!--Own JS-->
<script src="<?=base_url()?>assets/js/slider.js?v=<?=filemtime('assets/js/slider.js');?>"></script>
<script src="<?=base_url()?>assets/js/modal.js?v=<?=filemtime('assets/js/modal.js');?>"></script>
<script src="<?=base_url()?>assets/js/main.js?v=<?=filemtime('assets/js/main.js');?>"></script>
