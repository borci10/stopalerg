<!DOCTYPE html>
<html lang="SK">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
	<?php
		@$url = explode('.', $_SERVER['SERVER_NAME']);
		if (@$url[1] == 'onlima') {
			?>
			<meta name="robots" content="noindex, nofollow">
			<?php
		}
	?>
	<?php
		if (isset($pageDescription)) {
			?>
				<meta name="description" content="<?=$pageDescription?>">
			<?php
		}
		if (isset($openGraph)) {
			foreach($openGraph as $ogKey => $ogValue) {
				?>
					<meta property="<?=$ogKey?>" content="<?=$ogValue?>" />
				<?php
			}
		}
	?>

	<script type="application/ld+json">
		{
			"@context": "http://schema.org",
			"@type": "Website",
			"name": "StopALERG",
			"url": "https://www.stopalerg.sk",
			"sameAs": [
				"https://www.facebook.com/Stopalerg/",
				"https://www.instagram.com/stopalerg/",
				"https://www.youtube.com/channel/UCkXujuYEWWtZY4xBOdKMRpA"
			]
		}
	</script>

	<?php $this->load->view('inc/fonts');?>
	<?php $this->load->view('inc/css');?>
	<?php $this->load->view('inc/favicon');?>
	<?php $this->load->view('inc/noscript');?>

	<title><?= $page_title ?? '' ?></title>

	<!--Analytics Scripts-->
	<!-- Global site tag (gtag.js) - Google Analytics -->
	<script async src="https://www.googletagmanager.com/gtag/js?id=UA-143274737-1"></script>
	<script>
		window.dataLayer = window.dataLayer || [];
		function gtag(){dataLayer.push(arguments);}
		gtag('js', new Date());

		gtag('config', 'UA-143274737-1');
	</script>

</head>
<body id="body" class="<?= ($this->session->userdata('admin') ? 1 : '') ?>">
	<header id="header">
		<div class="mobile-header">
			<div class="d-flex flex-row justify-content-between align-items-center">
				<div class="logo">
					<?php $this->load->view('inc/logo');?>
				</div>
				<div class="menu d-flex align-items-center">
					<div class="cart d-flex">
						<?php $this->load->view('inc/cart');?>
					</div>
					<div class="nav-icon-wrap ml-4">
						<div id="nav-icon">
							<span></span>
							<span></span>
							<span></span>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div id="burger-menu">
			<ul class="burger-menu-inner">
				<?php $this->load->view('inc/nav');?>
			</ul>
		</div>
		<div class="desktop-header">
			<div class="header-middle">
				<div class="container">
					<div class="d-flex flex-wrap justify-content-between align-items-center">
						<div class="logo">
							<?php $this->load->view('inc/logo');?>
						</div>
						<ul id="desktop-menu" class="d-flex justify-content-center align-items-center">
							<?php $this->load->view('inc/nav');?>
						</ul>
						<div id="eshop-menu" class="d-flex justify-content-end align-items-center">
							<?php $this->load->view('inc/cart');?>
						</div>
					</div>
				</div>
			</div>
		</div>
	</header>
	<?php
		if ($this->session->userdata('admin') == 1) {
			$this->load->view('admin/sidebar');
		}
	?>
	<main id="main">
	<?php
		if (isset($pageHeaderTitle)) {
			$this->load->view('inc/page-header-title');
		}
	?>