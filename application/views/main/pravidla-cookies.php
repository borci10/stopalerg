<section id="personal-data">
    <div class="container">
        <div class="row justify-content-center align-items-center">
            <div class="col-12">
                <h3>1. O cookies</h3>
                <p class="first-level">1.1 Cookie je súbor obsahujúci identifikátor (reťazec písmen a čísel), ktorý posiela webový server do webového prehliadača a je uložený prehliadačom. Identifikátor sa potom odosiela späť na server vždy, keď prehliadač požaduje od servera stránku.</p>
                <p class="first-level">1.2 Súbory cookies môžu byť buď „trvalé“ súbory cookies, alebo súbory „relácie“: trvalý súbor cookie bude uložený vo webovom prehliadači a zostane platný až do stanoveného dátumu vypršania platnosti, pokiaľ nie je pred dátumom vypršania platnosti vymazaný používateľom; cookies relácie vypršia na konci relácie používateľa, keď je webový prehliadač zatvorený.</p>
                <p class="first-level">1.3 Súbory cookies zvyčajne neobsahujú žiadne informácie, ktoré osobne identifikujú používateľa, ale osobné informácie, ktoré ukladáme, môžu byť prepojené s informáciami uloženými v cookies a získanými z nich.</p>
                <h3 style="">2. Cookies, ktoré používame</h3>
                <p class="first-level">2.1 Cookies používame na nasledujúce účely:</p>
                <p class="second-level">(a) overovanie – súbory cookies používame na vašu identifikáciu pri návšteve našich webových stránok a počas navigácie na našich webových stránkach;</p>
                <p class="second-level">(b) analýza – používame súbory cookies, ktoré nám pomáhajú analyzovať používanie a výkonnosť našich webových stránok a služieb;</p>
                <p class="second-level">(c) súhlas cookies – súbory cookies používame na ukladanie vašich preferencií k používaniu súborov cookies pri prehliadaní webovej stránky</p>
                <h3>3. Cookies používané našimi poskytovateľmi služieb</h3>
                <p class="first-level">3.1 Naši poskytovatelia služieb používajú súbory cookies a tieto cookies môžu byť uložené vo vašom počítači pri návšteve našich webových stránok.</p>
                <p class="first-level">3.2 Na analýzu používania našich webových stránok používame službu <em>Google Analytics</em>. Služba Google Analytics zhromažďuje informácie o používaní webových stránok prostredníctvom súborov cookies. Zhromaždené informácie týkajúce sa našich webových stránok sa používajú na vytváranie prehľadov o používaní našich webových stránok. Pravidlá ochrany osobných údajov spoločnosti Google sú k dispozícii na stránke: <a href="https://policies.google.com/privacy?hl=sk" target="_blank" rel="noopener">https://policies.google.com/privacy?hl=sk</a>.</p>
                <h3>4. Správa súborov cookies</h3>
                <p class="first-level">4.1 Väčšina prehliadačov vám umožňuje odmietnuť prijatie súborov cookies a odstrániť súbory cookies. Metódy na ich správu sa líšia v závislosti od prehliadača a verzie prehliadača. Môžete však získať aktuálne informácie o blokovaní a mazaní súborov cookies prostredníctvom týchto odkazov:</p>
                <p class="second-level">(a) <a href="https://support.google.com/chrome/answer/95647?hl=sk" target="_blank" rel="noopener">https://support.google.com/chrome/answer/95647?hl=sk</a> (Chrome);</p>
                <p class="second-level">(b) <a href="https://support.mozilla.org/sk/kb/povolenie-zakazanie-cookies" target="_blank" rel="noopener">https://support.mozilla.org/sk/kb/povolenie-zakazanie-cookies</a> (Firefox);</p>
                <p class="second-level">(c) <a href="http://www.opera.com/help/tutorials/security/cookies/" target="_blank" rel="noopener">http://www.opera.com/help/tutorials/security/cookies/</a> (Opera);</p>
                <p class="second-level">(d) <a href="https://support.microsoft.com/sk-sk/help/17442/windows-internet-explorer-delete-manage-cookies" target="_blank" rel="noopener">https://support.microsoft.com/sk-sk/help/17442/windows-internet-explorer-delete-manage-cookies</a> (Internet Explorer);</p>
                <p class="second-level">(e) <a href="https://support.apple.com/kb/PH21411?locale=sk_SK" target="_blank" rel="noopener">https://support.apple.com/kb/PH21411?locale=sk_SK</a> (Safari);</p>
                <p class="second-level">(f) <a href="https://privacy.microsoft.com/en-us/windows-10-microsoft-edge-and-privacy" target="_blank" rel="noopener">https://privacy.microsoft.com/en-us/windows-10-microsoft-edge-and-privacy</a> (Edge).</p>
                <p class="first-level">4.2 Blokovanie všetkých súborov cookies bude mať negatívny vplyv na použiteľnosť mnohých webových stránok.</p>
                <p class="first-level">4.3 Ak zablokujete cookies, nebudete môcť používať všetky funkcie našich webových stránok.</p>
            </div>
        </div>
    </div>
</section>