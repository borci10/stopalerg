<section>
	<div class="container">
		<div class="white-bg-box">
			<div class="row">
				<div class="col-12 text-center mb-5">
					<h1 class="h1">Čo je <span>alergia</span></h1>
				</div>
				<div class="col-md-6 mb-4">
					<p class="font-weight-bold">Dnes trpí alergiou každý piaty obyvateľ v priemyselne rozvinutých krajnách. Vychádza to z údajov Svetovej zdravotníckej organizácie (WHO). Niektoré odhady hovoria, že alergia sa vyskytuje až u tretiny populácie. Za posledných 10 rokov sa výskyt alergií v týchto krajinách zvýšil o štvrtinu a naďalej rastie. V prieskumoch WHO z roku 2000 sa alergické ochorenia objavili na druhom mieste, hneď za vysokým krvným tlakom a kardiovaskulárnymi ochoreniami.</p>
					<p>Dodnes presne nevieme, ktoré faktory spôsobujú rozvoj alergických ochorení vo svete, no významnú úlohu zohrávajú klimatické zmeny a znečisťovanie životného prostredia. Ďalším faktorom môže byť aj zmena citlivosti na alergény a dedičnosť.</p>
					<p>Rôzna je aj škála a prejavy alergií. Od najjednoduchšej formy - sennej nádchy, až po život ohrozujúci anafylaktický šok. Avšak ani senná nádcha nie je „obyčajná" nádcha. Je dokázané, že u ľudí so sennou nádchou, je až trikrát vyššia pravdepodobnosť rozvoja astmy.</p>
				</div>
				<div class="col-md-6 mb-4">
					<p>V liečbe alergických ochorení sa dnes najčastejšie používajú lieky - takzvané antihistaminiká. Tieto lieky však nie sú vhodné pre každého alergika. K ich najčastejším vedľajším účinkom patrí ospalosť či znížená pozornosť. Za alternatívnu liečbu alergií je možné považovať homeopatiu, fytoterapiu či akupunktúru. V posledných rokoch sa aj vďaka najnovším vedeckým zisteniam a experimentom stáva veľmi významnou výživová terapia pomocou prepeličích vajíčok.</p>
				</div>
				<div class="col-12">
					<h3 class="h2 text-center mb-4">Aké <span>alergie</span> poznáme</h3>
					<ol class="pl-3">
						<li class="mb-2">Alergická nádcha, známa ako senná nádcha, je typická častým kýchaním, zahlieneným či upchatým nosom, škriabaním v nosohltane, svrbením a slzením očí. Môže ju sprevádzať aj zhoršené dýchanie prechádzajúce do sipotu. Spôsobujú ju najmä peľ, roztoče, prach, baktérie, plesne a srsť z psov a mačiek. Môže byť sezónna a trvalá.</li>
						<li class="mb-2">Astma sa prejavuje problematickým vydychovaním, často pocitom dusenia. Spôsobuje ju podráždenie priedušiek. Astma môže vzniknúť ako náhla reakcia na konkrétny alergén, ale môže byť aj chronická.</li>
						<li class="mb-2">Kožné alergie sa najčastejšie prejavujú ekzémami, ktoré voláme aj dermatitídy. Ide o neinfekčný zápal kože. Objavujú sa v ľahších formách – lokálne liečiteľných, i v chronických formách, u ktorých je potrebné intenzívne dlhodobé liečenie. Poznáme atopický ekzém, ktorý sa vyskytuje už u batoliat a prejavuje sa svrbivým začervenaním kože, neskôr sa postihnuté časti vysušia a zdrsnejú, napokon oblasť začne mokvať a objavia sa chrasty. Kontaktný ekzém sa objavuje najčastejšie u dospelých pri kontakte s dráždivou látkou, napríklad rastlinou, jedlom, niklom obsiahnutom v šperkoch či chemikáliami.</li>
					</ol>
				</div>
			</div>
		</div>
	</div>
</section>
<?php $this->load->view('main/_product-detail');?>