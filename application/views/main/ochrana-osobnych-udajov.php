<section id="personal-data">
	<div class="container">
		<div class="row">
			<div class="col-12">
				<p>Tieto podmienky ochrany osobných údajov obsahujú dodatočné informácie upravujúce zabezpečenie
				ochrany osobných údajov, ktoré sa zadávajú a spracovávajú v súvislosti s využitím služieb webovej stránky stopalerg.sk</p>
				<h3>1. IDENTIFIKÁCIA PREVÁDZKOVATEĽA INFORMAČNÉHO SYSTÉMU (ĎALEJ AKO „IS“):</h3>
				<p class="first-level mb-0">Prevádzkovateľ je spoločnosť RM PHARM s. r.o.</p>
				<p class="first-level mb-0">Adresa: Vlárska 66, 831 01 Bratislava</p>
				<p class="first-level mb-0">Email: magnusova.renata@rmpharm.sk</p>
				<p class="first-level mb-0">Spoločnosť je zapísaná v Obchodnom registri: Okresný súd Bratislava I; oddiel: Sro; vložka č.: 99211/B</p>
				<p class="first-level mb-0">IČO: 47807512, DIČ: 2024100749, IČ DPH: SK2024100749</p>
				<h3>2. AKÉ OSOBNÉ ÚDAJE ZBIERAME, NA AKÝ ÚČEL A AKO DLHO ICH UCHOVÁVAME</h3>
				<p class="first-level">Prostredníctvom webovej stránky stopalerg.sk dochádza k spracúvaniu osobných údajov v súlade s platným
				nariadením Európskeho parlamentu a Rady EÚ (2016/679 Všeobecné nariadenie o ochrane osobných
				údajov „GDPR”), ako aj s inými platnými právnymi predpismi o ochrane osobných údajov , ktoré menia a
				dopĺňajú nariadenia GDPR.</p>
				<p class="first-level">Osobnými údajmi sa rozumejú všetky informácie o identifikovanej alebo identifikovateľnej fyzickej osobe;
				identifikovateľnou fyzickou osobou je fyzická osoba, ktorú možno priamo či nepriamo identifikovať, najmä
				odkazom na určitý identifikátor, napríklad meno, identifikačné číslo, lokalizačné údaje, sieťový identifikátor
				alebo odkazom na jeden či viac zvláštnych prvkov fyzickej, fyziologickej, genetickej, psychickej, ekonomickej,
				kultúrnej alebo spoločenskej identity tejto fyzickej osoby.</p>
				<p class="first-level">Osobné údaje sú zbierané a spracované len v nevyhnutnom rozsahu. Rozsah a typ osobných údajov závisí
				od účelu a právneho vzťahu, na ktorý tieto údaje spracúvame.</p>
				<p class="first-level">Pri nákupe prostredníctvom e-shopu stopalerg.sk bude predávajúci spracúvať Vaše údaje (ust. § 10 ods. 3
				písm. b) ZnOOÚ) v IS Internetový predaj (e-shop), IS Marketing, IS Účtovníctvo a IS Logistika predávajúceho
				v rozsahu:</p>
				<p class="second-level">- meno a priezvisko</p>
				<p class="second-level">- fakturačná adresa (ulica, č.d., psč a mesto/obec)</p>
				<p class="second-level">- adresa dodania (ulica, č.d., psč a mesto/obec)</p>
				<p class="second-level">- telefónne číslo</p>
				<p class="second-level">- email</p>
				<p class="first-level">Predávajúci bude spracúvať Vaše osobné údaje za účelom uzavretia a evidenciu spotrebiteľskej kúpnej
				zmluvy, vystavenia faktúry v nadväznosti na uzavretie spotrebiteľskej kúpnej zmluvy, zúčtovania kúpnej
				ceny, Vášho kontaktovania v záujme vybavenia, prípadnej úpravy a doručenia objednávky, a to najviac po
				dobu 3 rokov od posledného nákupu prostredníctvom e-shopu stopalerg.sk.</p>
				<h3>3. AKÝM TRETÍM STRANÁM POSKYTUJEME OSOBNÉ ÚDAJE?</h3>
				<p class="first-level">- Slovenská pošta, a.s. so sídlom Partizánska cesta 9 ,975 99 Banská Bystrica IČO 36 631 124
				(pre účely dodania objednaného tovaru)</p>
				<p class="first-level">- TandA Services s. r. o. so sídlom Potočná 3A , 900 27 Bernolákovo, IČO 47 433 141 (pre účely
				účtovníctva)</p>
				<h3>4. VYHLÁSENIA PREDÁVAJÚCEHO</h3>
				<p class="first-level">Predávajúci týmto vyhlasuje, že v súlade s ust. § 6 ods. 2 ZnOOÚ bude:</p>
				<p class="second-level">- získavať osobné údaje výlučne na vymedzený alebo ustanovený účel,</p>
				<p class="second-level">- spracúvať len také osobné údaje, ktoré svojím rozsahom a obsahom zodpovedajú účelu ich spracúvania a
				sú nevyhnutné na jeho dosiahnutie,</p>
				<p class="second-level">- spracúvať a využívať osobné údaje výlučne spôsobom, ktorý zodpovedá účelu, na ktorý boli zhromaždené; nebude
				združovať osobné údaje, ktoré boli získané osobitne na rozdielne účely,</p>
				<p class="second-level">- spracúvať len správne, úplné a podľa potreby aktualizované osobné údaje vo vzťahu k účelu spracúvania; nesprávne
				a neúplné osobné údaje bude blokovať a bez zbytočného odkladu ich opraví alebo doplní; nesprávne a neúplné
				osobné údaje, ktoré nemožno opraviť alebo doplniť tak, aby boli správne a úplné, zreteľne označí a bez zbytočného
				odkladu ich zlikviduje,</p>
				<p class="second-level">- spracúvať osobné údaje vo forme umožňujúcej identifikáciu dotknutých osôb počas doby nie dlhšej, ako je nevyhnutné na dosiahnutie účelu spracúvania,</p>
				<p class="second-level">- spracúvať osobné údaje v súlade s dobrými mravmi a konať spôsobom, ktorý neodporuje zákonu, a
				zlikviduje tie osobné údaje, ktorých účel spracúvania sa skončil.</p>
				<h3>5. COOKIES</h3>
				<p class="first-level">Cookie je súbor obsahujúci identifikátor (reťazec písmen a čísel), ktorý posiela webový server do webového prehliadača a je uložený prehliadačom. Identifikátor sa potom odosiela späť na server vždy, keď prehliadač požaduje od servera stránku.</p>
				<p class="first-level">Súbory cookies môžu byť buď „trvalé“ súbory cookies, alebo súbory „relácie“: trvalý súbor cookie bude uložený vo webovom prehliadači a zostane platný až do stanoveného dátumu vypršania platnosti, pokiaľ nie je pred dátumom vypršania platnosti vymazaný používateľom; cookies relácie vypršia na konci relácie používateľa, keď je webový prehliadač zatvorený.</p>
				<p class="first-level">Súbory cookies zvyčajne neobsahujú žiadne informácie, ktoré osobne identifikujú používateľa, ale osobné informácie, ktoré ukladáme, môžu byť prepojené s informáciami uloženými v cookies a získanými z nich.</p>
				<h3 class="pl-3">Cookies, ktoré používame</h3>
				<p class="first-level">Cookies používame na nasledujúce účely:</p>
				<p class="second-level">(a) overovanie – súbory cookies používame na vašu identifikáciu pri návšteve našich webových stránok a počas navigácie na našich webových stránkach;</p>
				<p class="second-level">(b) analýza – používame súbory cookies, ktoré nám pomáhajú analyzovať používanie a výkonnosť našich webových stránok a služieb;</p>
				<p class="second-level">(c) súhlas cookies – súbory cookies používame na ukladanie vašich preferencií k používaniu súborov cookies pri prehliadaní webovej stránky</p>
				<h3 class="pl-3">Cookies používané našimi poskytovateľmi služieb</h3>
				<p class="first-level">Naši poskytovatelia služieb používajú súbory cookies a tieto cookies môžu byť uložené vo vašom počítači pri návšteve našich webových stránok.</p>
				<p class="first-level">Na analýzu používania našich webových stránok používame službu <em>Google Analytics</em>. Služba Google Analytics zhromažďuje informácie o používaní webových stránok prostredníctvom súborov cookies. Zhromaždené informácie týkajúce sa našich webových stránok sa používajú na vytváranie prehľadov o používaní našich webových stránok. Pravidlá ochrany osobných údajov spoločnosti Google sú k dispozícii na stránke: <a href="https://policies.google.com/privacy?hl=sk" target="_blank" rel="noopener">https://policies.google.com/privacy?hl=sk</a>.</p>
				<h3 class="pl-3">Správa súborov cookies</h3>
				<p class="first-level">Väčšina prehliadačov vám umožňuje odmietnuť prijatie súborov cookies a odstrániť súbory cookies. Metódy na ich správu sa líšia v závislosti od prehliadača a verzie prehliadača. Môžete však získať aktuálne informácie o blokovaní a mazaní súborov cookies prostredníctvom týchto odkazov:</p>
				<p class="second-level">(a) <a href="https://support.google.com/chrome/answer/95647?hl=sk" target="_blank" rel="noopener">https://support.google.com/chrome/answer/95647?hl=sk</a> (Chrome);</p>
				<p class="second-level">(b) <a href="https://support.mozilla.org/sk/kb/povolenie-zakazanie-cookies" target="_blank" rel="noopener">https://support.mozilla.org/sk/kb/povolenie-zakazanie-cookies</a> (Firefox);</p>
				<p class="second-level">(c) <a href="http://www.opera.com/help/tutorials/security/cookies/" target="_blank" rel="noopener">http://www.opera.com/help/tutorials/security/cookies/</a> (Opera);</p>
				<p class="second-level">(d) <a href="https://support.microsoft.com/sk-sk/help/17442/windows-internet-explorer-delete-manage-cookies" target="_blank" rel="noopener">https://support.microsoft.com/sk-sk/help/17442/windows-internet-explorer-delete-manage-cookies</a> (Internet Explorer);</p>
				<p class="second-level">(e) <a href="https://support.apple.com/kb/PH21411?locale=sk_SK" target="_blank" rel="noopener">https://support.apple.com/kb/PH21411?locale=sk_SK</a> (Safari);</p>
				<p class="second-level">(f) <a href="https://privacy.microsoft.com/en-us/windows-10-microsoft-edge-and-privacy" target="_blank" rel="noopener">https://privacy.microsoft.com/en-us/windows-10-microsoft-edge-and-privacy</a> (Edge).</p>
				<p class="first-level">Blokovanie všetkých súborov cookies bude mať negatívny vplyv na použiteľnosť mnohých webových stránok.</p>
				<p class="first-level">Ak zablokujete cookies, nebudete môcť používať všetky funkcie našich webových stránok.</p>
				<h3>6. PRÁVA DOTKNUTEJ OSOBY</h3>
				<p class="first-level">Dotknutá osoba má právo prístupu k svojim osobným údajom, právo na ich opravu, aktualizáciu a doplnenie vrátane
				ďalších zákonných práv k týmto údajom najmä, právo na vysvetlenie a odstránenie nevyhovujúceho stavu podľa §28
				zák. č. 122/2013 Z.z. o ochrane osobných údajov v znení zákona č. 84/2014 Z.z Uživateľ má právo na trvalé alebo
				dočasné zastavenie spracovávania osobných údajov alebo ich odstránenie s výnimkou prípadov, keď je spracovanie
				nevyhnutné na plnenie uzatvorenej spotrebiteľskej zmluvy a to formou písomného vyjadrenia nesúhlasu so
				spracovaním osobných údajov (na emailovú alebo korešpodenčnú adresu prevádzkovateľa IS).</p>
				<p class="first-level">Dotknutá osoba má právo y zmysle ustanovenia §28 odsek 1 zákona o ochrane osobných údajov na základe písomnej
				žiadosti od prevádzkovateľa vyžadovať:</p>
				<p class="second-level">- potvrdenie, či sú alebo nie sú osobné údaje o nej spracúvané</p>
				<p class="second-level">- vo všeobecne zrozumiteľnej forme informácie o spracúvaní osobných údajov v IS v rozsahu podľa § 15 ods. 1 písm.
				a) až e) druhý až šiesty bod;</p>
				<p class="second-level">- vo všeobecne zrozumiteľnej forme presné informácie o zdroji, z ktorého získal jej osobné údaje na spracú- vanie</p>
				<p class="second-level">- vo všeobecne zrozumiteľnej forme zoznam jej osobných údajov, ktoré sú predmetom spracúvania</p>
				<p class="second-level">- opravu alebo likvidáciu svojich nesprávnych, neúplných alebo neaktuálnych osobných údajov, ktoré sú predmetom
				spracúvania</p>
				<p class="second-level">- likvidáciu jej osobných údajov, ktorých účel spracúvania sa skončil; ak sú predmetom spracúvania úradné doklady
				obsahujúce osobné údaje, môže požiadať o ich vrátenie</p>
				<p class="second-level">- likvidáciu jej osobných údajov, ktoré sú predmetom spracúvania, ak došlo k porušeniu zákona</p>
				<p class="second-level">- blokovanie jej osobných údajov z dôvodu odvolania súhlasu pred uplynutím času jeho platnosti, ak prevádzkovateľ
					spracúva osobné údaje na základe súhlasu dotknutej osoby.</p>
				<h3>7. KONTAKT</h3>
				<p class="first-level">V prípade akýchkoľvek otázok o tom, ako zhromažďujeme, ukladáme alebo využívame osobné údaje, nás
				neváhajte kontaktovať. Telefón: +421 903576402 Email: shop@stopalerg.sk</p>
				<p class="first-level">Napísať nám môžete na našu korešpondenčnú adresu: RM PHARM s. r.o., Vlárska 66, 831 01 Bratislava</p>
				<p class="first-level">Podmienky ochrany osobných údajov sa môžu priebežne meniť. Všetky aktualizácie týchto podmienok budú zverejňované na našom webovom sídle. Každá aktualizácia nadobúda účinnosť jej zverejnením na našom webovom sídle a preto odporúčame, aby ste pravidelne navštevovali naše webové sídlo a informovali
				sa o možných zmenách podmienok.</p>
				<p class="first-level">Prevádzkovateľ internetového obchodu zároveň informuje dotknuté osoby, že napriek prijatým
				bezpečnostným opatreniam nie je možné riziko zneužitia osobných údajov eliminovať na 100% a to
				predovšetkým pri okolnostiach vyššej moci alebo hackerských útokoch.</p>
				<p class="first-level">Posledná aktualizácia 20.5.2018</p>
			</div>
		</div>
	</div>
</section>