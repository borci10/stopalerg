<div class="container py-5">
	<div class="cart">
		<div class="white-bg-box">
			<div class="row justify-content-center align-items-center">
				<div class="col-12">
					<div class="white-bg-box text-center">
						<?php
							if (!empty($order_info)) {
								if ($order_info->payment == 'Dobierka') {
									echo '<h2 class="h4">Ďakujeme za Váš nákup, objednávku sme zaevidovali. O ďalšom priebehu Vás budeme informovať.</h2>';
								} else {
									echo '<h2 class="h4">Ďakujeme za Váš nákup, objednávku sme zaevidovali. Žiadame Vás o vykonanie platby prevodom na účet.</h2>';
									echo '<h4 class="mb-2 mt-5">Informácie o platbe</h4>';
									?>
									<div class="d-flex justify-content-center align-items-center">
										<table>
											<?php
											if (!empty($shopSettings['bank_name']) && !empty($shopSettings['bank_account_number']) && !empty($shopSettings['bank_code'])) {
												?>
												<tr>
													<td style="text-align:left;">Číslo účtu:&nbsp;&nbsp;&nbsp;</td>
													<td style="text-align:left;"><strong><?= $shopSettings['bank_account_number']; ?> / <?= $shopSettings['bank_code']; ?></strong></td>
												</tr>
												<?php
											}
											if (!empty($shopSettings['bank_iban'])) {
												?>
												<tr>
													<td style="text-align:left;">IBAN:&nbsp;&nbsp;&nbsp;</td>
													<td style="text-align:left;"><strong><?= $shopSettings['bank_iban']; ?></strong></td>
												</tr>
												<?php
											}
											if (!empty($order_info)) {
												?>
												<tr>
													<td style="text-align:left;">Variabilný symbol:&nbsp;&nbsp;&nbsp;</td>
													<td style="text-align:left;"><strong><?php echo generateVarSymbol($order_info->id); ?></strong></td>
												</tr>
												<?php
											}
											?>
										</table>
									</div>
									<?php
								}
							}
						?>
						<a href="<?=base_url()?>" class="own-btn mt-4">
							Prejsť na úvod
						</a>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
