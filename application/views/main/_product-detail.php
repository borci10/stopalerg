<section>
	<div class="container">
		<div class="container-inner">
			<div class="row justify-content-between align-items-center">
				<div class="col-xl-5 col-lg-6 text-lg-left text-center order-lg-1 order-2">
					<?php
					foreach($products ?? [] as $product) {
						?>
						<h2 class="h1"><?=strip_tags($product['name'])?></h2>
						<p class="text-lg color-secondary"><?=strip_tags($product['short_description'])?></p>
						<h3 class="mt-4 mb-3">Bojovník proti <span>alergii</span></h3>
						<p><?=strip_tags($product['long_description'])?></p>
						<div class="add-to-cart-holder mt-4">
							<div role="group" class="input-group my-1">
								<div class="input-group-prepend">
									<button type="button" class="btn js-subCountItem">-</button>
								</div>
								<input type="number" class="form-control js-countItem" min="1" max="<?=$product['quantity']?>" value="1" id="productQuantity<?=$product['id']?>">
								<div class="input-group-append">
									<button type="button" class="btn js-addCountItem">+</button>
								</div>
							</div>
							<button class="own-btn icon-left" id="addItemToShoppingCart" data-product_id="<?=$product['id']?>" <?=($product['quantity'] == 0 ? 'disabled' : '')?> title="Pridať produkt do košíka">
									<span class="icon-holder">
										<?php include 'images/icons/cart.svg';?>
									</span>
								Pridať do košíka
							</button>
						</div>
						<?php
					}
					?>
				</div>
				<div class="col-lg-6 text-center  order-lg-2 order-1 mb-lg-0 mb-5">
					<img src="<?=base_url()?>images/bojovnik-proti-alergii.png?v1" alt="Stopalerg" class="img-fluid">
				</div>
			</div>
		</div>
	</div>
</section>