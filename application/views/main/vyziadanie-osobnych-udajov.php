<?php
$this->data['title'] 	= 'Vyžiadanie osobných údajov';
$this->data['desc'] 	= 'Chcem vedieť viac';
$this->load->view('inc/half-slider');
?>
<section id="personal-data">
    <div class="container">
        <div class="row justify-content-center align-items-center">
            <div class="col-12">
                <p class="mb-3 text-md-center">Máte právo si vyžiadať osobné údaje, ktoré o Vás máme. Po odoslaní tohto formulára Vám údaje zašleme do 30 dní.</p>
                <div class="col-md-6 offset-md-3 col-12 py-4">
                    <form id="regForm" method="post" action="<?=base_url()?>main/send_email_vyziadanie_udajov">
                        <div class="col-12">
                            <input type="email" class="form-control full form_line" id="email_get_oou" name="email_get_oou" placeholder="Email" required>
                        </div>
                        <div class="col-12 my-3">
							<label class="checkbox">
								<input type="checkbox" name="oou_get_oou" value="oou_get_oou" required>
								<span class="checkbox-label">Súhlasím so spracovaním osobných údajov výhradne na účel získania týchto informácií</span>
							</label>
                        </div>
                        <div class="col-12 text-center">
                            <button type="submit" class="own-btn text-uppercase" value="submit" name="contactFormConfirm" id="contactFormConfirm">Odoslať</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>