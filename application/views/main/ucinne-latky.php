<section>
	<div class="container">
		<div class="white-bg-box">
			<div class="row">
				<div class="col-12 text-center mb-5">
					<h1 class="h1">Ovomukoidy <span>a ovoinhibítory</span></h1>
				</div>
				<div class="col-md-6 mb-md-0 mb-4">
					<p>Ak trpíte alergiou a hľadáte prírodný produkt, ktorý môžete kombinovať s liekmi na predpis, prípadne ich ním úplne nahradiť, StopALERG je vhodnou alternatívou pre vás. Je prírodný a šetrný k organizmu, je vhodný pre deti od 3 rokov a mnoho zákazničiek ho ocení napríklad v tehotenstve či pri dojčení.</p>
					<p class="font-weight-bold">Ovomukoidy a ovoinhibítory, prírodný zázrak na potlačenie alergie</p>
					<p>Pri alergii organizmus reaguje rôznymi prejavmi a vyplavujú sa rozličné bioaktívne látky. Uvoľňuje sa histamín, ktorý je zodpovedný za nepríjemné alergické reakcie. Na potlačenie jeho pôsobenia sa užívajú rôzne lieky, mnohé obsahujúce kortikoidy, so silnými a často nepríjemnými vedľajšími účinkami.</p>
					<p>Ovomukoidy a ovoinhibítory sú glykoproteíny nachádzajúce sa vo vaječnom bielku, ktoré majú schopnosť inhibovať niektoré z enzýmov prítomných na alergénoch. Môžu znižovať alergickú odpoveď organizmu naviazaním sa na niektoré typy serínových a trypsínu podobných proteáz exprimovaných na povrchu alergénov a takto potláčať vonkajšie prejavy alergických chorôb.</p>
				</div>
				<div class="col-md-6 mb-md-0 mb-4">
					<p class="font-weight-bold">„Organizmus alergika je zaplavený histamínom, vďaka ovomukoidom sa však ďalej neprodukuje a po jeho prirodzenom odbúraní sa už v tele nevyskytuje a alergické prejavy ustupujú."</p>
					<p>Ovomukoidy a ovoinhibítory však okrem toho, že pôsobia priamo v centre alergickej reakcie, majú silný preventívny účinok, preto je vhodné ich užívať pred očakávaným zvýšeným pôsobením alergénov. Vďaka užívaniu týchto látok sa alergény nedostanú až k bunkám, ktoré by stykom s nimi spustili alergickú reakciu.</p>
					<p class="font-weight-bold">Moc prírodných glykoproteínov poznali už naši predkovia</p>
					<p>Tieto účinné látky obsiahnuté v prepeličích vajíčkach sa na potlačenie nepríjemných alergických reakcií používajú už celé stáročia. V starovekej Číne sa používali v boji so sennou nádchou, astmou alebo atopickým ekzémom. Historické pramene odkazujú aj na dnešné Turecko a Francúzsko, kde sú u alergikov obľúbené dodnes.</p>
				</div>
			</div>
		</div>
	</div>
</section>
<?php $this->load->view('main/_product-detail');?>