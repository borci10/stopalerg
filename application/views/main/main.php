<div id="slider">
	<div class="container">
		<div class="slider-content-wrap d-flex flex-row">
			<div class="slider-content d-flex flex-column justify-content-center">
				<img src="<?=base_url()?>images/slider.png" class="slider-img" alt="Stopalerg">
				<div class="slider-heading-wrap">
					<img src="<?=base_url()?>images/slider-krabicka.png?v1" class="img-float" alt="Stopalerg">
					<h1>Dajte <span>stop</span> alergii</h1>
					<?php
						foreach($products ?? [] as $product) {
							?>
								<div id="addItemToShoppingCart" data-product_id="<?=$product['id']?>" <?=($product['quantity'] == 0 ? 'disabled' : '')?> title="Pridať produkt do košíka">
									<input type="hidden" class="form-control js-countItem" min="1" max="<?=$product['quantity']?>" value="1" id="productQuantity<?=$product['id']?>">
									<div class="price-holder">
										<span>len za</span>
										<span class="price"><?=cutePrice(priceCalc($product['price'], 1, 0, $product['tax'] / 100))?></span>
										<span>s DPH</span>
										<div class="cart-icon">
											<?php include 'images/icons/cart.svg';?>
										</div>
									</div>
								</div>
							<?php
						}
					?>
				</div>
				<div class="slider-content-text">
					<div class="mb-4">
						<p>„StopALERG uľaví od sennej nádchy, alergií na slnko či domácich miláčikov
							<br class="d-lg-inline d-none"> a účinne bojuje aj s atopickým ekzémom.“</p>
					</div>
					<a href="#how-it-works-inner" class="own-btn scrollTo icon-right">
						Chcem sa zbaviť alergie
						<span class="icon-holder">
							<?php include 'images/icons/right.svg';?>
						</span>
					</a>
					<img src="<?=base_url()?>images/icons.png" class="img-fluid" alt="Icons">
				</div>
			</div>
		</div>
	</div>
	<div id="loader">
		<div class="spinner-wrap">
			<svg viewBox="25 25 50 50">
				<circle cx="50" cy="50" r="20"></circle>
			</svg>
		</div>
	</div>
</div>
<section id="how-it-works">
	<div id="how-it-works-inner" class="container pt-4">
		<div class="container-inner">
			<div class="row justify-content-between align-items-center">
				<div class="col-lg-6 text-lg-left text-center order-lg-1 order-2">
					<img src="<?=base_url()?>images/graf.png" alt="Graf" class="img-fluid">
				</div>
				<div class="col-xl-5 col-lg-6 text-lg-left text-center order-lg-2 order-1 mb-lg-0 mb-5">
					<h2 class="h1">Prírodná úľava od <span>alergie</span></h2>
					<p class="text-lg color-secondary">Úľava so StopALERG-om prichádza bez vedľajších účinkov a u väčšina užívateľov v priebehu niekoľkých dní. </p>
				</div>
			</div>
		</div>
	</div>
</section>
<section>
	<div class="container">
		<div class="container-inner">
			<div class="row justify-content-between align-items-center">
				<div class="col-xl-5 col-lg-6 text-lg-left text-center mb-lg-0 mb-5">
					<h2 class="h1">Prírodná úľava od <span>alergie</span></h2>
					<p class="text-lg color-secondary">„StopALERG pomohol mne, najstaršej dcére aj manželovi. Účinky vidíte takmer okamžite. Ja som ho ocenila najmä v tehotenstve a počas kojenia, kedy som nemala iné vhodné možnosti na potlačenie príznakov alergií.“ – Zuzana Stachová Sejáková je naša stála zákazníčka a StopALERG pomáha v boji s alergiou aj jej dcére a manželovi.</p>
				</div>
				<div class="col-lg-6 text-center">
					<img src="<?=base_url()?>images/graf-2.png" alt="Graf" class="img-fluid">
				</div>
			</div>
		</div>
	</div>
</section>
<section>
	<div class="container">
		<div class="row justify-content-between align-items-center">
			<div class="col-12 text-center mb-5">
				<h2 class="h1">Prepeličie vajíčka <span>pomáhajú</span></h2>
			</div>
			<div class="col-md-3 mb-md-0 mb-4 text-center">
				<img src="<?=base_url()?>images/icons-png/travy-a-pel.png" class="img-fluid mb-4" alt="Trávy a peľ">
				<h3>Trávy a <span>peľ</span></h3>
				<p class="mb-0">Jedna z najrozšírenejších alergií, ktorá nám bráni tráviť čas v prírode. Chráňte sa prírodne, StopALERG je vhodná prevencia a pomáha aj v období najvyššieho výskytu alergénov.</p>
			</div>
			<div class="col-md-3 mb-md-0 mb-4 text-center">
				<img src="<?=base_url()?>images/icons-png/milacikovia.png" class="img-fluid mb-4" alt="Miláčikovia">
				<h3>Miláčikovia</h3>
				<p class="mb-0">Alergia na srsť domácich miláčikov môže mať rôzne formy. So zvieratkom sa nemusíte rozlúčiť, s príznakmi sa dá bojovať aj šetrne bez vedľajších účinkov.</p>
			</div>
			<div class="col-md-3 mb-md-0 mb-4 text-center">
				<img src="<?=base_url()?>images/icons-png/atopicky-ekzem.png" class="img-fluid mb-4" alt="Atopický ekzém">
				<h3>Atopický <span>ekzém</span></h3>
				<p class="mb-0">Časté ochorenie u detí aj dospelých, súvisí s oslabenou imunitou a príznaky sú veľmi nepríjemné. StopALERG je vhodný pre deti už od 3 rokov.</p>
			</div>
			<div class="col-md-3 mb-md-0 mb-4 text-center">
				<img src="<?=base_url()?>images/icons-png/roztoce.png" class="img-fluid mb-4" alt="Roztoče">
				<h3>Roztoče</h3>
				<p class="mb-0">Alergia na roztoče častokrát súvisí s ročným obdobím a zhoršuje sa so zvýšeným vírením prachu. Prírodný StopALERG pomôže uľaviť od dýchacích problémov.</p>
			</div>
		</div>
	</div>
</section>
<?php $this->load->view('main/_product-detail');?>
<section id="references">
	<div class="container">
		<div class="overlay">
			<div class="row mb-5">
				<div class="col-12 text-center">
					<h2 class="h1">Recenzie na <span>StopALERG</span></h2>
				</div>
			</div>
			<div id="references-carousel" class="owl-carousel owl-theme">
				<?php $this->load->view('review/review_item');?>
			</div>
			<div class="text-center mt-5">
				<a href="<?=base_url()?>recenzie" class="own-btn icon-right">
					Všetky recenzie
					<span class="icon-holder">
						<?php include 'images/icons/right.svg';?>
					</span>
				</a>
			</div>
		</div>
	</div>
</section>