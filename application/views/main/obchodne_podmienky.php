<section id="personal-data">
	<div class="container">
		<div class="row">
			<div class="col-12">
				<h3>I. Základné ustanovenia</h3>

				<p class="first-level">Tieto všeobecné obchodné podmienky upravujú vzájomné vzťahy pri kúpe tovaru ponúkaného na internetovom obchode <a href="https://www.sopalerg.sk/">www.stopalerg.sk</a> medzi zmluvný mi stranami kúpnej zmluvy, kedy na jednej strane je RM PHARM s.r.o., Vlárska 66, 831 01, Bratislava, IČO: 47 807 512, DI Č: 2024100749, IČ DPH: SK2024100749 ako <strong>predávajúci</strong> a na strane druhej je <strong>kupujúci</strong>. Kupujúcim je súkromná osoba, živnostník, právnická osoba, alebo organizácia.

				<p class="first-level">Všetky zmluvné vzť ahy medzi predávajúcim a kupujúcim sa spravujú právnym poriadkom Slovenskej republiky. V prípade, ak je zmluvnou stranou na strane kupujúceho osoba v postavení spotrebiteľa, riadia sa právne vzť ahy neupravené týmito všeobecnými obchodnými podmienkami zákonom č. 40/1964 Z.z. Občianskeho zákonníka v platnom znení, zákonom č . 250/2007 Z. z. o ochrane spotrebiteľ a v platnom znení a zákonom č . 102/2014 Z. z. o ochrane spotrebiteľa pri podomovom predaji a zásielkovom predaji v platnom znení. V prípade, ak je zmluvnou stranou na strane kupujúceho podnikateľ, riadia sa právne vzťahy neupravené týmito všeobecnými obchodný mi podmienkami príslušnými ustanoveniami zák. č 513/1991 Zb., Obchodného zákonníka v znení neskorších predpisov, ako aj súvisiacimi predpismi.

				<h3>II. Bezpečnosť a ochrana informácií</h3>

				<p class="first-level">Odoslaním objednávky kupujúci vyjadruje súhlas so spracovaním a archiváciou jeho osobných údajov v databáze predávajúceho v súlade so zákonom č. 122/2013 Z.z. o ochrane osobných údajov v platnom a účinnom znení.

				<h3>III. Postup spracovania objednávky a uzatvorenia kúpnej zmluvy</h3>

				<p class="first-level">Kupujúci si vyberie tovar, vyplní požadované údaje, vyberie si spôsob platby a spôsob dodania tovaru a odošle objednávku. Internetový obchod automaticky eviduje a potvrdí objednávku, čím sa uzatvára kúpna zmluva. Kupujúci odoslaním objednávky akceptuje obchodné podmienky predávajúceho.

				<h3>IV. Platobné podmienky a dodanie tovaru</h3>

				<p class="first-level">Kupujúci si môže zvoliť spôsob platby na dobierku, bankovým prevodom, alebo prostredníctvom služby TatraPay.

				<p class="first-level">Pri platbe na dobierku si predávajúci účtuje ku konečnej sume príplatok za dobierku a poš tovné. Tovar predávajúci expeduje prostredníctvom Slovenskej po šty a.s. najneskôr 72 hodín po prijatí objednávky od kupujúceho. Slovenská pošta tovar dodá približne za 2-4 pracovné dni. V prípade, že nezastihne príjemcu, v poštovej schránke nechá oznámenie o uložení zásielky na po šte. Doba úschovy je 10 dní.

				<p class="first-level">Pri platbe bankovým prevodom alebo prostredníctvom služby TatraPay predávajúci expeduje tovar najneskôr 72 hodín po prijatí platby na účet predávajúceho. Kupujúci si môže zvoli ť spôsob doru čenia tovaru prostredníctvom Slovenskej pošty a.s., alebo osobný odber na adrese odberného miesta: Lekáreň Santal, Dobrovič ova 10, 811 09 Bratislava. Tovar bude na odbernom mieste pre kupujúceho pripravený najneskôr do 2 pracovných dní od prijatia platby na účet predávajúceho.

				<p class="first-level font-weight-bold mb-0">Neprevzatie zásielky zákazníkom

				<p class="first-level">V prípade neprevzatia zásielky kupujúcim (na základe objednávky) si vyhradzujeme právo uplatniť svoj nárok na náhradu vzniknutej škody. V prípade, že si zákazník tovar neprevezme pri prvom doručení, a požaduje opätovné doručenie tovaru, je povinný uhradiť náklady prvého ako aj opätovného doručenia tovaru.

				<h3>V. Ceny</h3>

				<p class="first-level">Všetky ceny v internetovom obchode sú konečné vrátane DPH. Ku konečnej sume si predávajúci účtuje iba poštovné a v prípade zásielky na dobierku, aj príplatok za dobierku. Predávajúci má právo ceny meniť podľa vlastného uváž enia, nie však meniť ceny tovaru po odoslaní objednávky. Akciové ceny platia po dobu časovo určenú. Cena dopravy je súčasťou položiek v objednávke kupujúceho. Cena dopravy sa môže meni ť v závislosti na množstve objednaného tovaru, podľa aktuálnych akcií a pod., čo môže kupujúci sledovať pri zobrazení svojho košíka. Balné je zdarma.

				<h3>VI. Odstúpenie od zmluvy</h3>

				<p class="first-level">
				Kupujúci má právo odstúpiť od zmluvy do 14 dní od prevzatia tovaru v
				prípade, že tovar objednal inak ako osobne a bol mu doručený
				ľubovoľnou prepravnou službou. V prípade, ak spotrebiteľ využije právo
				na odstúpenie od zmluvy v uvedenej lehote, berie na vedomie, že
				dodávateľ je oprávnený požadovať náhradu preukázateľne vynaložených
				nevyhnutných nákladov spojených s odstúpením od zmluvy. Ak sa
				kupujúci rozhodne pre odstúpenie v tejto lehote, je nutné splniť nižšie
				uvedené podmienky:
				</p>
				<p class="second-level">
				a) Odoslať list (najlepšie e-mailom na adresu
				shop@stopalerg.sk s textom: <br>"Chcem jednostranne odstúpiť od zmluvy
				zo dňa DD.MM.RRRR č. (číslo faktúry) a požadujem vrátenie uhradenej
				čiastky za tovar na účet číslo:................. Dátum a podpis.
				</p>
				<p class="second-level">
				b) Tovar doručiť na adresu predávajúceho spolu s priloženou kópiou
				vyššie uvedeného listu.
				</p>
				<p class="second-level">
				c) Tovar odoslaný a doručený späť na adresu
				predávajúceho <strong>musí byť v pôvodnom a nepoškodenom obale,
				najlepšie nerozbalený, nesmie byť použitý, musí byť nepoškodený a
				kompletný, spolu s kópiou dokladu o zaplatení.</strong> Neposielajte tovar na
				dobierku, inak nebude prevzatý. Odporúčame Vám tovar poistiť. V
				prípade nedodržania niektorej z uvedených náležitostí má predávajúci
				právo odmietnuť vrátený tovar alebo pomerne znížiť čiastku, za ktorú
				bude tovar prijatý naspäť. Lehota na vrátenie tovaru sa považuje za
				zachovanú, ak bol tovar odovzdaný na prepravu najneskôr v posledný
				deň lehoty. Náklady spojené s vrátením tovaru k nám na prevádzku
				znáša zákazník.
				</p>
				<p class="second-level">
				d) Pri splnení všetkých vyššie uvedených podmienok pre
				vrátenie tovaru, vám peniaze za tovar zašleme prevodom na účet, a to
				najneskôr do 14 pracovných dní po prijatí tovaru. V hotovosti či šekom
				peniaze neuhrádzame.
				</p>
				<p class="second-level">
				e)	V prípade nesplnenia niektorej z vyššie uvedených podmienok nebudeme akceptovať odstúpenie od zmluvy a tovar bude vrátený na
				náklady kupujúceho naspäť. Predávajúci je oprávnený účtovať kupujúcemu prípadné ďalšie vzniknuté náklady.
				</p>
				<h3>VII. Reklamácia</h3>

				<p class="first-level">Kupujúci môže náš tovar reklamovať e-mailom na kontakty uvedené v sekcii kontakt. Musí uviesť dôvody reklamácie a doručiť nám reklamovaný tovar.</p>

				<p class="first-level">Predávajúci sa zaväzuje v prípade opodstatnenej reklamácie vybaviť ju do 30 dní a na vlastné náklady poslať kupujúcemu nový, nepoškodený výrobok rovnakého druhu. Poš kodený tovar je nutné vrátiť predávajúcemu, poš tovné si hradí kupujúci.</p>

				<p class="first-level">Veríme, že na našich produktoch nebude čo reklamovať.</p>

				<p class="first-level font-weight-bold mb-0">Poškodenie tovaru pri doručení</p>
				<p class="first-level">Kupujúci je povinný si skontrolovať tovar pri doručení. V prípade evidentného po škodenia tovaru vzniknutého počas doručenia má nárok na vrátenie tovaru. Kupujúci spíše s prepravcom reklamáciu, tovar neprevezme. Kupujúci nás bude neodkladne informovať o tejto skuto č nosti. Tovar nám bude vrátený a my kupujúcemu pošleme nový tovar na naš e náklady.</p>

				<h3>VIII. Záverečné ustanovenia</h3>

				<p class="first-level">Vo veciach, ktoré nie sú obsiahnuté v týchto obchodných podmienkach, sa uplatnia prísluš né ustanovenia Obč ianskeho zákonníka. Kupujúci zaslaním objednávky tovaru akceptuje tieto obchodné podmienky, ku ktorým sa potvrdením objednávky zaviazal aj predávajúci.</p>

				<p class="first-level">Tieto Obchodné podmienky platia od 26.03.2017 do vydania nových všeobecných obchodných podmienok. Predávajúci si vyhradzuje právo zmeniť všeobecné obchodné podmienky bez predchádzajúceho upozornenia.</p>

				<h3>Ďalšie všeobecné podmienky</h3>

				<p class="first-level">Používanie webovej stránky je pre každého používateľa zadarmo.</p>

				<p class="first-level">Registrácia na stránku je dobrovoľná a bezplatná.</p>

				<p class="first-level">V Bratislave, dňa 26.03.2017</p>
			</div>
		</div>
	</div>
</section>