<section id="kontakt">
	<div class="container">
		<div class="row">
			<div class="col-lg-6 mb-lg-0 mb-5">
				<div class="white-bg-box">
					<h3 class="text-uppercase pb-4"><span>Informácie</span></h3>
					<p class="mb-0 font-weight-bold">Prevádzkovateľ obchodu / Výhradný distribútor:</p>
					<p class="mb-3">RM PHARM s.r.o., Vlárska 66, 831 01 Bratislava, IČO: 47 807 512</p>
					<p class="mb-0 font-weight-bold">Odberné miesto:</p>
					<p class="mb-0">Lekáreň Santal, Dobrovičova 10, 811 09 Bratislava</p>
					<p class="mb-3">(Pondelok - Piatok: 8:00 - 18:00, Sobota - Nedeľa: zatvorené)</p>
					<p class="mb-0 font-weight-bold">Výrobca:</p>
					<p class="mb-0">Hedelab, Route de Wallonie 138, 7011 Ghlin, Belgicko</p>
					<p class="mb-0">Prípravok bol schválený Úradom verejného zdravotníctva SR – číslo OHVBPKV/7353/2014/Ht</p>
				</div>
			</div>
			<div class="col-lg-6">
				<form id="kontakt-form" method="post" action="<?=base_url();?>main/email_send">
					<div class="row mb-4">
						<div class="col-6">
							<input type="text" class="form-control" name="e-mail" placeholder="Email" required>
						</div>
						<div class="col-6">
							<input type="text" class="form-control" name="meno" placeholder="Vaše meno" required>
						</div>
					</div>
					<div class="row mb-4">
						<div class="col-12">
							<input type="text" class="form-control" name="phone" placeholder="Telefón" required>
						</div>
					</div>
					<div class="row mb-4">
						<div class="col-12">
							<textarea class="form-control" name="sprava" placeholder="Správa" rows="7"></textarea>
						</div>
					</div>
					<div class="row mb-4">
						<div class="col-12">
							<label class="checkbox">
								<input type="checkbox" name="orderOou" value="oou" required>
								<span class="checkbox-label">Súhlasím so <a href="<?=base_url()?>ochrana-osobnych-udajov" target="_blank" class="fancyLink">spracovaním osobných údajov</a>.</span>
							</label>
						</div>
					</div>
					<div class="row">
						<div class="col-12">
							<button class="own-btn pull-right button-form-send">Odoslať</button>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
</section>
