<?php
	if (isset($totalCount)) {
		?>
			<div class="col-12">
				<div class="d-flex justify-content-end align-items-center">
					<div class="mr-3">
						<span id="actualResults"><b>1 - <?=min($this->config->item('sql_result_limit'), $totalCount)?></b></span> z <b><?=$totalCount?></b>
					</div>
					<div class="">
						<div class="input-group">
						<span class="input-group-btn">
							<button class="btn btn-cart js-prevPage" type="button"><i class="fa fa-caret-left" aria-hidden="true"></i></button>
						</span>
							<span class="input-group-btn px-1">
							<button class="btn btn-cart js-nextPage" type="button"><i class="fa fa-caret-right" aria-hidden="true"></i></button>
						</span>
						</div>
					</div>
				</div>
			</div>
			<?php
				$query = '';
				if ($_SERVER['QUERY_STRING']) {
					$query = '?' . $_SERVER['QUERY_STRING'];
				}
				
				$url = '';
				if ($this->uri->segment(1)) {
					$url .= $this->uri->segment(1) . '/';
				}
				if ($this->uri->segment(2)) {
					$url .= $this->uri->segment(2) . '/';
				}
			?>
			<script>
				var actualPage = 0;

				$(document).on('click', '.js-nextPage', function() {
					$.ajax({
						url: '<?=base_url()?><?=$url?>indexPage<?=$query?>',
						data: {page : +actualPage + +1, category:'<?=$this->uri->segment(2) ?? -1?>'},
						dataType: 'text',
						type: 'post',
						success: function (data) {
							if (data) {
								actualPage++;		
								$('#contentBody').html(data);
								$('#actualResults').html('<b>' + Math.max(actualPage * <?=$this->config->item('sql_result_limit')?>, 1) + ' - ' + Math.min((actualPage+1) * <?=$this->config->item('sql_result_limit')?>, <?=$totalCount?>) + '</b>');
							}
						}
					});
				});

				$(document).on('click', '.js-prevPage', function() {
					$.ajax({
						url: '<?=base_url()?><?=$this->uri->segment(1)?>/indexPage<?=$query?>',
						data: {page : actualPage - 1, category:'<?=$this->uri->segment(2) ?? -1?>'},
						dataType: 'text',
						type: 'post',
						success: function (data) {
							if (data) {
								actualPage--;
								$('#contentBody').html(data);
								$('#actualResults').html('<b>' + Math.max(actualPage * <?=$this->config->item('sql_result_limit')?>, 1) + ' - ' + Math.min((actualPage+1) * <?=$this->config->item('sql_result_limit')?>, <?=$totalCount?>) + '</b>');
							}
						}
					});
				});
			</script>
		<?php
	}
?>