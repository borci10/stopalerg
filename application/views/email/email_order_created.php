Dobrý deň pán/pani <?=$orderSummary['surname']?>,<br><br>
ďakujeme Vám za objednávku na našej stránke stopalerg.sk. Teší nás, že ste si vybrali produkt StopALERG.<br><br>
Tento e-mail je potvrdenie, že sme Vašu objednávku číslo: <?=generateVarSymbol($orderSummary['orderId'])?> prijali a začíname ju spracovávať.<br><br>
<h2>Objednávka - <?=generateVarSymbol($orderSummary['orderId'])?></h2>
<table style="width: 500px;">
	<thead style="text-align: left;">
		<tr>
			<th>Názov produktu</th>
			<th>Cena za kus</th>
			<th>Množstvo</th>
			<th>Cena</th>
		</tr>
	</thead>
	<tbody>
		<?php
			foreach($order_items as $item) {
				?>
					<tr>
						<td class="font-weight-semibold">
							<?php
							if ($item['id_product_child'] == 0) {
								?>
								<a href="<?=base_url()?>products/<?=$item['link']?>" class="link" target="_blank"><?=$item['name']?></a>
								<?php
							} else {
								?>
								<a href="<?=base_url()?>products/<?=$item['link']?>/<?=$item['product_variable_link']?>" class="link" target="_blank"><?=$item['name']?></a>
								<?php
							}
							?>
						</td>
						<td>
							<?=cutePrice(priceCalc($item['product_price'], 1, $item['discount'], $item['tax'] / 100))?>
						</td>
						<td>
							<?=$item['quantity']?>
						</td>
						<td>
							<?=cutePrice(priceCalc($item['product_price'], $item['quantity'], $item['discount'], $item['tax'] / 100))?>
						</td>
					</tr>
				<?php
			}
		?>
		<tr>
			<td><?=$orderSummary['delivery']?></td>
			<td></td>
			<td></td>
			<td><?=cutePrice($orderSummary['delivery_price'], $this->config->item('currency'))?></td>
		</tr>
		<tr>
			<td><?=$orderSummary['payment']?></td>
			<td></td>
			<td></td>
			<td><?=cutePrice($orderSummary['payment_price'], $this->config->item('currency'))?></td>
		</tr>
	</tbody>
	<tfoot class="tfoot-fancy">
		<tr>
			<td></td>
			<td></td>
			<td><b>Celková cena</b></td>
			<td><b><?=cutePrice($orderSummary['cart_price'] + $orderSummary['delivery_price'] + $orderSummary['payment_price'], $this->config->item('currency'))?></b></td>
		</tr>
	</tfoot>
</table>

<?php
	if($orderSummary['payment'] != 'Dobierka') {
		?>
			<table style="width: 300px;">
				<tr>
					<td colspan="2"><strong>Platobné údaje</strong></td>
				</tr>
				<tr>
					<td>Číslo účtu</td>
					<td><?=$shopSettings['bank_account_number'];?> / <?=$shopSettings['bank_code'];?></td>
				</tr>
				<tr>
					<td>IBAN</td>
					<td><?=$shopSettings['bank_iban'];?></td>
				</tr>
				<tr>
					<td>Variabilný symbol</td>
					<td><?=generateVarSymbol($orderSummary['orderId'])?></td>
				</tr>
			</table>
		<?php
	}
?>

<table style="width: 300px;">
	<tr>
		<td colspan="2"><strong>Dodacie údaje</strong></td>
	</tr>
	<tr>
		<td>Meno</td>
		<td><?=$orderSummary['name']?></td>
	</tr>
	<tr>
		<td>Priezvisko</td>
		<td><?=$orderSummary['surname']?></td>
	</tr>
	<tr>
		<td>Email</td>
		<td><?=$orderSummary['email']?></td>
	</tr>
	<tr>
		<td>Telefón</td>
		<td><?=$orderSummary['phone_number']?></td>
	</tr>
	<tr>
		<td>Mesto</td>
		<td><?=$orderSummary['city']?></td>
	</tr>
	<tr>
		<td>Ulica</td>
		<td><?=$orderSummary['street']?></td>
	</tr>
	<tr>
		<td>PSČ</td>
		<td><?=$orderSummary['zip_code']?></td>
	</tr>
	<?php
		if (@$orderSummary['diff_delivery'] == 1) {
			?>
				<tr>
					<td colspan="2"><strong>Adresa doručenia</strong></td>
				</tr>
				<?php
					if (isset($orderSummary['delivery_name'])) {
						?>
						<tr>
							<td class="font-weight-bold">Meno</td>
							<td><?=$orderSummary['delivery_name']?></td>
						</tr>
						<?php
					}
				?>
				<?php
					if (isset($orderSummary['delivery_surname'])) {
						?>
						<tr>
							<td class="font-weight-bold">Priezvisko</td>
							<td><?=$orderSummary['delivery_surname']?></td>
						</tr>
						<?php
					}
				?>
				<tr>
					<td class="font-weight-bold">Mesto</td>
					<td><?=$orderSummary['delivery_city']?></td>
				</tr>
				<tr>
					<td class="font-weight-bold">Ulica</td>
					<td><?=$orderSummary['delivery_street']?></td>
				</tr>
				<tr>
					<td class="font-weight-bold">PSČ</td>
					<td><?=$orderSummary['delivery_zip_code']?></td>
				</tr>
			<?php
		}
	?>
</table>
<br><br>O ďalšom postupe Vás budeme čoskoro informovať e-mailom.<br><br>
Ďakujeme za dôveru a prajeme Vám príjemný deň.<br><br>
Váš StopALERG.
<br><br>-----------------------<br><br>
<strong>RM PHARM S.R.O.</strong><br>
Vlárska 66<br>
Bratislava 831 01<br>
IČO: 47807512<br>
DIČ: 2024100749<br>
IČ DPH: SK2024100749<br>
email: shop@stopalerg.sk