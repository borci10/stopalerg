<?php
	if (isset($_SESSION['shoppingCart'][1][0]['itemCount'])) {
		?>
			<div>
				<div class="container">
					<div class="row">
						<div class="col-12">
							<ul id="progress-bar-holder" class="list-unstyled d-flex justify-content-between">
								<li class="progress-bar <?=(($this->uri->segment(2) == '' || $this->uri->segment(2) == 'personal-info' || $this->uri->segment(2) == 'payment-info' || $this->uri->segment(2) == 'order-summary') ? 'active' : '')?>">
									<a href="<?=base_url()?>cart">
										<div class="icon-holder icon-md">
											<?php include 'images/icons/online-shop.svg';?>
										</div>
									</a>
								</li>
								<?php
								if ($this->cart_model->checkShoppingCartStepDone()) {
									?>
									<li class="progress-bar active">
										<a href="<?=base_url()?>cart/personal-info">
											<div class="icon-holder icon-md">
												<?php include 'images/icons/dossier.svg';?>
											</div>
										</a>
									</li>
									<?php
								} else {
									?>
									<li>
										<div class="disabled">
											<div class="icon-holder icon-md">
												<?php include 'images/icons/dossier.svg';?>
											</div>
										</div>
									</li>
									<?php
								}
								?>
								<?php
								if ($this->cart_model->checkPersonalInfoStepDone() && $this->cart_model->checkShoppingCartStepDone()) {
									?>
									<li class="progress-bar active">
										<a href="<?=base_url()?>cart/payment-info">
											<div class="icon-holder icon-md">
												<?php include 'images/icons/wallet.svg';?>
											</div>
										</a>
									</li>
									<?php
								} else {
									?>
									<li class="progress-bar">
										<div class="disabled">
											<div class="icon-holder icon-md">
												<?php include 'images/icons/wallet.svg';?>
											</div>
										</div>
									</li>
									<?php
								}
								?>
								<?php
								if ($this->cart_model->checkPaymentInfoStepDone() && $this->cart_model->checkPersonalInfoStepDone() && $this->cart_model->checkShoppingCartStepDone()) {
									?>
									<li class="progress-bar active">
										<a href="<?=base_url()?>cart/order-summary">
											<div class="icon-holder icon-md">
												<?php include 'images/icons/report.svg';?>
											</div>
										</a>
									</li>
									<?php
								} else {
									?>
									<li class="progress-bar">
										<div class="disabled">
											<div class="icon-holder icon-md">
												<?php include 'images/icons/report.svg';?>
											</div>
										</div>
									</li>
									<?php
								}
								?>
							</ul>
						</div>
					</div>
				</div>
			</div>
		<?php
	}
?>