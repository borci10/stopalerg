<div class="cart">
	<div class="container py-5">
		<div class="white-bg-box">
			<form method="post" action="<?=base_url()?>cart/paymentInfo">
				<div class="row payment-wrap">
					<div class="col-lg-6 mb-4 payment-option">
						<p><b>Spôsob dodania</b></p>
						<?php
							$first = 1; // default check first option
							foreach($delivery as $deliveyOption) {
								?>
									<div class="form-check mt-1">
										<input class="form-check-input" type="radio" name="delivery" id="delivery_<?=$deliveyOption['id']?>" value="<?=$deliveyOption['id']?>" <?=is_checked(@$_SESSION['delivery'], $deliveyOption['title'], $first)?> required>
										<label class="form-check-label" for="delivery_<?=$deliveyOption['id']?>">
											<?=$deliveyOption['title']?>
											<span><?=cutePrice($deliveyOption['price'] + $supplierPrice)?></span>
										</label>
										<span class="check"></span>
									</div>
								<?php
								$first = 0;
							}
						?>
					</div>
					<div class="col-lg-6 mb-4">
						<p><b>Spôsob platby</b></p>
						<?php
							$first = 1; // default check first option
							foreach($payment as $paymentOption) {
								if (($paymentOption['uid'] == 'ttp')) {
									if (!$this->config->item('trust_pay')) {
										continue;
									}
								}
								?>
									<div class="form-check">
										<input class="form-check-input" type="radio" name="payment" id="payment_<?=$paymentOption['id']?>" value="<?=$paymentOption['id']?>" <?=is_checked(@$_SESSION['payment'], $paymentOption['title'], $first)?> required>
										<label class="form-check-label" for="payment_<?=$paymentOption['id']?>">
											<?=$paymentOption['title']?>
											<span><?=cutePrice($paymentOption['price'])?></span>
										</label>
										<span class="check"></span>
									</div>
								<?php
								$first = 0;
							}
						?>
					</div>
				</div>
				<div class="cart-footer text-center">
					<a href="<?=base_url()?>cart/personal-info" class="own-btn transparent my-1 mx-1">Späť</a>
					<button type="submit" class="own-btn my-1 mx-1" value="Pokračovať k sumáru" name="formConfirm" id="formConfirm">Sumár objednávky</button>
				</div>
			</form>
		</div>
	</div>
</div>