<div class="cart">
	<div class="container py-5">
		<div class="white-bg-box">
			<form id="regForm" method="post" data-toggle="validator" role="form">
				<div class="row cart-form">
					<div class="col-md-6 mb-4">
						<label for="cartName">Meno *</label>
						<input
								type="text"
								class="form-control"
								id="cartName"
								maxlength="20"
								name="cartName"
								value="<?=set_value('cartName', $this->session->userdata('name'));?>"
								required
						>
						<div class="help-block with-errors"><?php echo form_error('cartName'); ?></div>
					</div>
					<div class="col-md-6 mb-4">
						<label for="cartSurname">Priezvisko *</label>
						<input
								type="text"
								class="form-control"
								id="cartSurname"
								maxlength="20"
								name="cartSurname"
								value="<?=set_value('cartSurname', $this->session->userdata('surname'));?>"
								required
						>
						<div class="help-block with-errors"><?php echo form_error('cartSurname'); ?></div>
					</div>
					<div class="col-md-6 mb-4">
						<label for="cartEmail">Emailová adresa *</label>
						<input
							type="email"
							class="form-control"
							id="cartEmail"
							name="cartEmail"
							value="<?=set_value('cartEmail', $this->session->userdata('email'));?>"
							required
						>
						<div class="help-block with-errors"><?php echo form_error('cartEmail'); ?></div>
					</div>
					<div class="col-md-6 mb-4">
						<label for="cartPhone">Telefónne číslo *</label>
						<input
								type="text"
								class="form-control"
								id="cartPhone"
								name="cartPhone"
								value="<?=set_value('cartPhone', $this->session->userdata('phone_number'));?>"
								required
						>
						<div class="help-block with-errors"><?php echo form_error('cartPhone'); ?></div>
					</div>
					<div class="col-md-4 mb-4">
						<label for="cartCity">Mesto *</label>
						<input
								type="text"
								class="form-control"
								id="cartCity"
								name="cartCity"
								value="<?=set_value('cartCity', $this->session->userdata('city'));?>"
								required
						>
						<div class="help-block with-errors"><?php echo form_error('cartCity'); ?></div>
					</div>
					<div class="col-md-4 mb-4">
						<label for="cartStreet">Ulica *</label>
						<input
							type="text"
							class="form-control"
							id="cartStreet"
							name="cartStreet"
							value="<?=set_value('cartStreet', $this->session->userdata('street'));?>"
							required
						>
						<div class="help-block with-errors"><?php echo form_error('cartStreet'); ?></div>
					</div>
					<div class="col-md-4 mb-4">
						<label for="cartZipCode">PSČ *</label>
						<input
							type="text"
							class="form-control"
							id="cartZipCode"
							name="cartZipCode"
							value="<?=set_value('cartZipCode', $this->session->userdata('zip_code'));?>"
							pattern="^\d{3} ?\d{2}$"
							required
						>
						<div class="help-block with-errors"><?php echo form_error('cartZipCode'); ?></div>
					</div>
				</div>
				<div class="form-group row">
					<div class="col-12">
						<label class="checkbox">
							<input type="checkbox" class="form-check-input" value="1" id="cartDifferentDeliveyAdress" name="cartDifferentDeliveyAdress" <?=is_checked($this->session->userdata('diff_delivery'), 1)?>>
							<span class="checkbox-label">Doručiť na inú adresu</span>
						</label>
					</div>
				</div>
				<div class="deliveryInfoForm" style="display:<?=($this->session->userdata('diff_delivery') ? 'block' : 'none')?>;">
					<div class="row cart-form">
						<div class="col-md-6 mb-4">
							<label for="cartDeliveryName">Meno</label>
							<input
									type="text"
									class="form-control"
									id="cartDeliveryName"
									maxlength="20"
									name="cartDeliveryName"
									value="<?=set_value('cartDeliveryName', $this->session->userdata('delivery_name'));?>"
							>
							<div class="help-block with-errors"><?php echo form_error('cartDeliveryName'); ?></div>
						</div>
						<div class="col-md-6 mb-4">
							<label for="cartDeliverySurname">Priezvisko</label>
							<input
									type="text"
									class="form-control"
									id="cartDeliverySurname"
									maxlength="20"
									name="cartDeliverySurname"
									value="<?=set_value('cartDeliverySurname', $this->session->userdata('delivery_surname'));?>"
							>
							<div class="help-block with-errors"><?php echo form_error('cartSurname'); ?></div>
						</div>
						<div class="col-md-4 mb-4">
							<label for="cartDeliveryCity">Mesto *</label>
							<input
									type="text"
									class="form-control"
									id="cartDeliveryCity"
									name="cartDeliveryCity"
									value="<?=set_value('cartDeliveryCity', $this->session->userdata('delivery_city'));?>"
							>
							<div class="help-block with-errors"><?php echo form_error('cartDeliveryCity'); ?></div>
						</div>
						<div class="col-md-4 mb-4">
							<label for="cartDeliveryStreet">Ulica *</label>
							<input
								type="text"
								class="form-control"
								id="cartDeliveryStreet"
								name="cartDeliveryStreet"
								value="<?=set_value('cartDeliveryStreet', $this->session->userdata('delivery_street'));?>"
							>
							<div class="help-block with-errors"><?php echo form_error('cartDeliveryStreet'); ?></div>
						</div>
						<div class="col-md-4 mb-4">
							<label for="cartDeliveryZipCode">PSČ *</label>
							<input
								type="text"
								class="form-control"
								id="cartDeliveryZipCode"
								name="cartDeliveryZipCode"
								value="<?=set_value('cartDeliveryZipCode', $this->session->userdata('delivery_zip_code'));?>"
								pattern="^\d{3} ?\d{2}$"
							>
							<div class="help-block with-errors"><?php echo form_error('cartDeliveryZipCode'); ?></div>
						</div>
					</div>
				</div>
				<div class="cart-footer text-center">
					<a href="<?=base_url()?>cart" class="own-btn transparent my-1 mx-1">Späť</a>
					<button type="submit" class="own-btn my-1 mx-1" value="Pokračovať" name="formConfirm" id="formConfirm">Platobné informácie <i class="fa fa-angle-right"></i></button>
				</div>
			</form>
		</div>
	</div>
</div>