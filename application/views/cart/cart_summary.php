<div class="cart summary">
	<div class="container py-5">
		<div class="white-bg-box">
			<h4 class="d-lg-none d-block text-center"><span>Dodacie údaje</span></h4>
			<table class="table mb-5">
				<thead class="thead-fancy thead-fancy-border-top">
					<tr>
						<th colspan="2">Dodacie údaje</th>
					</tr>
				</thead>
				<tbody class="tbody-fancy">
					<tr>
						<td class="font-weight-bold">Meno</td>
						<td><?=$_SESSION['name']?></td>
					</tr>
					<tr>
						<td class="font-weight-bold">Priezvisko</td>
						<td><?=$_SESSION['surname']?></td>
					</tr>
					<tr>
						<td class="font-weight-bold">Email</td>
						<td><?=$_SESSION['email']?></td>
					</tr>
					<tr>
						<td class="font-weight-bold">Telefón</td>
						<td><?=$_SESSION['phone_number']?></td>
					</tr>
					<tr>
						<td class="font-weight-bold">Mesto</td>
						<td><?=$_SESSION['city']?></td>
					</tr>
					<tr>
						<td class="font-weight-bold">Ulica</td>
						<td><?=$_SESSION['street']?></td>
					</tr>
					<tr>
						<td class="font-weight-bold">PSČ</td>
						<td><?=$_SESSION['zip_code']?></td>
					</tr>
				</tbody>
			</table>
			<?php
				if ($this->session->userdata('diff_delivery') == 1) {
					?>
						<h4 class="d-lg-none d-block text-center"><span>Adresa doručenia</span></h4>
						<table class="table mb-5">
							<thead class="thead-fancy thead-fancy-border-top">
								<tr>
									<th colspan="2">Adresa doručenia</th>
								</tr>
							</thead>
							<tbody class="tbody-fancy">
								<?php
									if (isset($_SESSION['delivery_name'])) {
										?>
											<tr>
												<td class="font-weight-bold">Meno</td>
												<td><?=$_SESSION['delivery_name']?></td>
											</tr>
										<?php
									}
								?>
								<?php
								if (isset($_SESSION['delivery_surname'])) {
									?>
									<tr>
										<td class="font-weight-bold">Priezvisko</td>
										<td><?=$_SESSION['delivery_surname']?></td>
									</tr>
									<?php
								}
								?>
								<tr>
									<td class="font-weight-bold">Mesto</td>
									<td><?=$_SESSION['delivery_city']?></td>
								</tr>
								<tr>
									<td class="font-weight-bold">Ulica</td>
									<td><?=$_SESSION['delivery_street']?></td>
								</tr>
								<tr>
									<td class="font-weight-bold">PSČ</td>
									<td><?=$_SESSION['delivery_zip_code']?></td>
								</tr>
							</tbody>
						</table>
					<?php
				}
			?>
			<h4 class="d-lg-none d-block text-center"><span>Sumár objednávky</span></h4>
			<table class="table mb-5">
				<thead class="thead-fancy thead-fancy-border-top">
					<tr>
						<th>Názov produktu</th>
						<th>Cena za kus</th>
						<th>Množstvo</th>
						<th>Cena</th>
					</tr>
				</thead>
				<tbody class="tbody-fancy">
					<?php
						foreach($shoppingCart as $productId => $productVariation) {
							$parentProductInfo = $this->product_model->getProductShort($productId);
							foreach($productVariation as $variationId => $variationItemCount) {
								if ($variationId == 0) {
									$product = [
										'id' 		=> $parentProductInfo->id,
										'child_id' 	=> 0,
										'name' 		=> $parentProductInfo->name,
										'price' 	=> $parentProductInfo->price,
										'discount' 	=> $parentProductInfo->discount,
										'tax' 		=> $parentProductInfo->tax,
										'quantity' 	=> $parentProductInfo->quantity,
									];
								} else {
									$variableProductInfo = $this->product_model->getProductVariableShort($variationId);
									$product = [
										'id' 		=> $parentProductInfo->id,
										'child_id' 	=> $variableProductInfo->id,
										'name' 		=> $variableProductInfo->name,
										'price' 	=> $variableProductInfo->price,
										'discount' 	=> $variableProductInfo->discount,
										'tax' 		=> $parentProductInfo->tax,
										'quantity' 	=> $variableProductInfo->quantity,
									];
								}
								?>
									<tr>
										<td class="font-weight-bold"><?=$product['name']?></td>
										<td><?=cutePrice(priceCalc($product['price'], 1, $product['discount'], $product['tax'] / 100))?></td>
										<td><?=$_SESSION['shoppingCart'][$product['id']][$product['child_id']]['itemCount']?>x</td>
										<td><?=cutePrice(priceCalc($product['price'], $_SESSION['shoppingCart'][$product['id']][$product['child_id']]['itemCount'], $product['discount'], $product['tax'] / 100))?></td>
									</tr>
								<?php
							}
						}
					?>
					<tr>
						<td class="font-weight-bold"><?=$_SESSION['delivery']?></td>
						<td class="d-lg-table-cell d-none"></td>
						<td class="d-lg-table-cell d-none"></td>
						<td><?=cutePrice($_SESSION['delivery_price'])?></td>
					</tr>
					<tr>
						<td class="font-weight-bold"><?=$_SESSION['payment']?></td>
						<td class="d-lg-table-cell d-none"></td>
						<td class="d-lg-table-cell d-none"></td>
						<td><?=cutePrice($_SESSION['payment_price'])?></td>
					</tr>
				</tbody>
				<tfoot class="tfoot-fancy">
					<tr>
						<td></td>
						<td></td>
						<td><b>Celková cena</b></td>
						<td><b><?=cutePrice($_SESSION['cart_price'] + $_SESSION['delivery_price'] + $_SESSION['payment_price'])?></b></td>
					</tr>
				</tfoot>
			</table>
			<form method="post" action="<?=base_url()?>cart/order-summary">
				<div class="form-check mb-3">
					<label class="checkbox">
						<input type="checkbox" name="oou" class="form-check-input" required>
						<span class="checkbox-label">Oboznámil som sa s <a href="<?=base_url()?>ochrana-osobnych-udajov" class="link" target="_blank">ochranou osobných údajov.</a></span>
					</label>
				</div>
				<div class="form-check mb-3">
					<label class="checkbox">
						<input type="checkbox" name="vop" class="form-check-input" required>
						<span class="checkbox-label">Oboznámil som sa s <a href="<?=base_url()?>obchodne-podmienky" class="link" target="_blank">obchodnými podmienkami.</a></span>
					</label>
				</div>
				<div class="form-check">
					<label class="checkbox">
						<input type="checkbox" name="cartNewsletter" id="cartNewsletter" class="form-check-input" value="1">
						<span class="checkbox-label">Chcem sa prihlásiť na odber noviniek</span>
					</label>
				</div>
				<div class="cart-footer text-center mt-4">
					<a href="<?=base_url()?>cart/payment-info" class="own-btn transparent my-1 mx-1">Späť</a>
					<input type="submit" class="own-btn my-1 mx-1" value="Objednať s povinnosťou platby" name="formConfirm" id="formConfirm">
				</div>
			</form>
		</div>
	</div>
</div>