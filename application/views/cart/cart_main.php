<div class="cart">
	<div class="container py-5">
		<?php
			if (isset($_SESSION['shoppingCart'][1][0]['itemCount']) || isset($_SESSION['shoppingCart'][2][0]['itemCount'])) {
					?>
						<div class="white-bg-box">
							<table class="table">
								<thead class="thead-fancy xs-table-header-none">
									<tr>
										<th></th>
										<th>Názov produktu</th>
										<th>Cena za kus</th>
										<th>Množstvo</th>
										<th>Celková cena</th>
										<th></th>
									</tr>
								</thead>
								<tbody class="tbody-fancy cartTableXs">
									<?php
										$totalPrice = 0;
										$row		= 0;
										foreach($shoppingCart as $productId => $productVariation) {
											$parentProductInfo = $this->product_model->getProductShort($productId);
											foreach($productVariation as $variationId => $variationItemCount) {
												if ($variationId == 0) {
													$product = [
														'id' 		=> $parentProductInfo->id,
														'child_id' 	=> 0,
														'name' 		=> $parentProductInfo->name,
														'price' 	=> $parentProductInfo->price,
														'discount' 	=> $parentProductInfo->discount,
														'tax' 		=> $parentProductInfo->tax,
														'quantity' 	=> $parentProductInfo->quantity,
														'photo' 	=> $parentProductInfo->file_name,
													];
												} else {
													$variableProductInfo = $this->product_model->getProductVariableShort($variationId);
													$product = [
														'id' 		=> $parentProductInfo->id,
														'child_id' 	=> $variableProductInfo->id,
														'name' 		=> $variableProductInfo->name,
														'price' 	=> $variableProductInfo->price,
														'discount' 	=> $variableProductInfo->discount,
														'tax' 		=> $parentProductInfo->tax,
														'quantity' 	=> $variableProductInfo->quantity,
														'photo' 	=> $parentProductInfo->file_name,
													];
												}
												$totalPrice += priceCalc($product['price'], $_SESSION['shoppingCart'][$product['id']][$product['child_id']]['itemCount'], $product['discount'], $product['tax'] / 100);
												?>
													<tr id="row<?=$row?>">
														<td>
															<div class="img-wrap">
																<img src="<?=base_url()?>uploads/products/thumbs/<?=$product['photo']?>" class="img-fluid">
															</div>
														</td>
														<td>
															<?=$product['name']?>
														</td>
														<td>
															<?=cutePrice(priceCalc($product['price'], 1, $product['discount'], $product['tax'] / 100));?></td>
														<td>
															<div role="group" class="input-group">
																<div class="input-group-prepend">
																	<button type="button" class="btn js-removeItemFromCart" data-product_id="<?=$product['id']?>" data-product_child_id="<?=$product['child_id']?>" data-row="<?=$row?>">-</button>
																</div>
																<span id="itemQuantity<?=$row?>" class="form-control"><?=$_SESSION['shoppingCart'][$product['id']][$product['child_id']]['itemCount']?></span>
																<div class="input-group-append">
																	<button type="button" class="btn js-addItemToCart" data-product_id="<?=$product['id']?>" data-product_child_id="<?=$product['child_id']?>" data-row="<?=$row?>">+</button>
																</div>
															</div>
														</td>
														<td class="d-none">
															<span id="itemAvailableQuantity<?=$row?>"><?=$product['quantity']?></span>
														</td>
														<td>
															<span id="itemTotalPrice<?=$row?>" class="total-price"><?=cutePrice(priceCalc($product['price'], $_SESSION['shoppingCart'][$product['id']][$product['child_id']]['itemCount'], $product['discount'], $product['tax'] / 100))?></span>
														</td>
														<td>
															<span class="pull-right">
																<button class="own-btn transparent removeItemAllFromCart" data-product_name="<?=$product['name']?>" data-product_id="<?=$product['id']?>" data-product_child_id="<?=$product['child_id']?>" data-row="<?=$row?>" title="Odstrániť z košíka">X</button>
															</span>
														</td>
													</tr>
												<?php
												$row++;
											}
										}
									?>
								</tbody>
								<tfoot class="tfoot-fancy xsCartTfoot">
									<tr>
										<td></td>
										<td></td>
										<td></td>
										<td><b>Sumár</b></td>
										<td><b><span id="totalPrice"><?=cutePrice($totalPrice, $this->config->item('currency'))?><span></b></td>
										<td></td>
									</tr>
								</tfoot>
							</table>
						</div>
						<div class="cart-footer text-center mt-5">
							<a href="<?=base_url()?>cart/personal-info" class="own-btn">Pokračovať na dodacie informácie</a>
						</div>
					<?php
			} else {
				?>
					<div class="white-bg-box">
						<div class="row justify-content-center align-items-center">
							<div class="col-md-6">
								<div class="white-bg-box text-center">
									<h4>Váš nákupný košík je prázdny</h4>
									<a href="<?=base_url()?>products" class="own-btn icon-left">
										<span class="icon-holder">
											<?php include 'images/icons/cart.svg';?>
										</span>
										Nakupovať
									</a>
								</div>
							</div>
						</div>
					</div>
				<?php
			}
		?>
	</div>
</div>