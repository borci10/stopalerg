<section id="references-detail">
	<div class="container">
		<div class="row mb-5">
			<div class="col-12 text-center">
				<h1 class="h1"><?=$review->header?></h1>
				<h5 class="mb-0"><span><?=$review->name?></span></h5>
				<small><?=date('d.m.Y', strtotime($review->date))?></small>
			</div>
		</div>
		<div class="reference-holder">
			<div class="reference-content">
				<p><?=$review->content;?></p>
			</div>
			<div class="reference-review">
				<div class="reference-img">
					<img src="<?=base_url()?>uploads/review/<?=$review->image?>" alt="reference-img">
				</div>
				<div class="review-icons d-flex justify-content-between mt-3 mb-2">
					<?php
						for($i = 1; $i <= ($review->rating / 2); $i++) {
							?>
								<div class="icon-holder icon-xs">
									<?php include 'images/icons/star.svg';?>
								</div>
							<?php
						}
					?>
				</div>
				<small>Hodnotenie produktu</small>
				<span class="ranking">100%</span>
			</div>
		</div>
	</div>
</section>
<?php
	if ($review->comments) {
		?>
			<div class="container">
				<div class="row justify-content-center mt-5">
					<div class="col-12 text-center">
						<h3 class="h2 mb-3">Komentáre</h3>
					</div>
					<?php
						foreach($review_comments as $review_comment) {
							?>
								<div class="col-md-10">
									<div class="review">
										<p class="mb-0"><strong><?=$review_comment['user_name']?></strong></p>
										<small><?=cuteDateTime($review_comment['date'])?></small>
										<p><?=nl2br($review_comment['content'])?></p>
										<?php
											if ($this->session->userdata('admin') == 1) {
												?>
													<a href="<?=base_url()?>admin/reviews/deleteComment/<?=$review_comment['id']?>" onclick="return confirm('Naozaj chcete odstrániť komentár <?=$review_comment['user_name']?>?');" class="adminEditPencilBtn" title="Upraviť album"><i class="fas fa-trash-alt"></i></a>
												<?php
											}
										?>
									</div>
								</div>
							<?php
						}
					?>
					<div class="col-md-10 my-5">
						<form method="post">
							<div class="form-group">
								<input type="text" class="form-control" name="commentName" placeholder="Meno" required>
							</div>
							<div class="form-group">
								<textarea name="commentText" class="form-control" placeholder="Váš komentár" rows="6" required></textarea>
							</div>
							<button type="submit" class="own-btn" value="addComment" name="addComment">Pridať komentár</button>
						</form>
					</div>
				</div>
			</div>
		<?php
	}
?>