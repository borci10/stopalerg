<?php
	foreach($reviews as $review) {
		?>
		<div class="reference-holder">
			<div class="reference-content">
				<h5><?=$review['name']?></h5>
				<small><?=cuteDate($review['date'])?></small>
				<a href="<?=base_url()?>recenzie/<?=$review['link']?>"><h3><?=$review['header']?></h3></a>
				<p><?=strip_tags(word_limiter($review['content'], 50, '...'));?></p>
				<div class="reference-more-info">
					<a href="<?=base_url()?>recenzie/<?=$review['link']?>" class="own-btn transparent icon-right">
						Pozrieť celú recenziu
						<span class="icon-holder">
											<?php include 'images/icons/right.svg';?>
										</span>
					</a>
					<div class="comments-count">
						<span>Počet komentárov:</span> <span class="count"><?=$review['comment_count']?></span>
					</div>
				</div>
			</div>
			<div class="reference-review">
				<div class="reference-img">
					<img src="<?=base_url()?>uploads/review/400/<?=$review['image']?>" alt="<?=$review['header']?>">
				</div>
				<div class="review-icons d-flex justify-content-between mt-3 mb-2">
					<?php
						for($i = 1; $i <= ($review['rating'] / 2); $i++) {
							?>
								<div class="icon-holder icon-xs">
									<?php include 'images/icons/star.svg';?>
								</div>
							<?php
						}
					?>
				</div>
				<small>Hodnotenie produktu</small>
				<span class="ranking">100%</span>
			</div>
		</div>
		<?php
	}
?>