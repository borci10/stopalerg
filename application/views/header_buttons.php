<?php
	if (isset($createButtonAdress) && isset($createButtonCaption)) {
		?>
			<div class="margin-right-15 d-inline">
				<a href="<?=base_url()?><?=$createButtonAdress?>" class="btn btn-success margin-left-10"><i class="fa fa-plus" aria-hidden="true"></i> <?=$createButtonCaption?></a>
			</div>
		<?php
	}
	
	if (isset($exportButtonAdress) && isset($exportButtonCaption)) {
		?>
			<div class="margin-right-15 d-inline">
				<a href="<?=base_url()?><?=$exportButtonAdress?>" class="btn btn-primary margin-left-10"><i class="fa fa-download" aria-hidden="true"></i> <?=$exportButtonCaption?></a>
			</div>
		<?php
	}
?>