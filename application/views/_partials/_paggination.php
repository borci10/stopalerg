<div id="pagination">
	<?php
		if (isset($totalCount)) {
			$actualPage = $this->input->get('page') ?? 1;
			$totalPages = ceil($totalCount / $this->config->item('sql_result_limit'));
			$currentURL = current_url();
			$URLparams  = removeqsvar($_SERVER['QUERY_STRING'], 'page');

			if ($actualPage < 1) {
				$actualPage = 1;
			}
			if ($actualPage > $totalPages) {
				$actualPage = $totalPages;
			}

			switch($actualPage) {
				case 1:
					$shift = 2;
					break;
				case 2:
					$shift = 1;
					break;
				case $totalPages - 1:
					$shift = -1;
					break;
				case $totalPages:
					$shift = -2;
					break;
				default:
					$shift = 0;
			}

			if ($totalPages > 5) {
				?>
					<a href="<?=current_url()?>?<?=$URLparams?>&page=1" class="ownBtn <?=($actualPage == 1 ? 'blue' : 'transparent')?>">1</a>
						<?php
							if ($actualPage >= 5) {
								echo '&hellip;';
							}
							for ($i = (-2 + $shift); $i <= (2 + $shift); $i++) {
								if ((($actualPage + $i) != 1) && (($actualPage + $i) != $totalPages)) {
									?>
										<a href="<?=current_url()?>?<?=$URLparams?>&page=<?=($actualPage + $i)?>" class="ownBtn <?=($actualPage == ($actualPage + $i) ? 'blue' : 'transparent')?>"><?=($actualPage + $i)?></a>
									<?php
								}
							}
							if ($actualPage <= $totalPages - 4) {
								echo '&hellip;';
							}
						?>
					<a href="<?=current_url()?>?<?=$URLparams?>&page=<?=$totalPages?>" class="ownBtn <?=($actualPage == $totalPages ? 'blue' : 'transparent')?>"><?=$totalPages?></a>
				<?php
			} else {
				for ($i = 1; $i <= $totalPages; $i++) {
					?>
						<a href="<?=current_url()?>?<?=$URLparams?>&page=<?=$i?>" class="ownBtn <?=($actualPage == $i ? 'blue' : 'transparent')?>"><?=$i?></a>
					<?php
				}
			}
		}
	?>
</div>
