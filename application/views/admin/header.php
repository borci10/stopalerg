<!DOCTYPE html>
<html lang="SK">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, viewport-fit=cover">

	<link href="https://fonts.googleapis.com/css?family=Roboto:400,500,700,900&amp;subset=latin-ext" rel="stylesheet">
	<link rel="stylesheet" href="<?=base_url()?>assets/css/popper/popper.css" />
	<link rel="stylesheet" href="<?=base_url()?>assets/css/perfect-scrollbar/perfect-scrollbar.css" />
	<link rel="stylesheet" href="<?=base_url()?>assets/css/bootstrap-reboot.min.css"/>
	<link rel="stylesheet" href="<?=base_url()?>assets/css/bootstrap-grid.min.css"/>
	<link rel="stylesheet" href="<?=base_url()?>assets/css/bootstrap-utilities.min.css"/>
	<link rel="stylesheet" href="<?=base_url()?>assets/css/bootstrap-carousel.min.css"/>
	<link rel="stylesheet" href="<?=base_url()?>assets/css/bootstrap-custom.min.css"/>
	<link rel="stylesheet" href="<?=base_url()?>assets/css/bootstrap-admin.min.css"/>
	<link rel="stylesheet" href="<?=base_url()?>assets/css/fontawesome/all.min.css">
	<link rel="stylesheet" href="<?=base_url()?>assets/css/jquery/jquery-ui.min.css" />
	<link rel="stylesheet" href="<?=base_url()?>assets/css/admin.min.css?v=<?=filemtime('assets/css/admin.min.css');?>" />
	<link rel="stylesheet" href="<?=base_url()?>assets/css/jquery.richtext/richtext.min.css">

	<script src="<?=base_url()?>assets/js/jquery/jquery-3.4.1.min.js"></script>
	<script src="<?=base_url()?>assets/js/jquery/jquery-ui.min.js"></script>
	<script src="<?=base_url()?>assets/js/tether/tether.min.js"></script>
	<script src="<?=base_url()?>assets/js/validator/bootstrap_validator.min.js"></script>
	<script src="<?=base_url()?>assets/js/popper/popper.min.js"></script>
	<script src="<?=base_url()?>assets/js/perfect-scrollbar/perfect-scrollbar.min.js"></script>
	<script src="<?=base_url()?>assets/js/bootstrap/bootstrap.min.js"></script>
	<script src="<?=base_url()?>assets/js/validator/validator_messages.js"></script>
	<script src="<?=base_url()?>assets/js/admin.js?=<?=filemtime('assets/js/admin.js');?>"></script>
	<script src="<?=base_url()?>assets/js/jquery.richtext/jquery.richtext.min.js"></script>

	<title><?=$page_title?></title>
</head>
<body>
<?php
	if($this->session->userdata('admin') == 1) {
		?>
			<header class="admin">
				<div class="admin-header">
					<div class="left-side">
						<div class="animated-icon admin-menu-toggle">
							<i class="fas fa-bars open-bar"></i>
							<i class="fas fa-times close-bar"></i>
						</div>
						<a href="<?=base_url()?>" class="logo-link">
							<img src="<?=base_url()?>images/admin/onlima-demo-logo.png" class="img-fluid" alt="Logo">
						</a>
					</div>
					<div class="right-side">
						<ul id="admin-header-menu">
							<li>
								<a href="https://mail.websupport.sk/" target="_blank" rel="noreferrer noopener">
									<div class="animated-icon">
										<i class="fas fa-envelope px-2"></i>
									</div>
								</a>
							</li>
							<li>
								<a href="<?=base_url()?>logout">
									<div class="animated-icon">
										<i class="fas fa-power-off px-2"></i>
									</div>
								</a>
							</li>
						</ul>
					</div>
				</div>
			</header>
			<main>
			<?php $this->load->view('inc/alerts');?>
			<?php $this->load->view('admin/sidebar');?>
			<div id="admin-content-wrap">
			<?php $this->load->view('admin/_partials/breadcrumbs');?>
			<div id="admin-content">
		<?php
	}
?>