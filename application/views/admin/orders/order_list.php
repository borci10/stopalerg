<div class="card-content">
	<div class="table-responsive">
		<table class="table mb-0">
			<thead class="thead-fancy">
			<tr>
				<th>Objednávka č.</th>
				<th>Email zákazníka</th>
				<th>Telefón zákazníka</th>
				<th>Dátum objednávky</th>
				<th>Cena</th>
				<th>Stav</th>
				<th></th>
			</tr>
			</thead>
			<tbody class="tbody-fancy">
			<?php
			foreach($orders as $order) {
				?>
					<tr>
						<td>
							<span class="responsive-desc">Objednávka č.</span>
							#<?=generateVarSymbol($order['id'])?>
						</td>
						<td>
							<span class="responsive-desc">Email zákazníka</span>
							<?=$order['email']?>
						</td>
						<td>
							<span class="responsive-desc">Telefón zákazníka</span>
							<?=$order['phone_number']?>
						</td>
						<td>
							<span class="responsive-desc">Dátum objednávky</span>
							<?=cuteDateTime($order['order_date'])?>
						</td>
						<td>
							<span class="responsive-desc">Cena</span>
							<?=cutePrice($order['order_price'] + $order['delivery_price'] + $order['payment_price'])?>
						</td>
						<td>
							<span class="responsive-desc">Stav</span>
							<span class="<?=Order_model::$statuses[$order['status']]['class']?>"><?=Order_model::$statuses[$order['status']]['name']?></span>
						</td>
						<td class="text-lg-right">
							<a href="<?=base_url()?>admin/orders/detail/<?=$order['id']?>" class="own-btn-icon  blue" title="Zobraziť objednávku" data-toggle="tooltip" data-placement="top"><i class="fa fa-eye"></i></a>
						</td>
					</tr>
				<?php
			}
			?>
			</tbody>
		</table>
	</div>
</div>
<div class="cardBody text-md-right">
	<?php
		$this->load->view('_partials/_paggination', $this->data);
	?>
</div>
