<div style="width:100%;display:block;">
	<div style="height:100px;width:25%;max-width:25%;display:inline-block;position:relative;text-align:left;">
		<?php
			if ($this->session->userdata('logo_path')) {
				?>
					<img src="<?=base_url()?>uploads/logos/<?=$this->session->userdata('global_user_id') . '/' . $this->session->userdata('logo_path')?>" height="40" style="margin-top:3px;">
				<?php
			} else {
				?>
					<div></div>
				<?php
			}
		?>
	</div>
	<div style="display:inline-block;position:relative;width:51%;text-align:center;background-color:<?=$documentColor;?>;margin-left:49%;margin-top:-100px;4">
		<h1 style="font-family:helvetica; font-size:20px; font-weight:400; font-style:normal; margin:0; padding:2px; padding-bottom:6px; padding-top:6px; padding-right:6px; padding-left: 6px; color:white;">Daňový doklad - <?=$documentHeader?></h1>
	</div>
</div>