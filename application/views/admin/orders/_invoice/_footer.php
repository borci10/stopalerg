<div style="text-align: center;font-size:11px; padding-bottom:15px;">
	<div class="footer" style="margin-bottom: 10px;">
		<div class="footer__text footer__text--left"><img class="footer__icon" style="margin-right: 5px;" src="<?=base_url()?>images/pdficons/2x/baseline_email_black_18dp.png" width="18" alt="Email"> <?=$shopSettings['company_email']?></div>
		<div class="footer__text"></div>
		<div class="footer__text footer__text--right"><img class="footer__icon" style="margin-right: 5px;" src="<?=base_url()?>images/pdficons/2x/baseline_account_box_black_18dp.png" width="18" alt="Company"> <?=$shopSettings['company_name']?></div>
	</div>
	<span style="color: #555; font-size: 10px;">Powered by Onlima</span>
</div>