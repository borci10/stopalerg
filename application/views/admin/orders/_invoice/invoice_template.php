<link rel="stylesheet" href="<?=base_url()?>assets/css/invoice/invoice_template.css" />
<style>
	/* .table-heading{
		color: <?=$this->data['documentColor'];?>;
	} */
	/* .table-heading{
		border-bottom:1px solid <?=$this->data['documentColor'];?> !important;
	} */
	.table-heading{
		border-bottom: 1px solid #ccc !important;
	}
	.special-table{
		border-bottom: 1px solid #ccc !important;
		margin-bottom: 15px;
	}
	.invoice1_header_h3{
		/* color: <?=$this->data['documentColor'];?>; */
		color: black;
	}
	.line,.long-line{
		height:2px;background-color:<?=$this->data['documentColor'];?>;
	}
	.invoice1_color_text{
		color:<?=$this->data['documentColor'];?>;
	}
</style>
<div class="wrapper">
	<div class="col-6" style="height:490px;">
		<h3 class="invoice1_header_h3">Dodávateľ</h3>
		<div class="border-container">
			<span><b><?=$shopSettings['company_name']?></b></span><br />
			<span><?=$shopSettings['company_street']?></span><br />
			<span><?=$shopSettings['company_zip_code']?> <?=$shopSettings['company_city']?></span><br />
			<span>Slovensko</span><br />
			<table style="margin-bottom: 15px; margin-top: 10px;" class="table invoice1_table">
				<tr>
					<td width="20%;">IČO:</td>
					<td><?=$shopSettings['company_ico']?></td>
				</tr>
				<tr>
					<td>DIČ:</td>
					<td><?=$shopSettings['company_dic']?></td>
				</tr>
				<?php
					if (!empty($shopSettings['company_icdph'])) {
						?>
							<tr>
								<td>IČ DPH</td>
								<td><?=$shopSettings['company_icdph']?></td>
							</tr>
						<?php
					}
				?>
			</table>
			<table style="margin-bottom: 15px; margin-top: 10px;" class="table invoice1_table">
				<tr>
					<td>
						<p>Zapísaná v obchodnom registri vedenom na Okresnom
					súde Bratislava 1, číslo zápisu: 99211/B</p>
					</td>
				</tr>
			</table>
		</div>
		<!-- <div class="line"></div> -->
		<div class="border-container-2">
			<div class="margin-top-10">
				<table>
					<?php
						if (!empty($shopSettings['bank_name']) && !empty($shopSettings['bank_account_number']) && !empty($shopSettings['bank_code'])) {
							?>
								<tr>
									<td class="smaller-font" style="text-align:left;">Číslo účtu:&nbsp;&nbsp;&nbsp;</td>
									<td class="smaller-font"><strong><?=$shopSettings['bank_account_number'];?> / <?=$shopSettings['bank_code'];?></strong></td>
								</tr>
							<?php
						}
						if (!empty($shopSettings['bank_iban'])) {
							?>
								<tr>
									<td class="smaller-font" style="text-align:left;">IBAN:&nbsp;&nbsp;&nbsp;</td>
									<td class="smaller-font"><?=$shopSettings['bank_iban'];?></td>
								</tr>
							<?php
						}
					?>
				</table>
				<table style="margin-top:10px;">
					<tr>
						<td class="smaller-font" style="text-align:left;">Variabilný symbol:&nbsp;&nbsp;&nbsp;</td>
						<td class="smaller-font"><strong><?php echo generateVarSymbol($order_info->id);?></strong></td>
					</tr>
					<tr>
						<td class="smaller-font" style="text-align:left;">Forma úhrady:&nbsp;&nbsp;&nbsp;</td>
						<td class="smaller-font"><?php echo $order_info->payment?></td>
					</tr>
				</table>
			</div>
		</div>
	</div>
	<div style="height:440px; margin-left:360px; margin-top:-490px;" class="col-6">
		<h3 class="invoice1_header_h3">Odberateľ</h3>
		<div style="margin-left: 12px; padding-top: 16px;">
			<span><b><?=$order_info->name?> <?=$order_info->surname?></b></span><br />
			<span><?=$order_info->street?></span><br />
			<span><?=$order_info->zip_code?> <?=$order_info->city?></span><br /><br />
			<?php
				if (false) {
					?>
						<div class="row">
							<table class="table invoice1_table" style="margin-left:2px;">
								<tr>
									<td width="20%;">IČO:</td>
									<td><?=$companyInfo['ico']?></td>
								</tr>
								<?php
									if (!empty($companyInfo['dic'])) {
										?>
											<tr>
												<td>DIČ:</td>
												<td><?=$companyInfo['dic']?></td>
											</tr>
										<?php
									}
								?>
								<?php
									if (!empty($companyInfo['ic_dph'])) {
										?>
											<tr>
												<td>IČ DPH</td>
												<td><?=$companyInfo['ic_dph']?></td>
											</tr>
										<?php
									}
								?>
							</table>
						</div>
					<?php
				}
			?>
		</div>
		<div class="margin-top-20" style="margin-top:40px; margin-left: -10px; border-top: 2px dotted gray;">
			<table class="table invoice1_table" style="padding:15px; margin-left: 25px; margin-top: 20px;">
				<tr>
					<td>Dátum vystavenia:</td>
					<td><?=cuteDate($order_info->order_date);?></td>
				</tr>
				<tr>
					<td>Dátum splatnosti:</td>
					<td>
						<?php
						if($order_info->payment != 'Dobierka') {
							?>
								<strong><?=date('d.m.Y', strtotime($order_info->order_date. ' + 5 days'));?></strong>
							<?php
						}
						else {
							?>
								<strong><?=date('d.m.Y', strtotime($order_info->order_date. ' + 14 days'));?></strong>
							<?php
						}
						?>
					</td>
				</tr>
				<tr>
					<td>Dátum dodania sl./tov.:</td>
					<td><?=cuteDate($order_info->order_date);?></td>
				</tr>
			</table>
		</div>
	</div>

	<div class="row" style="margin-top:50px;">
		<div class="col-12">
			<table class="table margin-top-25 helvetica_font special-table" style="margin-top: -85px;">
				<thead class="invoice1_thead-fancy table-head">
					<tr>
						<th class="table-heading" style="width:350px; padding-left: 10px;"><b>Názov položky</b></th>
						<th class="table-heading" style="width:70px; text-align:center;"><b>Množstvo</b></th>
						<?php
							if ($shopSettings['vat_payer']) {
								?>
									<th class="table-heading" style="width:135px; text-align:center;"><b>Jednotková cena</b></th>
									<th class="table-heading" style="width:50px; text-align:center;"><b>DPH</b></th>
									<th class="table-heading" style="text-align:right; padding-right: 10px;"><b>Cena s DPH</b></th>
								<?php
							} else {
								?>
									<th class="table-heading" style="text-align:center;"><b>Jednotková cena</b></th>
									<th class="table-heading" style="text-align:right; padding-right: 10px;"><b>Celková cena</b></th>
								<?php
							}
						?>
					</tr>
				</thead>
				<tbody class="invoice1_tbody-fancy main-table">
					<?php
						$totalSum 	= 0;
						$taxes 		=  array_fill_keys(array_keys(array_flip(array_unique($productTax))), 0);
						foreach($order_items as $orderItem) {
							$totalSum += priceCalc($orderItem['product_price'], $orderItem['quantity'], $orderItem['discount']);
							?>
								<tr>
									<td class="table-data" style="padding-left: 10px;"><?=$orderItem['name']?></td>
									<td class="table-data" style="text-align:center;"><?=$orderItem['quantity']?></td>
									<?php
										if ($shopSettings['vat_payer']) {
											$taxes[$orderItem['tax']] += vatCalcRaw($orderItem['product_price'], $orderItem['quantity'], $orderItem['discount'], $orderItem['tax']);
											?>
												<td class="table-data" style="text-align:center;"><?=cutePrice(priceCalc($orderItem['product_price'], 1, $orderItem['discount']))?></td>
												<td class="table-data" style="text-align:center;"><?=number_format($orderItem['tax'], 2, ',', ' ')?>%</td>
												<td class="table-data" style="text-align:right; padding-right: 10px;"><?=cutePrice(priceCalc($orderItem['product_price'], $orderItem['quantity'], $orderItem['discount'], $orderItem['tax'] / 100, true))?></td>
											<?php
										} else {	
											?>
												<td class="table-data" style="text-align:center;"><?=cutePrice(priceCalc($orderItem['product_price'], 1))?></td>
												<td class="table-data" style="text-align:right; padding-right: 10px;"><?=cutePrice(priceCalc($orderItem['product_price'], $orderItem['quantity']))?></td>
											<?php
										}
									?>
								</tr>
							<?php
						}
					?>
					<tr>
						<td class="table-data" style="padding-left: 10px;">Spôsob doručenia: <?=$order_info->delivery?></td>
						<td class="table-data" style="text-align:center;"></td>
						<?php
							if ($shopSettings['vat_payer']) {
								?>
									<td></td>
									<td></td>
									
								<?php
							} else {	
								?>
									<td></td>
								<?php
							}
						?>
						<td class="table-data" style="text-align:right; padding-right: 10px;"><?=cutePrice($order_info->delivery_price)?></td>
					</tr>
					<tr>
						<td class="table-data" style="padding-left: 10px;">Platba: <?=$order_info->payment?></td>
						<td class="table-data" style="text-align:center;"></td>
						<?php
							if ($shopSettings['vat_payer']) {
								?>
									<td></td>
									<td></td>
									
								<?php
							} else {	
								?>
									<td></td>
								<?php
							}
						?>
						<td class="table-data" style="text-align:right; padding-right: 10px;"><?=cutePrice($order_info->payment_price)?></td>
					</tr>
				</tbody>
			</table>
		</div>
	</div>
	<div class="row">
		<div class="col-6" style="height:100px; position: relative; border-right: 2px dotted gray; margin-top: 20px;">	
	
		</div>
		<div class="col-6" style="margin-left:400px;margin-top:-110px;">
			<div>
				<?php
					if ($shopSettings['vat_payer']) {
						?>
							<table>
								<tr>
									<td style="text-align:right;">Suma bez DPH:&nbsp;&nbsp;&nbsp;</td>
									<td><?=cutePrice($totalSum)?></td>
								</tr>
								<?php
									if (count($taxes) > 1) {
										foreach($taxes as $key => $value) {
											?>
												<tr>
													<td style="text-align:right;">DPH <?=number_format($key, 2, '.', ' ')?>%:&nbsp;&nbsp;&nbsp;</td>
													<td><?=cutePrice($value)?></td>
												</tr>
											<?php
										}
									}
								?>
								<tr>
									<td style="text-align:right;">DPH celkom:&nbsp;&nbsp;&nbsp;</td>
									<td><?=cutePrice(array_sum($taxes))?></td>
								</tr>	
								<tr>
									<td style="text-align:right;"><b>Celkom s DPH:</b>&nbsp;&nbsp;&nbsp;</td>
									<td><b><?=cutePrice($totalSum + array_sum($taxes) + $order_info->delivery_price + $order_info->payment_price)?></b></td>
								</tr>
							</table>
						<?php
					} else {
						?>
							<table>	
								<tr>
									<td class="invoice1_color_text" style="text-align:right;"><b>Suma na úhradu:</b>&nbsp;&nbsp;&nbsp;</td>
									<td class="invoice1_color_text"><b><?=cutePrice($totalSum + $order_info->delivery_price + $order_info->payment_price)?></b></td>
								</tr>
							</table>
						<?php
					}
				?>
			</div>
		</div>
	</div>
</div>