<div class="cardBodyWrapper">
	<div class="row">
		<div class="col-lg-8">
			<div class="card-content">
				<div class="cardHead">
					<h2>Obsah objednávky</h2>

				</div>
				<div class="cardInnerBody">
					<table class="table">
						<thead class="thead-fancy thead-fancy-border-top">
							<tr>
								<th>Produkt</th>
								<th>Cena za kus</th>
								<th>Počet kusov</th>
								<th>Cena</th>
							</tr>
						</thead>
						<tbody class="tbody-fancy">
							<?php
								$totalOrderPrice = $order_info->delivery_price + $order_info->payment_price;
								foreach($order_items as $item) {
									?>
										<tr>
											<td class="font-weight-semibold">
												<?php
													if ($item['id_product_child'] == 0) {
														?>
															<a href="<?=base_url()?>products/<?=$item['link']?>" class="link" target="_blank"><?=$item['name']?></a>
														<?php
													} else {
														?>
															<a href="<?=base_url()?>products/<?=$item['link']?>/<?=$item['product_variable_link']?>" class="link" target="_blank"><?=$item['name']?></a>
														<?php
													}
												?>
											</td>
											<td>
												<span class="responsive-desc">Cena za kus</span>
												<?=cutePrice(priceCalc($item['product_price'], 1, $item['discount'], $item['tax'] / 100))?>
											</td>
											<td>
												<span class="responsive-desc">Počet kusov</span>
												<?=$item['quantity']?>
											</td>
											<td>
												<span class="responsive-desc">Cena</span>
												<?=cutePrice(priceCalc($item['product_price'], $item['quantity'], $item['discount'], $item['tax'] / 100))?>
											</td>
									<?php
									$totalOrderPrice += priceCalc($item['product_price'], $item['quantity'], $item['discount'], $item['tax'] / 100);
								}
							?>
							<tr>
								<td class="font-weight-semibold"><?=$order_info->payment?></td>
								<td class="d-lg-table-cell d-none"></td>
								<td class="d-lg-table-cell d-none"></td>
								<td><?=cutePrice($order_info->payment_price)?></td>
							</tr>
							<tr>
								<td class="font-weight-semibold"><?=$order_info->delivery?></td>
								<td class="d-lg-table-cell d-none"></td>
								<td class="d-lg-table-cell d-none"></td>
								<td><?=cutePrice($order_info->delivery_price)?></td>
							</tr>
						</tbody>
						<tfoot class="tfoot-fancy">
							<tr>
								<td class="d-lg-table-cell d-none"></td>
								<td class="d-lg-table-cell d-none"></td>
								<td><b>Celková cena</b></td>
								<td><b><?=cutePrice($totalOrderPrice)?></b></td>
							</tr>
						</tfoot>
					</table>
				</div>
			</div>
			<div class="card-content">
				<div class="cardHead">
					<h2>Postup pri zmene stavu objednávky</h2>
				</div>
				<div class="cardInnerBody">
					<p class="font-weight-bold mb-0">Dobierka</p>
					<p><span class="own-badge">Prijatá</span> -> <span class="own-badge green">Vyexpedovaná</span> -> <span class="own-badge gold">Vybavená <small>(odošle faktúru)</small></span></p>
					<p class="font-weight-bold mb-0">Prevodom na účet - zákazník už ZAPLATIL</p>
					<p><span class="own-badge">Prijatá <small>(odošle faktúru)</small></span> -> <span class="own-badge blue">Zaplatená</span> -> <span class="own-badge green">Vyexpedovaná</span> -> <span class="own-badge gold">Vybavená</span></p>
					<p class="font-weight-bold mb-0">Prevodom na účet - zákazník ešte NEZAPLATIL</p>
					<p><span class="own-badge">Prijatá <small>(odošle faktúru)</small></span> -> <span class="own-badge red">Čakajúca <small>(znova odošle faktúru)</small></span> -> <span class="own-badge blue">Zaplatená</span> -> <span class="own-badge green">Vyexpedovaná</span> -> <span class="own-badge gold">Vybavená</span></p>
					<hr>
					<p class="font-weight-bold">Ak zadávate čislo zásielky, treba ho najprv uložiť, a až potom zmeniť stav objednávky na <span class="own-badge green">Vyexpedovaná</span></p>
				</div>
			</div>
		</div>
		<div class="col-lg-4">
			<div class="card-content">
				<div class="cardHead">
					<h2>Stav objednávky</h2>

				</div>
				<div class="cardInnerBody">
					<div class="text-center mb-3">
						<p class="d-inline-block mb-0">Aktuálny stav objednávky:</p>
						<span class="float-none ml-2 <?=Order_model::$statuses[$order_info->status]['class']?>"><?=Order_model::$statuses[$order_info->status]['name']?></span>
					</div>
					<form class="form text-center">
						<select class="form-control" name="orderStatus" id="orderStatus">
							<?php
								foreach (Order_model::$statuses as $key => $status) {
									?>
										<option value="<?=$key?>" <?=is_selected($order_info->status, $key)?>><?=$status['name']?></option>
									<?php
								}
							?>
						</select>
						<button type="button" class="own-btn mt-2" id="changeOrderStatus">Zmeniť stav</button>
					</form>
				</div>
			</div>
			<div class="card-content">
				<div class="cardHead">
					<h2 class="mb-0">Číslo zásielky</h2>
					<p>zadať a <strong>uložiť pred</strong> zmenou stavu na <span class="own-badge green">Vyexpedovaná</span></p>
				</div>
				<div class="cardInnerBody">
					<form method="post" class="form text-center">
						<input class="form-control mt-3" name="tracking_number" placeholder="Číslo zásielky" value="<?=$order_info->tracking_number?>" required>
						<button type="submit" class="own-btn mt-2" value="trackingNumberConfirm" name="trackingNumberConfirm">Uložiť</button>
					</form>
				</div>
			</div>
			<div class="card-content">
				<div class="cardHead">
					<h2>Faktúra</h2>

				</div>
				<div class="cardInnerBody">
					<a href="<?=base_url()?>admin/orders/invoice/<?=$order_info->id?>" class="own-btn mt-2" target="_blank">Zobraziť faktúru</a>
				</div>
			</div>
			<div class="card-content">
				<div class="cardHead">
					<h2>Dodacie údaje</h2>
				</div>
				<div class="cardInnerBody">
					<div class="table-responsive">
						<table class="table">
							<tbody class="tbody-fancy">
								<tr>
									<td class="font-weight-semibold">Meno</td>
									<td><?=$order_info->name?></td>
								</tr>
								<tr>
									<td class="font-weight-semibold">Priezvisko</td>
									<td><?=$order_info->surname?></td>
								</tr>
								<tr>
									<td class="font-weight-semibold">Email</td>
									<td><?=$order_info->email?></td>
								</tr>
								<tr>
									<td class="font-weight-semibold">Telefón</td>
									<td><?=$order_info->phone_number?></td>
								</tr>
								<tr>
									<td class="font-weight-semibold">Mesto</td>
									<td><?=$order_info->city?></td>
								</tr>
								<tr>
									<td class="font-weight-semibold">Ulica</td>
									<td><?=$order_info->street?></td>
								</tr>
								<tr>
									<td class="font-weight-semibold">PSČ</td>
									<td><?=$order_info->zip_code?></td>
								</tr>
								<?php
									if (@$order_info->diff_delivery == 1) {
										?>
										<tr>
											<td colspan="2">Adresa doručenia</td>
										</tr>
										<?php
											if (isset($order_info->delivery_name)) {
												?>
												<tr>
													<td class="font-weight-semibold">Meno</td>
													<td><?=$order_info->delivery_name?></td>
												</tr>
												<?php
											}
											?>
											<?php
											if (isset($order_info->delivery_surname)) {
												?>
												<tr>
													<td class="font-weight-semibold">Priezvisko</td>
													<td><?=$order_info->delivery_surname?></td>
												</tr>
												<?php
											}
										?>
										<tr>
											<td class="font-weight-semibold">Mesto</td>
											<td><?=$order_info->delivery_city?></td>
										</tr>
										<tr>
											<td class="font-weight-semibold">Ulica</td>
											<td><?=$order_info->delivery_street?></td>
										</tr>
										<tr>
											<td class="font-weight-semibold">PSČ</td>
											<td><?=$order_info->delivery_zip_code?></td>
										</tr>
										<?php
									}
								?>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<script>
	$(document).on('click', '#changeOrderStatus', function() {
		var orderId 	= '<?=$order_info->id?>';
		var status 		= $('#orderStatus').val();
		var statusText 	= $('#orderStatus option:selected').text();

		if (status != 0) {
			if (confirm("Zmeniť status objednávky na " + statusText + "?")) {
				$.ajax({
					type: 'POST',
					url: '<?=base_url()?>admin/orders/changeOrderStatus/' + orderId + '/' + status
				})
				.done(function(data) {
					location.reload();
				})
				.fail(function(jqXHR, textStatus, errorThrown) {
					console.log(jqXHR);
					console.log(textStatus);
					console.log(errorThrown);
				});
			}
		} else {
			alert("Zvoľte stav!");
		}
	});
</script>


