<div class="card-content">
	<div class="row">
		<div class="col-12">
			<div class="table-responsive">
				<table class="table">
					<thead>
					<tr>
						<th>Email</th>
						<th>Dátum pridania</th>
						<th></th>
					</tr>
					</thead>
					<tbody>
						<?php
							foreach($emails as $email) {
								?>
								<tr>
									<td>
										<span class="responsive-desc">Názov emailu</span>
										<a href="<?=base_url()?>admin/newsletter/edit/<?=$email['id']?>"><?=$email['email']?></a>
									</td>
									<td>
										<span class="responsive-desc">Dátum pridania</span>
										<?=cuteDate($email['date'])?>
									</td>
									<td>
										<div class="text-lg-right">
											<a href="<?=base_url()?>admin/newsletter/edit/<?=$email['id']?>" class="own-btn-icon green" title="Upraviť email" data-toggle="tooltip" data-placement="top"><i class="fas fa-pencil-alt" aria-hidden="true"></i></a>
											<a href="<?=base_url()?>admin/newsletter/delete/<?=$email['id']?>" onclick="return confirm('Naozaj chcete odstrániť album <?=$email['email']?>?');" class="own-btn-icon red" title="Odstrániť email" data-toggle="tooltip" data-placement="top"><i class="fas fa-trash-alt" aria-hidden="true"></i></a>
										</div>
									</td>
								</tr>
								<?php
							}
						?>
					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>
<div class="card-content text-md-right">
	<a href="<?=base_url()?>admin/newsletter/add" class="own-btn green">Pridať email</a>
	<a href="<?=base_url()?>admin/newsletter/export" class="own-btn blue">Stiahnuť CSV</a>
</div>