<div class="card-content">
	<form id="addContentForm" method="post" role="form">
		<div class="row">
			<div class="form-group has-feedback col-md-9">
				<label for="newsletterEmail">Email *</label>
				<input
						type="email"
						class="form-control"
						id="newsletterEmail"
						name="newsletterEmail"
						value="<?php echo set_value('newsletterEmail', is_set(@$newsletter->email)); ?>"
						required
				>
				<div class="help-block with-errors"><?php echo form_error('newsletterEmail'); ?></div>
			</div>
			<div class="form-group has-feedback col-md-3">
				<label for="newsletterDate">Dátum *</label>
				<input
						type="text"
						class="form-control js-date"
						id="newsletterDate"
						name="newsletterDate"
						autocomplete="off"
						value="<?php echo set_value('newsletterDate', is_set(@cuteDate(@$newsletter->date))); ?>"
						required
				>
				<div class="help-block with-errors"><?php echo form_error('newsletterDate'); ?></div>
			</div>
		</div>
		<div class="form-group">
			<input type="submit" class="own-btn my-1" value="<?=$button?>" name="formConfirm" id="formConfirm">
			<a href="<?=base_url()?>admin/newsletter" class="own-btn red my-1">Zrušiť</a>
		</div>
	</form>
</div>
<script>
	$(function(){
		$(".js-date").datepicker({
			dateFormat: "dd.mm.yy",
			changeMonth: true,
			changeYear: true
		});
	});
</script>