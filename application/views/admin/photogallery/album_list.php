<div class="card-content">
	<div class="row">
		<div class="col-12">
			<div class="table-responsive">
				<table class="table">
					<thead>
					<tr>
						<th>Názov</th>
						<th>Dátum</th>
						<th>Popis</th>
						<th></th>
					</tr>
					</thead>
					<tbody>
					<?php
					foreach($albums as $album) {
						?>
						<tr>
							<td>
								<span class="responsive-desc">Názov albumu</span>
								<a href="<?=base_url()?>admin/photogallery/edit/<?=$album['id']?>"><?=$album['name']?></a>
							</td>
							<td>
								<span class="responsive-desc">Dátum pridania</span>
								<?=cuteDate($album['date'])?>
							</td>
							<td>
								<?=word_limiter($album['description'], 5, '...')?>
							</td>
							<td>
								<div class="text-lg-right">
									<a href="<?=base_url()?>admin/photogallery/edit/<?=$album['id']?>" class="own-btn-icon green" title="Upraviť album" data-toggle="tooltip" data-placement="top"><i class="fas fa-pencil-alt" aria-hidden="true"></i></a>
									<a href="<?=base_url()?>admin/photogallery/delete/<?=$album['id']?>" onclick="return confirm('Naozaj chcete odstrániť album <?=$album['name']?>?');" class="own-btn-icon red" title="Odstrániť album" data-toggle="tooltip" data-placement="top"><i class="fas fa-trash-alt" aria-hidden="true"></i></a>
								</div>
							</td>
						</tr>
						<?php
					}
					?>
					</tbody>
				</table>
			</div>

		</div>
	</div>
</div>
<div class="card-content text-md-right">
	<a href="<?=base_url()?>admin/photogallery/create" class="own-btn green">Pridať album</a>
</div>