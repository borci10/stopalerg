<div class="col-xl-3 col-lg-4 col-sm-6 col-12 py-3 photoSortable" id="photoItem_<?=$photo['id']?>">
	<div class="photoItem ui-state-default ui-sortable-handle">
		<input type="hidden" name="albumPhotoNames[]" value="<?=$photo['path']?>">
		<div class="photoContainer">
			<img src="<?=base_url()?>uploads/photogallery/thumbs/<?=$photo['path']?>" class="img-fluid">
		</div>
		<div class="photoInfo">
			<input type="text" class="full" name="albumPhotoTitle[]" placeholder="Title" value="<?=$photo['title']?>">
			<input type="text" class="full" name="albumPhotoAlt[]" placeholder="Alt" value="<?=$photo['alt']?>">
		</div>
		<div class="photoEdit">
			<button type="button" class="own-btn-icon red removeImageFromAlbum" data-img_id="<?=$photo['id']?>" title="Odstrániť fotografiu"><i class="fa fa-trash"></i></button>
		</div>
	</div>
</div>