<style>
	.ui-state-highlight {
		width: calc(25% - 30px);
		margin-left: 15px;
		margin-right: 15px;
		height: 360px;
	}
</style>
<div class="card-content">
	<div class="row">
		<div class="col-12">
			<form id="albumForm" method="post" role="form">
				<div class="row">
					<div class="form-group has-feedback col-md-9">
						<label for="albumName">Názov albumu *</label>
						<input
								type="text"
								class="form-control"
								id="albumName"
								maxlength="50"
								name="albumName"
								value="<?php echo set_value('albumName', is_set(@$album_info->name)); ?>"
								required
						>
						<div class="help-block with-errors"><?php echo form_error('albumName'); ?></div>
					</div>
					<div class="form-group has-feedback col-md-3">
						<label for="albumDate">Dátum *</label>
						<input
								type="text"
								class="form-control js-date"
								id="albumDate"
								maxlength="50"
								name="albumDate"
								autocomplete="off"
								value="<?php echo set_value('albumDate', is_set(@cuteDate($album_info->date))); ?>"
								required
						>
						<div class="help-block with-errors"><?php echo form_error('albumDate'); ?></div>
					</div>
					<div class="form-group has-feedback col-12">
						<label for="albumDescription">Popis</label>
						<textarea class="form-control richTextEdior" id="albumDescription" name="albumDescription" rows="4"><?php echo set_value('albumDescription', is_set(@$album_info->description)); ?></textarea>
						<div class="help-block with-errors"><?php echo form_error('albumDescription'); ?></div>
					</div>
					<div class="col-12">
						<h4>Fotografie</h4>
						<div id="albumPhotos" class="sortable row">
							<div class="col-xl-3 col-lg-4 col-sm-6 col-12 py-3">
								<div id="photoInputDiv">
									<div class="fileContainer_label">Vybrať foto<small id="fileHelp" class="form-text text-muted">Max veľkosť 10MB. <br />Podporované typy .png, .jpg a .gif.</small></div>
									<input type="file" class="form-control-file fileContainer " id="photoInput" multiple>
								</div>
							</div>
							<?php
								if (isset($albumPhotos)) {
									foreach($albumPhotos as $this->data['photo']) {
										$this->load->view('admin/photogallery/_photo_item', $this->data);
									}
								}
							?>
						</div>
						<div class="form-group">
							<input type="submit" class="own-btn" value="<?=$button_title?>" name="formConfirm" id="formConfirm">
							<a href="<?=base_url()?>admin/photogallery" class="own-btn red">Zrušiť</a>
						</div>
					</div>
				</div>
			</form>
		</div>
	</div>
</div>

<script>
	//upload image after input change			
	$(document).ready(function (e) {
		$('#photoInput').change(function () {
			var file_data = $('#photoInput').prop('files');

			for(let i = 0; i < file_data.length; i++) {
				var form_data = new FormData();
				form_data.append('file', file_data[i]);

				$('#albumPhotos').append('<div class="col-3" style="height: 300px;" id="photoPlaceholder' + i + '"><div class="fileContainer_label"><i class="fa fa-spinner fa-pulse fa-3x fa-fw" aria-hidden="true"></i></div></div>');
				$.ajax({
					url: '<?=base_url()?>Admin/photogallery/uploadPhoto',
					dataType: 'text',
					cache: false,
					contentType: false,
					processData: false,
					data: form_data,
					type: 'post',
				})
				.done(function(data) {
					data = JSON.parse(data);
					if (data[0] == 'ok') {
						$('#albumPhotos').append(data[1]);
					} else {
						alert('Pri nahrávaní fotografie došlo k chybe! Skúste to prosím znovu.');
						$('.fileContainer_label').html('Vybrať foto<small id="fileHelp" class="form-text text-muted">Max veľkosť 10MB. <br />Podporované typy .png, .jpg a .gif.</small>');
					}
				})
				.fail(function(jqXHR, textStatus, errorThrown) {
					alert('Pri nahrávaní fotografie došlo k chybe! Skúste to prosím znovu.');
					console.log(jqXHR);
					console.log(textStatus);
					console.log(errorThrown);
				})
				.always(function() {
					$('#photoInput').val('');
					$('#photoPlaceholder' + i).remove();
				});
			}
		});
	});

	//remove image
	$(document).on('click', '.removeImageFromAlbum', function(e) {
		var imgId = $(this).data('img_id');

		if (confirm('Naozaj chcete odstrániť fotografiu?')) {
			$.ajax({
				url: '<?=base_url()?>Admin/photogallery/removePhoto',
				data: {imgId: imgId},
				dataType: 'text',
				type: 'post',
				success: function (data) {
					$('#photoItem_' + data).remove();
				}
			});
		}
		e.preventDefault();
	});

	//make image list sortable
	$(function() {
		$( ".sortable" ).sortable({
			items: ".photoSortable",
			placeholder: "ui-state-highlight"
		});
		$( ".sortable" ).disableSelection();
	});


	$(function(){
		$(".js-date").datepicker({
			dateFormat: "dd.mm.yy",
			changeMonth: true,
			changeYear: true
		});
	});


</script>


