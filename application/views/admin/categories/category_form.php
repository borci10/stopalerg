<div class="cardBodyWrapper">
	<form method="post" data-toggle="validator" role="form">
		<div class="card-content">
			<div class="cardHead">
				<h2>Fotografia</h2>

			</div>
			<div class="cardInnerBody">
				<div id="blogPhotos" class="row">
					<div class="col-xl-3 col-lg-4 col-md-6 col-12 my-2" id="photoInputDiv" style="display:<?=(empty($category->photo) ? 'block;' : 'none;')?>">
						<div class="form-group mb-0">
							<div class="fileContainer_label">Vybrať foto<small id="fileHelp" class="form-text text-muted">Max veľkosť 10MB. <br />Podporované typy .png, .jpg a .gif.</small></div>
							<input type="file" class="form-control-file fileContainer " id="photoInput" multiple>
						</div>
					</div>
					<?php
						if (!empty($category->photo)) {
							$this->data['photo'] = $category->photo;
							$this->load->view('admin/categories/_partials/image_upload_item', $this->data);
						}
					?>
				</div>
			</div>
		</div>
		<div class="cardBodyWrapper">
			<div class="card-content">
				<div class="cardHead">
					<h2>Informácie</h2>

				</div>
				<div class="cardInnerBody">
					<div class="form-group has-feedback">
						<label for="title">Názov kategórie *</label>
						<input
							type="text"
							class="form-control"
							id="title"
							maxlength="255"
							name="title"
							value="<?php echo set_value('title', is_set(@$category->title)); ?>"
							required
						>
						<div class="help-block with-errors"><?php echo form_error('title'); ?></div>
					</div>
					<div class="form-group mb-0">
						<input type="submit" class="own-btn green" value="<?=$button?>" name="formConfirm" id="formConfirm">
					</div>
				</div>
			</div>
		</div>
	</form>
</div>


<script>
	//upload image after input change
	$(document).ready(function (e) {
		$('#photoInput').change(function () {
			var file_data = $('#photoInput').prop('files')[0];
			var form_data = new FormData();
			form_data.append('file', file_data);
			$('.fileContainer_label').html('<i class="fa fa-spinner fa-pulse fa-3x fa-fw" aria-hidden="true"></i>');
			$.ajax({
				url: '<?=base_url()?>Admin/Categories/uploadPhoto',
				dataType: 'text',
				cache: false,
				contentType: false,
				processData: false,
				data: form_data,
				type: 'post'
			})
			.done(function(data) {
				data = JSON.parse(data);
				if (data[0] == 'ok') {
					id = data[1].slice(0, -4);
					$('#blogPhotos').append(data[2]);
					$('#photoInputDiv').hide();
					$('#photoInput').val('');
				} else {
					alert('Pri nahrávaní fotografie došlo k chybe! Skúste to prosím znovu.');
					$('.fileContainer_label').html('Vybrať foto<small id="fileHelp" class="form-text text-muted">Max veľkosť 10MB. <br />Podporované typy .png, .jpg a .gif.</small>');
				}
			})
			.fail(function(jqXHR, textStatus, errorThrown) {
				alert('Pri nahrávaní fotografie došlo k chybe! Skúste to prosím znovu.');
				$('.fileContainer_label').html('Vybrať foto<small id="fileHelp" class="form-text text-muted">Max veľkosť 10MB. <br />Podporované typy .png, .jpg a .gif.</small>');
				console.log(jqXHR);
				console.log(textStatus);
				console.log(errorThrown);
			})
			.always(function() {
				$('#photoInput').val('');
			});
		});
	});

	//remove image
	$(document).on('click', '.js-removeImage', function() {
		if (confirm("Naozaj chcete odstrániť fotografiu?")) {
			var imgName = $(this).data('img_name');

			$.ajax({
				url: '<?=base_url()?>Admin/Categories/removePhoto',
				data: {file_name: imgName},
				dataType: 'text',
				type: 'post',
				success: function (data) {
					id = data.slice(0, -4);
					$('#photoItem_' + id).remove();
					$('#inputFormField_' + id).remove();
					$('#photoInputDiv').show();
					$('.fileContainer_label').html('Vybrať foto<small id="fileHelp" class="form-text text-muted">Max veľkosť 10MB. <br />Podporované typy .png, .jpg a .gif.</small>');
				}
			});
		}
	});
</script>