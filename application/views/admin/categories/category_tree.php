<div class="card-content">
	<div class="categorySortable" id="categoryRoot">
		<?php
			foreach($categories as $this->data['category']) {
				$this->load->view('admin/categories/category_item', $this->data);
			}
		?>
	</div>
</div>
<div class="card-content">
	<button class="own-btn green js-saveCategoryTree" >Uložiť zmeny</button>
</div>

<script>
	//get full tree of category structure
	$(document).on('click', '.js-saveCategoryTree', function() {
		var categoryTree 	= [];
		var rootCategories 	= $('.categorySortable').children('.categorySortableItem');
		
		function findRecursiveCategory(element, parentId) {
			var elemChildList = [];

			for (let subcategory of element) {			
				elemChildList.push({
					id: 			$(subcategory).data('category_id'),
					title: 			$(subcategory).data('title'),
					link: 			$(subcategory).data('link'),
					photo: 			$(subcategory).data('photo'),
					show_category: 	$(subcategory).data('show_category'),
					parentId: 		parentId, 
					child: 			findRecursiveCategory($(subcategory).children('.categorySortableChild').children('.categorySortableItem'), $(subcategory).data('category_id'))
				});
			}

			return elemChildList;
		}
		
		for (let subcategory of rootCategories) {
			categoryTree.push({
				id: 			$(subcategory).data('category_id'),
				title: 			$(subcategory).data('title'),
				link: 			$(subcategory).data('link'),
				photo: 			$(subcategory).data('photo'),
				show_category: 	$(subcategory).data('show_category'),
				parentId: 		null, 
				child: 			findRecursiveCategory($(subcategory).children('.categorySortableChild').children('.categorySortableItem'), $(subcategory).data('category_id'))
			});
		}

		$.ajax({
			type: "POST",
			url: "<?=base_url();?>admin/category/changeCategoryStructure",
			data: {categorytree: JSON.stringify(categoryTree)},
			success: function(data) {
				location.reload();
            },
			error: function(data) {
                location.reload();
            },
		});
	});
	
	//toggle visibility of categories 
	$(document).on('click', '.js-toggle-cat-visib', function() {
		var childCategory = $(this).data('child_cat_id');
		
		if ($(this).text() == '-') {
			$(this).text('+');
		} else {
			$(this).text('-');
		}
		
		$('#categoryChildId_' + childCategory).slideToggle();
	});
	
	//make category list sortable
	$( function() {
		$( ".categorySortable" ).sortable({
			placeholder: "ui-state-highlight",
			connectWith: ".categorySortableChild, .categorySortable"
		});
		$( '.categorySortableChild' ).sortable({
			placeholder: "ui-state-highlight",
			connectWith: ".categorySortableChild, .categorySortable"
		});
		$( ".categorySortable", ".categorySortableChild" ).disableSelection();
	  } );

</script>

