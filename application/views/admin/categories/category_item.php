<div 
	class="categorySortableItem col-12" 
	style="cursor:move;" 
	data-category_id=<?=$category['category_id'] ?? 'null';?> 
	data-title=<?=$category['title'] ?? 'null';?> 
	data-link=<?=$category['link'] ?? 'null';?>
	data-photo=<?=$category['photo'] ?? 'null';?>
	data-show_category=<?=$category['show_category'] ?? 'null';?>
	>
	<div class="categorySortableWrap d-flex justify-content-between flex-wrap">
		<span class="categoryToggler js-toggle-cat-visib" data-child_cat_id="<?=$category['id']?>">-</span>
		<span class="categoryNameText <?=$category['id']?>" id="categoryNameText<?=$category['id']?>"><?=$category['title']?></span>
		<div id="categoryNameChangeControl<?=$category['id']?>" style="display:none;">
			<div class="input-group">
				<input type="text" id="categoryNameTextInput<?=$category['id']?>" value="<?=$category['title']?>" class="form-control">
				<span class="input-group-btn">
					<button class="own-btn ml-1 js-categoryNameChangeConfirm" data-category_id="<?=$category['id']?>">Uložiť</button>
				</span>
				<span class="input-group-btn">
					<button class="own-btn red ml-1 js-categoryNameChangeCancel" data-category_id="<?=$category['id']?>">Zrušiť</button>
				</span>
			</div>
		</div>
		<div class="categoryOptionButtons text-md-right">
			<?php
				if ($category['show_category']) {
					?>
						<a href="<?=base_url()?>admin/category/hide/<?=$category['id']?>" class="own-btn-icon  blue" title="Skryť kategóriu" data-toggle="tooltip" data-placement="top"><i class="fa fa-eye-slash"></i></a>
					<?php
				} else {
					?>
						<a href="<?=base_url()?>admin/category/show/<?=$category['id']?>" class="own-btn-icon  disabled" title="Zobraziť kategóriu" data-toggle="tooltip" data-placement="top"><i class="fa fa-eye"></i></a>
					<?php
				}
			?>
			<a href="<?=base_url()?>admin/category/edit/<?=$category['id']?>" class="own-btn-icon  green" title="Upraviť kategóriu" data-toggle="tooltip" data-placement="top"><i class="fas fa-pencil-alt" aria-hidden="true"></i></a>
			<a href="<?=base_url()?>admin/category/remove/<?=$category['id']?>" class="own-btn-icon  red" title="Odstrániť kategóriu" data-toggle="tooltip" data-placement="top" onclick="return confirm('Naozaj chcete odstrániť kategóriu <?=$category['title']?>?')"><i class="fas fa-trash-alt" aria-hidden="true"></i></a>
		</div>
	</div>
	<div id="categoryChildId_<?=$category['id']?>" class="categorySortableChild">
		<?php			
			foreach($category['childrens'] as $this->data['category']) {
				$this->load->view('admin/categories/category_item', $this->data);
			}
		?>
	</div>
</div>