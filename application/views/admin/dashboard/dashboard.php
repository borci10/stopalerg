<div id="dashboard">
	<div class="row">
		<div class="col-xl-3 col-lg-6 mb-4 col-sm-6 col-12">
			<div class="dashboardBox green">
				<div class="dashboardBoxInfo">
					<p><i class="fas fa-calculator"></i>Počet nákupov</p>
					<h4><?=$orderCount?></h4>
				</div>
			</div>
		</div>
		<div class="col-xl-3 col-lg-6 mb-4 col-sm-6 col-12">
			<div class="dashboardBox red">
				<div class="dashboardBoxInfo">
					<p><i class="fas fa-money-bill"></i>Hodnota nákupov</p>
					<h4><?=cutePrice($orderTotalPrice, $this->config->item('currency'))?></h4>
				</div>
			</div>
		</div>
		<div class="col-xl-3 col-lg-6 mb-4 col-sm-6 col-12">
			<div class="dashboardBox blue">
				<div class="dashboardBoxInfo">
					<p><i class="far fa-user"></i>Registrovaných zakázníkov</p>
					<h4><?=$customersCount?></h4>
				</div>
			</div>
		</div>
		<div class="col-xl-3 col-lg-6 mb-4 col-sm-6 col-12">
			<div class="dashboardBox orange">
				<div class="dashboardBoxInfo">
					<p><i class="fas fa-shopping-cart"></i>Aktívnych košíkov</p>
					<h4><?=$activeCartsCount?></h4>
				</div>
			</div>
		</div>
	</div>
	<div id="chart_div" style="height: 500px; padding:20px; background:white;"></div>
</div>

<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
<script type="text/javascript">
    google.charts.load('current', {'packages':['bar']});
    google.charts.setOnLoadCallback(drawChart);

    function drawChart() {
        var data = google.visualization.arrayToDataTable([
			['Mesiac'	, 'Počet objednávok'			,'Hodnota Objednávok'],
			['Január' 	, <?=$orderCountChart[1]?>	,<?=$orderSumCountChart[1]?> ],
			['Feburár'	, <?=$orderCountChart[2]?>  ,<?=$orderSumCountChart[2]?> ],
			['Marec'	, <?=$orderCountChart[3]?>  ,<?=$orderSumCountChart[3]?> ],
			['Apríl'	, <?=$orderCountChart[4]?>  ,<?=$orderSumCountChart[4]?> ],
			['Máj'		, <?=$orderCountChart[5]?>  ,<?=$orderSumCountChart[5]?> ],
			['Jún'		, <?=$orderCountChart[6]?>  ,<?=$orderSumCountChart[6]?> ],
			['Júl'		, <?=$orderCountChart[7]?>  ,<?=$orderSumCountChart[7]?> ],
			['August'	, <?=$orderCountChart[8]?>  ,<?=$orderSumCountChart[8]?> ],
			['September', <?=$orderCountChart[9]?>  ,<?=$orderSumCountChart[9]?> ],
			['Október'	, <?=$orderCountChart[10]?> ,<?=$orderSumCountChart[10]?>],
			['November'	, <?=$orderCountChart[11]?> ,<?=$orderSumCountChart[11]?>],
			['December'	, <?=$orderCountChart[12]?> ,<?=$orderSumCountChart[12]?>]
        ]);

        var options = {
			chart: {
				subtitle: 'Štatistiká nákupov za rok: <?=date('Y')?>',
			}
        };

        var chart = new google.charts.Bar(document.getElementById('chart_div'));

        chart.draw(data, google.charts.Bar.convertOptions(options));
     }
</script>