<div class="cardBodyWrapper">
	<div class="row">
		<div class="col-lg-8">
			<div class="card-content">
				<div class="cardHead">
					<h2>Objednávky</h2>

				</div>
				<div class="cardInnerBody">
					<table class="table">
						<thead class="thead-fancy thead-fancy-border-top">
							<tr>
								<th>Id</th>
								<th>Platba</th>
								<th>Doprava</th>
								<th>Cena objednávky</th>
								<th>Stav</th>
								<th></th>
							</tr>
						</thead>
						<tbody class="tbody-fancy">
							<?php
								$totalSum = 0;
								foreach($orders as $order) {
									?>
										<tr>
											<td class="font-weight-semibold">
												<a href="<?=base_url()?>admin/orders/detail/<?=$order['id']?>" class="link" target="_blank">#<?=$order['id']?></a>
											</td>
											<td>
												<span class="responsive-desc">Platba</span>
												<?=$order['payment'];?>
											</td>
											<td>
												<span class="responsive-desc">Doprava</span>
												<?=$order['delivery']?>
											</td>
											<td>
												<span class="responsive-desc">Cena objednávky</span>
												<?=cutePrice($order['delivery_price'] + $order['order_price']);?>
											</td>
											<td>
												<span class="responsive-desc">Stav</span>
												<span class="float-none ml-2 <?=Order_model::$statuses[$order['status']]['class']?>"><?=Order_model::$statuses[$order['status']]['name']?></span>
											</td>
											<td class="text-lg-right">
												<a href="<?=base_url()?>admin/orders/detail/<?=$order['id']?>" class="own-btn-icon  blue" title="Zobraziť objednávku" data-toggle="tooltip" data-placement="top"><i class="fa fa-eye"></i></a>
											</td>
									<?php
									$totalSum += $order['delivery_price'] + $order['order_price'];
								}
							?>
						</tbody>
						<tfoot class="tfoot-fancy">
							<tr>
								<td colspan="3"><b>Celková cena objednávok</b></td>
								<td colspan="3"><b><?=cutePrice($totalSum)?></b></td>
							</tr>
						</tfoot>
					</table>
				</div>
			</div>
		</div>
		<div class="col-lg-4">
			<div class="card-content">
				<div class="cardHead">
					<h2>Informácie</h2>

				</div>
				<div class="cardInnerBody">
					<div class="table-responsive">
						<table class="table">
							<tbody class="tbody-fancy">
								<tr>
									<td class="font-weight-semibold">Meno</td>
									<td><?=$customer_info->name?></td>
								</tr>
								<tr>
									<td class="font-weight-semibold">Priezvisko</td>
									<td><?=$customer_info->surname?></td>
								</tr>
								<tr>
									<td class="font-weight-semibold">Email</td>
									<td><?=$customer_info->email?></td>
								</tr>
								<tr>
									<td class="font-weight-semibold">Telefón</td>
									<td><?=$customer_info->phone_number?></td>
								</tr>
								<tr>
									<td class="font-weight-semibold">Mesto</td>
									<td><?=$customer_info->city?></td>
								</tr>
								<tr>
									<td class="font-weight-semibold">Ulica</td>
									<td><?=$customer_info->street?></td>
								</tr>
								<tr>
									<td class="font-weight-semibold">PSČ</td>
									<td><?=$customer_info->zip_code?></td>
								</tr>
								<tr>
									<td class="font-weight-semibold">Registrácia</td>
									<td><?=cuteDate($customer_info->registration_time)?></td>
								</tr>
								<tr>
									<td class="font-weight-semibold">Posledná aktivita</td>
									<td><?=cuteDateTime($customer_info->last_login)?></td>
								</tr>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>