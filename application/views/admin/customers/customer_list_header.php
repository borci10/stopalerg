<div class="cardBody page_header">
	<div class="row py-3">
		<div class="col-lg-9">
			<form method="get">
				<div class="row">
					<div class="col-lg-3">
						<input type="text" class="form-control" name="keyWordFilter" placeholder="Hľadaný výraz" value="<?=$this->input->get('keyWordFilter')?>">
					</div>
					<div class="col-lg-2">
						<input type="text" class="form-control js-dateFrom" autocomplete="off" name="dateFromFilter" placeholder="Registrácia od" value="<?=$this->input->get('dateFromFilter')?>">
					</div>
					<div class="col-lg-2">
						<input type="text" class="form-control js-dateTo" autocomplete="off" name="dateToFilter" placeholder="Registrácia do" value="<?=$this->input->get('dateToFilter')?>">
					</div>
					<div class="col-lg-2">
						<button type="submit" class="own-btn">Filtrovať</button>
					</div>
				</div>
			</form>
		</div>
		<div class="col-lg-3 text-lg-right">
			<?php
				$this->load->view('_partials/_paggination', $this->data);
			?>
		</div>
	</div>
</div>

<script>
	$(function(){
		$(".js-dateFrom").datepicker({
			dateFormat: "dd.mm.yy",
			changeMonth: true,
			changeYear: true
		});
		$(".js-dateTo").datepicker({
			dateFormat: "dd.mm.yy",
			changeMonth: true,
			changeYear: true
		});
	});
</script>