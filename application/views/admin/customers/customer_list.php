<div class="card-content">
	<div class="table-responsive">
		<table class="table mb-0">
			<thead class="thead-fancy">
				<tr>
					<th>Id</th>
					<th>Meno</th>
					<th>Priezvisko</th>
					<th>Email</th>
					<th>Telefón</th>
					<th>Nákupy</th>
					<th>Registrácia</th>
					<th>Posledná návšteva</th>
					<th></th>
				</tr>
			</thead>
			<tbody class="tbody-fancy">
				<?php
					foreach($customers as $customer) {
						?>
							<tr>
								<td>
									<span class="responsive-desc">Id</span>
									<?=$customer['id'];?>
								</td>
								<td>
									<span class="responsive-desc">Meno</span>
									<?=$customer['name'];?>
								</td>
								<td>
									<span class="responsive-desc">Priezvisko</span>
									<?=$customer['surname'];?>
								</td>
								<td>
									<span class="responsive-desc">Email</span>
									<?=$customer['email'];?>
								</td>
								<td>
									<span class="responsive-desc">Telefón</span>
									<?=$customer['phone_number'];?>
								</td>
								<td>
									<span class="responsive-desc">Nákupy</span>
									<?=($customer['orders_price'] ? cutePrice($customer['orders_price']) : '-');?>
								</td>
								<td>
									<span class="responsive-desc">Registrácia</span>
									<?=cuteDate($customer['registration_time']);?>
								</td>
								<td>
									<span class="responsive-desc">Posledná návšteva</span>
									<?=cuteDateTime($customer['last_login']);?>
								</td>
								<td class="text-lg-right">
									<a href="<?=base_url()?>admin/customers/detail/<?=$customer['id']?>" class="own-btn-icon  blue" title="Zobraziť objednávku" data-toggle="tooltip" data-placement="top"><i class="fa fa-eye"></i></a>
								</td>
							</tr>
						<?php
					}
				?>
			</tbody>
		</table>
	</div>
</div>
<div class="cardBody text-md-right">
	<?php
		$this->load->view('_partials/_paggination', $this->data);
	?>
</div>