<div class="cardBodyWrapper">
	<form method="post" data-toggle="validator" role="form">
		<div class="card-content">
			<div class="cardHead">
				<h2>Fotografia</h2>
			</div>
			<div class="cardInnerBody">
				<div id="reviewPhotos" class="row">
					<div class="col-xl-3 col-lg-4 col-md-6 col-12 my-2" id="photoInputDiv" style="display:<?=(empty($review->image) ? 'block;' : 'none;')?>">
						<div class="form-group mb-0">
							<div class="fileContainer_label">Vybrať foto<small id="fileHelp" class="form-text text-muted">Max veľkosť 10MB. <br />Podporované typy .png, .jpg a .gif.</small></div>
							<input type="file" class="form-control-file fileContainer " id="photoInput" multiple>
						</div>
					</div>
					<?php
						if (!empty($review->image)) {
							$this->data['image'] = $review->image;
							$this->load->view('admin/review/_partials/image_upload_item', $this->data);
						}
					?>
				</div>
			</div>
		</div>
		<div class="cardBodyWrapper pt-5">
			<div class="card-content">
				<div class="cardInnerBody">
					<div class="form-group has-feedback">
						<label for="header">Názov recenzie *</label>
						<input
							type="text"
							class="form-control"
							id="header"
							maxlength="50"
							name="header"
							value="<?php echo set_value('header', is_set(@$review->header)); ?>"
							required
						>
						<div class="help-block with-errors"><?php echo form_error('header'); ?></div>
					</div>
					<div class="form-group has-feedback">
						<label for="rating">Hodnotenie *</label>
						<select class="form-control fa" id="rating" name="rating">
							<option value="0">-</option>
							<option value="2" <?=is_selected(@$review->rating, 2)?>>&#xf005;</option>
							<option value="4" <?=is_selected(@$review->rating, 4)?>>&#xf005;&#xf005;</option>
							<option value="6" <?=is_selected(@$review->rating, 6)?>>&#xf005;&#xf005;&#xf005;</option>
							<option value="8" <?=is_selected(@$review->rating, 8)?>>&#xf005;&#xf005;&#xf005;&#xf005;</option>
							<option value="10"<?=is_selected(@$review->rating, 10)?>>&#xf005;&#xf005;&#xf005;&#xf005;&#xf005;</option>
						</select>
					</div>
					<div class="form-group has-feedback">
						<label for="content">Obsah</label>
						<textarea class="form-control js-richText" id="content" name="content" rows="6"><?php echo set_value('content', is_set(@$review->content)); ?></textarea>
						<div class="help-block with-errors"><?php echo form_error('content'); ?></div>
					</div>
					<div class="form-group mb-0">
						<input type="submit" class="own-btn green" value="<?=$button?>" name="formConfirm" id="formConfirm">
					</div>
				</div>
			</div>
		</div>
	</form>
</div>


<script>
	//upload image after input change
	$(document).ready(function (e) {
		$('#photoInput').change(function () {
			var file_data = $('#photoInput').prop('files')[0];
			var form_data = new FormData();
			form_data.append('file', file_data);
			$('.fileContainer_label').html('<i class="fa fa-spinner fa-pulse fa-3x fa-fw" aria-hidden="true"></i>');
			$.ajax({
				url: '<?=base_url()?>Admin/Review/uploadPhoto',
				dataType: 'text',
				cache: false,
				contentType: false,
				processData: false,
				data: form_data,
				type: 'post'
			})
			.done(function(data) {
				data = JSON.parse(data);
				if (data[0] == 'ok') {
					id = data[1].slice(0, -4);
					$('#reviewPhotos').append(data[2]);
					$('#photoInputDiv').hide();
					$('#photoInput').val('');
				} else {
					alert('Pri nahrávaní fotografie došlo k chybe! Skúste to prosím znovu.');
					$('.fileContainer_label').html('Vybrať foto<small id="fileHelp" class="form-text text-muted">Max veľkosť 10MB. <br />Podporované typy .png, .jpg a .gif.</small>');
				}
			})
			.fail(function(jqXHR, textStatus, errorThrown) {
				alert('Pri nahrávaní fotografie došlo k chybe! Skúste to prosím znovu.');
				$('.fileContainer_label').html('Vybrať foto<small id="fileHelp" class="form-text text-muted">Max veľkosť 10MB. <br />Podporované typy .png, .jpg a .gif.</small>');
				console.log(jqXHR);
				console.log(textStatus);
				console.log(errorThrown);
			})
			.always(function() {
				$('#photoInput').val('');
			});
		});
	});

	//remove image
	$(document).on('click', '.js-removeImage', function() {
		if (confirm("Naozaj chcete odstrániť fotografiu?")) {
			var imgName = $(this).data('img_name');

			$.ajax({
				url: '<?=base_url()?>Admin/Review/removePhoto',
				data: {file_name: imgName},
				dataType: 'text',
				type: 'post',
				success: function (data) {
					id = data.slice(0, -4);
					$('#photoItem_' + id).remove();
					$('#inputFormField_' + id).remove();
					$('#photoInputDiv').show();
					$('.fileContainer_label').html('Vybrať foto<small id="fileHelp" class="form-text text-muted">Max veľkosť 10MB. <br />Podporované typy .png, .jpg a .gif.</small>');
				}
			});
		}
	});
	
	$('.js-richText').richText({
		videoEmbed: false,
		imageUpload: false,
		fileUpload: false,	
	});
</script>