<div class="card-content">
	<div class="table-responsive">
		<table class="table mb-0">
			<thead>
			<tr>
				<th>Obrázok</th>
				<th>Názov</th>
				<th>Hodnotenie</th>
				<th>Dátum pridania</th>
				<th></th>
			</tr>
			</thead>
			<tbody>
				<?php
					foreach($reviews as $review) {
						?>
							<tr>
								<td>
									<img src="<?=(file_exists('uploads/review/400/' . (empty($review['image']) ? '-' : $review['image'])) ? base_url() . 'uploads/review/400/' . $review['image'] : base_url() . 'images/No_image_available.jpg')?>" class="img-fluid" alt="Img">
								</td>
								<td>
									<span class="responsive-desc">Názov</span>
									<a href="<?=base_url()?>admin/reviews/edit/<?=$review['id']?>"><?=character_limiter($review['header'],50)?></a>
								</td>
								<td>
									<span class="responsive-desc">Hodnotenie</span>
									<?php
										for($i = 1; $i <= ($review['rating'] / 2); $i++) {
											echo '<i class="far fa-star"></i>';
										}
										if ($review['rating'] % 2) {
											echo '<i class="far fa-star-half"></i>';
										}
									?>
								</td>
								<td>
									<span class="responsive-desc">Dátum pridania</span>
									<?=cuteDate($review['date'])?>
								</td>
								<td>
									<div class="text-lg-right">
										<?php
										if ($review['published']) {
											?>
											<a href="<?=base_url()?>admin/reviews/unpublish/<?=$review['id']?>" class="own-btn-icon  blue" title="Zrušiť publikáciu" data-toggle="tooltip" data-placement="top"><i class="fa fa-eye"></i></a>
											<?php
										} else {
											?>
											<a href="<?=base_url()?>admin/reviews/publish/<?=$review['id']?>" class="own-btn-icon  disabled" title="Publikovať" data-toggle="tooltip" data-placement="top"><i class="fa fa-eye-slash"></i></a>
											<?php
										}
										?>
										<a href="<?=base_url()?>admin/reviews/edit/<?=$review['id']?>" class="own-btn-icon  green" title="Upraviť recenziu" data-toggle="tooltip" data-placement="top"><i class="fas fa-pencil-alt" aria-hidden="true"></i></a>
										<a href="<?=base_url()?>admin/reviews/delete/<?=$review['id']?>" onclick="return confirm('Naozaj chcete odstrániť recenziu <?=$review['header']?>?');" class="own-btn-icon  red" title="Odstrániť recenziu" data-toggle="tooltip" data-placement="top"><i class="fas fa-trash-alt" aria-hidden="true"></i></a>
									</div>
								</td>
							</tr>
						<?php
					}
				?>
			</tbody>
		</table>
	</div>
</div>
