<div class="col-xl-3 col-lg-4 col-md-6 col-12 my-2 photoSortable" id="photoItem_<?=substr($image, 0, -4)?>">
	<div class="photoItem ui-state-default">
		<div class="photoWrap">
			<input type="hidden" name="image" value="<?=$image?>" id="inputFormField_<?=substr($image, 0, -4)?>">
			<img src="<?=base_url()?>uploads/review/400/<?=$image?>" class="img-fluid">
			<button type="button" class="own-btn-icon  red js-removeImage" data-img_name="<?=$image?>" title="Odstrániť fotografiu" data-toggle="tooltip" data-placement="right"><i class="fa fa-trash"></i></button>
		</div>
	</div>
</div>