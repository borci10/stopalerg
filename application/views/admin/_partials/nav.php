<li class="<?=(($this->uri->segment(1) == 'admin' && $this->uri->segment(2) == '') ? 'active' : '')?>">
	<a href="<?=base_url()?>admin">
		<i class="fas fa-chart-line"></i>
		<span>Prehľad</span>
	</a>
</li>
<li class="<?=(($this->uri->segment(2) == 'orders') ? 'active' : '')?>">
	<a href="<?=base_url()?>admin/orders">
		<i class="fas fa-shopping-cart"></i>
		<span>Objednávky</span>
	</a>
</li>
<li class="<?=(($this->uri->segment(2) == 'customers') ? 'active' : '')?>">
	<a href="<?=base_url()?>admin/customers">
		<i class="fas fa-user-friends"></i>
		<span>Zákazníci</span>
	</a>
</li>
<li class="<?=(($this->uri->segment(2) == 'reviews') ? 'active' : '')?>">
	<a href="<?=base_url()?>admin/reviews">
		<i class="fas fa-star"></i>
		<span>Recenzie</span>
	</a>
</li>
<li class="<?=(($this->uri->segment(2) == 'newsletter') ? 'active' : '')?>">
	<a href="<?=base_url()?>admin/newsletter">
		<i class="fas fa-envelope"></i>
		<span>Newsletter</span>
	</a>
</li>
<li class="<?=(($this->uri->segment(2) == 'products') ? 'active' : '')?>">
	<a href="<?=base_url()?>admin/products">
		<i class="fas fa-box-open"></i>
		<span>Produkty</span>
	</a>
</li>
<li class="<?=(($this->uri->segment(2) == 'delivery') ? 'active' : '')?>">
	<a href="<?=base_url()?>admin/delivery">
		<i class="fas fa-truck"></i>
		<span>Možnosti dopravy</span>
	</a>
</li>
<li class="<?=(($this->uri->segment(2) == 'payment') ? 'active' : '')?>">
	<a href="<?=base_url()?>admin/payment">
		<i class="far fa-credit-card"></i>
		<span>Možnosti platby</span>
	</a>
</li>
<li class="<?=(($this->uri->segment(2) == 'settings') ? 'active' : '')?>">
	<a href="<?=base_url()?>admin/settings">
		<i class="fas fa-cogs"></i>
		<span>Nastavenia</span>
	</a>
</li>
<li class="logout">
	<a href="<?=base_url()?>logout">
		<i class="fas fa-power-off"></i>
		<span>Odhlásiť</span>
	</a>
</li>