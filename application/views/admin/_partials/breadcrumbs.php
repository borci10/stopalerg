<div id="breadcrumbs">
	<div class="d-flex justify-content-between align-items-center flex-wrap">
		<div class="left">
			<h1 class="breadcrumbsTitle"><?=$breadcrumbsTitle?></h1>
		</div>
		<div class="right d-flex justify-content-end align-items-center flex-wrap">
			<?php
			if($this->uri->segment(2) == 'products') {
				if($this->uri->segment(3)) {
					?>
						<a href="<?=base_url()?>admin/products" class="own-btn red">Späť</a>
					<?php
				} else {
					?>
						<a href="<?=base_url()?>admin/products/add" class="own-btn green mr-1">Pridať nový produkt</a>
						<a href="<?=base_url()?>export/exportProducts" class="own-btn blue mx-1">Exportovať produkty</a>
					<?php
				}
			}
				if($this->uri->segment(2) == 'reviews') {
					if($this->uri->segment(3)) {
						?>
							<a href="<?=base_url()?>admin/reviews" class="own-btn red">Späť</a>
						<?php
					} else {
						?>
							<a href="<?=base_url()?>admin/reviews/add" class="own-btn green mr-1">Pridať novú recenziu</a>
						<?php
					}
				}
				if($this->uri->segment(2) == 'content') {
					if($this->uri->segment(3)) {
						?>
							<a href="<?=base_url()?>admin/content" class="own-btn red">Späť</a>
						<?php
					} else {
						?>
							<a href="<?=base_url()?>admin/content/add" class="own-btn mr-1 green">Pridať novú podstránku</a>
						<?php
					}
				}
				if($this->uri->segment(2) == 'category') {
					if($this->uri->segment(3)) {
						?>
							<a href="<?=base_url()?>admin/category" class="own-btn red">Späť</a>
						<?php
					} else {
						?>
							<a href="<?=base_url()?>admin/category/add" class="own-btn green ml-2">Pridať kategóriu</a>
						<?php
					}
				}
			?>
		</div>
	</div>
</div>