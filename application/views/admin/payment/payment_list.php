<div class="card-content">
	<div class="table-responsive">
		<table class="table mb-0">
			<thead class="thead-fancy">
				<tr>
					<th>Spôsob platby</th>
					<th>Extra poplatok</th>
					<th></th>
				</tr>
			</thead>
			<tbody class="tbody-fancy">
				<?php
					foreach($payments as $payment) {
						if (($this->payment_model->getPaymentUid($payment['id']) == 'ttp')) {
							if (!$this->config->item('trust_pay')) {
								continue;
							}
						}
						?>
							<tr>
								<td>
									<span class="responsive-desc">Možnosť platby</span>
									<?=$payment['title']?>
								</td>
								<td>
									<span class="responsive-desc">Extra poplatok</span>
									<?=cutePrice($payment['price'])?>
								</td>
								<td>
									<div class="text-lg-right">
										<a href="<?=base_url()?>admin/payment/edit/<?=$payment['id']?>" class="own-btn-icon  green" title="Upraviť možnosť platby" data-toggle="tooltip" data-placement="top"><i class="fas fa-pencil-alt" aria-hidden="true"></i></a>
									</div>
								</td>
							</tr>
						<?php
					}
				?>
			</tbody>
		</table>
	</div>
</div>