<div class="card-content">
	<form method="post" data-toggle="validator" role="form">
		<div class="row">
			<div class="form-group has-feedback col-md-6 col-12">
				<label for="price">Cena *</label>
				<div class="input-group">
					<input
						type="number"
						class="form-control"
						id="price"
						name="price"
						value="<?php echo set_value('price', is_set(@$payment->price)); ?>"
						placeholder="0.00"
						min="0"
						step="0.01"
					>
					<span class="input-group-addon"><?=$this->config->item('currency');?></span>
				</div>
				<div class="help-block with-errors"><?php echo form_error('price'); ?></div>
			</div>
		</div>
		<div class="mt-1">
			<button type="submit" class="own-btn green" value="formConfirm" name="formConfirm" id="formConfirm">Uložiť</button>
			<a href="<?=base_url()?>admin/payment" class="own-btn red">Zrušiť</a>
		</div>
	</form>
</div>


