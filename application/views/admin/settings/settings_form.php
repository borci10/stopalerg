<div class="cardBodyWrapper">
	<div class="card-content">
		<div class="cardHead">
			<h2>Údaje o firme</h2>

		</div>
		<div class="cardInnerBody">
			<form method="post" data-toggle="validator" role="form">
				<div class="d-flex justify-content-end">
					<div class="form-group has-feedback mb-0">
						<label class="checkbox-container" for="companyVatPayer">
							<input type="checkbox" class="form-check-input" name="vat_payer" id="companyVatPayer" value="1" <?=is_checked($vat_payer, 1)?>>
							* Platca DPH
							<span class="checkmark"></span>
						</label>
					</div>
				</div>
				<div class="row">
					<div class="col-lg-4 col-12">
						<div class="form-group has-feedback">
							<label for="company_name">Názov spoločnosti</label>
							<input
								type="text"
								class="form-control"
								id="company_name"
								name="company_name"
								maxlength="255"
								value="<?=set_value('company_name', @$company_name); ?>"
							>
							<div class="help-block with-errors"><?php echo form_error('company_email'); ?></div>
						</div>
					</div>
					<div class="col-lg-4 col-12">
						<div class="form-group has-feedback">
							<label for="company_email">Email</label>
							<input
								type="email"
								class="form-control"
								id="company_email"
								name="company_email"
								maxlength="255"
								value="<?=set_value('company_email', @$company_email); ?>"
							>
							<div class="help-block with-errors"><?php echo form_error('company_email'); ?></div>
						</div>
					</div>
					<div class="col-lg-4 col-12">
						<div class="form-group has-feedback">
							<label for="company_phone">Telefón</label>
							<input
								type="text"
								class="form-control"
								id="company_phone"
								name="company_phone"
								maxlength="255"
								value="<?=set_value('company_phone', @$company_phone); ?>"
							>
							<div class="help-block with-errors"><?php echo form_error('company_phone'); ?></div>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-lg-4 col-12">
						<div class="form-group has-feedback">
							<label for="company_ico">IČO</label>
							<input
								type="text"
								class="form-control"
								id="company_ico"
								name="company_ico"
								maxlength="255"
								value="<?=set_value('company_ico', @$company_ico); ?>"
							>
							<div class="help-block with-errors"><?php echo form_error('company_ico'); ?></div>
						</div>
					</div>
					<div class="col-lg-4 col-12">
						<div class="form-group has-feedback">
							<label for="company_dic">DIČ</label>
							<input
								type="text"
								class="form-control"
								id="company_dic"
								name="company_dic"
								maxlength="255"
								value="<?=set_value('company_dic', @$company_dic); ?>"
							>
							<div class="help-block with-errors"><?php echo form_error('company_dic'); ?></div>
						</div>
					</div>
					<div class="col-lg-4 col-12">
						<div class="form-group has-feedback" id="companyIcdphField" style="display:<?=$vat_payer ? 'block' : 'none'?>;">
							<label for="company_icdph">IČ DPH *</label>
							<input
								type="text"
								class="form-control"
								id="company_icdph"
								name="company_icdph"
								maxlength="255"
								value="<?=set_value('company_icdph', @$company_icdph); ?>"
							>
							<div class="help-block with-errors"><?php echo form_error('company_icdph'); ?></div>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-lg-4 col-12">
						<div class="form-group has-feedback">
							<label for="company_street">Ulica</label>
							<input
								type="text"
								class="form-control"
								id="company_street"
								name="company_street"
								maxlength="255"
								value="<?=set_value('company_street', @$company_street); ?>"
							>
							<div class="help-block with-errors"><?php echo form_error('company_street'); ?></div>
						</div>
					</div>
					<div class="col-lg-4 col-12">
						<div class="form-group has-feedback">
							<label for="company_city">Mesto</label>
							<input
								type="text"
								class="form-control"
								id="company_city"
								name="company_city"
								maxlength="255"
								value="<?=set_value('company_city', @$company_city); ?>"
							>
							<div class="help-block with-errors"><?php echo form_error('company_city'); ?></div>
						</div>
					</div>
					<div class="col-lg-4 col-12">
						<div class="form-group has-feedback">
							<label for="company_zip_code">PSČ</label>
							<input
								type="text"
								class="form-control"
								id="company_zip_code"
								name="company_zip_code"
								maxlength="255"
								value="<?=set_value('company_zip_code', @$company_zip_code); ?>"
								pattern="^\d{3} ?\d{2}$"
							>
							<div class="help-block with-errors"><?php echo form_error('company_zip_code'); ?></div>
						</div>
					</div>
				</div>
				<button type="submit" class="own-btn" value="confirm" name="changeCompanyInfoConfirm" id="changeCompanyInfoConfirm">Uložiť firemné údaje</button>
			</form>
		</div>
	</div>
	<div class="card-content">
		<div class="cardHead">
			<h2>Informácie o banke</h2>

		</div>
		<div class="cardInnerBody">
			<form method="post" data-toggle="validator" role="form">
				<div class="row">
					<div class="col-lg-4 col-12">
						<div class="form-group has-feedback">
							<label for="bank_name">Názov banky</label>
							<input
								type="text"
								class="form-control"
								id="bank_name"
								maxlength="255"
								name="bank_name"
								value="<?=set_value('bank_name', @$bank_name); ?>"
							>
							<div class="help-block with-errors"><?php echo form_error('bank_name'); ?></div>
						</div>
					</div>
					<div class="col-lg-4 col-12">
						<div class="form-group has-feedback">
							<label for="bank_account_number">Číslo účtu</label>
							<input
								type="text"
								class="form-control"
								id="bank_account_number"
								maxlength="255"
								name="bank_account_number"
								value="<?=set_value('bank_account_number', @$bank_account_number); ?>"
							>
							<div class="help-block with-errors"><?php echo form_error('bank_account_number'); ?></div>
						</div>
					</div>
					<div class="col-lg-4 col-12">
						<div class="form-group has-feedback">
							<label for="bank_code">Kód banky</label>
							<input
								type="text"
								class="form-control"
								id="bank_code"
								maxlength="255"
								name="bank_code"
								value="<?=set_value('bank_code', @$bank_code); ?>"
							>
							<div class="help-block with-errors"><?php echo form_error('bank_code'); ?></div>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-lg-4 col-12">
						<div class="form-group has-feedback">
							<label for="bank_iban">IBAN</label>
							<input
								type="text"
								class="form-control"
								id="bank_iban"
								maxlength="255"
								name="bank_iban"
								value="<?=set_value('bank_iban', @$bank_iban); ?>"
							>
							<div class="help-block with-errors"><?php echo form_error('bank_iban'); ?></div>
						</div>
					</div>
					<div class="col-lg-4 col-12">
						<div class="form-group has-feedback">
							<label for="bank_swift">SWIFT</label>
							<input
								type="text"
								class="form-control"
								id="bank_swift"
								maxlength="255"
								name="bank_swift"
								value="<?=set_value('bank_swift', $bank_swift); ?>"
							>
							<div class="help-block with-errors"><?php echo form_error('bank_swift'); ?></div>
						</div>
					</div>
				</div>
				<button type="submit" class="own-btn" value="confirm" name="changeBankInfoConfirm" id="changeBankInfoConfirm">Uložiť údaje o banke</button>
			</form>
		</div>
	</div>
</div>

<script>
	$(document).on('change', '#companyVatPayer', function() {
		$('#companyIcdphField').slideToggle();
		if ($('#companyVatPayer').is(':checked')) {
			$("#companyIcdph").prop('required',true);
		} else {
			$("#companyIcdph").prop('required',false);
		}
	});
</script>