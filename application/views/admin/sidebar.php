<div id="admin-bar">
	<a href="<?=base_url()?>admin/settings">
		<div class="user-profile">
			<img src="<?=base_url()?>images/admin/user.png" class="img-fluid" alt="Fotka">
			<div class="user-info">
				<p>Administrátor</p>
				<p class="name"><?php echo($this->session->userdata('name'))?></p>
			</div>
		</div>
	</a>
	<div id="admin-menu">
		<ul>
			<?php $this->load->view('admin/_partials/nav');?>
		</ul>
	</div>
</div>