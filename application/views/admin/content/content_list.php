<div class="card-content">
	<div class="table-responsive">
		<table class="table mb-0">
			<thead class="thead-fancy">
				<tr>
					<th>Id</th>
					<th>Názov podstránky</th>
					<th></th>
				</tr>
			</thead>
			<tbody class="tbody-fancy">
				<?php
					foreach($subpages as $subpage) {
						?>
							<tr>
								<td>
									<span class="responsive-desc">Id</span>
									<?=$subpage['id'];?>
								</td>
								<td>
									<span class="responsive-desc">Názov podstránky</span>
									<a href="<?=base_url()?>admin/content/edit/<?=$subpage['id']?>"><?=$subpage['title'];?></a>
								</td>
								<td>
									<div class="text-lg-right">
										<a href="<?=base_url()?>content/<?=$subpage['id'];?>" class="own-btn-icon  blue" title="Zobraziť podstránku" data-toggle="tooltip" data-placement="top"><i class="fa fa-eye"></i></a>
										<a href="<?=base_url()?>admin/content/edit/<?=$subpage['id']?>" class="own-btn-icon  green" title="Upraviť podstránku"><i class="fas fa-pencil-alt" aria-hidden="true"></i></a>
										<a href="<?=base_url()?>admin/content/remove/<?=$subpage['id']?>" class="own-btn-icon  red" title="Odstrániť podstránku" onclick="return confirm('Naozaj chcete odstrániť podstránku <?=$subpage['title']?>?')"><i class="fas fa-trash-alt" aria-hidden="true"></i></a>
									</div>
								</td>
							</tr>
						<?php
					}
				?>
			</tbody>
		</table>
	</div>
</div>