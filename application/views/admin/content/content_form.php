<div class="card-content">
	<form id="addContentForm" method="post" data-toggle="validator" role="form">
		<div class="form-group has-feedback">
			<label for="subpageTitle">Názov podstránky *</label>
			<input
				type="text"
				class="form-control"
				id="subpageTitle"
				maxlength="50"
				name="subpageTitle"
				value="<?php echo set_value('subpageTitle', is_set(@$subpageContent->title)); ?>"
				required
			>
			<div class="help-block with-errors"><?php echo form_error('subpageTitle'); ?></div>
		</div>
		<div class="form-group has-feedback">
			<label for="subpageText">Obsah</label>
			<textarea class="form-control richTextEdior" id="subpageText" name="subpageText" rows="6"><?php echo set_value('subpageText', is_set(@$subpageContent->content)); ?></textarea>
			<div class="help-block with-errors"><?php echo form_error('subpageText'); ?></div>
		</div>

		<input type="submit" class="own-btn" value="<?=$button_title?>" name="formConfirm" id="formConfirm">
		<a href="<?=base_url()?>admin/content" class="own-btn red">Zrušiť</a>
	</form>
</div>

<script>
	$('.richTextEdior').richText();
</script>