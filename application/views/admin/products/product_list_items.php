<?php
	foreach($products as $product) {
		?>
			<tr>
				<td>
					<?php
						if ($product['file_name']) {
							?>
								<img src="<?=cuteImage($product['file_name'])?>" class="img-fluid" alt="Img">
							<?php
						} else {
							?>
								<img src="<?=base_url()?>images/No_image_available.jpg" class="img-fluid" alt="Img">
							<?php
						}
					?>	
				</td>
				<td class="productName">
					<a href="<?=base_url()?>admin/products/edit/<?=$product['id']?>" class="link"><?=$product['name']?></a>
				</td>
				<td>
					<span class="responsive-desc">Cena bez DPH</span>
					<?=cutePrice($product['price'])?>
				</td>
				<td>
					<span class="responsive-desc">Cena s DPH</span>
					<?=cutePrice(priceCalc($product['price'], 1, 0, $product['tax'] / 100))?>
				</td>
				<td>
					<span class="responsive-desc">Zľava</span>
					<?=$product['discount']?> %
				</td>
				<td>
					<span class="responsive-desc">Množstvo</span>
					<?=$product['quantity']?>
				</td>
				<td>
					<span class="responsive-desc">Dátum pridania</span>
					<?=cuteDate($product['creation_date'])?>
				</td>
				<td>
					<div class="text-lg-right">
						<?php
							if ($product['show_product']) {
								?>
									<a href="<?=base_url()?>admin/products/hide/<?=$product['id']?>" class="own-btn-icon  blue" title="Skryť produkt z ponuky" data-toggle="tooltip" data-placement="top"><i class="fa fa-eye"></i></a>
								<?php
							} else {
								?>
									<a href="<?=base_url()?>admin/products/show/<?=$product['id']?>" class="own-btn-icon  disabled" title="Zobraziť produkt v ponuke" data-toggle="tooltip" data-placement="top"><i class="fa fa-eye-slash"></i></a>
								<?php
							}
						?>
						<a href="<?=base_url()?>admin/products/edit/<?=$product['id']?>" class="own-btn-icon  green" title="Upraviť produkt" data-toggle="tooltip" data-placement="top"><i class="fas fa-pencil-alt" aria-hidden="true"></i></a>
						<a href="<?=base_url()?>admin/products/remove/<?=$product['id']?>" class="own-btn-icon  red" title="Odstrániť produkt"  data-toggle="tooltip" data-placement="top" onclick="return confirm('Naozaj chcete odstrániť produkt <?=$product['name']?>?')"><i class="fas fa-trash-alt" aria-hidden="true"></i></a>
					</div>
				</td>
			</tr>
		<?php
	}
?>