<form id="addProductForm" method="post" data-toggle="validator" role="form" class="cardBodyWrapper">
	<div class="card-content">
		<?php
			$this->load->view('admin/products/product_photos', $this->data);
		?>
	</div>
	<div class="row">
		<div class="col-md-8 col-12">
			<div class="card-content">
				<div class="cardHead">
					<h2>Informácie o produkte</h2>

				</div>
				<div class="cardInnerBody">
					<div class="form-group has-feedback">
						<label for="productName">Názov produktu *</label>
						<input
							type="text"
							class="form-control"
							id="productName"
							maxlength="50"
							name="productName"
							value="<?php echo set_value('productName', is_set(@$productInfo->name)); ?>"
							required
						>
						<div class="help-block with-errors"><?php echo form_error('productName'); ?></div>
					</div>
					<div class="form-group has-feedback">
						<label for="productShortDescription">Stručný popis</label>
						<textarea class="form-control" id="productShortDescription" name="productShortDescription" rows="2"><?php echo set_value('productShortDescription', is_set(@$productInfo->short_description)); ?></textarea>
						<div class="help-block with-errors"><?php echo form_error('productShortDescription'); ?></div>
					</div>
					<div class="form-group has-feedback">
						<label for="productLongDescription">Popis</label>
						<textarea class="form-control richTextEdior" id="productLongDescription" name="productLongDescription" rows="6"><?php echo set_value('productLongDescription', is_set(@$productInfo->long_description)); ?></textarea>
						<div class="help-block with-errors"><?php echo form_error('productLongDescription'); ?></div>
					</div>
				</div>
			</div>
		</div>
		<div class="col-md-4 col-12">
			<div class="card-content">
				<div class="cardHead">
					<h2>Základné nastevenie</h2>

				</div>
				<div class="cardInnerBody">
					<div class="row">
						<div class="form-group has-feedback col-md-6 col-sm-6 col-12">
							<label for="productPrice">Cena bez DPH</label>
							<div class="input-group">
								<input
									type="number"
									class="form-control js-price"
									id="productPrice"
									name="productPrice"
									value="<?php echo set_value('productPrice', is_set(@$productInfo->price)); ?>"
									placeholder="0.00"
									min="0"
									step="0.01"
								>
								<span class="input-group-addon"><?=$this->config->item('currency');?></span>
							</div>
							<div class="help-block with-errors"><?php echo form_error('productPrice'); ?></div>
						</div>
						<div class="form-group has-feedback col-md-6 col-sm-6 col-12">
							<label for="productPriceWithTax">Cena s DPH</label>
							<div class="input-group">
								<input
									type="number"
									class="form-control js-priceWithTax"
									id="productPriceWithTax"
									value="<?=priceCalc($productInfo->price ?? 0, 1, 0, ($productInfo->tax ?? 0) / 100); ?>"
									placeholder="0.00"
									min="0"
									step="0.01"
								>
								<span class="input-group-addon"><?=$this->config->item('currency');?></span>
							</div>
							<div class="help-block with-errors"><?php echo form_error('productPrice'); ?></div>
						</div>
						<div class="form-group has-feedback col-md-12 col-sm-12 col-12">
							<label for="productTax">DPH</label>
							<div class="input-group">
								<input
									type="number"
									class="form-control js-tax"
									id="productTax"
									name="productTax"
									value="<?php echo set_value('productTax', is_set(@$productInfo->tax, $this->config->item('tax') * 100)); ?>"
									placeholder="0"
									step="0.01"
									min="0"
									max="100"
								>
								<span class="input-group-addon">%</span>
							</div>
							<div class="help-block with-errors"><?php echo form_error('productTax'); ?></div>
						</div>
						<div class="form-group has-feedback col-md-6 col-sm-6 col-12">
							<label for="productDiscount">Zľava</label>
							<div class="input-group">
								<input
									type="number"
									class="form-control"
									id="productDiscount"
									name="productDiscount"
									value="<?php echo set_value('productDiscount', is_set(@$productInfo->discount)); ?>"
									placeholder="0"
									step="0.01"
									min="0"
									max="100"
								>
								<span class="input-group-addon">%</span>
							</div>
							<div class="help-block with-errors"><?php echo form_error('productDiscount'); ?></div>
						</div>
						<div class="form-group has-feedback col-md-6 col-sm-6 col-12">
							<label for="productQuantity">Na sklade</label>
								<div class="input-group">
								<input
									type="number"
									class="form-control"
									id="productQuantity"
									name="productQuantity"
									value="<?php echo set_value('productQuantity', is_set(@$productInfo->quantity)); ?>"
									min="0"
									placeholder="0"
								>
								<span class="input-group-addon">ks</span>
							</div>
							<div class="help-block with-errors"><?php echo form_error('productQuantity'); ?></div>
						</div>
						<div class="form-group has-feedback col-md-6 col-sm-6 col-6 mb-0">
							<label for="supplier">Dodávateľ</label>
							<select class="form-control" id="supplier" name="supplier">
								<option value="">-</option>
								<?php
									foreach($suppliers as $supplier) {
										?>
											<option value="<?=$supplier['id']?>" <?=is_selected(@$productInfo->supplier_id, $supplier['id'])?>><?=$supplier['title']?></option>
										<?php
									}
								?>
							</select>
						</div>
						<div class="form-group has-feedback col-md-6 col-sm-6 col-6 mb-0">
							<label for="origin">Krajina pôvodu</label>
							<select class="form-control" id="origin" name="origin">
								<option value="">-</option>
								<option value="Slovensko" <?=is_selected(@$productInfo->origin, 'Slovensko')?>>Slovensko</option>
								<option value="Česká republika" <?=is_selected(@$productInfo->origin, 'Česká republika')?>>Česká republika</option>
								<option value="Poľsko" <?=is_selected(@$productInfo->origin, 'Poľsko')?>>Poľsko</option>
								<option value="Maďarsko" <?=is_selected(@$productInfo->origin, 'Maďarsko')?>>Maďarsko</option>
								<option value="Nemecko" <?=is_selected(@$productInfo->origin, 'Nemecko')?>>Nemecko</option>
							</select>
						</div>
					</div>
				</div>
			</div>
			<div class="card-content">
				<div class="cardHead">
					<h2>Doprava</h2>

				</div>
				<div class="cardInnerBody">
					<div class="row">
						<div class="form-group has-feedback col-xl-6 col-lg-6 col-md-6 col-sm-6">
							<label for="productHeight">Výška</label>
							<div class="input-group">
								<input
									type="number"
									class="form-control"
									id="productHeight"
									name="productHeight"
									value="<?php echo set_value('productHeight', is_set(@$productInfo->height)); ?>"
									placeholder="0"
									step="0.01"
									min="0"
								>
								<span class="input-group-addon">m</span>
							</div>
							<div class="help-block with-errors"><?php echo form_error('productHeight'); ?></div>
						</div>
						<div class="form-group has-feedback col-xl-6 col-lg-6 col-md-6 col-sm-6">
							<label for="productWidth">Šírka</label>
							<div class="input-group">
								<input
									type="number"
									class="form-control"
									id="productWidth"
									name="productWidth"
									value="<?php echo set_value('productWidth', is_set(@$productInfo->width)); ?>"
									placeholder="0"
									step="0.01"
									min="0"
								>
								<span class="input-group-addon">m</span>
							</div>
							<div class="help-block with-errors"><?php echo form_error('productWidth'); ?></div>
						</div>
						<div class="form-group has-feedback col-xl-6 col-lg-6 col-md-6 col-sm-6 mb-0">
							<label for="productDepth">Hĺbka</label>
							<div class="input-group">
								<input
									type="number"
									class="form-control"
									id="productDepth"
									name="productDepth"
									value="<?php echo set_value('productDepth', is_set(@$productInfo->depth)); ?>"
									placeholder="0"
									step="0.01"
									min="0"
								>
								<span class="input-group-addon">m</span>
							</div>
							<div class="help-block with-errors"><?php echo form_error('productDepth'); ?></div>
						</div>
						<div class="form-group has-feedback col-xl-6 col-lg-6 col-md-6 col-sm-6 mb-0">
							<label for="productWeight">Hmotnosť</label>
							<div class="input-group">
								<input
									type="number"
									class="form-control"
									id="productWeight"
									name="productWeight"
									value="<?php echo set_value('productWeight', is_set(@$productInfo->weight)); ?>"
									placeholder="0"
									step="0.01"
									min="0"
								>
								<span class="input-group-addon">kg</span>
							</div>
							<div class="help-block with-errors"><?php echo form_error('productWeight'); ?></div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="row">
		<?php
			if($this->config->item('product_variable')) {
				?>
					<div class="col-md-12 col-12 ">
						<div class="card-content">
							<div class="cardHead">
								<h2>Variabilné produkty</h2>
							</div>
							<div class="cardInnerBody">
								<div class="form-check mr-3 pl-0">
									<label class="checkbox-container" class="form-check-label">
										<input class="form-check-input" type="checkbox" name="productVariable" value="1" <?=is_checked(@$productInfo->variable_product, 1)?>>
										<span class="checkmark"></span>
										Variabilný produkt
									</label>
								</div>
									<table class="table">
										<thead class="thead-fancy">
											<tr>
												<th>Názov</th>
												<th>Cena</th>
												<th>Zľava</th>
												<th>Množstvo</th>
												<th></th>
											</tr>
										</thead>
										<tbody class="tbody-fancy js-variableProducts">
											<?php
												foreach($productVariables ?? [] as $this->data['variableProduct']) {
													$this->load->view('admin/products/partials/_product_variable_item', $this->data);
												}
											?>
										</tbody>
									</table>
								<button type="button" class="own-btn js-addVariableProduct margin-top-10">Pridať variabilný produkt</button>
							</div>
						</div>
					</div>
				<?php
				}
			?>
		<?php
			if($this->config->item('product_attributes')) {
				?>
					<div class="col-md-6 col-12 ">
						<div class="card-content">
							<div class="cardHead">
								<h2>Vlastnosti produktu</h2>
							</div>
							<div class="cardInnerBody">
								<div class="form-group">
									<div id="productAttributes">
										<?php
											$this->data['rowNumber'] = 0;
											if (!empty($productAttributes)) {
												foreach($productAttributes as $this->data['attribute']) {
													$this->load->view('admin/products/partials/_product_attribute_item', $this->data);
													$this->data['rowNumber']++;
												}
											}
										?>
									</div>
								</div>
								<button type="button" class="own-btn js-addProductAttribute margin-top-10">Pridať novú vlastnosť</button>
							</div>
						</div>
					</div>
				<?php
			}
		?>
		<?php
			if($this->config->item('product_similar')) {
				?>
					<div class="col-md-6 col-12 ">
						<div class="card-content">
							<div class="cardHead">
								<h2>Odporúčané prodkuty</h2>
							</div>
							<div class="cardInnerBody">
								<p>Môžete nastaviť odporúčané produkty ručne. Ak si však želáte, aby boli odporúčané produkty vyberané automaticky na základe predošlých nákupov. Nechajte toto pole prázdne.</p>
								<input type="text" class="form-control js-searchTextInput" id="searchTextInput" placeholder="Hľadať nový odporúčaný produkt">
								<div id="productSimilar" class="margin-top-5">
									<?php
										if (!empty($productSimilar)) {
											foreach($productSimilar as $productSimilarItem) {
												?>
													<div class="productSimilarItem" id="productSimilarItem<?=$productSimilarItem['id']?>">
														<?=$productSimilarItem['name']?> <?=$productSimilarItem['price']?> <?=$this->config->item('currency')?>
														<button class="own-btn-icon  red js-productSimilarRemove" data-id="<?=$productSimilarItem['id']?>"><i class="fas fa-times"></i></button>
														<input type="hidden" name="productSimilar[]" id="productSimilar<?=$productSimilarItem['id']?>" value="<?=$productSimilarItem['id']?>">
													</div>
												<?php
											}
										}
									?>
								</div>
							</div>
						</div>
					</div>
				<?php
			}
		?>
		<?php
			if ($this->config->item('product_recomended')) {
				?>
					<div class="col-md-6 col-12 ">
						<div class="card-content">
							<div class="cardHead">
								<h2>Miera odporúčanosti</h2>
							</div>
							<div class="cardInnerBody">
								<p>Pri zoradení produktov podľa kritéria "odporúčame vám", budú produkty s vyšším číslom na popredných miestach.</p>
								<div class="form-group has-feedback">
									<p>
										<label for="recomendationRatio">Hodnota odporúčania:</label>
										<input type="text" id="recomendationRatio" name="recomendationRatio" readonly style="border:0; color:#f6931f; font-weight:bold;">
									</p>
									<div id="slider-range-max"></div>
								</div>
							</div>
						</div>
					</div>
				<?php
			}
		?>
	</div>
	<div class="card-content">
		<div class="cardInnerBody">
			<button type="submit" class="own-btn green" value="formConfirm" name="formConfirm" id="formConfirm"><?=$buttone_title?></button>
			<a href="<?=base_url()?>admin/products" class="own-btn red">Zrušiť</a>
		</div>
	</div>
</form>

<?php
	if($this->config->item('product_variable')) {
		$this->load->view('admin/products/modals/variable_product_edit');
	}
?>

<script>
	$('.richTextEdior').richText({
		videoEmbed: false,
		imageUpload: false,
		fileUpload: false,	
	});
	
	var processing 				= false;
	var attributeRowNumber 		= <?=$rowNumber ?? 0;?>;
	var attributeSuggestions 	= <?=$attribute_suggestions ?? null;?>;
	
	//add suggestion product attribute
	$(document).on('input', ".js-productAttribute", function() {
		$('.js-productAttribute').autocomplete({
			source: attributeSuggestions
		});	
	});

	//add suggestions in similar products
	$(document).on('keydown', '.js-searchTextInput', function() {	
		var searchValue = $('#searchTextInput').val();
		var suggestions;
		
		if (searchValue.length > 0) {
			$.ajax({
				url: '<?=base_url()?>Admin/Products/getSearchSuggestionsSimilarProducts',
				data: {search_expression: searchValue},
				dataType: 'text', 
				type: 'post',
				success: function(suggest) {					
					suggestions = JSON.parse(suggest);
					$( ".js-searchTextInput" ).autocomplete({
						source: $.map(suggestions, function(e) {return {label:e.name + ' - ' + e.price + " <?=$this->config->item('currency_unicode')?>", id:e.id}}),
						delay: 500,
						minLength: 2,
						select: function (event, ui) {		
							//if item exist, append div with item and remove button	
							if($("#productSimilarItem" + ui.item.id).length == 0) {	
								$('#productSimilar').append('<div class="productSimilarItem" id="productSimilarItem' + ui.item.id + '">' + ui.item.label + ' <button class="own-btn-icon  red js-productSimilarRemove" data-id="' + ui.item.id + '"><i class="fa fa-trash"></i></button><input type="hidden" name="productSimilar[]" id="productSimilar' + ui.item.id + '" value="' + ui.item.id + '"></div>');
							}
							ui.item.value = "";
						}
					});
				}
			});
		}
	});
	
	//START vat calculation
	$(document).on('input', '#productPrice', function() {
		var price 	= $('#productPrice').val();
		var tax 	= $('#productTax').val();
		
		$('#productPriceWithTax').val(priceCalc(price, tax));
	});
	
	$(document).on('input', '#productPriceWithTax', function() {
		var priceWithTax 	= $('#productPriceWithTax').val();
		var tax 			= $('#productTax').val();
		
		$('#productPrice').val(priceWithTax - vatCalc(priceWithTax, tax));
	});
	
	$(document).on('input', '#productTax', function() {
		var price 	= $('#productPrice').val();
		var tax 	= $('#productTax').val();
		
		$('#productPriceWithTax').val(priceCalc(price, tax));
	});
	//END vat calculation
	
	//remove similar product from form
	$(document).on('click', '.js-productSimilarRemove', function() {
		var similarProductId = $(this).data('id');
		$('#productSimilarItem' + similarProductId).remove();
	});
	
	//append attribute text inputs
	$(document).on('click', '.js-addProductAttribute', function() {
		$.ajax({
		   url: '<?=base_url()?>Admin/Products/addProductAttributeHTML/' + attributeRowNumber,
		   success: function(html) {
			  $("#productAttributes").append(html);
			  attributeRowNumber++;
		   }
		});
	});
	
	//remove attribute text inputs
	$(document).on('click', '.js-removeAttribute', function() {
		if (confirm('Naozaj chcete odstrániť tento atribút?')) {
			$('#productAttrinuteRow_' + $(this).data('attribute_row_number')).remove();
		}
	});

	//slider to recomended
	$(function() {
		$( "#slider-range-max" ).slider({
			range: "max",
			min: 0,
			max: 100,
			value: <?=$productInfo->recomended ?? 0?>,
			slide: function( event, ui ) {
				$( "#recomendationRatio" ).val( ui.value );
			}
		});
		$( "#recomendationRatio" ).val( $( "#slider-range-max" ).slider( "value" ) );
	});
	
	//variable products modal
	$(document).on('click', '.js-addVariableProduct', function() {
		$('#variableProductId').val('');
		$('#variableProductName').val('');
		$('#variableProductShortDescription').val('');
		$('#variableProductPrice').val('<?=@$productInfo->price ?? 0?>');
		$('#variableProductDiscount').val(0);
		$('#variableProductQuantity').val(0);
		$('#variableProductEditLabel').html('Pridať variabilný produkt');
		$('#variableProductEdit').modal('show');
	});
	
	//variable products form submit
	$(document).on('submit', "#variableProductForm", function( event ) {	
		if (!event.isDefaultPrevented()) {
			var variableProductId 				= $('#variableProductId').val();
			var variableProductName 			= $('#variableProductName').val();
			var variableProductShortDescription = $('#variableProductShortDescription').val();
			var variableProductPrice			= $('#variableProductPrice').val();
			var variableProductDiscount			= $('#variableProductDiscount').val();
			var variableProductQuantity			= $('#variableProductQuantity').val();
			var variableProductPhotos			= $('.js-variableProductPhoto-id').map(function () {return $(this).val()}).get();
			
			if (!processing) {
				processing = true;
				$('.js-variableProductFormSubmit').html('Ukladá sa <i class="fa fa-spinner fa-pulse fa-fw" aria-hidden="true"></i>');
				
				if (variableProductId) {
					$.ajax({
						url: '<?=base_url()?>Admin/Products/updateVariableProduct', 
						data: {
							variableProductId : variableProductId,
							variableProductName : variableProductName,
							variableProductShortDescription : variableProductShortDescription,
							variableProductPrice : variableProductPrice,
							variableProductDiscount : variableProductDiscount,
							variableProductQuantity : variableProductQuantity,
							variableProductPhotos : variableProductPhotos
						},
						dataType: 'text', 
						type: 'post',
						success: function (data) {
							data = JSON.parse(data);
							if (data[0] == 'ok') {
								$('#variableProductNameRowId_' + data[1]).html(data[2].name);
								$('#variableProductPriceRowId_' + data[1]).html(data[2].price);
								$('#variableProductDiscountRowId_' + data[1]).html(data[2].discount);
								$('#variableProductQuantityRowId_' + data[1]).html(data[2].quantity);
							} else {
								alert('Vyskytla sa chyba. Skúste to prosím znovu.');
							}
							processing = false;
							$('.js-variableProductFormSubmit').html('Uložiť');
							$('#variableProductEdit').modal('hide');
						}
					});
				} else {
					$.ajax({
						url: '<?=base_url()?>Admin/Products/addVariableProduct/<?=$this->uri->segment(4)?>', 
						data: {
							variableProductName : variableProductName,
							variableProductShortDescription : variableProductShortDescription,
							variableProductPrice : variableProductPrice,
							variableProductDiscount : variableProductDiscount,
							variableProductQuantity : variableProductQuantity,
							variableProductPhotos : variableProductPhotos
						},
						dataType: 'text', 
						type: 'post',
						success: function (data) {
							data = JSON.parse(data);
							if (data[0] == 'ok') {
								$('.js-variableProducts').append(data[1]);
							} else {
								alert('Vyskytla sa chyba. Skúste to prosím znovu.');
							}
							processing = false;
							$('.js-variableProductFormSubmit').html('Uložiť');
							$('#variableProductEdit').modal('hide');
						}
					});
				}
			} else {
				alert('Ukladá sa. Počkajte prosím');
			}
		}
		
		event.preventDefault();
	});
	
	//variable products show/hide
	$(document).on('click', '.js-toggleVariableProduct', function() {	
		var variableProductId	= $(this).data('variable_product_id');
		
		$.ajax({
			url: '<?=base_url()?>Admin/Products/toggleVariableProduct', 
			data: {
				variableProductId : variableProductId, 
			},
			dataType: 'text', 
			type: 'post',
			success: function (data) {
				data = JSON.parse(data);
				if (data[0] == 'ok') {
					if ($('#variableProductToggleButtonId_' + data[1]).hasClass('js-show')) {
						$('#variableProductToggleButtonId_' + data[1]).removeClass('blue');
						$('#variableProductToggleButtonId_' + data[1]).removeClass('js-show');
						$('#variableProductToggleButtonId_' + data[1]).addClass('disabled');
						$('#variableProductToggleButtonId_' + data[1]).addClass('js-hide');
						$('#variableProductToggleButtonId_' + data[1]).html('<i class="fa fa-eye"></i>');
						$('#variableProductToggleButtonId_' + data[1]).prop('title', 'Zobraziť produkt v ponuke');
					} else {
						$('#variableProductToggleButtonId_' + data[1]).removeClass('disabled');
						$('#variableProductToggleButtonId_' + data[1]).removeClass('js-hide');
						$('#variableProductToggleButtonId_' + data[1]).addClass('blue');
						$('#variableProductToggleButtonId_' + data[1]).addClass('js-show');
						$('#variableProductToggleButtonId_' + data[1]).html('<i class="fa fa-eye-slash"></i>');
						$('#variableProductToggleButtonId_' + data[1]).prop('title', 'Skryť produkt z ponuky');
					}
				} else {
					alert('Vyskytla sa chyba. Skúste to prosím znovu.');
				}
			}
		});
	});
	
	//variable products edit
	$(document).on('click', '.js-editVariableProduct', function() {	
		var variableProductId	= $(this).data('variable_product_id');
		
		$.ajax({
			url: '<?=base_url()?>Admin/Products/editVariableProduct', 
			data: {
				variableProductId : variableProductId, 
			},
			dataType: 'text', 
			type: 'post',
			success: function (data) {
				data = JSON.parse(data);
				if (data[0] == 'ok') {
					$('#variableProductId').val(data[1].id);
					$('#variableProductName').val(data[1].name);
					$('#variableProductShortDescription').val(data[1].short_description);
					$('#variableProductPrice').val(data[1].price);
					$('#variableProductDiscount').val(data[1].discount);
					$('#variableProductQuantity').val(data[1].quantity);
					$('.js-variablePhotosItems').remove();
					$('#variableProductPhotos').append(data[2]);
					$('#variableProductEditLabel').html('Upraviť variabilný produkt');
					$('#variableProductEdit').modal('show');
				} else {
					alert('Vyskytla sa chyba. Skúste to prosím znovu.');
				}
			},
			error: function (request, status, error) {
				alert(request.responseText);
			}
		});
	});
	
	//variable products delete
	$(document).on('click', '.js-deleteVariableProduct', function() {
		if (confirm('Naozaj chcete odstrániť produkt ' + $(this).data('variable_product_name') + '?')) {
			var variableProductId	= $(this).data('variable_product_id');
			
			$.ajax({
				url: '<?=base_url()?>Admin/Products/removeVariableProduct', 
				data: {
					variableProductId : variableProductId, 
				},
				dataType: 'text', 
				type: 'post',
				success: function (data) {
					data = JSON.parse(data);
					if (data[0] == 'ok') {
						$('#variableProductRowId_' + data[1]).remove();
					} else {
						alert('Vyskytla sa chyba. Skúste to prosím znovu.');
					}
				}
			});
		}
	});
	
	function priceCalc(price, vat) {
		vat = vat || 0;
		return (price * (1 + (vat / 100))).toFixed(2);
	}
	
	function vatCalc(price, vat) {
		vat = vat || 0;
		return (price * (vat / (100 + +vat))).toFixed(2);
	}
</script>


