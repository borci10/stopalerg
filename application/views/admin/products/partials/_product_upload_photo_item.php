<div class="col-xl-3 col-lg-4 col-md-4 col-sm-6 col-12 my-2 photoSortable" id="photoItem_<?=$productPhoto['id']?>">
	<div class="photoItem ui-state-default">
		<div class="photoWrap">
			<img src="<?=cuteImage($productPhoto['file_name'], 1)?>" class="img-fluid">
			<button type="button" class="own-btn-icon  red removeImageFromAlbum" data-img_id="<?=$productPhoto['id']?>" title="Odstrániť fotografiu" data-toggle="tooltip" data-placement="right"><i class="fa fa-trash"></i></button>
			<span class="own-btn-icon  green js-editImage" title="Upraviť obrázok" data-toggle="tooltip" data-placement="right"><i class="fas fa-pencil-alt"></i></span>
		</div>
		<div class="form-check">
			<input class="form-check-input" type="radio" name="productMainPhoto" id="productMainPhoto_<?=$productPhoto['id']?>" value="<?=$productPhoto['id']?>" <?=is_checked(@$productInfo->main_photo_id, $productPhoto['id'], $defaultCheck ?? 0)?>>
			<label class="form-check-label" for="productMainPhoto_<?=$productPhoto['id']?>">
				Hlavná fotografia
			</label>
		</div>
		<div class="productPhotoInfo">
			<input type="hidden" name="productPhoto[id][]" value="<?=$productPhoto['id']?>">
			<input type="text" class="form-control" name="productPhoto[title][]" placeholder="Title" value="<?=$productPhoto['title']?>">
			<input type="text" class="form-control" name="productPhoto[alt][]" placeholder="Alt" value="<?=$productPhoto['alt']?>">
		</div>
	</div>
</div>