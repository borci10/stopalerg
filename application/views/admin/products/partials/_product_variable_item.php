<tr id="variableProductRowId_<?=$variableProduct['id']?>">
	<input type="hidden" value="<?=$variableProduct['id']?>" name="productVariables[]">
	<td id="variableProductNameRowId_<?=$variableProduct['id']?>"><?=$variableProduct['name']?></td>
	<td id="variableProductPriceRowId_<?=$variableProduct['id']?>"><?=($variableProduct['price'] ?? 0) ?> <?=$this->config->item('currency')?></td>
	<td id="variableProductDiscountRowId_<?=$variableProduct['id']?>"><?=($variableProduct['discount'] ?? 0)?> %</td>
	<td id="variableProductQuantityRowId_<?=$variableProduct['id']?>"><?=($variableProduct['quantity'] ?? 0) ?></td>
	<td>
		<div class="text-md-right">
			<?php
				if ($variableProduct['show_product']) {
					?>
						<span id="variableProductToggleButtonId_<?=$variableProduct['id']?>" class="own-btn-icon  blue js-show js-toggleVariableProduct" data-variable_product_id="<?=$variableProduct['id']?>" title="Skryť produkt z ponuky"><i class="fa fa-eye-slash"></i></span>
					<?php
				} else {
					?>
						<span id="variableProductToggleButtonId_<?=$variableProduct['id']?>" class="own-btn-icon  disabled js-hide js-toggleVariableProduct" data-variable_product_id="<?=$variableProduct['id']?>" title="Zobraziť produkt v ponuke"><i class="fa fa-eye"></i></span>
					<?php
				}
			?>
			<span class="own-btn-icon  green js-editVariableProduct" data-variable_product_id="<?=$variableProduct['id']?>" title="Upraviť produkt"><i class="fas fa-pencil-alt" aria-hidden="true"></i></span>
			<span class="own-btn-icon  red js-deleteVariableProduct" data-variable_product_id="<?=$variableProduct['id']?>" data-variable_product_name="<?=$variableProduct['name']?>" title="Odstrániť produkt"><i class="fas fa-trash-alt" aria-hidden="true"></i></span>
		</div>
	</td>
</tr>