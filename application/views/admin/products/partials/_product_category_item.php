<?php
	foreach($categories as $category) {
		?>
			<div class="form-check mr-3" style="margin-left:<?=($depth * 20)?>px;">
				<label class="checkbox-container" class="form-check-label">
					<input class="form-check-input" type="checkbox" name="productCategory[]" value="<?=$category['category_id']?>" <?=(@in_array($category['category_id'], $product_categories) ? 'checked' : '')?>>
					<span class="checkmark"></span>
					<?=$category['title']?>
				</label>
			</div>
		<?php
		$this->data['depth'] 		= $depth + 1;
		$this->data['categories'] 	= $category['childrens'];
		$this->load->view('admin/products/partials/_product_category_item', $this->data);
	}
?>