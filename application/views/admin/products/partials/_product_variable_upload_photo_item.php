<div class="col-xl-3 col-lg-4 col-md-4 col-sm-6 col-12 my-2 photoSortable js-variablePhotosItems" id="variablePhotoItem_<?=$variableProductPhoto['id']?>">
	<div class="photoItem ui-state-default">
		<div class="photoWrap">
			<img src="<?=base_url() . 'uploads/product_variables/thumbs/' . $variableProductPhoto['file_name']?>" class="img-fluid">
			<button type="button" class="own-btn-icon  red removeVariableProductPhoto" data-img_id="<?=$variableProductPhoto['id']?>" title="Odstrániť fotografiu" data-toggle="tooltip" data-placement="right"><i class="fa fa-trash"></i></button>
			<span class="own-btn-icon  green js-editImage" title="Upraviť obrázok" data-toggle="tooltip" data-placement="right"><i class="fas fa-pencil-alt"></i></span>
		</div>
		<div class="productPhotoInfo">
			<input type="hidden" class="js-variableProductPhoto-id" name="variableProductPhoto[id][]" value="<?=$variableProductPhoto['id']?>">
			<input type="text" class="form-control" name="variableProductPhoto[title][]" placeholder="Title" value="<?=$variableProductPhoto['title']?>">
			<input type="text" class="form-control" name="variableProductPhoto[alt][]" placeholder="Alt" value="<?=$variableProductPhoto['alt']?>">
		</div>
	</div>
</div>