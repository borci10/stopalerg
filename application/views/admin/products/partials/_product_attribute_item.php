<div class="input-group mb-2" id="productAttrinuteRow_<?=$rowNumber?>">
	<input type="text" class="form-control js-productAttribute mr-1" value="<?=@$attribute['title']?>" name="productAttributes[title][]" placeholder="Názov vlastnosti">
	<input type="text" class="form-control" value="<?=@$attribute['value']?>" name="productAttributes[value][]" placeholder="Hodnota">
	<span class="input-group-btn ml-2">
		<button class="own-btn red js-removeAttribute" type="button" data-attribute_row_number="<?=$rowNumber?>">Zrušiť</button>
	</span>
</div>
