<div class="cardHead">
	<h2>Fotografie</h2>
</div>
<div class="cardInnerBody">
	<div id="productAdminPhotos" class="sortable row">
		<div class="col-xl-3 col-lg-4 col-md-4 col-sm-6 col-12 my-2">
			<div class="form-group" id="photoInputDiv">
				<div class="fileContainer_label">Vybrať foto<small id="fileHelp" class="form-text text-muted">Max veľkosť 10MB. <br />Podporované typy .png, .jpg a .gif.</small></div>
				<input type="file" class="form-control-file fileContainer " id="photoInput" multiple>
			</div>
		</div>
		<?php
			if (isset($productPhotos)) {
				$this->data['defaultCheck'] = 1;
				foreach($productPhotos as $this->data['productPhoto']) {
					$this->load->view('admin/products/partials/_product_upload_photo_item', $this->data);
					$this->data['defaultCheck'] = 0;
				}
			}
		?>
	</div>
</div>

<script>	
	//upload image after input change
	$(document).ready(function (e) {
		$('#photoInput').change(function () {
			var file_data = $('#photoInput').prop('files');
						
			for(let i = 0; i < file_data.length; i++) {
				var form_data = new FormData();
				form_data.append('file', file_data[i]);

				$('#productAdminPhotos').append('<div class="col-3" style="height: 300px;" id="photoPlaceholder' + i + '"><div class="fileContainer_label"><i class="fa fa-spinner fa-pulse fa-3x fa-fw" aria-hidden="true"></i></div></div>');
				$.ajax({
					url: '<?=base_url()?>admin/products/uploadPhoto', 
					dataType: 'text', 
					cache: false,
					contentType: false,
					processData: false,
					data: form_data,
					type: 'POST'
				})
				.done(function(data) {
					data = JSON.parse(data);
					if (data[0] == 'ok') {
						$('#productAdminPhotos').append(data[1]);
					} else {
						alert('Pri nahrávaní fotografie došlo k chybe! Skúste to prosím znovu.');
					}
				})
				.fail(function(jqXHR, textStatus, errorThrown) {
					alert('Pri nahrávaní fotografie došlo k chybe! Skúste to prosím znovu.');
					console.log(jqXHR);
					console.log(textStatus);
					console.log(errorThrown);
				})
				.always(function() {
					$('#photoInput').val('');
					$('#photoPlaceholder' + i).remove();
				});
			}
		});
	});
	
	//remove image
	$(document).on('click', '.removeImageFromAlbum', function(e) {
		var imgId = $(this).data('img_id');
		
		if (confirm('Naozaj chcete odstrániť fotografiu?')) {
			$.ajax({
				url: '<?=base_url()?>admin/products/removePhoto', 
				data: {imgId: imgId},
				dataType: 'text', 
				type: 'post',
				success: function (id) {	
					$('#photoItem_' + id).remove();
				},
				error: function (request, status, error) {
					alert(request.responseText);
				}
			});
		}
		e.preventDefault();
	});
	
	//make image list sortable
	$(function() {
		$( ".sortable" ).sortable({
			items: ".photoSortable"
		});
		$( ".sortable" ).disableSelection();
	});


</script>


