<div class="modal fade" id="variableProductEdit" role="dialog" aria-labelledby="variableProductEditLabel" aria-hidden="true">
	<div class="modal-dialog modal-dialog-centered" role="document">
		<form id="variableProductForm" data-toggle="validator" role="form">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title" id="variableProductEditLabel">Variabilný produkt</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div id="variableProductEditContant" class="modal-body">
					<div id="variableProductPhotos" class="sortable row">
						<div class="col-xl-3 col-lg-4 col-md-4 col-sm-6 col-12 my-2">
							<div class="form-group" id="photoInputDiv">
								<div class="fileContainer_label">Vybrať foto<small id="fileHelp" class="form-text text-muted">Max veľkosť 10MB. <br />Podporované typy .png, .jpg a .gif.</small></div>
								<input type="file" class="form-control-file fileContainer " id="variablePhotoInput" multiple>
							</div>
						</div>
					</div>
					<input type="hidden" value="" name="variableProductId" id="variableProductId">
					<div class="form-group has-feedback">
						<label for="variableProductName">Názov *</label>
						<input 
							type="text" 
							class="form-control" 
							id="variableProductName" 
							maxlength="50" 
							name="variableProductName" 
							required				
						>		
						<div class="help-block with-errors"><?php echo form_error('variableProductName'); ?></div>
					</div>
					<div class="form-group has-feedback">
						<label for="variableProductShortDescription">Stručný popis</label>
						<textarea class="form-control" id="variableProductShortDescription" name="variableProductShortDescription" rows="2"></textarea>
						<div class="help-block with-errors"><?php echo form_error('variableProductShortDescription'); ?></div>
					</div>
					<div class="row" style="margin-left:-15px; margin-right:-15px;">
						<div class="form-group has-feedback col-md-4 col-12">
							<label for="variableProductPrice">Cena</label>
							<div class="input-group">
								<input 
									type="number" 
									class="form-control" 
									id="variableProductPrice" 
									name="variableProductPrice"
									value="<?php echo set_value('variableProductPrice', is_set(@$productInfo->price, 0)); ?>"
									placeholder="0.00"
									min="0"
									step="0.01"
								>
								<span class="input-group-addon"><?=$this->config->item('currency');?></span>
							</div>
							<div class="help-block with-errors"><?php echo form_error('variableProductPrice'); ?></div>
						</div>
						<div class="form-group has-feedback col-md-4 col-12">
							<label for="variableProductDiscount">Zľava</label>
							<div class="input-group">
								<input 
									type="number" 
									class="form-control" 
									id="variableProductDiscount" 
									name="variableProductDiscount"
									value="0"
									placeholder="0"
									step="0.01"
									min="0"
									max="100"
								>
								<span class="input-group-addon">%</span>
							</div>
							<div class="help-block with-errors"><?php echo form_error('variableProductDiscount'); ?></div>
						</div>
						<div class="form-group has-feedback col-md-4 col-12">
							<label for="variableProductQuantity">Na sklade</label>
							<div class="input-group">
								<input 
									type="number" 
									class="form-control" 
									id="variableProductQuantity" 
									name="variableProductQuantity"
									value="0"
									min="0"
									placeholder="0"
								>
								<span class="input-group-addon">ks</span>
							</div>
							<div class="help-block with-errors"><?php echo form_error('variableProductQuantity'); ?></div>
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="own-btn red" data-dismiss="modal">Zrušiť</button>
					<button type="submit" class="own-btn green js-variableProductFormSubmit">Uložiť</button>
				</div>
			</div>
		</form>
	</div>
</div>

<script>	
	//upload variable image after input change
	$(document).ready(function (e) {
		$('#variablePhotoInput').change(function () {
			var file_data = $('#variablePhotoInput').prop('files');
						
			for(let file of file_data) {
				var form_data = new FormData();
				form_data.append('file', file);
			
				$('.fileContainer_label').html('<i class="fa fa-spinner fa-pulse fa-3x fa-fw" aria-hidden="true"></i>');			
				$.ajax({
					url: '<?=base_url()?>Admin/Products/uploadVariablePhoto', 
					dataType: 'text', 
					cache: false,
					contentType: false,
					processData: false,
					data: form_data,
					type: 'post',
					success: function (data) {
						data = JSON.parse(data);
						if (data[0] == 'ok') {
							$('#variableProductPhotos').append(data[1]);
							$('#variablePhotoInput').val('');
							$('.fileContainer_label').html('Vybrať foto<small id="fileHelp" class="form-text text-muted">Max veľkosť 10MB. <br />Podporované typy .png, .jpg a .gif.</small>');
						} else {
							$('.fileContainer_label').html('Vybrať foto<small id="fileHelp" class="form-text text-muted">Max veľkosť 10MB. <br />Podporované typy .png, .jpg a .gif.</small>');
							alert('Nastala chyba s nahrávaním súboru. Max veľkosť 10MB. Podporované typy .png, .jpg a .gif.');
						}
					},
				});
			}
		});
	});
	
	//remove variable image
	$(document).on('click', '.removeVariableProductPhoto', function(e) {
		var img_id = $(this).data('img_id');
		
		if (confirm('Naozaj chcete odstrániť fotografiu?')) {
			$.ajax({
				url: '<?=base_url()?>admin/products/removeVariablePhoto', 
				data: {id: img_id},
				dataType: 'text', 
				type: 'post',
				success: function (data) {	
					$('#variablePhotoItem_' + data).remove();
				}
			});
		}
		e.preventDefault();
	});
</script>