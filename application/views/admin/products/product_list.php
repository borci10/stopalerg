<div class="card-content">
	<div class="table-responsive">
		<table class="table mb-0">
			<thead class="thead-fancy">
				<tr>
					<th>Obrázok</th>
					<th>Názov produktu</th>
					<th>Cena bez DPH</th>
					<th>Cena s DPH</th>
					<th>Zľava</th>
					<th>Množstvo</th>
					<th>Dátum pridania</th>
					<th></th>
				</tr>
			</thead>
			<tbody class="tbody-fancy" id="contentBody">
				<?php
					$this->load->view('admin/products/product_list_items', $this->data);
				?>
			</tbody>
		</table>
	</div>
</div>

<script>
	$(document).on('click', '.js-checkAll', function() {
		$('.js-massActionCheckbox').not(this).prop('checked', this.checked);
	});
	
	$(document).on('click', '.js-confirmMassAction', function() {
		var massAction 		= $('#productMassAction').val();
		var massActionText 	= $('#productMassAction option:selected').text();
		var productIds 		= [];
		
		$('.js-massActionCheckbox:checked').each(function() {
			productIds.push($(this).val());
		});
		
		if (confirm('Naozaj chcete vykonať hromadnú akciu "' + massActionText + '" ?')) {
			$.ajax({
				url: '<?=base_url()?>admin/products/massAction', 
				data: {
					productIdsArr : productIds,
					massAction : massAction,
				},
				dataType: 'text', 
				type: 'post',
				success: function (data) {
					location.reload();
				}
			});
		}	
	});
</script>