<div class="card-content">
	<p><strong>Zdroj 1</strong></p>
	<form class="js-import-form" data-form_num="1" method="post" action="">

		<div class="form-group has-feedback col-12 px-0">
			<input class="form-control mb-2" name="xmlUrl" type="text" placeholder="Url">
		</div>
		<button type="submit" value="Import" class="own-btn  js-import-form-btn-1" disabled>Importovať z XML</button>
	</form>
</div>
<div class="card-content">
	<p><strong>Zdroj 1</strong></p>
	<form class="js-import-form" data-form_num="1" method="post" action="">

		<div class="form-group has-feedback col-12 px-0">
			<input class="form-control mb-2" name="xmlUrl" type="text" placeholder="Url">
		</div>
		<button type="submit" value="Import" class="own-btn  js-import-form-btn-2" disabled>Importovať z XML</button>
	</form>
</div>
<div class="card-content">
	<p><strong>CSV</strong></p>
	<form method="post" action="<?=base_url()?>import/parseData" enctype="multipart/form-data">
		<input name="file" type="file" class="mb-2"><br />
		<button type="submit" value="Import" class="own-btn  js-import-form-btn-3" disabled>Importovať z CSV</button>
	</form>
</div>

<script>
	$(document).on('submit', '.js-import-form', function() {
		$('.js-import-form-btn-' + $(this).data('form_num')).html('<i class="fa fa-spinner fa-pulse fa-3x fa-fw" aria-hidden="true" style="font-size:13px;"></i> Importuje sa. Import môže trvať aj 10 minút.');
		$('.js-import-form-btn-1').attr("disabled", true);
		$('.js-import-form-btn-2').attr("disabled", true);
		$('.js-import-form-btn-3').attr("disabled", true);
	});
</script>