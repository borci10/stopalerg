<section class="flex-wrapper">
	<div id="login">
		<div class="card">
			<div class="card-header">
				<h1>Zabudnuté heslo</h1>
			</div>
			<div class="card-body">
				<form method="post" role="form">
					<div class="form-group has-feedback">
						<input type="email" name="email" class="form-control" placeholder="Zadajte email" required>
					</div>
					<input type="submit" class="own-btn green full-width mt-3" value="Zaslať nové heslo" name="formConfirm">
				</form>
				<div class="text-center mt-4">
					<a href="<?=base_url()?>login" class="lost-password">Späť na prihlásenie</a>
				</div>
			</div>
		</div>
	</div>
</section>