<section class="flex-wrapper">
	<div id="login">
		<div class="card xl">
			<div class="card-header">
				<h1>Registrácia</h1>
			</div>
			<div class="card-body">
				<form id="regForm" method="post" data-toggle="validator" role="form">
					<div class="row">
						<div class="col-12  form-group has-feedback">
							<label for="regName" class="sr-only">Meno/Názov firmy *</label>
							<input
								type="text"
								class="form-control"
								id="regName"
								maxlength="255"
								name="regName"
								placeholder="Meno/Názov firmy *"
								value="<?php echo set_value('regName'); ?>"
								required
							>
							<div class="help-block with-errors"><?php echo form_error('regName'); ?></div>
						</div>
						<div class="col-md-6 col-12  form-group has-feedback">
							<label for="regEmail" class="sr-only">Emailová adresa *</label>
							<input
								type="email"
								class="form-control"
								id="regEmail"
								name="regEmail"
								placeholder="Emailová adresa *"
								value="<?php echo set_value('regEmail'); ?>"
								required
							>
							<div class="help-block with-errors"><?php echo form_error('regEmail'); ?></div>
						</div>
						<div class="col-md-6 col-12  form-group has-feedback">
							<label for="regPhone" class="sr-only">Telefónne číslo *</label>
							<input
									type="text"
									class="form-control"
									id="regPhone"
									name="regPhone"
									placeholder="Telefónne číslo *"
									value="<?php echo set_value('regPhone'); ?>"
									required
							>
							<div class="help-block with-errors"><?php echo form_error('regPhone'); ?></div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-4 col-12  form-group has-feedback">
							<label for="regCity" class="sr-only">Mesto *</label>
							<input
									type="text"
									class="form-control"
									id="regCity"
									name="regCity"
									placeholder="Mesto *"
									value="<?php echo set_value('regCity'); ?>"
									required
							>
							<div class="help-block with-errors"><?php echo form_error('regCity'); ?></div>
						</div>
						<div class="col-md-4 col-12  form-group has-feedback">
							<label for="regStreet" class="sr-only">Ulica *</label>
							<input
								type="text"
								class="form-control"
								id="regStreet"
								name="regStreet"
								placeholder="Ulica *"
								value="<?php echo set_value('regStreet'); ?>"
								required
							>
							<div class="help-block with-errors"><?php echo form_error('regStreet'); ?></div>
						</div>
						<div class="col-md-4 col-12  form-group has-feedback">
							<label for="regZipCode" class="sr-only">PSČ *</label>
							<input
								type="text"
								class="form-control"
								id="regZipCode"
								name="regZipCode"
								placeholder="PSČ *"
								value="<?php echo set_value('regZipCode'); ?>"
								pattern="^\d{3} ?\d{2}$"
								required
							>
							<div class="help-block with-errors"><?php echo form_error('regZipCode'); ?></div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-6 col-12  form-group has-feedback">
							<label for="regPassword" class="sr-only">Heslo *</label>
							<input
								type="password"
								class="form-control"
								id="regPassword"
								name="regPassword"
								placeholder="Heslo *"
								data-minlength="8"
								required
							>
							<div class="help-block with-errors"><?php echo form_error('regPassword'); ?></div>
						</div>
						<div class="col-md-6 col-12  form-group has-feedback">
							<label for="regPassword2" class="sr-only">Zopakujte heslo *</label>
							<input
								type="password"
								class="form-control"
								id="regPassword2"
								name="regPassword2"
								placeholder="Zopakujte heslo *"
								data-match="#regPassword"
								data-minlength="8"
								required
							>
							<div class="help-block with-errors"><?php echo form_error('regPassword2'); ?></div>
						</div>
					</div>
					<div class="row pt-4">
						<div class="col-12 form-check mb-1">
							<label class="checkbox">
								<input type="checkbox" name="oou" class="form-check-input" required>
								<span class="checkbox-label">Oboznámil som sa s <a href="<?=base_url()?>ochrana-osobnych-udajov" class="link" target="_blank">ochranou osobných údajov.</a></span>
							</label>
						</div>
						<div class="col-12 form-check">
							<label class="checkbox">
								<input type="checkbox" name="op" class="form-check-input" required>
								<span class="checkbox-label">Oboznámil som sa s <a href="<?=base_url()?>obchodne-podmienky" class="link" target="_blank">obchodnými podmienkami.</a></span>
							</label>
						</div>
					</div>
					<div class="text-center">
						<input type="submit" class="own-btn green mt-3" value="Registrovať" name="regConfirm" id="regConfirm">
					</div>
				</form>
				<div class="text-center mt-4">
					<a href="<?=base_url()?>login" class="lost-password">Späť na prihlásenie</a>
				</div>
			</div>
		</div>
	</div>
</section>

