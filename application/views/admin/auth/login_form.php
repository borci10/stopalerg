<section class="flex-wrapper">
	<div id="login">
		<div class="card">
			<div class="card-header">
				<h1>Prihlásenie</h1>
			</div>
			<div class="card-body">
				<form method="post" role="form">
					<div class="form-group has-feedback">
						<div class="input-group">
							<input
									type="email"
									class="form-control"
									id="loginEmail"
									name="loginEmail"
									aria-describedby="inputGroupPrependEmail"
									placeholder="Email"
									value="<?php echo set_value('loginEmail'); ?>"
									required
							>
						</div>
						<div class="help-block with-errors"><?php echo form_error('loginEmail'); ?></div>
					</div>
					<div class="form-group has-feedback mb-0">
						<div class="input-group">
							<input
									type="password"
									class="form-control"
									id="loginPassword"
									name="loginPassword"
									aria-describedby="inputGroupPrependPass"
									placeholder="Heslo"
									required
							>
						</div>
						<div class="help-block with-errors"><?php echo form_error('loginPassword'); ?></div>
					</div>
					<div class="text-right mt-3">
						<a href="<?=base_url()?>registration" class="lost-password"><i class="fas fa-plus mr-1"></i>Registrácia</a>
					</div>
					<div class="text-right mt-1">
						<a href="<?=base_url()?>lost-password" class="lost-password"><i class="fas fa-redo mr-1"></i>Zabudnuté heslo</a>
					</div>
					<input type="submit" class="own-btn green full-width mt-3" value="Prihlásiť" name="loginConfirm">
				</form>
				<div class="text-center mt-4">
					<a href="<?=base_url()?>" class="lost-password">Späť na úvod</a>
				</div>
			</div>
		</div>
	</div>
</section>