<div class="card-content">
	<div class="table-responsive">
		<table class="table mb-0">
			<thead class="thead-fancy">
				<tr>
					<th>Spôsob dopravy</th>
					<th>Cena</th>
					<th></th>
				</tr>
			</thead>
			<tbody class="tbody-fancy">
				<?php
					foreach($delivery as $deliveyOption) {
						?>
							<tr>
								<td>
									<span class="responsive-desc">Spôsob dopravy</span>
									<?=$deliveyOption['title']?>
								</td>
								<td>
									<span class="responsive-desc">Cena</span>
									<?=cutePrice($deliveyOption['price'], $this->config->item('currency'))?>
								</td>
								<td>
									<div class="text-lg-right">
										<a href="<?=base_url()?>admin/delivery/edit/<?=$deliveyOption['id']?>" class="own-btn-icon  green" title="Upraviť spôsob dopravy" data-toggle="tooltip" data-placement="top"><i class="fas fa-pencil-alt" aria-hidden="true"></i></a>
										<a href="<?=base_url()?>admin/delivery/remove/<?=$deliveyOption['id']?>" class="own-btn-icon  red" title="Odstrániť spôsob dopravy" data-toggle="tooltip" data-placement="top" onclick="return confirm('Naozaj chcete odstrániť spôsob dopravy <?=$deliveyOption['title']?>?')"><i class="fas fa-trash-alt" aria-hidden="true"></i></a>
									</div>
								</td>
							</tr>
						<?php
					}
				?>
			</tbody>
		</table>
	</div>
</div>