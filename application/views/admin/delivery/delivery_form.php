<div class="card-content">
	<form method="post" data-toggle="validator" role="form">
		<div class="row">
			<div class="form-group has-feedback col-md-6 col-12">
				<label for="deliveryTitle">Spôsob dopravy *</label>
				<input
					type="text"
					class="form-control"
					id="deliveryTitle"
					maxlength="50"
					name="deliveryTitle"
					value="<?php echo set_value('deliveryTitle', is_set(@$delivery->title)); ?>"
					required
				>
				<div class="help-block with-errors"><?php echo form_error('deliveryTitle'); ?></div>
			</div>
			<div class="form-group has-feedback col-md-6 col-12">
				<label for="deliveryPrice">Cena *</label>
				<div class="input-group">
					<input
						type="number"
						class="form-control"
						id="deliveryPrice"
						name="deliveryPrice"
						value="<?php echo set_value('deliveryPrice', is_set(@$delivery->price)); ?>"
						placeholder="0.00"
						min="0"
						step="0.01"
					>
					<span class="input-group-addon"><?=$this->config->item('currency');?></span>
				</div>
				<div class="help-block with-errors"><?php echo form_error('deliveryPrice'); ?></div>
			</div>

		</div>
		<div class="mt-1">
			<button type="submit" class="own-btn green" value="formConfirm" name="formConfirm" id="formConfirm">Uložiť</button>
			<a href="<?=base_url()?>admin/delivery" class="own-btn red">Zrušiť</a>
		</div>
	</form>
</div>


