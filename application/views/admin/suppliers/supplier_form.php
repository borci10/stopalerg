<div class="card-content">
	<form method="post" data-toggle="validator" role="form">
		<div class="row">
			<div class="form-group has-feedback col-md-6 col-12">
				<label for="supplierTitle">Názov dodávateľa *</label>
				<input
					type="text"
					class="form-control"
					id="supplierTitle"
					maxlength="50"
					name="supplierTitle"
					value="<?php echo set_value('supplierTitle', is_set(@$supplier->title)); ?>"
					required
				>
				<div class="help-block with-errors"><?php echo form_error('supplierTitle'); ?></div>
			</div>
			<div class="form-group has-feedback col-md-6 col-12">
				<label for="supplierPrice">Cena *</label>
				<div class="input-group">
					<input
						type="number"
						class="form-control"
						id="supplierPrice"
						name="supplierPrice"
						value="<?php echo set_value('supplierPrice', is_set(@$supplier->price)); ?>"
						placeholder="0.00"
						min="0"
						step="0.01"
					>
					<span class="input-group-addon"><?=$this->config->item('currency');?></span>
				</div>
				<div class="help-block with-errors"><?php echo form_error('supplierPrice'); ?></div>
			</div>

		</div>
		<div class="mt-1">
			<button type="submit" class="own-btn green" value="formConfirm" name="formConfirm" id="formConfirm">Uložiť</button>
			<a href="<?=base_url()?>admin/suppliers" class="own-btn red">Zrušiť</a>
		</div>
	</form>
</div>


