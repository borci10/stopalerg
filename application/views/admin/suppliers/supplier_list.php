<div class="card-content">
	<form id="deliveryOptionsForm" class="form-inline" method="post" action="<?=base_url()?>admin/suppliers" data-toggle="validator" role="form">
		<div class="input-group mr-3">
			<input
					type="text"
					class="form-control"
					id="supplierTitle"
					maxlength="50"
					name="supplierTitle"
					placeholder="Názov dodávateľa"
					value="<?php echo set_value('supplierTitle'); ?>"
					required
			>
		</div>
		<div class="input-group mr-3">
			<input
					type="number"
					class="form-control"
					id="supplierPrice"
					maxlength="30"
					name="supplierPrice"
					value="<?php echo set_value('supplierPrice'); ?>"
					placeholder="0.00"
					min="0"
					step="0.01"
					required
			>
			<span class="input-group-addon"><?=$this->config->item('currency')?></span>
		</div>
		<div class="submitWrap">
			<button type="submit" class="own-btn green" value="formConfirm" name="formConfirm" id="formConfirm">Pridať nového dodávateľa</button>
		</div>
	</form>
</div>
<div class="card-content">
	<div class="table-responsive">
		<table class="table mb-0">
			<thead class="thead-fancy">
				<tr>
					<th>Dodávateľ</th>
					<th>Cena</th>
					<th></th>
				</tr>
			</thead>
			<tbody class="tbody-fancy">
				<?php
					foreach($suppliers as $supplier) {
						?>
							<tr>
								<td>
									<span class="responsive-desc">Dodávateľ</span>
									<?=$supplier['title']?>
								</td>
								<td>
									<span class="responsive-desc">Cena</span>
									<?=cutePrice($supplier['price'])?>
								</td>
								<td>
									<div class="text-lg-right">
										<a href="<?=base_url()?>admin/suppliers/edit/<?=$supplier['id']?>" class="own-btn-icon  green" title="Upraviť dodávateľa" data-toggle="tooltip" data-placement="top"><i class="fas fa-pencil-alt" aria-hidden="true"></i></a>
										<a href="<?=base_url()?>admin/suppliers/remove/<?=$supplier['id']?>" class="own-btn-icon  red" title="Odstrániť dodávateľa" data-toggle="tooltip" data-placement="top" onclick="return confirm('Naozaj chcete odstrániť dodávateľa <?=$supplier['title']?>?')"><i class="fas fa-trash-alt" aria-hidden="true"></i></a>
									</div>
								</td>
							</tr>
						<?php
					}
				?>
			</tbody>
		</table>
	</div>
</div>