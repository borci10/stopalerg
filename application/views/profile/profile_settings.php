<div class="cart container">
	<div class="white-bg-box">
		<div class="productItemHeader">
			<h3>Osobné údaje</h3>
		</div>
		<div class="p-3">
			<form id="personalInfo" class="cart-form" method="post" data-toggle="validator" role="form">
				<div class="row">
					<div class="col-md-6 form-group has-feedback">
						<label for="changeName">Meno/Názov firmy *</label>
						<input
							type="text"
							class="form-control"
							id="changeName"
							maxlength="255"
							name="changeName"
							value="<?php echo (set_value('changeName') ? set_value('changeName') : $this->session->userdata('name')); ?>"
							required
						>
						<div class="help-block with-errors"><?php echo form_error('changeName'); ?></div>
					</div>
					<div class="col-md-6 form-group has-feedback">
						<label for="changeBirthDate">Dátum narodenia *</label>
						<input
							type="date"
							class="form-control"
							id="changeBirthDate"
							maxlength="20"
							name="changeBirthDate"
							value="<?=set_value('changeBirthDate', $this->session->userdata('birth_date'));?>"
							required
						>
						<div class="help-block with-errors"><?php echo form_error('changeBirthDate'); ?></div>
					</div>
					<div class="col-md-6 form-group has-feedback">
						<label for="changeEmail">Emailová adresa *</label>
						<input
							type="email"
							class="form-control"
							id="changeEmail"
							name="changeEmail"
							value="<?php echo (set_value('changeEmail') ? set_value('changeEmail') : $this->session->userdata('email')); ?>"
							required
						>
						<div class="help-block with-errors"><?php echo form_error('changeEmail'); ?></div>
					</div>
					<div class="col-md-6 form-group has-feedback">
						<label for="changePhone">Telefónne číslo *</label>
						<input
								type="text"
								class="form-control"
								id="changePhone"
								name="changePhone"
								value="<?php echo (set_value('changePhone') ? set_value('changePhone') : $this->session->userdata('phone_number')); ?>"
								required
						>
						<div class="help-block with-errors"><?php echo form_error('changePhone'); ?></div>
					</div>
					<div class="col-md-4 form-group has-feedback">
						<label for="changeStreet">Ulica *</label>
						<input
							type="text"
							class="form-control"
							id="changeStreet"
							name="changeStreet"
							value="<?php echo (set_value('changeStreet') ? set_value('changeStreet') : $this->session->userdata('street')); ?>"
							required
						>
						<div class="help-block with-errors"><?php echo form_error('changeStreet'); ?></div>
					</div>
					<div class="col-md-4 form-group has-feedback">
						<label for="changeCity">Mesto *</label>
						<input
							type="text"
							class="form-control"
							id="changeCity"
							name="changeCity"
							value="<?php echo (set_value('changeCity') ? set_value('changeCity') : $this->session->userdata('city')); ?>"
							required
						>
						<div class="help-block with-errors"><?php echo form_error('changeCity'); ?></div>
					</div>
					<div class="col-md-4 form-group has-feedback">
						<label for="changeZipCode">PSČ *</label>
						<input
							type="text"
							class="form-control"
							id="changeZipCode"
							name="changeZipCode"
							value="<?php echo (set_value('changeZipCode') ? set_value('changeZipCode') : $this->session->userdata('zip_code')); ?>"
							pattern="^\d{3} ?\d{2}$"
							required
						>
						<div class="help-block with-errors"><?php echo form_error('changeZipCode'); ?></div>
					</div>
				</div>
				<div class="text-center mt-4">
					<p class="font-weight-bold">Dodacie údaje (v prípade, že sa od osobných líšia)</p>
				</div>
				<div class="row">
					<div class="col-md-4 form-group has-feedback">
						<label for="changeDeliveryStreet">Ulica</label>
						<input
							type="text"
							class="form-control"
							id="changeDeliveryStreet"
							name="changeDeliveryStreet"
							value="<?=set_value('changeDeliveryStreet', $this->session->userdata('delivery_street'));?>"
						>
						<div class="help-block with-errors"><?php echo form_error('changeDeliveryStreet'); ?></div>
					</div>
					<div class="col-md-4 form-group has-feedback">
						<label for="changeDeliveryCity">Mesto</label>
						<input
							type="text"
							class="form-control"
							id="changeDeliveryCity"
							name="changeDeliveryCity"
							value="<?=set_value('changeDeliveryCity', $this->session->userdata('delivery_city'));?>"
						>
						<div class="help-block with-errors"><?php echo form_error('changeDeliveryCity'); ?></div>
					</div>
					<div class="col-md-4 form-group has-feedback">
						<label for="changeDeliveryZipCode">PSČ</label>
						<input
							type="text"
							class="form-control"
							id="changeDeliveryZipCode"
							name="changeDeliveryZipCode"
							value="<?=set_value('changeDeliveryZipCode', $this->session->userdata('delivery_zip_code'));?>"
							pattern="^\d{3} ?\d{2}$"
						>
						<div class="help-block with-errors"><?php echo form_error('changeDeliveryZipCode'); ?></div>
					</div>
				</div>
				<div class="col-12 text-center mt-3">
					<input type="submit" class="own-btn" value="Zmeniť údaje" name="changeUserDataConfirm" id="changeUserDataConfirm">
				</div>
			</form>
		</div>
	</div>
</div>
<div class="cart container mt-4">
	<div class="white-bg-box">
		<div class="productItemHeader">
			<h3>Zmena hesla</h3>
		</div>
		<div class="p-3">
			<form id="PassChange" class="cart-form" method="post" data-toggle="validator" role="form">
				<div class="row">
					<div class="col-md-4 form-group has-feedback">
						<label for="changePasswordOld">Staré heslo *</label>
						<input
							type="password"
							class="form-control"
							id="changePasswordOld"
							name="changePasswordOld"
							required
						>
						<div class="help-block with-errors"><?php echo form_error('changePasswordOld'); ?></div>
					</div>
					<div class="col-md-4 form-group has-feedback">
						<label for="changePasswordNew1">Nové heslo *</label>
						<input
							type="password"
							class="form-control"
							id="changePasswordNew1"
							name="changePasswordNew1"
							data-minlength="8"
							required
						>
						<div class="help-block with-errors"><?php echo form_error('changePasswordNew1'); ?></div>
					</div>
					<div class="col-md-4 form-group has-feedback">
						<label for="changePasswordNew2">Zopakujte nové heslo *</label>
						<input
							type="password"
							class="form-control"
							id="changePasswordNew2"
							name="changePasswordNew2"
							data-match="#changePasswordNew1"
							data-minlength="8"
							required
						>
						<div class="help-block with-errors"><?php echo form_error('changePasswordNew2'); ?></div>
					</div>
					<div class="col-12 text-center mt-3">
						<input type="submit" class="own-btn" value="Zmeniť heslo" name="changePasswordConfirm" id="changePasswordConfirm">
					</div>
				</div>
			</form>
		</div>
	</div>
</div>


