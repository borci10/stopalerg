<div class="container">
	<div class="sideMenu">
		<ul class="list-unstyled d-flex flex-md-row flex-column align-items-center justify-content-center">
			<li class="m-1 <?=(($this->uri->segment(2) == 'orders') ? 'categoryActive' : '')?>"><a href="<?=base_url()?>profile/orders" class="own-btn transparent">Moje nákupy</a></li>
			<li class="m-1 <?=(($this->uri->segment(2) == 'settings') ? 'categoryActive' : '')?>"><a href="<?=base_url()?>profile/settings" class="own-btn transparent">Nastavenia profilu</a></li>
			<li class="m-1"><a href="<?=base_url()?>logout" class="own-btn transparent">Odhlásiť</a></li>
		</ul>
	</div>
</div>
