<div class="cart container">
	<div class="white-bg-box">
		<h1 class="h4 text-lg-left text-center"><strong>STAV OBJEDNÁVKY:</strong> <span><?=Order_model::$statuses[$order_info->status]['name']?></span></h1>
		<table class="table mt-3">
			<thead class="thead-fancy thead-fancy-border-top">
				<tr>
					<th>Produkt</th>
					<th>Cena za kus</th>
					<th>Počet kusov</th>
					<th>Cena</th>
				</tr>
			</thead>
			<tbody class="tbody-fancy">
				<?php
					$totalOrderPrice = $order_info->delivery_price + $order_info->payment_price;
					foreach($order_items as $item) {
						?>
							<tr>
								<td class="font-weight-bold">
									<?php
										if ($item['id_product_child'] == 0) {
											?>
												<a href="<?=base_url()?>products/<?=$item['link']?>" class="link" target="_blank"><?=$item['name']?></a>
											<?php
										} else {
											?>
												<a href="<?=base_url()?>products/<?=$item['link']?>/<?=$item['product_variable_link']?>" class="link" target="_blank"><?=$item['name']?></a>
											<?php
										}
									?>
								</td>
								<td><?=cutePrice(priceCalc($item['product_price'], 1, $item['discount'], $item['tax'] / 100))?></td>
								<td><?=$item['quantity']?>x</td>
								<td><?=cutePrice(priceCalc($item['product_price'], $item['quantity'], $item['discount'], $item['tax'] / 100))?></td>
						<?php
						$totalOrderPrice += priceCalc($item['product_price'], $item['quantity'], $item['discount'], $item['tax'] / 100);
					}
				?>
				<tr>
					<td class="font-weight-bold"><?=$order_info->payment?></td>
					<td class="d-lg-table-cell d-none"></td>
					<td class="d-lg-table-cell d-none"></td>
					<td><?=cutePrice($order_info->payment_price)?></td>
				</tr>
				<tr>
					<td class="font-weight-bold"><?=$order_info->delivery?></td>
					<td class="d-lg-table-cell d-none"></td>
					<td class="d-lg-table-cell d-none"></td>
					<td><?=cutePrice($order_info->delivery_price)?></td>
				</tr>
			</tbody>
			<tfoot class="tfoot-fancy">
				<tr>
					<td class="d-lg-table-cell d-none"></td>
					<td class="d-lg-table-cell d-none"></td>
					<td><b>Celková cena</b></td>
					<td><b><?=cutePrice($totalOrderPrice)?></b></td>
				</tr>
			</tfoot>
		</table>
	</div>
</div>