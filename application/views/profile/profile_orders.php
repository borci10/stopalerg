<div class="cart container">
	<div class="white-bg-box">
		<table class="table">
			<thead class="thead-fancy">
				<tr>
					<th>Číslo objednávky</th>
					<th class="xs-none">Dátum objednávky</th>
					<th>Cena</th>
					<th>Stav</th>
				</tr>
			</thead>
			<tbody class="tbody-fancy">
				<?php
					foreach($orders as $order) {
						?>
							<tr>
								<td>
									<a href="<?=base_url()?>profile/orders/detail/<?=$order['id']?>">#<?=generateVarSymbol($order['id'])?></a>
								</td>
								<td class="xs-none"><?=cuteDateTime($order['order_date'])?></td>
								<td><?=cutePrice($order['order_price'] + $order['delivery_price'] + $order['payment_price'])?></td>
								<td><span class="ownBadge <?=Order_model::$statuses[$order['status']]['class']?>"><?=Order_model::$statuses[$order['status']]['name']?></span></td>
							</tr>
						<?php
					}
				?>
			</tbody>
		</table>
	</div>
</div>