		</div>
	</div>
</main>
<footer>
	<div class="container">
		<div class="footer-top">
			<div class="row">
				<div class="col-12 text-center">
					<h2 class="h1">Kde nájdete <span>StopALERG?</span></h2>
				</div>
			</div>
		</div>
		<div class="footer-middle">
			<div class="row mx-0">
				<div class="col-lg-3 col-sm-6 col-12 text-sm-left text-center">
					<a href="https://www.pilulka.sk/stopalerg-zuvacie-tablety-1x60-ks" target="_blank" rel="noreferrer noopener">
						<img src="<?=base_url()?>images/pilulka.png" class="img-fluid" alt="Pilulka">
						<h4 class="my-3">Pilulka.sk</h4>
						<p class="mb-0">Online lekáreň</p>
					</a>
				</div>
				<div class="col-lg-3 col-sm-6 col-12 text-sm-left text-center">
					<a href="https://www.facebook.com/Lek%C3%A1re%C5%88-Santal-262264157198322/" target="_blank" rel="noreferrer noopener">
						<img src="<?=base_url()?>images/santal.png" class="img-fluid" alt="Santal">
						<h4 class="my-3">Lekáreň Santal</h4>
						<p class="mb-0">Dobrovičova 10, 811 09 Bratislava</p>
					</a>
				</div>
				<div class="col-lg-3 col-sm-6 col-12 text-sm-left text-center">
					<a href="https://www.facebook.com/pg/biovcielka/about/?ref=page_internal" target="_blank" rel="noreferrer noopener">
						<img src="<?=base_url()?>images/bio-vcielka.jpg" class="img-fluid" alt="Bio-Vcielka">
						<h4 class="my-3">Predajňa Bio - Včielka</h4>
						<p class="mb-0">OC Galéria - Bratislavská 5b, 949 01 Nitra</p>
					</a>
				</div>
				<div class="col-lg-3 col-sm-6 col-12 text-sm-left text-center">
					<a href="https://www.facebook.com/ANOEL-zdrav%C3%A1-v%C3%BD%C5%BEiva-318156035231095/" target="_blank" rel="noreferrer noopener">
						<img src="<?=base_url()?>images/anoel.jpg" class="img-fluid" alt="ANOEL">
						<h4 class="my-3">ANOEL</h4>
						<p class="mb-0">Predajna zdravej výživy Komárno</p>
					</a>
				</div>
			</div>
		</div>
		<div class="footer-bottom">
			<div class="container-inner">
				<div class="row">
					<div class="col-md-5">
						<div class="contact-info-box d-flex justify-content-md-between justify-content-center flex-wrap">
							<ul id="footer-menu">
								<li>
									<a href="<?=base_url()?>obchodne-podmienky">Obchodné podmienky</a>
								</li>
								<li>
									<a href="<?=base_url()?>ochrana-osobnych-udajov">Ochrana osobných údajov</a>
								</li>
								<li>
									<a href="<?=base_url()?>ochrana-osobnych-udajov">Pravidlá cookies</a>
								</li>
							</ul>
						</div>
					</div>
					<div class="col-md-2 my-md-0 my-4">
						<div class="logo">
							<?php $this->load->view('inc/logo');?>
						</div>
						<div class="social-icons">
							<div class="icon-holder icon-xs">
								<a href="https://www.facebook.com/Stopalerg/" target="_blank" rel="noreferrer noopener">
									<?php include 'images/icons/facebook.svg';?>
								</a>
							</div>
							<div class="icon-holder icon-xs">
								<a href="https://www.youtube.com/channel/UCkXujuYEWWtZY4xBOdKMRpA" target="_blank" rel="noreferrer noopener">
									<?php include 'images/icons/youtube.svg';?>
								</a>
							</div>
							<div class="icon-holder icon-xs">
								<a href="https://www.instagram.com/stopalerg/" target="_blank" rel="noreferrer noopener">
									<?php include 'images/icons/instagram-solid.svg';?>
								</a>
							</div>
						</div>
					</div>
					<div class="col-md-5">
						<div class="contact-info-box d-flex justify-content-between flex-md-row flex-column align-items-center flex-wrap">
							<div class="icon-holder icon-xxs">
								<a href="tel:+421903576402">
									<?php include 'images/icons/old-phone.svg';?>
									<span>+421 903 576 402</span>
								</a>
							</div>
							<div class="icon-holder icon-xxs">
								<a href="mailto:shop@stopalerg.sk">
									<?php include 'images/icons/at.svg';?>
									<span>shop<!-- >@. -->@<!-- >@. -->stopalerg<!-- >@. -->.<!-- >@. -->sk</span>
								</a>
							</div>
							<div class="icon-holder icon-xxs">
								<a href="https://goo.gl/maps/uvYoVJK5bRfYJ8ZM8" rel="noreferrer noopener" target="_blank">
									<?php include 'images/icons/pin.svg';?>
									<span>Vlárska 66, 831 01 Bratislava</span>
								</a>
							</div>
						</div>
					</div>
					<div class="col-12 pt-4 text-center">
						<p class="mb-0">2019 &copy; <?=$this->config->item('owner')['company']?> | <a href="https://www.onlima.sk/tvorba-eshopu" target="_blank" title="Onlima.sk" rel="noreferrer noopener">Onlima.sk</a></p>
					</div>
				</div>
			</div>
		</div>
	</div>
</footer>
<?php
	foreach($products ?? [] as $product) {
		?>
		<div id="add-to-cart-fixed-btn">
			<div class="add-to-cart-holder">
				<div role="group" class="input-group">
					<div class="input-group-prepend">
						<button type="button" class="btn js-subCountItem">-</button>
					</div>
					<input type="number" class="form-control js-countItem" min="1" max="<?=$product['quantity']?>" value="1" id="productQuantity<?=$product['id']?>">
					<div class="input-group-append">
						<button type="button" class="btn js-addCountItem">+</button>
					</div>
				</div>
				<button class="own-btn" id="addItemToShoppingCart" data-product_id="<?=$product['id']?>" <?=($product['quantity'] == 0 ? 'disabled' : '')?> title="Pridať produkt do košíka">
					<span class="icon-holder">
						<?php include 'images/icons/add-to-cart.svg'; ?>
					</span>
					<span class="own-btn-text">
						Pridať do košíka
					</span>
				</button>
			</div>
		</div>
		<?php
	}
?>
<div id="up">
	<?php include 'images/icons/arrow-up.svg';?>
</div>
<?php $this->load->view('inc/js');?>
<?php $this->load->view('inc/scripts');?>
<?php $this->load->view('inc/cookies');?>
<?php $this->load->view('inc/alerts');?>
<?php $this->load->view('inc/texts');?>
<?php $this->load->view('product/modals/add_cart_modal.php');?>
