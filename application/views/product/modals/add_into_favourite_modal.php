<div class="modal fade" id="modalAddItemToFavourite" style="margin-top:80px;" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog" role="document" style="width:90% !important; max-width:980px;">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalLabel">Produkt bol pridaný k obľúbeným</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div id="modalAddItemToFavouriteContent" class="modal-body">
				<div class="row">
					<div class="col-4">
						<img src="" class="img-fluid modalFavouriteImage">
					</div>
					<div class="col-8">
						<h2><span class="modalFavouriteName"></span></h2>
						<p><span class="modalFavouritePrice"></span></p>
						<p><span class="modalFavouriteQuantity"></span></p>
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-primary" data-dismiss="modal"><i class="fa fa-arrow-left"></i> Späť na zoznam</button>
				<a href="<?=base_url()?>/products/favourite" class="btn btn-danger"><i class="fa fa-heart" aria-hidden="true"></i> Zoznam obľúbených prodktuov</a>
			</div>
		</div>
	</div>
</div>