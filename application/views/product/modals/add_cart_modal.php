<div class="modal fade" id="modalAddItemToCart" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-lg modal-dialog-centered" role="document">
		<div class="modal-content">
			<div id="modalAddItemToCartContent" class="modal-body">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
				<div class="row justify-content-center align-items-center text-center">
					<div class="col-12 mb-3">
						<img src="" class="img-fluid modalCartImage" alt="Stopalerg">
					</div>
					<div class="col-12">
						<h3>Produkt	<span class="modalCartName"></span>	bol vložený do košíka.</h3>
						<h4 class="mt-2 mb-5"><span class="modalCartQuantity mr-2"></span> <span class="js-modalCartPrice modalCartPrice"></span></h4>
					</div>
					<div class="col-12">
						<a href="<?=base_url()?>cart" class="own-btn my-1 mx-1">Nákupný košík</a>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>