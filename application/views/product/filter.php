<div class="filter">
	<form id="productFilter" method="get" data-toggle="validator" role="form">
		<div class="row">
			<div class="form-group has-feedback col-md-6 col-sm-12">
				<input type="hidden" value="<?=(is_numeric($this->input->get('priceFrom')) ? $this->input->get('priceFrom') : 0)?>" name="priceFrom" 	id="priceFrom">
				<input type="hidden" value="<?=(is_numeric($this->input->get('priceTo')) ? $this->input->get('priceTo') : 200)?>" 	name="priceTo" 		id="priceTo">
				<div class="d-inline-flex w-100 pt-2">
					<div id="amount" class="pr-3" style="color:#a90329; font-weight:bold;"></div>
					<div id="slider-range" class="w-100"></div>
				</div>
			</div>
			<div class="form-group col-md-6 col-sm-12">
				<select class="form-control" id="orderBy" name="orderBy">
					<?php
						if ($this->config->item('product_reviews')) {
							?>
								<!--<option value="ratingDesc"	<?=is_selected($this->input->get('orderBy'), 'ratingDesc')?>>Najobľúbenejšie</option>-->
							<?php
						}
					?>
					<?php
						if ($this->config->item('product_recomended')) {
							?>
								<option value="recommendedDesc" 	<?=is_selected($this->input->get('orderBy'), 'recommendedDesc')?>>Najodporúčanejšie</option>
							<?php
						}
					?>
					<option value="soldDesc" 	<?=is_selected($this->input->get('orderBy'), 'soldDesc')?>>Najpredávanejšie</option>
					<option value="priceDesc" 	<?=is_selected($this->input->get('orderBy'), 'priceDesc')?>>Najdrahšie</option>
					<option value="priceAsc"	<?=is_selected($this->input->get('orderBy'), 'priceAsc')?>>Najlacnejšie</option>
				</select>
			</div>
		</div>
		<div class="row">
			<div class="form-group has-feedback col-md-6 col-sm-12">
				<input type="hidden" value="<?=(is_numeric($this->input->get('ageFrom')) ? $this->input->get('ageFrom') : 0)?>" name="ageFrom" 	id="ageFrom">
				<input type="hidden" value="<?=(is_numeric($this->input->get('ageTo')) ? $this->input->get('ageTo') : 18)?>" 	name="ageTo" 	id="ageTo">
				<div class="d-inline-flex w-100 pt-2">
					<div id="age" class="pr-3" style="color:#a90329; font-weight:bold;"></div>
					<div id="slider-range-age" class="w-100"></div>
				</div>
			</div>
			<div class="form-group col-md-6 col-sm-12">
				<select class="form-control" id="origin" name="origin">
					<option value="" 				<?=is_selected($this->input->get('origin'), '')?>>Pôvod tovaru - nerozhoduje</option>
					<option value="Slovensko" 		<?=is_selected(@$this->input->get('origin'), 'Slovensko')?>>Slovensko</option>
					<option value="Česká republika" <?=is_selected(@$this->input->get('origin'), 'Česká republika')?>>Česká republika</option>
					<option value="Poľsko" 			<?=is_selected(@$this->input->get('origin'), 'Poľsko')?>>Poľsko</option>
					<option value="Maďarsko"		<?=is_selected(@$this->input->get('origin'), 'Maďarsko')?>>Maďarsko</option>
					<option value="Nemecko" 		<?=is_selected(@$this->input->get('origin'), 'Nemecko')?>>Nemecko</option>
				</select>
			</div>
		</div>
		<div class="row">
			<div class="form-group col-md-6 col-sm-12">
				<select class="form-control" id="color" name="color">
					<option value="" <?=is_selected($this->input->get('color'), '')?>>Všetky farby</option>
					<?php
						foreach($colorFilter ?? [] as $color) {
							?>
								<option value="<?=$color?>" <?=is_selected($this->input->get('color'), $color)?>><?=$color?></option>
							<?php
						}
					?>
				</select>
			</div>
			<div class="form-group col-md-6 col-sm-12">
				<select class="form-control" id="material" name="material">
					<option value="" <?=is_selected($this->input->get('material'), '')?>>Všetky materiály</option>
					<?php
						foreach($materialFilter ?? [] as $material) {
							?>
								<option value="<?=$material?>" <?=is_selected($this->input->get('material'), $material)?>><?=$material?></option>
							<?php
						}
					?>
				</select>
			</div>
		</div>
		
		<div class="row">
			<div class="form-check col-md-3 col-sm-12">
				<label class="checkbox-container" for="handmadeCheck">
					<input type="checkbox" value="1" name="handmade" id="handmadeCheck" <?=is_checked($this->input->get('handmade'), 1)?>>
					Handmade
					<span class="checkmark"></span>
				</label>
			</div>
			<div class="form-check col-md-3 col-sm-12">
				<label class="checkbox-container" for="stockCheck">
					<input type="checkbox" value="1" name="stock" id="stockCheck" <?=is_checked($this->input->get('stock'), 1)?>>
					Na sklade
					<span class="checkmark"></span>
				</label>
			</div>
			<div class="form-check col-md-3 col-sm-12">
				<label class="checkbox-container" for="discountCheck">
					<input type="checkbox" value="1" name="discount" id="discountCheck" <?=is_checked($this->input->get('discount'), 1)?>>
					V zľave
					<span class="checkmark"></span>
				</label>
			</div>
		</div>
		<button class="primaryBtn" type="submit">Filtorvať</button>
	</form>
</div>

<script>
	$( function() {
		$( "#slider-range" ).slider({
			range: true,
			min: 0,
			max: 1200,
			values: [<?=(is_numeric($this->input->get('priceFrom')) ? $this->input->get('priceFrom') : 0)?>, <?=(is_numeric($this->input->get('priceTo')) ? $this->input->get('priceTo') : 200)?>],
			slide: function(event, ui) {
				$("#amount").html(ui.values[0] + "<?=$this->config->item('currency_unicode')?>&nbsp;-&nbsp;" + ui.values[1] + "<?=$this->config->item('currency_unicode')?>");
				$('#priceFrom').val(ui.values[0]);
				$('#priceTo').val(ui.values[1]);
			}
		});
		$("#amount").html($("#slider-range").slider("values", 0) + "<?=$this->config->item('currency_unicode')?>&nbsp;-&nbsp;" + $("#slider-range").slider("values", 1) + "<?=$this->config->item('currency_unicode')?>");
		
		$( "#slider-range-age" ).slider({
			range: true,
			min: 0,
			max: 99,
			values: [<?=(is_numeric($this->input->get('ageFrom')) ? $this->input->get('ageFrom') : 0)?>, <?=(is_numeric($this->input->get('ageTo')) ? $this->input->get('ageTo') : 18)?>],
			slide: function(event, ui) {
				$("#age").html(ui.values[0] + "r.&nbsp;-&nbsp;" + ui.values[1] + "r.");
				$('#ageFrom').val(ui.values[0]);
				$('#ageTo').val(ui.values[1]);
			}
		});
		$("#age").html($("#slider-range-age").slider("values", 0) + "r.&nbsp;-&nbsp;" + $("#slider-range-age").slider("values", 1) + "r.");
	});
</script>