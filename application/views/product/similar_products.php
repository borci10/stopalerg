<div class="container pb-5">
	<div class="fancy_box padding-20 margin-top-10">
		<h2>Odporúčané produkty</h2>
		<?php
			if (!empty($similarProducts)) {
				$this->data['products'] = $similarProducts;
				$this->load->view('product/items', $this->data);
			} else {
				?>
					<p>Žiadne návrhy</p>
				<?php
			}
		?>
	</div>
</div>