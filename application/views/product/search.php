<div id="search-field-holder" class="d-flex mx-3">
	<input type="text" class="form-control js-searchTextInput<?=$device?>" placeholder="Hľadať..." value="<?=is_set(@$search_expression)?>">
	<span class="input-group-btn">
		<button class="own-btn product-search-btn" type="submit">
			<span class="icon-holder icon-xs">
				<?php include 'images/icons/search.svg';?>
			</span>
		</button>
	</span>
</div>