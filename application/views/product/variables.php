<?php
	if ($productInfo->variable_product) {
		?>
			<div class="fancy_box margin-top-10">
				<div class="productItemHeader">
					<h2>Variácie produktu</h2>
				</div>
				<div class="p-0">
					<table class="table table-sm table-no-first-border mb-0">
						<tbody class="tbody-fancy">
							<?php
								if (isset($parentProduct)) {
									?>
										<tr>
											<td>
												<a href="<?=base_url()?>products/<?=$parentProduct->link?>">
													<?php
														if ($parentProduct->file_name) {
															?>
																<img src="<?=cuteImage($parentProduct->file_name)?>" height="48" alt="<?=$parentProduct->name?>">
															<?php
														} else {
															?>
																<img src="<?=base_url()?>images/No_image_available.jpg" height="48" alt="Img">
															<?php
														}
													?>
												</a>
											</td>
											<td><?=$parentProduct->name?></td>
											<td>
												<?php
													if ($parentProduct->discount) {
														?>
															<?=cutePrice(priceCalc($parentProduct->price, 1, $parentProduct->discount, $parentProduct->tax / 100));?>
															<s class="text-muted"><?=cutePrice(priceCalc($parentProduct->price, 1, 0, $parentProduct->tax / 100));?></s>
															-<?=$parentProduct->discount?>%
														<?php
													} else {
														?>
															<?=cutePrice(priceCalc($parentProduct->price, 1, 0, $parentProduct->tax / 100));?>
														<?php
													}
												?>
											</td>
											<td>
												<a href="<?=base_url()?>products/<?=$productInfo->link?>" class="btn btn-primary">Pozrieť</a>
											</td>
										</tr>
									<?php
								}
							?>
							<?php
								foreach($productVariables ?? [] as $productVariable) {
									if ($this->uri->segment(3) != $productVariable['link']) {
										?>
											<tr>
												<td>
													<a href="<?=base_url()?>products/<?=$productInfo->link?>/<?=$productVariable['link']?>">
														<?php
															if ($productVariable['file_name']) {
																?>
																	<img src="<?=base_url()?>uploads/product_variables/thumbs/<?=$productVariable['file_name']?>" height="48" alt="<?=$productVariable['name']?>">
																<?php
															} else {
																?>
																	<img src="<?=base_url()?>images/No_image_available.jpg" height="48" alt="Img">
																<?php
															}
														?>
													</a>
												</td>
												<td><?=$productVariable['name']?></td>
												<td>
													<?php
														if ($productVariable['discount']) {
															?>
																<?=cutePrice(priceCalc($productVariable['price'], 1, $productVariable['discount'], $productInfo->tax / 100));?>
																<s class="text-muted"><?=cutePrice(priceCalc($productVariable['price'], 1, 0, $productInfo->tax / 100));?></s>
																-<?=$productVariable['discount']?>%
															<?php
														} else {
															?>
																<?=cutePrice(priceCalc($productVariable['price'], 1, 0, $productInfo->tax / 100));?>
															<?php
														}
													?>
												</td>
												<td>
													<a href="<?=base_url()?>products/<?=$productInfo->link?>/<?=$productVariable['link']?>" class="btn btn-primary">Pozrieť</a>
												</td>
											</tr>
										<?php
									}
								}
							?>
						</tbody>
					</table>
				</div>
			</div>
		<?php
	}
?>