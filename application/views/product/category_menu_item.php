<?php
	$childrenCount = count($category['childrens']);
?>
<li class="menuItem <?=($childrenCount > 0 ? 'parentCategory' : '')?> <?=($this->uri->segment(2) == $category['link'] ? 'categoryActive' : '')?>">
	<a href="<?=base_url()?>category/<?=$category['link']?>"><?=$category['title']?></a>
	<?=($childrenCount > 0 ? '<span class="js-expand-toggle"><i class="fa fa-caret-down"></i></span>' : '')?>
	<?php
		if($childrenCount > 0) {
			?>
				<ul class="children">
			<?php
		}
		foreach($category['childrens'] as $category) {
			?>
				<?php
					$this->data['category'] = $category;
					$this->load->view('product/category_menu_item', $this->data);
				?>
			<?php
		}
		if($childrenCount > 0) {
			?>
				</ul>
			<?php
		}
	?>
</li>

