<?php
	foreach($products ?? [] as $product) {
		?>
			<div id="productItem_<?=$product['id']?>" class="col-xl-3 col-lg-4 col-md-6 col-12 my-3 productItemList">
				<div class="productItem">
					<a href="<?=base_url()?>products/<?=$product['link']?>">
						<div class="productItemImg mb-3">
							<?php
								if (!empty($product['file_name'])) {
									?>
										<img src="<?=cuteImage($product['file_name'], 1)?>" class="img-fluid w-100">
									<?php
								} else {
									?>
										<img src="<?=base_url()?>images/No_image_available.jpg" class="img-fluid w-100">
									<?php
								}
							?>
							<?php
								if (@$product['discount'] > 0) {
									?>
										<div class="productItemDiscount">
											<p>-<?=$product['discount']?>%</p>
										</div>
									<?php
								}
							?>
						</div>
						<div class="productItemHeader">
							<h3><?=ucfirst($product['name'])?></h3>
						</div>
						<div class="productItemInfo">
							<div class="productItemDescription">
								<p class="productShortDesc"><?=character_limiter($product['short_description'],60, '...')?></p>
							</div>
						</div>
					</a>
					<div class="productItemPriceWrap">
						<div class="productItemPrice">
							<?php
							if (@$product['discount'] > 0) {
								?>
								<h4 class="lastPrice"><?=cutePrice(priceCalc($product['price'], 1, 0, $product['tax'] / 100))?></h4>
								<?php
							}
							?>
							<h4 class="price"><?=cutePrice(priceCalc($product['price'], 1, $product['discount'], $product['tax'] / 100))?></h4>
						</div>
						<div class="productItemBuy">
							<div class="add-to-cart-holder mt-4">
								<div role="group" class="input-group my-1">
									<div class="input-group-prepend">
										<button type="button" class="btn js-subCountItem">-</button>
									</div>
									<input type="number" class="form-control js-countItem" min="1" max="<?=$product['quantity']?>" value="1" id="productQuantity<?=$product['id']?>">
									<div class="input-group-append">
										<button type="button" class="btn js-addCountItem">+</button>
									</div>
								</div>
								<button class="own-btn" id="addItemToShoppingCart" data-product_id="<?=$product['id']?>" <?=($product['quantity'] == 0 ? 'disabled' : '')?> title="Pridať produkt do košíka">
									<span class="icon-holder">
										<?php include 'images/icons/cart.svg';?>
									</span>
								</button>
							</div>
						</div>
					</div>
				</div>
			</div>
		<?php
	}
?>
