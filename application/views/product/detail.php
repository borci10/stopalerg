<div class="container pt-5">
	<script type="text/javascript" src="<?=base_url()?>assets/js/lightbox/lightbox.min.js"></script>
	<link rel="stylesheet" href="<?=base_url()?>assets/css/lightbox/lightbox.min.css" />
	<div class="cardBody">
		<div class="row mx-0">
			<div class="col-md-6 col-sm-12 margin-top-10 px-0">
				<div class="fancy_box">
					<div class="pb-3">
						<?php
							if (!empty($productPhotos)) {
								?>
									<div class="text-center">
										<a href="<?=cuteImage($productPhotos[0]['file_name'])?>" data-lightbox="1" data-title="<?=$productPhotos[0]['title']?>" data-alt="<?=$productPhotos[0]['alt']?>">
											<img src="<?=cuteImage($productPhotos[0]['file_name'])?>" class="img-fluid" id="productBigPhoto">
										</a>
									</div>
									<div class="col-12 px-0">
										<div class="row">
											<?php
												foreach($productPhotos as $productPhoto) {
													?>
														<div class="col-3 nopadding">
															<a href="<?=cuteImage($productPhoto['file_name'])?>" data-lightbox="1" data-title="<?=$productPhoto['title']?>" data-alt="<?=$productPhoto['alt']?>">
																<img src="<?=cuteImage($productPhoto['file_name'])?>" class="productSmallPhoto img-fluid" title="<?=$productPhoto['title']?>" title="<?=$productPhoto['alt']?>">
															</a>
														</div>
													<?php
												}
											?>
										</div>
									</div>
								<?php
							} else {
								if (!empty($variableProductPhotos)) {
									?>
										<div>
											<a href="<?=base_url()?>uploads/product_variables/<?=$variableProductPhotos[0]['file_name']?>" data-lightbox="1" data-title="<?=$variableProductPhotos[0]['title']?>" data-alt="<?=$variableProductPhotos[0]['alt']?>">
												<img src="<?=base_url()?>uploads/product_variables/<?=$variableProductPhotos[0]['file_name']?>" class="img-fluid" id="productBigPhoto">
											</a>
										</div>
										<div class="col-12 px-0">
											<div class="row">
												<?php
													foreach($variableProductPhotos as $variableProductPhoto) {
														?>
															<div class="col-3 nopadding">
																<a href="<?=base_url()?>uploads/product_variables/<?=$variableProductPhotos[0]['file_name']?>" data-lightbox="1" data-title="<?=$variableProductPhoto['title']?>" data-alt="<?=$variableProductPhoto['alt']?>">
																	<img src="<?=base_url()?>uploads/product_variables/thumbs/<?=$variableProductPhotos[0]['file_name']?>" class="productSmallPhoto img-fluid" title="<?=$variableProductPhoto['title']?>" title="<?=$variableProductPhoto['alt']?>">
																</a>
															</div>
														<?php
													}
												?>
											</div>
										</div>
									<?php
								} else {
									?>
										<img src="<?=base_url()?>images/No_image_available.jpg" width="100" height="100">
									<?php
								}
							}
						?>
					</div>
				</div>
				<?php
					if ($this->config->item('product_attributes')) {
						$this->load->view('product/attributes', $this->data);
					}

					if ($this->config->item('product_reviews')) {
						$this->load->view('product/reviews', $this->data);
					}
				?>
			</div>
			<div class="col-md-6 col-sm-12">
				<div class="white-bg-box">
					<?php
						if ($this->config->item('product_tags')) {
							echo getProductTag($productInfo->creation_date, $productInfo->discount, $productInfo->quantity, $product_rating ?? 0);
						}
					?>
					<div class="fancy_box padding-20 margin-top-10">
						<p class="mb-0"><?=$productInfo->short_description?></p>
						<div class="mt-5">
							<div class="productItemHeader">
								<h3>Popis produktu</h3>
							</div>
							<div>
								<p><?=$productInfo->long_description?></p>
							</div>
						</div>
						<h3>
							<?php
								if (@$productInfo->discount > 0) {
									?>
										<s class="text-muted lastPrice"><?=cutePrice(priceCalc($productInfo->price, 1, 0, $productInfo->tax / 100))?></s>
									<?php
								}
							?>
							<br />
							<span class="price">Cena s DPH <?=cutePrice(priceCalc($productInfo->price, 1, $productInfo->discount, $productInfo->tax / 100))?></span>
							<br />
							<span class="text-muted">Cena bez DPH <?=cutePrice(priceCalc($productInfo->price, 1, $productInfo->discount))?></span>
						</h3>
						<?php
							if ($productInfo->deleted == 0) {
								?>
									<div class="add-to-cart-holder mt-4">
										<div role="group" class="input-group my-1">
											<div class="input-group-prepend">
												<button type="button" class="btn js-subCountItem">-</button>
											</div>
											<input type="number" class="form-control js-countItem" min="1" max="<?=$productInfo->quantity?>" value="1" id="productQuantity<?=$productInfo->id?>">
											<div class="input-group-append">
												<button type="button" class="btn js-addCountItem">+</button>
											</div>
										</div>
										<button class="own-btn icon-left" id="addItemToShoppingCart" data-product_id="<?=$productInfo->id?>" data-product_child_id="<?=$productInfo->child_id ?? null?>" <?=($productInfo->quantity == 0 ? 'disabled' : '')?> title="Pridať produkt do košíka">
											<span class="icon-holder">
												<?php include 'images/icons/cart.svg';?>
											</span>
											Pridať do košíka
										</button>
									</div>
								<?php
							} else {
								?>
									<p>Tento produkt už nie je dotupný</p>
								<?php
							}
						?>
					</div>
					<?php
						if ($this->config->item('product_variable')) {
							$this->load->view('product/variables', $this->data);
						}
					?>
				</div>
			</div>
		</div>
	</div>
</div>