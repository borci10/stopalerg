<div class="container">
	<div class="row">
		<div class="col-xl-3 col-lg-3 col-md-12">
			<ul id="expandableMenu" class="mt-3">
				<li class="menuItem headCategory headCategory-1 <?=($this->uri->segment(1) == 'novinky' ? 'categoryActive' : '')?>">
					<a href="<?=base_url()?>novinky" class="js-headerItem">Novinky</a>
				</li>
				<?php
					foreach($categories as $category) {
						$this->data['category'] = $category;
						$this->load->view('product/category_menu_item', $this->data);
					}
				?>
			</ul>
			<script>
				$(document).on('click', '.js-expand-toggle', function(e) {
					$(this).parent().find('> .children').slideToggle();
					e.preventDefault();
				});
				// var actualCategoryId = '<?=@$actualCategoryId?>';
				// $(document).on('click', '.js-expand-list', function(e) {
					// console.log($(this).data('visible'));
					// if ($(this).data('visible') == 'hidden') {
						// $('.js-parent-' + $(this).data('category_id')).show();
						// $('.js-parent-' + $(this).data('category_id')).data('visible', 'hidden');
						// $(this).data('visible', 'shown');
					// } else {
						// $('.js-parents-' + $(this).data('category_id')).hide();
						// $(this).data('visible', 'hidden');
					// }

					// e.preventDefault();
				// });

				// $(document).ready(function() {
					// $('.js-parent-' + actualCategoryId).show();
					// $('.js-parent-' + actualCategoryId).data('visible', 'hidden');
				// });
			</script>
		</div>