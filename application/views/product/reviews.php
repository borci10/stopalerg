<div class="fancy_box margin-top-10">
	<div class="productItemHeader">
		<h2>Hodnotenia užívateľov</h2>
	</div>
	<div class="p-3">
		<?php
			if (empty($product_reviews)) {
				?>
					<p>Tento produkt zatiaľ nikto nedohodnotil</p>
				<?php
			} else {
				?>
					<span>Celkové hodnotenie <?=round($product_rating);?>%</span>
					<div class="progress">
						<div class="progress-bar" style="width: <?=round($product_rating);?>%;" role="progressbar" aria-valuenow="<?=round($product_rating)?>" aria-valuemin="0" aria-valuemax="100"></div>
					</div>
				<?php
				foreach($product_reviews as $review) {
					?>
						<div class="row margin-top-10">
							<div class="col-12 px-0">
								<b><?=$review['name']?></b>
								<span class="pull-right"><?=cuteDateTime($review['review_time'])?></span>
								<p><?=$review['comment']?></p>
							</div>
						</div>
					<?php
				}
			}
		?>

		<?php
			if ($this->session->userdata('admin') == 1) {
				if (!$aldready_reviewed) {  // if user didnt made a review
					?>
						<form id="addProductForm" method="post" action="<?=base_url()?>products/addReview/<?=$productInfo->id?>" data-toggle="validator" role="form">
							<div class="form-group">
								<label for="productRating">Vaše hodnotenie</label>
								<input 
									type="number" 
									class="form-control" 
									id="productRating" 
									name="productRating"
									min="0"
									max="100"
								>
							</div>
							<div class="form-group">
								<textarea class="form-control" id="productReviewText" name="productReviewText" rows="3"></textarea>
							</div>
							<input type="submit" class="btn btn-cart" value="Pridať hodnotenie" name="productReviewConfirm" id="productReviewConfirm">
						</form>
					<?php
				}
			}
		?>
	</div>
</div>