<?php
	if (
		($productInfo->origin)		||
		(!empty($productAttributes))
	) {
		?>
			<div class="fancy_box margin-top-10">
				<div class="productItemHeader">
					<h2>Vlastnosti produktu</h2>
				</div>
				<div class="p-3">
					<table class="table table-sm table-no-first-border mb-0">
						<?php 
							if ($productInfo->origin) {
								?>
									<tr>
										<td>Krajina pôvodu</td>
										<td><?=$productInfo->origin?></td>
									</tr>
								<?php
							}
						?>
						<?php
							foreach($productAttributes ?? [] as $attribute) {
								?>
									<tr>
										<td><?=$attribute['title']?></td>
										<td><?=$attribute['value']?></td>
									</tr>
								<?php
							}
						?>
					</table>
				</div>
			</div>
		<?php
	}
?>