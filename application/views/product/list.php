<section>
	<div class="container">
		<div class="row">
			<?php
				if (count($products) > 0) {
					$this->load->view('product/items', $this->data);
				}
				else {
					?>
						<div class="col-12 text-center">
							<h2 class="mb-0">Zvoleným kritériam nevyhovujú žiadne produkty.</h2>
						</div>
					<?php
				}
			?>
		</div>
	</div>
</section>
