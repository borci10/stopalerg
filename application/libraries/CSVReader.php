<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class CSVReader {

    var $fields;           
    var $separator 		= ';';  
    var $enclosure 		= '"';  
    var $max_row_size 	= 4096;
	var $profile		= ['uid', 'name', 'price', 'discount', 'quantity'];

    function parse_file($filePath) {
        $file 		= fopen($filePath, 'r');
        $content	= [];
		
        while(($row = fgetcsv($file, $this->max_row_size, $this->separator, $this->enclosure)) != false) {            
            if($row != null) { 
                $arr    	= [];
                $values 	= $this->escape_string(explode(',', $row[0]));
				
				if (count($values) == count($this->profile)) {
					for($i = 0; $i < count($this->profile); $i++){
						$arr[$this->profile[$i]] = $values[$i];
					}
					$content[] = $arr;
				}
            }
        }
        fclose($file);
        return $content;
    }

    function escape_string($data){
        $result =   array();
        foreach($data as $row){
            $result[] = trim(str_replace('"', '',$row));
        }
        return $result;
    }   
}
?> 