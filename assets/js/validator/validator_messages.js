$.fn.validator.Constructor.DEFAULTS.errors = {
	required: 'Toto pole je povinné!',
	match: "Heslá sa musia zhodovať!",
	minlength: "Heslo musí mať aspoň 8 znakov!"
};