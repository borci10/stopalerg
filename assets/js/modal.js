(function($){
	$(function() {
		var modal = $('.modal-own');
		//Modal Open
		$('.modal-own-open').on('click', function (e) {
			modal.removeClass('active');
			var modalName = $(this).data('modal');
			e.preventDefault();
			modal.each(function (index) {
				if( $(this).data('modal') === modalName ) {
					$(this).addClass('active');
				}
			})
		});

		//Close modal
		$('.modal-own-close').on('click', function (e) {
			e.preventDefault();
			modal.removeClass('active');
		});

		//ESC close modal
		$(document).on('keyup',function(e) {
			if (e.keyCode === 27) {
				if(modal.hasClass('active')) {
					modal.removeClass('active');
				}
			}
		});

		//Touch somewhere close modal
		$(document).click(function(event) {
			if (!$(event.target).closest(".modal-own-body, .modal-own-open").length) {
				modal.removeClass("active");
			}
		});

		//Susses send modal
		modal.on('click', '.ownBtn', function (e) {
			e.preventDefault();
			var current = $(this).closest('.modal-own');
			current.addClass('success');
			setTimeout(function() {
				current.removeClass('success active')
			}, 2000);
		});

	});
})(jQuery);




