(function($){
    $(function() {

		$('.parent-category').on('click', function(e) {
			e.preventDefault(e);
			$(this).parent().find('.children').slideToggle();
			$(this).parent().toggleClass('active');
		});

		$('.close-alert').on('click', function(e) {
			e.preventDefault(e);
			$('.alert-box-wrap').fadeOut();
		});

		$('.admin-menu-toggle').on('click', function(e) {
			e.preventDefault(e);
			$(this).toggleClass('active');
			$('#admin-bar').toggleClass('active');
		});

		$('.editImage').on('click', function(e) {
			e.preventDefault(e);
			$(this).parent().parent().find('.productPhotoInfo').slideToggle();
		});

		$('.card-head').on('click', function(e) {
			e.preventDefault(e);
			$(this).parent().find('.cardInnerBody').slideToggle();
			$(this).toggleClass('deactive')
		});

		if($('#admin-bar').length !== 0 ) {
			const ps = new PerfectScrollbar('#admin-bar', {
				swipeEasing: true,
				suppressScrollX: true
			});
		}

		$(function () {
			$('[data-toggle="tooltip"]').tooltip()
		});


    });
})(jQuery);



	
	