window.onload = function(){
	//Slider Height
	const sliderHeight = () => {
		let headerHeight = document.getElementById("header").offsetHeight;
		let allSliderWraps = document.getElementsByClassName("slider-content-wrap");
		let clientHeight = document.documentElement.clientHeight;

		if(document.getElementById('slider')) {
			if (document.getElementById('slider').classList.contains("half-slider")) {
				clientHeight = (clientHeight - headerHeight) / 2 + 'px';
			} else {
				clientHeight = clientHeight - headerHeight + 'px';
			}


			//Slider item height
			for (var e = 0, leng = allSliderWraps.length; e < leng; e++) {
				allSliderWraps[e].style.minHeight = clientHeight;
			}

			//Slider Height
			document.getElementById('slider').style.minHeight = clientHeight;
		}


	};
	window.addEventListener('resize', sliderHeight);
	sliderHeight();
};

