(function($){
	$(function() {
		var scroll = $(window).scrollTop();
		var burger = $('#burger-menu');

		//Alert close
		$('.close-alert').on('click', function(e) {
			e.preventDefault(e);
			$('.alert-box-wrap').fadeOut();
		});

		//Slider Image Load
		var carouselSlider = $('#slider');
		var carouselSliderImages = carouselSlider.find('.slider-img');
		if( carouselSliderImages.length !== 0 ) {
			carouselSliderImages.each(function () {
				var tmpImg = new Image();
				tmpImg.src = $(this).attr('src');
				tmpImg.onload = function () {
					carouselSlider.css('background-image', 'url(' + tmpImg.src + ')');
					carouselSlider.addClass('active');
					$('.spinner-wrap').fadeOut(600, function () {
						$('#loader').fadeOut(600, function () {
						});
					});
				};
			});
		}
		else {
			carouselSlider.addClass('active default');
			$('.spinner-wrap').fadeOut(600, function () {
				$('#loader').fadeOut(600);
			});
		}

		/*ScrollTo class - scroll to element*/
		$(".scrollTo").on('click', function (e) {
			e.preventDefault();
			var headerEle = ($('.desktop-header').css('display') === 'block') ? $('.header-middle').height() : $('.mobile-header').height();
			var target = $(this).data('href') ? $(this).data('href') : $(this).attr('href');
			hideMobileMenu();
			var cleanHash = target.substring(target.indexOf('#'));
			if($(cleanHash).length !== 0) {
				e.preventDefault();
				$('html, body').animate({
					scrollTop: ($(cleanHash).offset().top - headerEle)
				}, 'slow');
			}
		});

		/*Scroll bellow slider Arrow*/
		$(".scrollBellowSlider").on('click', function (e) {
			e.preventDefault();
			var slider = $('#slider');
			var headerEle = ($('.desktop-header').css('display') === 'block') ? $('.header-middle').height() : $('.mobile-header').height();
			var nextEle = slider.offset().top + slider.outerHeight(true) - headerEle;
			$('html, body').animate({
				scrollTop: nextEle
			}, 1000);
			return false;
		});

		/*Up arrow scroll to TOP*/
		$('body').on('click', '#up', function () {
			$('html, body').animate({scrollTop: 0}, 500, 'swing');
			$('#nav-icon').removeClass('open');
			burger.stop().slideUp();
			return false;
		});

		/*Open burger menu*/
		$('#nav-icon').on('click touch', function(e) {
			e.stopPropagation();
			$(this).toggleClass('open');
			burger.stop().slideToggle();
			return false;
		});

		//Mobile menu dropdown
		$('.menu-dropdown-toggle-mobile').on('click touch', function(e) {
			e.stopPropagation();
			$(this).toggleClass('open');
			$(this).parent().find('.menu-dropdown').stop().slideToggle();
			return false;
		});

		$(document).on('click', function(event){
			var container = $("#burger-menu");
			if (!container.is(event.target) &&            // If the target of the click isn't the container...
				container.has(event.target).length === 0) // ... nor a descendant of the container
			{
				hideMobileMenu();
			}
		});

		function stickyTop() {
			if (scroll >= 400) {
				$("#add-to-cart-fixed-btn").addClass('active');
				$("#up").addClass('active');
			}
			else {
				$("#add-to-cart-fixed-btn").removeClass('active');
				$("#up").removeClass('active');
			}
		}

		function stickyHeader() {
			if (scroll >= 1) {
				$('header').addClass('sticky');
			}
			else {
				$('header').removeClass('sticky');
			}
		}

		function hideMobileMenu() {
			if($('.mobile-header').is(':visible'))
			{
				burger.slideUp();
				$('#nav-icon').removeClass('open');
			}
		}

		stickyHeader();
		stickyTop();

		$(window).scroll(function () {
			scroll = $(window).scrollTop();
			stickyHeader();
			stickyTop();
		});

		$('#references-carousel').owlCarousel({
			loop: false,
			dots: true,
			nav: false,
			items: 1,
			smartSpeed: 500,
			autoHeight: true,
			autoplay: false
		});

	});
})(jQuery);



	
	