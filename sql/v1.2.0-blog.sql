-- phpMyAdmin SQL Dump
-- version 4.5.2
-- http://www.phpmyadmin.net
--
-- Hostiteľ: 127.0.0.1
-- Čas generovania: Ne 30.Dec 2018, 11:34
-- Verzia serveru: 5.7.9
-- Verzia PHP: 7.0.0

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Databáza: `demo_web`
--

-- --------------------------------------------------------

--
-- Štruktúra tabuľky pre tabuľku `blog`
--

DROP TABLE IF EXISTS `blog`;
CREATE TABLE IF NOT EXISTS `blog` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `header` varchar(200) NOT NULL,
  `link` varchar(255) NOT NULL,
  `image` varchar(60) DEFAULT NULL,
  `content` longtext NOT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `published` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;

--
-- Sťahujem dáta pre tabuľku `blog`
--

INSERT INTO `blog` (`id`, `header`, `link`, `image`, `content`, `date`, `published`) VALUES
(4, 'Island', 'island', '5ce36e0b937d318b9997c32ff353164d.jpg', 'Lorem Ipsum je fiktívny text, používaný pri návrhu tlačovín a typografie. Lorem Ipsum je štandardným výplňovým textom už od 16. storočia, keď neznámy tlačiar zobral sadzobnicu plnú tlačových znakov a pomiešal ich, aby tak vytvoril vzorkovú knihu. Prežil nielen päť storočí, ale aj skok do', '2018-05-16 10:19:50', 1),
(8, 'Čo dokážeme', 'co-dokazeme', '379d79cfae97def901151602d002aa5f.jpg', 'April showers bring GDPR updates, Beacon 2.0 developments, and a salary study! I think it goes something like that? Luckily, chief of all things GDPR at Help Scout, Megan, is here to tell us more. But first, Leah with the video rundown...', '2018-05-21 14:58:17', 1),
(5, 'Rodina a deti', 'rodina-a-deti', '87ff0779dd4dbb092b5b0f05dd5ad0ae.jpg', 'April showers bring GDPR updates, Beacon 2.0 developments, and a salary study! I think it goes something like that? Luckily, chief of all things GDPR at Help Scout, Megan, is here to tell us more. But first, Leah with the video rundown...', '2018-05-18 18:04:43', 1),
(6, 'Golfove ihriská', 'golfove-ihriska', '0695acd78f8d9a98a8d92a9e2b315459.jpg', 'Megan, along with our core privacy team, has been working hard to ensure that we’ve got all GDPR requirements checked off well before the May 25th deadline. If you’re currently opening a new tab to search for the meaning of GDPR, read on.', '2018-05-18 18:05:01', 0),
(7, 'Može sa bežný človek dostať na Mars?', 'moze-sa-bezny-clovek-dostat-na-mars', '918e6b5a107a8a8e8b15da4954251e11.jpg', 'Megan, along with our core privacy team, has been working hard to ensure that we’ve got all GDPR requirements checked off well before the May 25th deadline. If you’re currently opening a new tab to search for the meaning of GDPR, read on.', '2018-05-18 18:05:53', 1);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
