ALTER TABLE `delivery_options` RENAME TO `delivery`;
ALTER TABLE `delivery` CHANGE `title` `title` VARCHAR(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL;
ALTER TABLE `delivery` CHANGE `price` `price` DECIMAL(10,2) NOT NULL;
ALTER TABLE `orders` ADD `payment_price` DECIMAL(10,2) NOT NULL DEFAULT '0' AFTER `payment`;
ALTER TABLE `order_product` CHANGE `product_price` `product_price` DECIMAL(10,2) NOT NULL;