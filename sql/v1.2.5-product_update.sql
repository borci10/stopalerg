ALTER TABLE `product` CHANGE `name` `name` TEXT CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL;
ALTER TABLE `product` CHANGE `link` `link` TEXT CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL;
ALTER TABLE `product_variables` ADD `show_product` BOOLEAN NOT NULL DEFAULT FALSE AFTER `quantity`;