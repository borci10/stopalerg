-- phpMyAdmin SQL Dump
-- version 4.5.2
-- http://www.phpmyadmin.net
--
-- Hostiteľ: 127.0.0.1
-- Čas generovania: St 27.Mar 2019, 16:45
-- Verzia serveru: 5.7.9
-- Verzia PHP: 7.0.0

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Databáza: `demo_web`
--

-- --------------------------------------------------------

--
-- Štruktúra tabuľky pre tabuľku `blog`
--

DROP TABLE IF EXISTS `blog`;
CREATE TABLE IF NOT EXISTS `blog` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `header` varchar(255) NOT NULL,
  `link` varchar(255) NOT NULL,
  `image` varchar(60) DEFAULT NULL,
  `content` longtext NOT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `language` varchar(2) NOT NULL DEFAULT 'SK',
  `comments` tinyint(1) NOT NULL DEFAULT '0',
  `published` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=15 DEFAULT CHARSET=utf8;

--
-- Sťahujem dáta pre tabuľku `blog`
--

INSERT INTO `blog` (`id`, `header`, `link`, `image`, `content`, `date`, `language`, `comments`, `published`) VALUES
(14, 'Operator of the slitting and packaging line', 'operator-of-the-slitting-and-packaging-line', NULL, '<div><br></div>', '2018-11-29 11:24:23', 'EN', 1, 1),
(9, 'Operátor rezacej a baliacej linky', 'operator-rezacej-a-baliacej-linky', '73ea2f66813d43c37af5299973f2d688.JPG', '<b style="font-size: 1.05rem;">Náplň práce:&nbsp;</b><div><b><br></b><ul><li>obsluha vysokozdvižného vozíka (obsluha motorového vozíka)<br></li><li>kontrola prichádzajúceho materiálu a odchádzajúcich produktov&nbsp;<br></li><li>obsluha a nastavovanie strihacej a baliacej linky podľa predpísaných pracovných postupov a pokynov nadriadeného&nbsp;<br></li><li>balenie zvitkov\r\nvýroba paliet<br></li></ul></div>', '2018-09-03 22:32:59', 'SK', 1, 1);

-- --------------------------------------------------------

--
-- Štruktúra tabuľky pre tabuľku `blog_comments`
--

DROP TABLE IF EXISTS `blog_comments`;
CREATE TABLE IF NOT EXISTS `blog_comments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `blog_id` int(11) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `user_name` text NOT NULL,
  `content` text NOT NULL,
  `date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `blog_id` (`blog_id`),
  KEY `user_id` (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

--
-- Sťahujem dáta pre tabuľku `blog_comments`
--

INSERT INTO `blog_comments` (`id`, `blog_id`, `user_id`, `user_name`, `content`, `date`) VALUES
(1, 14, NULL, 'Michal Sejč', 'Rád by som sa k tomu vyjadril.', '2019-01-14 19:57:11'),
(2, 14, NULL, 'Šaňo', 'Nemáš praudu!', '2019-01-14 20:04:39');

-- --------------------------------------------------------

--
-- Štruktúra tabuľky pre tabuľku `content`
--

DROP TABLE IF EXISTS `content`;
CREATE TABLE IF NOT EXISTS `content` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `link` varchar(30) NOT NULL,
  `header` varchar(200) NOT NULL,
  `content` longblob,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Sťahujem dáta pre tabuľku `content`
--

INSERT INTO `content` (`id`, `link`, `header`, `content`) VALUES
(1, 'home', 'Domov', 0x3c6469763e3c7374726f6e67207374796c653d226d617267696e3a203070783b2070616464696e673a203070783b20636f6c6f723a2072676228302c20302c2030293b20666f6e742d66616d696c793a202671756f743b4f70656e2053616e732671756f743b2c20417269616c2c2073616e732d73657269663b20746578742d616c69676e3a206a7573746966793b223e4c6f72656d20497073756d3c2f7374726f6e673e3c7370616e207374796c653d22636f6c6f723a2072676228302c20302c2030293b20666f6e742d66616d696c793a202671756f743b4f70656e2053616e732671756f743b2c20417269616c2c2073616e732d73657269663b20746578742d616c69676e3a206a7573746966793b223e266e6273703b69732073696d706c792064756d6d792074657874206f6620746865207072696e74696e6720616e64207479706573657474696e6720696e6475737472792e204c6f72656d20497073756d20686173206265656e2074686520696e6475737472792773207374616e646172642064756d6d79207465787420657665722073696e6365207468652031353030732c207768656e20616e20756e6b6e6f776e207072696e74657220746f6f6b20612067616c6c6579206f66207479706520616e6420736372616d626c656420697420746f206d616b65206120747970652073706563696d656e20626f6f6b2e20497420686173207375727669766564206e6f74206f6e6c7920666976652063656e7475726965732c2062757420616c736f20746865206c65617020696e746f20656c656374726f6e6963207479706573657474696e672c2072656d61696e696e6720657373656e7469616c6c7920756e6368616e6765642e2049742077617320706f70756c61726973656420696e207468652031393630732077697468207468652072656c65617365206f66204c657472617365742073686565747320636f6e7461696e696e67204c6f72656d20497073756d2070617373616765732c20616e64206d6f726520726563656e746c792077697468206465736b746f70207075626c697368696e6720736f667477617265206c696b6520416c64757320506167654d616b657220696e636c7564696e672076657273696f6e73206f66204c6f72656d20497073756d2e3c2f7370616e3e3c62723e3c2f6469763e);

-- --------------------------------------------------------

--
-- Štruktúra tabuľky pre tabuľku `newsletter_emails`
--

DROP TABLE IF EXISTS `newsletter_emails`;
CREATE TABLE IF NOT EXISTS `newsletter_emails` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(255) NOT NULL,
  `date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

--
-- Sťahujem dáta pre tabuľku `newsletter_emails`
--

INSERT INTO `newsletter_emails` (`id`, `email`, `date`) VALUES
(1, 'michal.sejc@gmail.com', '2018-06-28 18:54:32'),
(2, 'sejo@mail.t-com.sk', '2018-06-28 18:54:32'),
(3, 'm1s00@centrum.cz', '2018-06-28 19:10:47'),
(4, 'kundrikova@centrum.sk', '2018-06-28 19:13:38');

-- --------------------------------------------------------

--
-- Štruktúra tabuľky pre tabuľku `photogallery_albums`
--

DROP TABLE IF EXISTS `photogallery_albums`;
CREATE TABLE IF NOT EXISTS `photogallery_albums` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(1000) CHARACTER SET utf8 NOT NULL,
  `link` varchar(255) CHARACTER SET utf8 NOT NULL,
  `description` text CHARACTER SET utf8 NOT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

--
-- Sťahujem dáta pre tabuľku `photogallery_albums`
--

INSERT INTO `photogallery_albums` (`id`, `name`, `link`, `description`, `date`) VALUES
(6, 'Čína - Peking', 'cina-peking', 'Čína (čín. 中国 – pchin-jin: Zhōng guó; doslova: „ krajina uprostred“), dlhý tvar Čínska ľudová republika (čín. 中华人民共和国 – pchin-jin: Zhōng huá rén mín gòng hé guó), je rozlohou najväčší štát východnej Ázie a štvrtý najväčší na svete. V krajine žije vyše 1,3 miliardy obyvateľov (stav k roku 2007), čo z nej robí najľudnatejší štát sveta.', '2016-08-25 22:00:00'),
(5, 'Island', 'island', 'Prvými obyvateľmi Islandu boli pravdepodobne Nóri a Kelti (írski a škótski), ktorí prichádzali na Island koncom 9. storočia a aj v 10. storočí. Vikingskí náčelníci zakladali tzv. „thingy“, čo boli miestne snemy, ktoré verejne urovnávali spory a konflikty medzi osadníkmi. Okolo roku 930 bol založený Althing a tento národný snem môžeme pokladať za najstarší európsky parlament. Jeho rozhodnutím bolo v rokoch 999 – 1000 prijaté kresťanstvo. Paradoxom uznesenia Althingu bolo, že hoci sa v zemi ustanovilo za náboženstvo kresťanstvo, v druhom bode uznesenia sa nariaďovalo všetkým lodiam, aby pri priblížení k zemi zložili z čelení dračie hlavy, aby neodstrašili ochranných duchov zeme. V 16. storočí po reformácii krajina prijala luteranizmus.', '2018-04-30 22:00:00'),
(7, 'Kuba', 'kuba', 'Kuba má významnú geografickú polohu a ovláda značnú časť amerického stredomoria. Hlavné a zároveň aj najväčšie mesto je Havana (La Habana). Od Floridy ju na západe delí iba 180 km široký Floridský prieliv. Od polostrova Yucatán 210 km široký Yucatánsky prieliv, od ostrova Haiti na východe prieliv Paso de los Vientos, široký iba 77 km, od Bahamských ostrovov na severovýchode Bahamský prieliv a na juhu sa približuje Jamajke na vzdialenosť 140 km. Rozprestiera sa na najväčšom ostrove Veľkých Antíl. Na hlavný ostrov pripadá 94 % územia. Má pretiahnutý tvar je 1 275 km dlhý a 31 až 191 km široký. Ďalej sa rozprestiera na 1 600 väčších či menších ostrovoch. Celková plocha štátu je 110 922 km²;. Pobrežie s celkovou dĺžkou 3 500 km je značne členité, zväčša ploché, močaristé, len na juhovýchode strmé a neprístupné.', '2017-01-17 23:00:00');

-- --------------------------------------------------------

--
-- Štruktúra tabuľky pre tabuľku `photogallery_photos`
--

DROP TABLE IF EXISTS `photogallery_photos`;
CREATE TABLE IF NOT EXISTS `photogallery_photos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `album_id` int(11) DEFAULT NULL,
  `title` varchar(100) CHARACTER SET utf8 DEFAULT NULL,
  `alt` varchar(100) CHARACTER SET utf8 DEFAULT NULL,
  `path` varchar(100) CHARACTER SET utf8 NOT NULL,
  `photo_order` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=67 DEFAULT CHARSET=latin1;

--
-- Sťahujem dáta pre tabuľku `photogallery_photos`
--

INSERT INTO `photogallery_photos` (`id`, `album_id`, `title`, `alt`, `path`, `photo_order`) VALUES
(37, NULL, NULL, NULL, '3bfac36af6a6278e26e921c51e4fad3a.jpg', NULL),
(38, 5, '', '', '39f41a2cd1855bff34470295c955c799.jpg', 1),
(39, 5, '', '', '4c78640ef831e5b8f9e5e089ce323705.jpg', 3),
(40, 5, '', '', 'f8fe8eecba39d5e5a7cbd635ba373a0a.jpg', 4),
(41, 5, '', '', '56fd7206315fe3c32e2dffe298e19e22.jpg', 5),
(42, 5, '', '', '1ffc006045bac8f1d21f2d58ae031fe2.jpg', 6),
(43, 5, '', '', '923d3a74b8e6698df60ba30227cee031.jpg', 2),
(46, 6, '', '', 'd0cb78f77efb19211a85984786af34fa.JPG', 3),
(47, 6, '', '', '2451f9872fa2ed9dcd0f38c33530d32e.jpg', 2),
(48, 6, '', '', '71b98cc05c9f00ccef08545d68dfe378.JPG', 4),
(49, 6, '', '', '386798997a036b4c0823fa9490bc9ab7.jpg', 5),
(50, 6, '', '', 'cd4133ae7136a250601919ee1f8fe7b3.JPG', 6),
(51, 6, '', '', 'a9e970eae0c9aeba1fd7d3323130f20d.jpg', 7),
(52, 6, '', '', 'a2418b556b4a333a350fbaf56eda084a.jpg', 1),
(53, 6, '', '', '03e071a67fca0b717d3ff9bb1e094dd9.jpg', 8),
(55, 7, '', '', 'fc8fa4af7e0abf5f1f1792e32aa465b7.jpg', 6),
(56, 7, '', '', 'ed888eb1d66e4afdee23f9ab44c221d7.jpg', 3),
(57, 7, '', '', '437d8534219ea588966a2342453ea685.jpg', 4),
(58, 7, '', '', 'e7316d0db8e4abdc64e9d686d6b66d29.jpg', 5),
(60, 7, '', '', '386f119069c17892a29da7a7608566d2.jpg', 1),
(61, 7, '', '', 'c1cd45df45fbfa4b2ba0e12e8ba87be6.jpg', 2),
(62, NULL, NULL, NULL, '3142f8767fea0ac2260c2937a969354e.jpg', NULL),
(63, NULL, NULL, NULL, '9dea5e331dc285705799e2e5c02db79b.jpg', NULL),
(64, NULL, NULL, NULL, 'b39804292b521099d5b2406e24084500.jpg', NULL);

-- --------------------------------------------------------

--
-- Štruktúra tabuľky pre tabuľku `references`
--

DROP TABLE IF EXISTS `references`;
CREATE TABLE IF NOT EXISTS `references` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `photo` varchar(64) DEFAULT NULL,
  `description` text NOT NULL,
  `signature` varchar(255) NOT NULL,
  `date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

--
-- Sťahujem dáta pre tabuľku `references`
--

INSERT INTO `references` (`id`, `name`, `photo`, `description`, `signature`, `date`) VALUES
(1, 'Michal', '127bcd0052fbddae1c77a0403bd35716.png', 'aaaa', 'Michal Sejč', '2019-03-19 00:00:00');

-- --------------------------------------------------------

--
-- Štruktúra tabuľky pre tabuľku `users`
--

DROP TABLE IF EXISTS `users`;
CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `email` varchar(60) NOT NULL,
  `password` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

--
-- Sťahujem dáta pre tabuľku `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `password`) VALUES
(1, 'Sejco', 'michal.sejc@gmail.com', '$2y$10$gWZApv6h.4i7xvEE85eDlucHSytySluicLAaA80Ziciu.WEmFoI62');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
