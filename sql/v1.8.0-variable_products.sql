ALTER TABLE `order_product` ADD `id_product_child` INT NULL DEFAULT NULL AFTER `id_order`;
ALTER TABLE `carts` ADD `id_product_child` INT NULL DEFAULT NULL AFTER `id_product`;
ALTER TABLE `product_variables_photos` ADD INDEX(`variable_id`);
ALTER TABLE `product_variables` ADD INDEX(`id_product`);