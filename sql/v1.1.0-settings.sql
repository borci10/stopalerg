-- phpMyAdmin SQL Dump
-- version 4.5.2
-- http://www.phpmyadmin.net
--
-- Hostiteľ: 127.0.0.1
-- Čas generovania: So 29.Dec 2018, 18:46
-- Verzia serveru: 5.7.9
-- Verzia PHP: 7.0.0

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Databáza: `trololo`
--

-- --------------------------------------------------------

--
-- Štruktúra tabuľky pre tabuľku `settings`
--

DROP TABLE IF EXISTS `settings`;
CREATE TABLE IF NOT EXISTS `settings` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `key` varchar(255) CHARACTER SET utf8 NOT NULL,
  `value` varchar(255) CHARACTER SET utf8 NOT NULL,
  PRIMARY KEY (`id`),
  KEY `key` (`key`)
) ENGINE=MyISAM AUTO_INCREMENT=16 DEFAULT CHARSET=latin1;

--
-- Sťahujem dáta pre tabuľku `settings`
--

INSERT INTO `settings` (`id`, `key`, `value`) VALUES
(1, 'bank_name', 'CSOB'),
(2, 'bank_account_number', '11111'),
(3, 'bank_code', '2222'),
(4, 'bank_iban', 'SK08 0 000 000 000 00000 0000'),
(5, 'bank_swift', 'Swift kod'),
(8, 'company_ico', '15151515'),
(9, 'company_dic', 'Stavmat'),
(10, 'company_icdph', 'dffdfdfdf'),
(11, 'company_street', 'Polianky 17'),
(12, 'company_city', 'Bratislava'),
(13, 'company_zip_code', '83101'),
(14, 'company_phone', '904267939'),
(15, 'vat_payer', '1'),
(6, 'company_name', 'Stavmat'),
(7, 'company_email', 'michal.sejc@gmail.com');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
