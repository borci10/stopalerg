ALTER TABLE `product_variables` CHANGE `discount` `discount` DECIMAL(5,2) NOT NULL;
ALTER TABLE `product_variables` CHANGE `quantity` `quantity` INT(11) NOT NULL;