ALTER TABLE `categories` ADD `photo` VARCHAR(63) NOT NULL AFTER `link`;
ALTER TABLE `categories` CHANGE `title` `title` VARCHAR(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL;
ALTER TABLE `categories` CHANGE `photo` `photo` VARCHAR(63) CHARACTER SET utf8 COLLATE utf8_general_ci NULL;